

# 12MHz clock
create_clock -period 83 [get_ports usb_clk]
create_clock -period 83 [get_ports epuck_pclk]
create_clock -period 50 [get_ports xu4_spi1_clk]
create_clock -period 16 [get_ports fifo_clk]
#create_clock -period 10 [get_clocks clk]


set_false_path -from [get_clocks epuck_pclk] -to [get_clocks fifo_clk]
set_false_path -from [get_clocks fifo_clk] -to [get_clocks epuck_pclk]
#set_false_path -from [get_clocks *] -to [get_clocks *]

