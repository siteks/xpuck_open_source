//
// Block RAM based clock domain crossing fifo
//
module fifo4096x8 (
    // Input 
    input           din_clk,
    input           din_valid,
    output          din_enable,
    input   [7:0]   din,

    // Output
    input           dout_clk,
    output  reg     dout_valid,
    input           dout_enable,
    output  [7:0]   dout

);

// There are 16 block rams, each 4k bits. Using 8 gives 4k x 8bits,
// can handle a stall of min 340us @pclk=12MHz, actually more because of hsync time.
// The largest observed stall on iMac running ubuntu 14.04 3.13.0-32 
// under parallels was about 25us. We assume that the Odroid board may have
// larger latencies..
//
localparam PTR      = 12;
localparam CAPACITY = (1<<PTR);
localparam MARGIN   = 10;

reg     [PTR-1:0]   wptr, rptr;
wire    [PTR-1:0]   wptr_grey, rptr_grey;
reg     [PTR-1:0]   wptr_grey_1, rptr_grey_1;
reg     [PTR-1:0]   wptr_grey_2, rptr_grey_2;
reg     [PTR-1:0]   wptr_crossed, rptr_crossed;
wire    [PTR-1:0]   data_items_wside;
wire    [PTR-1:0]   data_items_rside;
wire    [7:0]       wdata;
wire    [7:0]       rdata;
wire                we;
reg                 re;
wire                data_avail;
wire                space_avail;

assign din_enable   = space_avail;
assign we           = din_valid & space_avail;
assign wdata        = din;

always @(posedge din_clk)
begin
    if (we)
    begin
        wptr        <= wptr + 1;
    end
end



// Catcher register
reg [PTR-1:0]       n_rptr;
reg                 n_dout_valid;

always @*
begin
    n_dout_valid    = dout_valid;
    n_rptr          = rptr;
    re              = 1'b0;

    if ((~dout_valid | (dout_valid & dout_enable)) & data_avail)
    begin
        re              = 1'b1;
        n_dout_valid    = 1'b1;
        n_rptr          = rptr + 1;
    end
    else if (dout_valid & dout_enable & ~data_avail)
        n_dout_valid    = 1'b0;

end

always @(posedge dout_clk)
begin
    dout_valid  <= n_dout_valid;
    rptr        <= n_rptr;
end

// Address clash prevention
wire [PTR-1:0] rptr_mashed = (rptr == wptr) ? ~rptr : rptr;

ram4096x8 U_fiforam (
    .wclk       (din_clk),
    .write_en   (we),
    .waddr      (wptr),
    .din        (wdata),

    .rclk       (dout_clk),
    .read_en    (re),
    .raddr      (rptr_mashed),
    .dout       (dout)
);

// Clock crossing done using grey-coded pointers
assign rptr_grey = rptr ^ (rptr >> 1);
assign wptr_grey = wptr ^ (wptr >> 1);
always @(posedge din_clk)
begin
    rptr_grey_1  <= rptr_grey;
    rptr_grey_2  <= rptr_grey_1;
end
always @(posedge dout_clk)
begin
    wptr_grey_1  <= wptr_grey;
    wptr_grey_2  <= wptr_grey_1;
end

// convert back to binary
integer i;
always @*
begin
    wptr_crossed[PTR-1] = wptr_grey_2[PTR-1];
    for(i=PTR-2; i>=0; i=i-1) 
    begin
        wptr_crossed[i] = wptr_grey_2[i] ^ wptr_crossed[i+1];
    end
    rptr_crossed[PTR-1] = rptr_grey_2[PTR-1];
    for(i=PTR-2; i>=0; i=i-1) 
    begin
        rptr_crossed[i] = rptr_grey_2[i] ^ rptr_crossed[i+1];
    end
end
//
// The pointers start out at zero. Whenever they are not the same, there is
// data in the fifo. The wptr always leads the rptr
// When there is no space left, the wptr has caught up with the rptr by
// wrapping round. If the top bit is the same, each ptr is on the same side of
// a wrap boundary
//
//  eg a 256 entry fifo possible cases
//  wptr    rptr
//  00      00      empty
//  01      00      01
//  80      00      80
//  ff      00      ff
//  ff      7f      80
//  00      7f      81
//  01      7f      82
//  01      80      81
//  01      ff      02
//  f0      e0      10
//
assign data_items_wside = wptr - rptr_crossed;
assign data_items_rside = wptr_crossed - rptr;
assign space_avail      = data_items_wside < (CAPACITY - MARGIN);
assign data_avail       = data_items_rside != 0;
                



// Just for sim
initial
begin
    wptr = 0;
    rptr = 0;
    dout_valid = 0;
end


endmodule

