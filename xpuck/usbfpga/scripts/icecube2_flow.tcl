#!/usr/bin/tclsh8.4
# Sample flow script for iCEcube2 
############################################# 
# User Configurable section 
#############################################


set dev [lindex $argv 2]
set pkg [lindex $argv 3]
append device $dev "-" $pkg
set top_module [lindex $argv 1]
set proj_dir [pwd]
set output_dir "impl"
set edif_file [lindex $argv 0]
puts $dev
puts $pkg
puts $device
puts $top_module
puts $edif_file
puts $argv

append tool_options ":edifparser -y src/" $edif_file ".pcf"

########################################### 
# Tool Interface 
############################################
set sbt_root $::env(SBT_DIR)
append sbt_tcl $sbt_root "/tcl/sbt_backend_synpl.tcl" 
source $sbt_tcl

run_sbt_backend_auto $device $top_module $proj_dir $output_dir $tool_options $edif_file

exit
