#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <time.h>
#include <sched.h>
#include <sys/mman.h>
#include <string.h>
#include <fcntl.h>                                                              
#include <sys/ioctl.h>                                                          
#include <linux/spi/spidev.h>                                                   
#include <unistd.h>

#define MY_PRIORITY     (99)        /*  we use 49 as the PRREMPT_RT use 50
                                    as the priority of kernel tasklets
                                    and interrupt handler by default */

#define MAX_SAFE_STACK  (8*1024)    /* The maximum stack size which is
                                    guaranteed safe to access without
                                    faulting */

#define NSEC_PER_SEC    (1000000000) /* The number of nsecs per sec. */

#define BUFSIZE         (50)

static void pabort(const char *s)                                               
{                                                                               
        perror(s);                                                              
        abort();                                                                
}                                                                               
                                                                                
static const char *device = "/dev/spidev1.0";                                   
static uint8_t mode = 0;                                                            
static uint8_t bits = 16;                                                        
static uint32_t speed = 15000000;                                                 
                                                                                

static void transfer(int fd)                                                    
{                                                                               
        int ret;                                                                
        uint16_t tx[] = {                                                        
                0x0400, 0x0380,
                0x0000, 0x0000,
                0x0000, 0x0000,
                0x0000, 0x0000,
                0x0000, 0x0000,
                0x0000, 0x0000,
                0x0000, 0x0000,
                0x0000, 0x0000,
                0x0000, 0x0000,
                0x0000, 0x0000,
                0x0000, 0x0000,
                0x0000, 0x0000,
                0x000
        };                                                                      
        uint16_t rx[BUFSIZE/2] = {0, };                                     
                                                                                
        struct spi_ioc_transfer tr[2];                                          
        tr[0].tx_buf = (unsigned long)tx;
        tr[0].rx_buf = (unsigned long)rx;                                       
        tr[0].len = BUFSIZE;                                             
        tr[0].speed_hz = speed;                                                 
        tr[0].bits_per_word = bits;                                             
        tr[0].delay_usecs = 0;
        tr[0].cs_change = 0;

        ret = ioctl(fd, SPI_IOC_MESSAGE(1), tr);                                
        if (ret < 1)                                                            
                pabort("can't send spi message");                               
        
        
        for (ret = 0; ret < BUFSIZE/2; ret++) {                            
            printf("%04x ", rx[ret]);                                       
        }                                                                       
        printf("\r");
        
}                                                                               

void stack_prefault(void) 
{
    unsigned char dummy[MAX_SAFE_STACK];
    memset(dummy, 0, MAX_SAFE_STACK);
    return;
}

int main(int argc, char* argv[])
{
    struct  timespec        t;
    struct  sched_param     param;
    int     interval        = 1000000;

    int     ret = 0;                                                            
    int     fd;                                                                 
                                                
    //-------------------------------------------------------------------------
    // Open SPI device and check we can communicate with it
    //-------------------------------------------------------------------------
    fd = open(device, O_RDWR);                                              
    if (fd < 0)     pabort("can't open device");       
    ret = ioctl(fd, SPI_IOC_WR_MODE, &mode);                                
    if (ret == -1)  pabort("can't set spi mode");                                   
    ret = ioctl(fd, SPI_IOC_RD_MODE, &mode);                                
    if (ret == -1)  pabort("can't get spi mode");                                         
    ret = ioctl(fd, SPI_IOC_WR_BITS_PER_WORD, &bits);                       
    if (ret == -1)  pabort("can't set bits per word");                                                        
    ret = ioctl(fd, SPI_IOC_RD_BITS_PER_WORD, &bits);
    if (ret == -1)  pabort("can't get bits per word");
    ret = ioctl(fd, SPI_IOC_WR_MAX_SPEED_HZ, &speed);                       
    if (ret == -1)  pabort("can't set max speed hz");
    ret = ioctl(fd, SPI_IOC_RD_MAX_SPEED_HZ, &speed);                       
    if (ret == -1)  pabort("can't get max speed hz");
                                                                            
    printf("spi mode: %d\n", mode);                                         
    printf("bits per word: %d\n", bits);                                    
    printf("max speed: %d Hz (%d KHz)\n", speed, speed/1000);               


    //-------------------------------------------------------------------------
    // Set up the real time stuff
    //-------------------------------------------------------------------------
    // Declare ourself as a real time task
    param.sched_priority = MY_PRIORITY;
    if(sched_setscheduler(0, SCHED_FIFO, &param) == -1) 
    {
        perror("sched_setscheduler failed");
        exit(-1);
    }

    // Lock memory
    //if(mlockall(MCL_CURRENT|MCL_FUTURE) == -1) 
    //{
    //    perror("mlockall failed");
    //    exit(-2);
    //}

    // Pre-fault our stack
    stack_prefault();

    clock_gettime(CLOCK_MONOTONIC ,&t);
    // start after one second
    t.tv_sec++;

    while(1) 
    {
        // wait until next shot
        clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &t, NULL);

        // do the stuff
        transfer(fd);

        // calculate next shot
        t.tv_nsec += interval;
        while (t.tv_nsec >= NSEC_PER_SEC) 
        {
            t.tv_nsec -= NSEC_PER_SEC;
            t.tv_sec++;
        }
    }
}

