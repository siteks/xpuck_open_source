#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <getopt.h>
#include <signal.h>
#include <errno.h>
#include <ftdi.h>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include "ros/ros.h"
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include "std_msgs/Int16.h"

#include "clock.h"

#include <deque>
#include <algorithm>

//#define DEBUG



void check_outfile(char *);

static FILE *outputFile;

static int check = 1;
static int exitRequested = 0;
static void sigintHandler(int signum)
{
   exitRequested = 1;
}

// Use as signal that ROS is ready before starting USB streaming
volatile int32_t  ready_flag = 0;

#define SLOTS 4096
class Fifo {
    int wptr;
    int rptr;
    uint8_t slots[SLOTS][512];
    int     slotlen[SLOTS];
    pthread_mutex_t mutex_fifo;
public:
    Fifo()
    {
        wptr = 0; rptr = 0;
    }
    int send(uint8_t *p, int l)
    {
        bool full = false;
        pthread_mutex_lock(&mutex_fifo);
            full = ((wptr + 1) % SLOTS) == rptr;
        pthread_mutex_unlock(&mutex_fifo);
        if (!full)
        {
            memcpy(slots[wptr], p, l);
            slotlen[wptr] = l;
            pthread_mutex_lock(&mutex_fifo);
                wptr = (wptr + 1) % SLOTS;
            pthread_mutex_unlock(&mutex_fifo);
        }
        /*for(int i=0;i<l;i++)
            printf("%02x ",p[i]);*/
        //printf("Write %d %d %d\n", wptr,rptr, l);
        return full;
    }
    int receive(uint8_t *p)
    {
        bool empty;
        int l = 0;
        pthread_mutex_lock(&mutex_fifo);
            empty = wptr == rptr;
        pthread_mutex_unlock(&mutex_fifo);
        if (!empty)
        {
            memcpy(p, slots[rptr], slotlen[rptr]);
            l = slotlen[rptr];
            pthread_mutex_lock(&mutex_fifo);
                rptr = (rptr + 1) % SLOTS;
            pthread_mutex_unlock(&mutex_fifo);
        }
        /*if (!empty)
        {
            printf("\nRead %d %d %d\n", wptr,rptr, l);
            for(int i=0;i<l;i++)
                printf("%02x ",p[i]);
            printf("\n");
        }*/
        return l;
    }
};

// Thread-safe fifo to bring the data from the usb interface to the ros side
static Fifo tfifo;

class Listener
{
public:
    Listener(int _argc, char** _argv) 
    : 
        argc        (_argc),
        argv        (_argv)
    {
        int ret;
        visualise           = false;
        detect_proportion   = 0.002;
        detect_threshold    = xsize * ysize / 4 * detect_proportion;
        log                 = false;
        grab                = false;

        for(int i = 1; i < argc; i++)
        {
            if (!strcmp(argv[i], "-v"))
                visualise = true;
            if (!strcmp(argv[i], "-l"))
                log = true;
            if (!strcmp(argv[i], "-g"))
                grab = true;
            if (!strcmp(argv[i], "-t"))
            {
                if (++i >= argc)
                {
                    printf("Missing threshold value\n");
                    exit(1);
                }
                detect_threshold = atoi(argv[i]);
            }
            if (!strcmp(argv[i], "-r"))
            {
                if (++i >= argc)
                {
                    printf("Missing resolution\n");
                    exit(1);
                }
                if (!strcmp(argv[i], "vga"))
                {
                    xsize = 640;
                    ysize = 480;
                }
                else if (!strcmp(argv[i], "qvga"))
                {
                    xsize = 320;
                    ysize = 240;
                }
                else if (!strcmp(argv[i], "qqvga"))
                {
                    xsize = 160;
                    ysize = 120;
                }


                resolution = argv[i];
            }
        }
        
    

        ret = pthread_create(&lt, NULL, &Listener::thread_helper, this);
        if (ret)
        {
            perror("Failure spawning thread:");
            exit(1);
        }
    }
    ~Listener()
    {
    }
private:
    int                 argc;
    char                **argv;
    pthread_t           lt;
    std::deque<uint8_t> f;
    uint8_t             p[512];
    bool                visualise;
    bool                log;
    bool                grab;
    float               detect_proportion;
    int                 detect_threshold;
    std::string         resolution;
    int                 xsize = 320;
    int                 ysize = 240;

    void *getvideo()
    {
        fprintf(stderr,"Started..\n");
        // Declare ourselves to ROS
        ros::init(argc, argv, "camera");
        ros::NodeHandle n;

        //if (visualise)
        //{
        //    cv::namedWindow("Display",cv::WINDOW_AUTOSIZE);
        //    cv::waitKey(1);
        //}
        const int           xsize_small = xsize / 2;
        const int           ysize_small = ysize / 2;
        const cv::Scalar    lower_red1(0, 100, 100);
        const cv::Scalar    upper_red1(10, 255, 255);
        const cv::Scalar    lower_red2(160, 100, 100);
        const cv::Scalar    upper_red2(179, 255, 255);
        const cv::Scalar    lower_green(40, 100, 100);
        const cv::Scalar    upper_green(80, 255, 255);
        const cv::Scalar    lower_blue(90, 50, 100);
        const cv::Scalar    upper_blue(130, 255, 255);
        // Define the regions, ignore top and bottom lines, they sometimes
        // have bad data in
        const int           halfy   = ysize_small / 2;
        const int           thirdx  = xsize_small / 3;
        // Define the regions of interest, exclude the top half of the screen because
        // we are only interested in blob detection below the horizon
        const cv::Rect      left_region(    0,          halfy, thirdx, halfy - 2);
        const cv::Rect      centre_region(  thirdx,     halfy, thirdx, halfy - 2);
        const cv::Rect      right_region(   thirdx * 2, halfy, thirdx, halfy - 2);

        int         res;
        int         l;
        int         state = 0;
        int         rlcount, rccount, rrcount;
        int         glcount, gccount, grcount;
        int         blcount, bccount, brcount;
        //cv::Mat img(cv::Size(160, 120), CV_8UC3);
        uint8_t     *dest;
        uint8_t     *limit;
        cv::Mat     framergb(cv::Size(xsize, ysize), CV_8UC3);
        cv::Mat     ycrcb(cv::Size(xsize, ysize), CV_8UC3);
        int         y = 0;
        int         fcount = 0;

        cv::Mat     imgsmall(cv::Size(xsize_small, ysize_small), CV_8UC3);
        cv::Mat     imghsv(cv::Size(xsize_small, ysize_small), CV_8UC3);
        cv::Mat     red_mask1(cv::Size(xsize_small, ysize_small), CV_8U);
        cv::Mat     red_mask2(cv::Size(xsize_small, ysize_small), CV_8U);
        cv::Mat     red_mask(cv::Size(xsize_small, ysize_small), CV_8U);
        cv::Mat     green_mask(cv::Size(xsize_small, ysize_small), CV_8U);
        cv::Mat     blue_mask(cv::Size(xsize_small, ysize_small), CV_8U);

        image_transport::ImageTransport it(n);
        sensor_msgs::ImagePtr           msg;

        image_transport::Publisher      cam_pub = it.advertise("camera/image", 1);

        ros::Publisher                  detect_pub = n.advertise<std_msgs::Int16>("camera/realrgbdetect", 1);
        std_msgs::Int16                 rgbd;



        Clock clock;
        clock.start();
        uint64_t times[8] = {0};
        uint64_t sumtimes[8] = {0};
        int loops = 0;
        int dsize = 0;
        uint8_t buffer[1000000];
        uint8_t *bp = buffer;
        int bidx = 0;
        int lidx = 0;

        int xp = 0;
        int yp = 0;



        fprintf(stderr, "reached main loop\n");
        ready_flag = 1;
        dest    = (uint8_t*)(ycrcb.ptr<cv::Vec3b>(0, 0));
        limit   = (uint8_t*)(ycrcb.ptr<cv::Vec3b>(ysize, 0));
        ros::Rate loop_rate(15);
        while (n.ok() && !exitRequested)
        //while(!exitRequested)
        {

            while (!(l = tfifo.receive(p)));
            dsize += l;

            //if (l > 0)
            //    printf("got buffer len %d\n",l);
            for(int i = 0; i < l; i++)
            {
                uint8_t c = p[i];
                //printf("%02x ",c);
                

                if (c == 0xff)
                {
                    bidx += xsize * 2;
                    lidx = 0;
                    // At start of line, we know that the shortest line is 320 * 2 = 640 bytes. The
                    // maximum size of USB packet is 510 bytes, so this will always work without missing
                    // an end of line marker
                    //memcpy(buffer + bidx, p + i, l - i - 1);
                    //i = l;
                }
                else if (c == 0)
                {
                    times[0] = clock.lap();
                    //printf("%d\n", yp);
                    yp = 0;
                    //bp = buffer;
                    bidx = 0;
                    lidx = 0;
                    // ccir601
                    // Cr -= 128
                    // Cb -= 128
                    // R = Y                + 1.403 * Cr
                    // G = Y - 0.344 * Cb   - 0.714 * Cr
                    // B = Y + 1.773 * Cb
                    //
                    // integer version from wiki https://en.wikipedia.org/wiki/YUV
                    // R    = Y + Cr + Cr>>2 + Cr>>3 + Cr>>5
                    //      = Y + 1.406 * Cr
                    // G    = Y - Cb>>2 - Cb>>4 - Cb>>5 - Cr>>1 - Cr>>3 - Cr>>4 - Cr>>5
                    //      = Y - 0.343 * Cb - 0.718 * Cr
                    // B    = Y + Cb + Cb>>1 + Cb>>2 + cb>>6
                    //
                    uint8_t *fb =  (uint8_t*)(framergb.ptr<cv::Vec3b>(0, 0));
                    for (int j = 0; j < xsize * ysize; j += 2)
                    {
                        // Byte ordering in CbYCrY
                        uint32_t d = *(uint32_t *)(buffer + (j << 1));
                        // Subtract 128 from CbCr, equivalent to inverting top bit
                        d ^= 0x00800080;
                        int8_t cr = d & 0xff;
                        int8_t cb = (d >> 16) & 0xff;
                        uint8_t y0 = (d >> 8) & 0xff;
                        uint8_t y1 = (d >> 24) & 0xff;
#if 1
                        // 9.5ms
                        int r0 = y0 + cr + (cr >> 2) + (cr >> 3) + (cr >> 5);
                        int r1 = y1 + cr + (cr >> 2) + (cr >> 3) + (cr >> 5);
                        int g0 = y0 - (cb >> 2) - (cb >> 4) - (cb >> 5) - (cr >> 1) - (cr >> 3) - (cr >> 4) - (cr >> 5);
                        int g1 = y1 - (cb >> 2) - (cb >> 4) - (cb >> 5) - (cr >> 1) - (cr >> 3) - (cr >> 4) - (cr >> 5);
                        int b0 = y0 + cb + (cb >> 1) + (cb >> 2) + (cb >> 6);
                        int b1 = y1 + cb + (cb >> 1) + (cb >> 2) + (cb >> 6);

#else
                        // 11.2ms
                        int r0 = y0 + 1.403 * cr;
                        int r1 = y1 + 1.403 * cr;
                        int g0 = y0 - 0.344 * cb - 0.714 * cr;
                        int g1 = y1 - 0.344 * cb - 0.714 * cr;
                        int b0 = y0 + 1.773 * cb;
                        int b1 = y1 + 1.773 * cb;
#endif
                        r0 = std::min(std::max(r0, 0), 255);
                        g0 = std::min(std::max(g0, 0), 255);
                        b0 = std::min(std::max(b0, 0), 255);
                        r1 = std::min(std::max(r1, 0), 255);
                        g1 = std::min(std::max(g1, 0), 255);
                        b1 = std::min(std::max(b1, 0), 255);
                        fb[0] = r0;
                        fb[1] = g0;
                        fb[2] = b0;
                        fb[3] = r1;
                        fb[4] = g1;
                        fb[5] = b1;
                        fb += 6;
                    }
                    times[1] = clock.elapsed(true);
                    cv::resize(framergb, imgsmall, imgsmall.size());
                    times[2] = clock.elapsed(true);
                    //cv::imshow("Display",imgsmall);
                    //cv::waitKey(1);
                    msg = cv_bridge::CvImage(std_msgs::Header(), "bgr8", framergb).toImageMsg();
                    cam_pub.publish(msg);


                    // At this point we have an image, try to find pure red, green, blue
                    // and count the pixels
                    cv::cvtColor(imgsmall, imghsv, CV_BGR2HSV);
                    
                    cv::inRange(imghsv, lower_red1, upper_red1, red_mask1);
                    cv::inRange(imghsv, lower_red2, upper_red2, red_mask2);
                    cv::inRange(imghsv, lower_green, upper_green, green_mask);
                    cv::inRange(imghsv, lower_blue, upper_blue, blue_mask);

                    red_mask = red_mask1 | red_mask2;


                    rlcount = cv::countNonZero(red_mask(left_region));
                    rccount = cv::countNonZero(red_mask(centre_region));
                    rrcount = cv::countNonZero(red_mask(right_region));
                    glcount = cv::countNonZero(green_mask(left_region));
                    gccount = cv::countNonZero(green_mask(centre_region));
                    grcount = cv::countNonZero(green_mask(right_region));
                    blcount = cv::countNonZero(blue_mask(left_region));
                    bccount = cv::countNonZero(blue_mask(centre_region));
                    brcount = cv::countNonZero(blue_mask(right_region));

                    rgbd.data = ((blcount > detect_threshold) ? 0x100 : 0)
                            |   ((glcount > detect_threshold) ? 0x80 : 0)
                            |   ((rlcount > detect_threshold) ? 0x40 : 0)
                            |   ((bccount > detect_threshold) ? 0x20 : 0)
                            |   ((gccount > detect_threshold) ? 0x10 : 0)
                            |   ((rccount > detect_threshold) ? 0x8 : 0)
                            |   ((brcount > detect_threshold) ? 0x4 : 0)
                            |   ((grcount > detect_threshold) ? 0x2 : 0)
                            |   ((rrcount > detect_threshold) ? 0x1 : 0);

                    detect_pub.publish(rgbd);
    
                    if (visualise)
                    {
                        cv::imshow("Display",imgsmall);
                        //cv::imshow("Display",framergb);
                        cv::imshow("Red", red_mask);
                        cv::imshow("Green", green_mask);
                        cv::imshow("Blue", blue_mask);
                        cv::waitKey(1);
                    }
                    if (grab && (fcount > 2))
                    {
                        grab = false;
                        cv::imwrite("frame.png", imgsmall);
                    }

                    fcount++;
                    ros::spinOnce();
                    ros::spinOnce();
                    loop_rate.sleep();

                    times[3] = clock.elapsed(true);
                    loops++;

                    if (log)
                    {
                        double f = 1e-3;
                        fprintf(stderr, "Detect: %03o Red L:%4d C:%4d R:%4d   Green L:%4d C:%4d R:%4d   Blue L:%4d C:%4d R:%4d  %f %8d %10.2f %10.2f %10.2f %10.2f %10.2f\n", rgbd.data,
                            rlcount, rccount, rrcount,
                            glcount, gccount, grcount,
                            blcount, bccount, brcount, 
                            1e-9*clock.current(), dsize, f * times[0], 
                                f * times[1], f * (times[2] - times[1]), f * (times[3] - times[2]),
                                    (double)dsize * 1e3 / times[2] );
                        dsize = 0;
                    }

                }
                else
                {
                    //*bp++ = c;
                    buffer[bidx + lidx++] = c;
                    //if (bp >= (buffer + 1000000))
                    //    bp = buffer;
                    if (bidx >= 1000000)
                        bidx = 0;
                }

            }


        }
        exitRequested = 1;
    }

    // http://stackoverflow.com/questions/1151582/pthread-function-from-a-class
    static void     *thread_helper(void *context)
    {
        return ((Listener*)context)->getvideo();
    }


};


int read_callback(uint8_t *buffer, int length, FTDIProgressInfo *progress, void *userdata)
{
    static int overruns = 0;
    if (exitRequested)
        exit(1);
    if (length)
    {
        // If we can't get the data into the fifo, it gets thrown away
        bool full = tfifo.send(buffer, length);
        if (full)
        {
            //fprintf(stderr, "Fifo overrun\n");
            overruns++;
        }
    }
    //if (0 && progress)
#ifdef DEBUG
    if (log && progress)
    {
        fprintf(stderr, "%10.02fs total time %9.3f MiB captured %7.1f kB/s curr rate %7.1f kB/s totalrate %d overruns\n",
            progress->totalTime,
            progress->current.totalBytes / (1024.0 * 1024.0),
            progress->currentRate / 1024.0,
            progress->totalRate / 1024.0,
            overruns);
        overruns = 0;
    }
#endif
    return exitRequested ? 1 : 0;
}

int main(int argc, char **argv)
{
    struct ftdi_context *ftdi;
    int err, c;
    exitRequested = 0;


    if ((ftdi = ftdi_new()) == 0)
    {
        fprintf(stderr, "ftdi_new failed\n");
        return EXIT_FAILURE;
    }

    if (ftdi_set_interface(ftdi, INTERFACE_A) < 0)
    {
        fprintf(stderr, "ftdi_set_interface failed\n");
        ftdi_free(ftdi);
        return EXIT_FAILURE;
    }

    if (ftdi_usb_open_desc(ftdi, 0x0403, 0x6010, NULL, NULL) < 0)
    {
        fprintf(stderr,"Can't open ftdi device: %s\n",ftdi_get_error_string(ftdi));
        ftdi_free(ftdi);
        return EXIT_FAILURE;
    }

    if(ftdi_set_latency_timer(ftdi, 2))
    {
        fprintf(stderr,"Can't set latency, Error %s\n",ftdi_get_error_string(ftdi));
        ftdi_usb_close(ftdi);
        ftdi_free(ftdi);
        return EXIT_FAILURE;
    }

    //signal(SIGINT, sigintHandler);

    // Start listener thread
    Listener lthread(argc, argv);

    //cv::namedWindow("Display",cv::WINDOW_AUTOSIZE);
    //cv::waitKey(1);

    // Actually start the streaming operation. This call will not return until
    // the program is interrupted
    //
    // Don't start until the ROS stuff is set up
    while (!ready_flag);
    printf("Starting USB stream..\n");

    err = ftdi_readstream(ftdi, read_callback, NULL, 8, 256);
    if (err < 0 && !exitRequested)
        exit(1);

    if (ftdi_set_bitmode(ftdi,  0xff, BITMODE_RESET) < 0)
    {
        fprintf(stderr,"Can't set synchronous fifo mode, Error %s\n",ftdi_get_error_string(ftdi));
        ftdi_usb_close(ftdi);
        ftdi_free(ftdi);
        return EXIT_FAILURE;
    }
    ftdi_usb_close(ftdi);
    ftdi_free(ftdi);
    signal(SIGINT, SIG_DFL);
    exit (0);
}


