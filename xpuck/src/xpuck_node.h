#ifndef XPUCK_NODE_H
#define XPUCK_NODE_H

#include <stdint.h>
#include "ros/ros.h"
#include "std_msgs/String.h"
#include "std_msgs/Int32MultiArray.h"
#include "std_msgs/Float32MultiArray.h"
#include "std_msgs/Int16.h"
#include "std_msgs/Int32.h"
#include "std_msgs/Float32.h"
#include "geometry_msgs/Twist.h"
#include "std_msgs/Bool.h"



//https://stackoverflow.com/questions/2342162/stdstring-formatting-like-sprintf
#include <memory>
template<typename ... Args>
std::string string_format( const std::string& format, Args ... args )
{
    size_t size = snprintf( nullptr, 0, format.c_str(), args ... ) + 1; // Extra space for '\0'
    std::unique_ptr<char[]> buf( new char[ size ] ); 
    snprintf( buf.get(), size, format.c_str(), args ... );
    return std::string( buf.get(), buf.get() + size - 1 ); // We don't want the '\0' inside
}

#define NSEC_PER_SEC    (1000000000)

uint64_t microseconds(struct timespec &t)
{
    return t.tv_nsec / 1000 + t.tv_sec * 1000000;
}
uint64_t microseconds()
{
    struct timespec t;
    clock_gettime(CLOCK_MONOTONIC, &t);
    return microseconds(t);
}
void advance_time(struct timespec &t, uint64_t delta)
{
    t.tv_nsec += delta;
    while (t.tv_nsec >= NSEC_PER_SEC)
    {
        t.tv_nsec -= NSEC_PER_SEC;
        t.tv_sec++;
    }
}

class Epuckcom
{
public:
    Epuckcom();
    ~Epuckcom();
private:
    // SPI interface stuff
    const int               bufsize;
    const char              *device;
    const uint8_t           mode;
    const uint8_t           bits;
    const uint32_t          speed;

    // SPI data buffers
    std::vector<uint16_t>   tx;
    std::vector<uint16_t>   rx;
    // SPI transfer structure
    struct                  spi_ioc_transfer tr[1];
    // SPI device and return value
    int                     fd;
    int                     ret;

    // Realtime thread to service the SPI
    pthread_t               rtt;

    // http://stackoverflow.com/questions/1151582/pthread-function-from-a-class
    void                    *spicom();
    static void             *thread_helper(void *context)
    {
        return ((Epuckcom *)context)->spicom();
    }
    void                    transfer();

    float                   acc_mean;
//    float                   acc[256];
//    float                   acc_var_sum;
//    float                   acc_std;
//    float                   acc_zero;
    float                   vx;
    int                     acc_idx;

public:
    // Variables for passing in motor control. By making aligned and 32 bit,
    // we guarantee that accesses will be atomic on ARM and Intel processors at least
    int32_t             lmotor          __attribute__((aligned(4)));
    int32_t             rmotor          __attribute__((aligned(4)));
    int32_t             prox_ir_off[8]  __attribute__((aligned(4)));
    int32_t             prox_ir_on[8]   __attribute__((aligned(4)));
    int32_t             accel[3]        __attribute__((aligned(4)));
    int32_t             leds[15]        __attribute__((aligned(4)));
    int32_t             steps[2]        __attribute__((aligned(4)));
    int32_t             batt_low        __attribute__((aligned(4)));
    float               integrated_xacc __attribute__((aligned(4)));


    volatile int32_t            flag            __attribute__((aligned(4)));
    volatile int32_t            kill            __attribute__((aligned(4)));
};




class Packet_from_hub
{
public:
    // Hold data that is received from the hub, access controlled by mutex
    Packet_from_hub() : n(0)
    {
        x       = 0.0f;
        y       = 0.0f;
        th      = 0.0f;
        tstamp  = 0;
        cmd_state = 0;
        fake    = 0;
        adj_fitness = 0.0f;
        adj_swarm[0] = 0.0f;
        adj_swarm[1] = 0.0f;
        for(int i =0; i < 8; prox[i++] = 0);
        pthread_mutex_init(&mtx, NULL);
    }
    ~Packet_from_hub()
    {
        pthread_mutex_destroy(&mtx);
    }
    void lock()
    {
        pthread_mutex_lock(&mtx);
    }
    void unlock()
    {
        pthread_mutex_unlock(&mtx);
    }
    void fill(std::string msg)
    {
        pthread_mutex_lock(&mtx);
        sscanf(msg.c_str(), "%s %" PRIu64 " %f %f %f %d %d %d %d %d %d %d %d %d %o %d"
                            " %d %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f"
                            " %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f"
               ,buf, &tstamp, &x, &y, &th, &cmd_state,
               prox, prox + 1, prox + 2, prox + 3, prox + 4, prox + 5, prox + 6, prox + 7, &blob, &fake,
               &n,
               range, bearing,
               range + 1, bearing + 1,
               range + 2, bearing + 2,
               range + 3, bearing + 3,
               range + 4, bearing + 4,
               range + 5, bearing + 5,
               range + 6, bearing + 6,
               range + 7, bearing + 7,
               range + 8, bearing + 8,
               range + 9, bearing + 9,
               range + 10, bearing + 10,
               range + 11, bearing + 11,
               range + 12, bearing + 12,
               &(adj_swarm[0]), &(adj_swarm[1]),
               range + 14, &vx,
               range + 15, &adj_fitness // We borrow this for fitness diffusion - only using 9 robots
        );
        pthread_mutex_unlock(&mtx);
    }
    uint64_t get_tstamp()
    {
        pthread_mutex_lock(&mtx);
        uint64_t t = tstamp;
        pthread_mutex_unlock(&mtx);
        return t;
    }
    int32_t             fake;
    int32_t             prox[8];
    int32_t             cmd_state;
    int32_t             blob;

    // Range and bearing stuff
    int32_t             n;
    float               range[16];
    float               bearing[16];
    float               adj_fitness;
    float               adj_swarm[2];

    // Velocity X component
    float               vx;

    // Ground truth pose
    float               x;
    float               y;
    float               th;

private:
    pthread_mutex_t     mtx;
    uint64_t            tstamp;
    char                buf[256];

};


class Packet_to_hub
{
public:
    Packet_to_hub() :
          wheel_left(0.0f),
          wheel_right(0.0f),
          resp_state(0),
          blob(0),
          v_light_swarm(0),
          v_heavy_swarm(0),
          rprobe(0.0f),
          v_diff(0.0f),
          v_light_raw(0.0f),
          v_light_filt(0.0f),
          v_heavy_raw(0.0f),
          v_heavy_filt(0.0f)
    {
        pthread_mutex_init(&mtx, NULL);
    }
    ~Packet_to_hub()
    {
        pthread_mutex_destroy(&mtx);
    }
    void lock()
    {
        pthread_mutex_lock(&mtx);
    }
    void unlock()
    {
        pthread_mutex_unlock(&mtx);
    }
    float       wheel_left;
    float       wheel_right;
    int         leds[15] = {0};
    int         resp_state;
    int         blob;
    float       v_light_swarm;
    float       v_heavy_swarm;
    float       rprobe;
    float       v_diff;
    float       v_light_raw;
    float       v_light_filt;
    float       v_heavy_raw;
    float       v_heavy_filt;
private:
    pthread_mutex_t     mtx;
};

class Telemetry
{
public:
    Telemetry(Epuckcom & _ept, bool _verbose=false);
    void update();
    float getval(int f, int type = 0);

    // Thread to talk to hub
    pthread_t               telem_thread;

    void                    *telemetry();
    static void             *thread_helper(void *context)
    {
        return ((Telemetry *)context)->telemetry();
    }
    std::string values(std::string name, Packet_to_hub &p, uint64_t tstamp)
    {
        p.lock();
        float wl        = p.wheel_left;
        float wr        = p.wheel_right;
        int resp_state  = p.resp_state;
        int blob        = p.blob;
        p.unlock();
        return string_format("%s %llu %1g %1g %1g %1g %1g %1g %1g %1g %1g %1g %d %1g %1g %d %04o %1g %1g %1g %1g %1g %1g %1g %1g",
                             name.c_str(), tstamp,
            temp, dbm, freq0, freq4, v0, a0, w0, v1, a1, w1, epb, wl, wr, resp_state, blob, p.v_light_swarm, p.v_heavy_swarm, 
            p.rprobe, p.v_diff, p.v_light_filt, p.v_heavy_filt, p.v_light_raw, p.v_heavy_raw);
    }



    Epuckcom &ept;

    Packet_from_hub     packet_from_hub;
    Packet_to_hub       packet_to_hub;


    bool verbose;
    const int bufsize = 256;
    int ftemp;
    int ffreq0;
    int ffreq4;
    int fvolt0;
    int famp0;
    int fwatt0;
    int fvolt1;
    int famp1;
    int fwatt1;
    int fdbm;
    int update_period;
    uint64_t    next_sample;
    float  temp, dbm, freq0, freq4, v0, a0, w0, v1, a1, w1;
    int epb;

};


class ROSinterface
{
public:
    ROSinterface(Epuckcom &_ept, Telemetry &_tlm, ros::NodeHandle n, bool _verbose);
    ~ROSinterface();
    void run();
private:
    Epuckcom        &ept;
    Telemetry       &tlm;
    bool            verbose;
    ros::Subscriber cmd_vel_sub;
    ros::Subscriber wheel_vel_sub;
    ros::Subscriber leds_sub;
    ros::Subscriber real_cam_sub;
    ros::Subscriber resp_state_sub;
    ros::Subscriber lfitness_sub;
    ros::Subscriber rprobe_sub;
    ros::Publisher  prox_pub;
    ros::Publisher  prox_on_pub;
    ros::Publisher  prox_off_pub;
    ros::Publisher  accel_pub;
    ros::Publisher  steps_pub;
    ros::Publisher  batt_pub;
    ros::Publisher  cmd_state_pub;
    ros::Publisher  cam_pub;
    ros::Publisher  randb_pub;
    ros::Publisher  pose_pub;
    ros::Publisher  adj_fitness_pub;
    ros::Publisher  adj_swarm_pub;
    void cmd_vel_callback(const geometry_msgs::Twist::ConstPtr& msg);
    void wheel_vel_callback(const std_msgs::Float32MultiArray::ConstPtr& msg);
    void leds_callback(const std_msgs::Int32MultiArray::ConstPtr& msg);
    void rgbd_callback(const std_msgs::Int16::ConstPtr& msg);
    void resp_state_callback(const std_msgs::Int32::ConstPtr& msg);
    void rprobe_callback(const std_msgs::Float32MultiArray::ConstPtr& msg);
    ros::Time       last_cmd;

    int16_t         real_cam_blob;
    int             resp_state;
    float           rprobe;
    float           v_diff_raw;
    float           v_diff_filt;

};



#endif //XPUCK_NODE_H

