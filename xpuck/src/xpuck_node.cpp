
#define __STDC_FORMAT_MACROS
#include <inttypes.h>

#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <time.h>
#include <sched.h>
#include <pthread.h>
#include <linux/spi/spidev.h>
#include <vector>
#include <signal.h>
#include "xpuck_node.h"


#define NSEC_PER_SEC    (1000000000)

#include <zmq.hpp>
#define HUB                 "192.168.2.32"
#define ZMQPORT_LOCALHARDWARE   "5683"
#define ZMQPORT_LOCALPOSE       "5681"
#define ZMQPORT_POSE            "5678"
#define ZMQPORT_XPUCK2HUB       "5679"

#define UDP_TOXPUCK     6700
#define UDP_FROMXPUCK   6701

#define BUFSIZE 1024


//#define LOG_LATENCY
//#define RAW_OUTPUT




// This ROS node is started at boot time and provides essential services to control the
// robot, both in sim and in real life.
// It runs as three threads:
//
// A realtime thread communicating with the hardware of the epuck over SPI
// A comms thread that listens for ZMQ messages from the hub and replies with telemetry and control data
// A ROS thread (the main thread) that presents a unified interface to the controller, comprising the
//  rest of the ROS graph.
//
// The node can work in two modes, real and fake.
//  Topic               Dir     Real                            Fake
//  wheel_vel           in      actual robot hardware           Argos simulated actuators
//  prox_array          out     hardware IR proximity sensors   Argos simulated IR sensors
//  camera/rgbdetect    out     camera/realrgbdetect            Argos simulated camera
//
// The telemetry thread gathers data from the hardware such as battery voltage, and handles
// the reception and response to ZMQ messages from the hub. The hub messages arrive at <some rate>
// and contain Vicon data, or its simulation, simulated sensory data, and signals showing the
// required mode and possible other controls. The telemetry thread replies to each message as fast as
// possible. The hub uses the round trip time for each message and reply to asses the current link
// quality

char hubstring[256];

int main(int argc, char** argv)
{
    printf("Starting SPI node %d args\n", argc);
    for(int i = 0; i < argc; i++)
        printf("arg %d:%s\n", i, argv[i]);

    strcpy(hubstring, HUB);

    bool verbose = false;
    printf("If you can't see this then the wrong binary is executing\n");
    for (int i = 1; i < argc; i++)
    {
        if (!strcmp(argv[i], "-v"))
        {
            printf("Verbose mode\n");
            verbose = true;
        }
        else if (!strcmp(argv[i], "-i"))
        {
            // Assume ip address
            strcpy(hubstring, argv[++i]);
        }

    }
    printf("Using hub IP address %s\n", hubstring);

    // Declare ourselves to ROS
    ros::init(argc, argv, "xpuck_node");
    ros::NodeHandle n;

    // Create RT thread to talk to epuck
    Epuckcom        ep_thread;

    // Create thread to talk to hub
    Telemetry       telem_thread(ep_thread, verbose);

    // Set up the ROS listener and callback
    ROSinterface    ros_interface(ep_thread, telem_thread, n, verbose);

    // Wait for stuff to happen
    ros_interface.run();


    return 0;
}


// from https://gist.github.com/diabloneo/9619917
void timespec_diff(struct timespec *start, struct timespec *stop,
                   struct timespec *result)
{
    if ((stop->tv_nsec - start->tv_nsec) < 0) {
        result->tv_sec = stop->tv_sec - start->tv_sec - 1;
        result->tv_nsec = stop->tv_nsec - start->tv_nsec + 1000000000;
    } else {
        result->tv_sec = stop->tv_sec - start->tv_sec;
        result->tv_nsec = stop->tv_nsec - start->tv_nsec;
    }

    return;
}

Epuckcom::Epuckcom()
:
    bufsize     (64),
    device      ("/dev/spidev1.0"),
    mode        (0),
    bits        (16),
    speed       (5000000),
    vx          (0),
    acc_idx     (0)
{
    // Allocate space in the buffers
    tx.resize(bufsize/2, 0);
    rx.resize(bufsize/2, 0);

    // Zero the motors
    lmotor = 0;
    rmotor = 0;

    // Clear the handshake flag
    flag = 0;
    // Clear kill flag
    kill = 0;

    // Zero the LEDS
    for (int i = 0; i < 15; i++)
        leds[i] = 0;

    // Set up the SPI structure
    tr[0].tx_buf        = (unsigned long)&(tx[0]);
    tr[0].rx_buf        = (unsigned long)&(rx[0]);
    tr[0].len           = bufsize;
    tr[0].speed_hz      = speed;
    tr[0].bits_per_word = bits;
    tr[0].delay_usecs   = 0;
    tr[0].cs_change     = 0;

    // Open SPI device
    fd = open(device, O_RDWR);                                              
    if (fd < 0)
    {
        perror("Can't open device:");
        exit(1);
    }
    // Set up the device parameters
    ret = ioctl(fd, SPI_IOC_WR_MODE, &mode);
    ret += ioctl(fd, SPI_IOC_WR_BITS_PER_WORD, &bits);
    ret += ioctl(fd, SPI_IOC_WR_MAX_SPEED_HZ, &speed);
    if (ret < 0)
    {
        perror("Failure setting mode, bits, or speed of SPI device:");
        exit(2);
    }
    printf("About to spawn RT thread\n");

    // Create the thread and make it realtime
    ret = pthread_create(&rtt, NULL, &Epuckcom::thread_helper, this);
    if (ret)
    {
        perror("Failure spawning thread:");
        exit(3);
    }


}


Epuckcom::~Epuckcom()
{
}

#define R(x)    ((x>>0)&0xff)
#define G(x)    ((x>>8)&0xff)
#define B(x)    ((x>>16)&0xff)


// Buffer layout (each element 16 bits)
//
//      Send data       Receive data
//  0   lmotor          
//  1   rmotor          acc(0)
//  2   RG  0           acc(1)
//  3   GB  1           acc(2)
//  4   BR              prox_off(0)
//  5   RG  2           prox_off(1)
//  6   GB  3           prox_off(2)
//  7   BR              prox_off(3)
//  8   RG  4           prox_off(4)
//  9   GB  5           prox_off(5)
//  10  BR              prox_off(6)
//  11  RG  6           prox_off(7)
//  12  GB  7           prox_on(0)
//  13  BR              prox_on(1)
//  14  RG  8           prox_on(2)
//  15  GB  9           prox_on(3)
//  16  BR              prox_on(4)
//  17  RG  10          prox_on(5)
//  18  GB  11          prox_on(6)
//  19  BR              prox_on(7)
//  20  RG  12          left_odom(0)
//  21  GB  13          left_odom(1)
//  22  BR              right_odom(0)
//  23  RG  14          right_odom(1)
//  24  -B              batt_low


void *Epuckcom::spicom()
{
    printf("Now spawned realtime thread\n");

    struct  sched_param     param;
    struct  timespec        t;
    struct  timespec        t_wake;
    struct  timespec        t_latency;
    // Exchange SPI packet every 5ms, 200Hz
    int     interval        = 5000000;
    long    min             = NSEC_PER_SEC;
    long    max             = 0;
    double  avg             = 0;
    long    count           = 0;
    int     bins[20];

    param.sched_priority = 80;
    if (sched_setscheduler(0, SCHED_FIFO, &param) == -1)
    {
        perror("sched_setschedular failed:");
        exit(4);
    }
    clock_gettime(CLOCK_MONOTONIC ,&t);
    // start after one second
    t.tv_sec++;
    while(!kill)
    {
        // wait until next shot
        clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &t, NULL);

        // Find out the latency
        clock_gettime(CLOCK_MONOTONIC ,&t_wake);
        timespec_diff(&t, &t_wake, &t_latency);

        count++;
        min = t_latency.tv_nsec < min ? t_latency.tv_nsec : min;

        // Only start max after a second to see if cache and mem effects
        // causing very high value, or if they occur at other times..
        if (count > 500)
            max = t_latency.tv_nsec > max ? t_latency.tv_nsec : max;
        avg = (avg * (count - 1) + t_latency.tv_nsec) / count;
    
        int bin = (t_latency.tv_nsec / 100000);
        if (bin > 19) bin = 19;
        bins[bin] ++;

#ifdef LOG_LATENCY
        if ((count % 500) == 0)
        {
            for (int i=0; i<20; i++)
                printf("%5d", bins[i]);
            printf(" %9ld %9ld %12f\n", min, max, avg);
        }
#endif

        // do the SPI transfer
        Epuckcom::transfer();

        // Move in potential new motor speed values
        tx[0] = (uint16_t)lmotor;
        tx[1] = (uint16_t)rmotor;

        // Move in the LED RGB values
        // Expected order is GRB
        int j = 0;
        for(int i=0; i<14; i+=2)
        {
            tx[2 + j + 0] = R(leds[i])     | (G(leds[i])<<8);
            tx[2 + j + 1] = G(leds[i+1])   | (B(leds[i])<<8);
            tx[2 + j + 2] = B(leds[i+1])   | (R(leds[i+1])<<8);
            j += 3;
        }
        tx[2 + j + 0] = R(leds[14])     | (G(leds[14])<<8);
        tx[2 + j + 1] = 0               | (B(leds[14])<<8);
        //tx[2 + j + 2] = 0;


        // Get the proximity sensors into ROS domain
        for (int i=0; i<8; i++)
        {
            prox_ir_off[i]  = (int32_t)rx[4+i];
            prox_ir_on[i]   = (int32_t)rx[12+i];
        }
        // Get the accelerometer data into the ROS domain
        accel[0] = (int32_t)rx[1];
        accel[1] = (int32_t)rx[2];
        accel[2] = (int32_t)rx[3];
        // Get the motor steps data into the ROS domain
        steps[0] = (int32_t)((rx[21] << 16) | rx[20]);
        steps[1] = (int32_t)((rx[23] << 16) | rx[22]);
        // Get the battery low signal into the ROS domain
        batt_low = (int32_t)rx[24];


        // Signal data ready
        flag = 1;

        // calculate next shot
        t.tv_nsec += interval;
        while (t.tv_nsec >= NSEC_PER_SEC)
        {
            t.tv_nsec -= NSEC_PER_SEC;
            t.tv_sec++;
        }
    }
}

void Epuckcom::transfer()
{
    ret = ioctl(fd, SPI_IOC_MESSAGE(1), tr);
    if (ret < 1)
    {
        perror("Tranfer failed:");
        exit(5);
    }
#ifdef RAW_OUTPUT
    for (ret = 0; ret < bufsize/2; ret++) {                            
        printf("%04x ", rx[ret]);                                       
    }                                                                       
    printf("\n");
#endif
}

ROSinterface::ROSinterface(Epuckcom &_ept,
                           Telemetry &_tlm,
                           ros::NodeHandle n,
                           bool _verbose)
        : ept(_ept),
          tlm(_tlm),
          verbose(_verbose)
{
    cmd_vel_sub     = n.subscribe("cmd_vel", 1, &ROSinterface::cmd_vel_callback,
                                  this, ros::TransportHints().tcpNoDelay());
    wheel_vel_sub   = n.subscribe("wheel_vel", 1, &ROSinterface::wheel_vel_callback,
                                  this, ros::TransportHints().tcpNoDelay());
    leds_sub        = n.subscribe("leds", 1, &ROSinterface::leds_callback,
                                  this, ros::TransportHints().tcpNoDelay());
    real_cam_sub    = n.subscribe("camera/realrgbdetect", 1, &ROSinterface::rgbd_callback,
                                  this, ros::TransportHints().tcpNoDelay());
    resp_state_sub  = n.subscribe("resp_state", 1, &ROSinterface::resp_state_callback,
                                  this, ros::TransportHints().tcpNoDelay());
    rprobe_sub      = n.subscribe("rprobe", 1, &ROSinterface::rprobe_callback,
                                  this, ros::TransportHints().tcpNoDelay());
    prox_pub        = n.advertise<std_msgs::Int32MultiArray>("prox_array", 1);
    prox_on_pub     = n.advertise<std_msgs::Int32MultiArray>("prox_on_array", 1);
    prox_off_pub    = n.advertise<std_msgs::Int32MultiArray>("prox_off_array", 1);
    accel_pub       = n.advertise<std_msgs::Int32MultiArray>("accel_array", 1);
    steps_pub       = n.advertise<std_msgs::Int32MultiArray>("steps_array", 1);
    batt_pub        = n.advertise<std_msgs::Bool>("batt_low", 1);
    cmd_state_pub   = n.advertise<std_msgs::Int32>("cmd_state", 1);
    cam_pub         = n.advertise<std_msgs::Int16>("camera/rgbdetect", 1);
    randb_pub       = n.advertise<std_msgs::Float32MultiArray>("randb_array", 1);
    pose_pub        = n.advertise<std_msgs::Float32MultiArray>("pose", 1);
    adj_fitness_pub = n.advertise<std_msgs::Float32>("adj_fitness", 1);
    adj_swarm_pub   = n.advertise<std_msgs::Float32MultiArray>("adj_swarm", 1);

    resp_state      = 0;
    real_cam_blob   = 0;
    printf("Created ROS topics and interface class\n");
}

ROSinterface::~ROSinterface()
{
}

void ROSinterface::cmd_vel_callback(const geometry_msgs::Twist::ConstPtr& msg)
{

    // Convert to left and right motor speeds from linear x and angular z velocities
    // L    = distance between wheels
    // k    = speed constant
    // 
    // xdot     = (vl + vr) / 2
    // omega    = (vl - vr) / L
    //  =>
    // vl = xdot - L.omega/2
    // vr = xdot + L.omega/2
    //
    // http://www.e-puck.org/index.php?option=com_content&view=article&id=7&Itemid=9
    // Motor stepping logic has been recoded to use half steps for smoother motion
    // There are 20 steps = 40 half-steps per motor revolution
    // Each motor is geared down 50:0
    // = 2000 steps per wheel revolution
    // Wheel diameter is 41mm
    // Distance between wheels is 53mm
    // Speed to epuck is specified in steps/s
    // So:
    // k = constant from ms^-1 to steps/s
    // k = 2000/(41x10^-3*pi) = 15527.31
    //
    //
    // Recoded for new Epuck firmware
    // No half steps, motor velocity commands are signed 11.5 fixed point steps per second
    // Maximum velocity is -1024.0:+1023.96875
    // k = 1000 steps per wheel rev / (41e-3 * pi) * 32
    const float k = 248437;
    const float L = 53e-3;

    float vl = k * (msg->linear.x - L * msg->angular.z / 2);
    float vr = k * (msg->linear.x + L * msg->angular.z / 2);
    ept.lmotor = (int32_t)vl;
    if (ept.lmotor < -32768)
        ept.lmotor = -32768;
    if (ept.lmotor > 32767)
        ept.lmotor = 32767;
    ept.rmotor = (int32_t)vr;
    if (ept.rmotor < -32768)
        ept.rmotor = -32768;
    if (ept.rmotor > 32767)
        ept.rmotor = 32767;
    ROS_INFO("Motor speed: [%d %d]",  (int32_t)vl,  (int32_t)vr);

    // Timestamp when we got this message
    last_cmd = ros::Time::now();
}

void ROSinterface::leds_callback(const std_msgs::Int32MultiArray::ConstPtr& msg)
{
    for (int i=0; i<15; i++)
        ept.leds[i] = msg->data[i];

    // Set outgoing data to hub
    tlm.packet_to_hub.lock();
    {
        for (int i=0; i<15; i++)
            tlm.packet_to_hub.leds[i] = msg->data[i];
    }
    tlm.packet_to_hub.unlock();
}

void ROSinterface::wheel_vel_callback(const std_msgs::Float32MultiArray::ConstPtr& msg)
{
    // Timestamp when we got this message
    last_cmd = ros::Time::now();
    // Capture commanded velocities
    float wl = msg->data[0];
    float wr = msg->data[1];

    // Set the actual motor speeds
    const float k = 248437;
    ept.lmotor = (int32_t)(k * wl);
    if (ept.lmotor < -32768)
        ept.lmotor = -32768;
    if (ept.lmotor > 32767)
        ept.lmotor = 32767;
    ept.rmotor = (int32_t)(k * wr);
    if (ept.rmotor < -32768)
        ept.rmotor = -32768;
    if (ept.rmotor > 32767)
        ept.rmotor = 32767;

    // If in simulation, don't actually move motors
    tlm.packet_from_hub.lock();
    {
        if (tlm.packet_from_hub.fake)
        {
            ept.lmotor = 0;
            ept.rmotor = 0;
        }
    }
    tlm.packet_from_hub.unlock();

    // Set outgoing data to hub
    tlm.packet_to_hub.lock();
    {
        tlm.packet_to_hub.wheel_left = wl;
        tlm.packet_to_hub.wheel_right = wr;
    }
    tlm.packet_to_hub.unlock();
}

void ROSinterface::rgbd_callback(const std_msgs::Int16::ConstPtr& msg)
{
    // Intercept incoming camera blob detection messages from camera and 
    // substitute fake data if we are running in fake mode
    // ##FIXME## should we call this directly when faking to ensure messages
    // always sent out even if camera not working? For now, no, we want
    // to ensure the real camera is at least sending, this is another check on that..
    real_cam_blob = msg->data;
    std_msgs::Int16 cam_blob;

    tlm.packet_to_hub.blob = real_cam_blob;
    tlm.packet_from_hub.lock();
    {
        //if (tlm.packet_from_hub.fake)
        //    cam_blob.data = tlm.packet_from_hub.blob;
        //else
            cam_blob.data = real_cam_blob;
    }
    tlm.packet_from_hub.unlock();
    cam_pub.publish(cam_blob);
}


void ROSinterface::resp_state_callback(const std_msgs::Int32::ConstPtr& msg)
{
    // Set outgoing data to hub
    resp_state = msg->data;
    tlm.packet_to_hub.lock();
    {
        tlm.packet_to_hub.resp_state = resp_state;
    }
    tlm.packet_to_hub.unlock();
}


void ROSinterface::rprobe_callback(const std_msgs::Float32MultiArray::ConstPtr& msg)
{
    // Set outgoing data to hub
    tlm.packet_to_hub.lock();
    {
        tlm.packet_to_hub.rprobe        = msg->data[0];
        tlm.packet_to_hub.v_diff        = msg->data[1];
        tlm.packet_to_hub.v_light_raw   = msg->data[2];
        tlm.packet_to_hub.v_light_filt  = msg->data[3];
        tlm.packet_to_hub.v_light_swarm = msg->data[4];
        tlm.packet_to_hub.v_heavy_raw   = msg->data[5];
        tlm.packet_to_hub.v_heavy_filt  = msg->data[6];
        tlm.packet_to_hub.v_heavy_swarm = msg->data[7];
    }
    tlm.packet_to_hub.unlock();
}

void ROSinterface::run()
{

    // Publish at a rate of 200Hz. This is locked to the realtime SPI
    // thread using an atomic flag, but any long latencies on the 
    // non-realtime thread will cause sensor data to be dropped from publishing.
    // Ie the data published will always be the latest.
    bool got_dofile = false;
    bool left_reset = false;
    int loops = 0;
    ros::Rate r(200);
    while (ros::ok())
    {
        // Publish the sensor data
        // Substitution of camera data from hub is done in the real camera data callback
        std_msgs::Int32MultiArray   prox_array;
        std_msgs::Int32MultiArray   prox_on_array;
        std_msgs::Int32MultiArray   prox_off_array;
        prox_array.data.clear();
        std_msgs::Int32             cmd_state;
        std_msgs::Float32MultiArray randb_array;
        randb_array.data.clear();
        std_msgs::Float32MultiArray pose;
        pose.data.clear();
        std_msgs::Float32           adj_fitness;
        std_msgs::Float32MultiArray adj_swarm;

        // Lock access and publish the stuff from the hub
        tlm.packet_from_hub.lock();
        {
            if (tlm.packet_from_hub.fake)
            {
                for (int i = 0; i < 8; i++)
                    prox_array.data.push_back(tlm.packet_from_hub.prox[i]);
                // Ensure the fake blob detect data works even if there is no camera
                std_msgs::Int16 cam_blob;
                cam_blob.data = tlm.packet_from_hub.blob;
                cam_pub.publish(cam_blob);
            }
            else
            {
                for (int i = 0; i < 8; i++)
                {
                    prox_array.data.push_back(ept.prox_ir_off[i] - ept.prox_ir_on[i]);
                    prox_on_array.data.push_back(ept.prox_ir_on[i]);
                    prox_off_array.data.push_back(ept.prox_ir_off[i]);
                }
            }
            int n = tlm.packet_from_hub.n;
            randb_array.data.push_back(n);
            for(int i = 0; i < n; i++)
            {
                randb_array.data.push_back(tlm.packet_from_hub.range[i]);
                randb_array.data.push_back(tlm.packet_from_hub.bearing[i]);
            }
            cmd_state.data = tlm.packet_from_hub.cmd_state;
            pose.data.push_back(tlm.packet_from_hub.x);
            pose.data.push_back(tlm.packet_from_hub.y);
            pose.data.push_back(tlm.packet_from_hub.th);
            pose.data.push_back(tlm.packet_from_hub.vx);
            adj_fitness.data = tlm.packet_from_hub.adj_fitness;
            adj_swarm.data.push_back(tlm.packet_from_hub.adj_swarm[0]);
            adj_swarm.data.push_back(tlm.packet_from_hub.adj_swarm[1]);
        }
        // Unlock again
        tlm.packet_from_hub.unlock();

        // Publish the topics
        prox_pub.publish(prox_array);
        prox_on_pub.publish(prox_on_array);
        prox_off_pub.publish(prox_off_array);
        randb_pub.publish(randb_array);
        cmd_state_pub.publish(cmd_state);
        pose_pub.publish(pose);
        adj_fitness_pub.publish(adj_fitness);
        adj_swarm_pub.publish(adj_swarm);

        std_msgs::Int32MultiArray accel_array;
        accel_array.data.clear();
        for (int i = 0; i < 3; i++)
		{
			accel_array.data.push_back(ept.accel[i]);
		}
        accel_pub.publish(accel_array);

        std_msgs::Int32MultiArray steps_array;
        steps_array.data.clear();
        for (int i = 0; i < 2; i++)
		{
			steps_array.data.push_back(ept.steps[i]);
		}
        steps_pub.publish(steps_array);

        std_msgs::Bool batt_low;
        batt_low.data = (bool)ept.batt_low;
        batt_pub.publish(batt_low);

        // Stop the motors if more than 0.2s since last command
        ros::Time now = ros::Time::now();
        if (now - last_cmd > ros::Duration(0.2))
        {
            ept.lmotor = 0;
            ept.rmotor = 0;
            tlm.packet_to_hub.lock();
            {
                tlm.packet_to_hub.wheel_left = 0;
                tlm.packet_to_hub.wheel_right = 0;
            }
            tlm.packet_to_hub.unlock();
        }

        // State 0 is the reset state, if we see this state after already having
        // seen a different state, stop all running ros nodes.
        // Because this node is launched with respawn="true", it will
        // automatically restart
        if (cmd_state.data == 0)
        {
            if (left_reset)
            {
                printf("Shutting down all ros nodes..\n");
                // kill any simulations running
                system("killall xps");
                int res = system("rosnode kill --all");
                if (res)
                {
                    printf("Failed to execute rosnode kill\n");
                    exit(1);
                }
                got_dofile = false;
            }
        }
        else
        {
            left_reset = true;
        }
        // Get the dofile if it hasn't already been fetched, then run it.
        // This will run an experiment. It is a python executable file.
        // The stuff started by the dofile is responsible for moving the resp_state
        if (!got_dofile && (cmd_state.data == 100))
        {
            printf("Fetching dofile..\n");
            std::string s = string_format("scp -p %s:/tmp/dofile /tmp/dofile", hubstring);
            int res = system(s.c_str());
            if (res)
            {
                printf("Failed to get dofile\n");
                exit(1);
            }
            printf("Running dofile..\n");
            res = system("python /tmp/dofile > /tmp/do.log");
            if (res)
            {
                printf("Failed to run dofile\n");
                exit(1);
            }
            got_dofile = true;
            printf("Completed dofile..\n");
        }


        // Handle any callbacks waiting
        ros::spinOnce();


        // Signal all ROS-side processing done
        ept.flag = 0;

        // Spin until next SPI transfer
        while (!ept.flag);

        //r.sleep();
        loops++;
    }
    printf("Shutting down xpuck_node\n");
    for (int i=0; i<15; i++)
    {
        ept.leds[i] = 0;
    }
    ept.kill = 1;
    ept.lmotor = 0;
    ept.rmotor = 0;
    // wait for a while to give time for the SPI transfer to occur
    sleep(1);
}



Telemetry::Telemetry(Epuckcom &_ept, bool _verbose) : ept(_ept), verbose(_verbose)
{
    // Create the telemetry thread
    int ret = pthread_create(&telem_thread, NULL, &Telemetry::thread_helper, this);
    if (ret)
    {
        perror("Failure spawning telemetry thread:");
        exit(3);
    }
    if (verbose)
        printf("Spawned telemetry thread\n");
}


void *Telemetry::telemetry()
{
    // This function is called on starting the telemetry thread
    // Get the update period for the INA231, use this for all updates
    FILE *fp = fopen("/sys/bus/i2c/drivers/INA231/3-0040/update_period","r");
    update_period = 250000;
    if (fp)
    {
        int i = fscanf(fp, "%d", &update_period);
        if (i != 1)
            update_period = 250000;
        fclose(fp);
    }
    printf("Update period %d\n", update_period);
    //next_sample = microseconds() + update_period;
    next_sample = microseconds();

    // Open all the sysfs files and maintain handles. Use system calls, rather
    // than buffered calls because otherwise the overhead is considerable.
    ffreq0  = open("/sys/devices/system/cpu/cpu0/cpufreq/scaling_cur_freq", O_RDONLY | O_CLOEXEC);
    ffreq4  = open("/sys/devices/system/cpu/cpu4/cpufreq/scaling_cur_freq", O_RDONLY | O_CLOEXEC);
    fvolt0  = open("/sys/bus/i2c/drivers/INA231/3-0040/sensor_V", O_RDONLY | O_CLOEXEC);
    famp0   = open("/sys/bus/i2c/drivers/INA231/3-0040/sensor_A", O_RDONLY | O_CLOEXEC);
    fwatt0  = open("/sys/bus/i2c/drivers/INA231/3-0040/sensor_W", O_RDONLY | O_CLOEXEC);
    fvolt1  = open("/sys/bus/i2c/drivers/INA231/3-0041/sensor_V", O_RDONLY | O_CLOEXEC);
    famp1   = open("/sys/bus/i2c/drivers/INA231/3-0041/sensor_A", O_RDONLY | O_CLOEXEC);
    fwatt1  = open("/sys/bus/i2c/drivers/INA231/3-0041/sensor_W", O_RDONLY | O_CLOEXEC);
    ftemp   = open("/sys/devices/10060000.tmu/temp", O_RDONLY | O_CLOEXEC);
    fdbm    = open("/proc/net/wireless", O_RDONLY | O_CLOEXEC);

    // epuck battery low
    epb     = 0;

    bool settime = false;

    // get initial values
    update();


    // Set up sockets
    zmq::context_t  context(1);

    // Subscriber to pose and timestamp messages from hub. Set conflate
    // on socket so that only the most recent message is available,
    // and subscribe to the messages that contain our pose
    zmq::socket_t   sub_pose(context, ZMQ_SUB);

    const int confl = true;
    std::string s = string_format("tcp://%s:%s", hubstring, ZMQPORT_POSE);
    sub_pose.setsockopt(ZMQ_CONFLATE, &confl, sizeof(int));
    sub_pose.connect(s.c_str());
    // find out who we are to filter messages
    char hostname[BUFSIZE];
    gethostname(hostname, BUFSIZE);
    sub_pose.setsockopt(ZMQ_SUBSCRIBE, hostname, strlen(hostname));

    // Publish messages back to the hub, these contain telemetry
    zmq::socket_t   pub_msg_out(context, ZMQ_PUB);
    s = string_format("tcp://%s:%s", hubstring, ZMQPORT_XPUCK2HUB);
    pub_msg_out.connect(s.c_str());

    zmq::message_t hub_msg;
    char buf[BUFSIZE];

    int32_t last_cmd_state = 0;
    while(1)
    {
        bool empty = true;
        std::string message = "";
        while (empty)
        {
            sub_pose.recv(&hub_msg);
            if (hub_msg.size())
            {
                message = std::string(static_cast<char*>(hub_msg.data()), hub_msg.size());
                empty = false;
            }
        }
        if (message.size())
        {
            // We have received a message from the hub,
            // reply with the telemetry message
            if (verbose)
                printf("Received:%s\n", message.c_str());

            packet_from_hub.fill(message);
            int len;

            // Construct string message
            uint64_t timestamp = packet_from_hub.get_tstamp();
            if (!settime)
            {
                // The xu4 has no battery backed clock and no access to wider network, so
                // set the time using the timestamp from the hub
                std::string cmd = string_format("sudo date -s '@%" PRIu64 "'", timestamp / 1000000);
                printf("setting date with command:%s %" PRIu64 "\n", cmd.c_str(), timestamp);
                system(cmd.c_str());
                settime = true;
            }
            std::string data = values(hostname, packet_to_hub, timestamp);
            if (verbose)
                printf("Sent:%s\n", data.c_str());

            len = data.size();
            zmq::message_t reply(len);
            memcpy((char*)reply.data(), &data[0], len);
            pub_msg_out.send(reply);


            update();
        }
    }


}

void Telemetry::update()
{
    // Check to see if it is time to sample the sensors
    uint64_t t = microseconds();
    if (t > next_sample)
    {
        next_sample += update_period;
        // These files are just a single numeric value
        freq0   = getval(ffreq0);
        freq4   = getval(ffreq4);
        v0      = getval(fvolt0);
        a0      = getval(famp0);
        w0      = getval(fwatt0);
        v1      = getval(fvolt1);
        a1      = getval(famp1);
        w1      = getval(fwatt1);
        // These need some parsing
        temp    = getval(ftemp, 1);
        dbm     = getval(fdbm, 2);
        //printf("%llu %f %f %f %f %f %f %f %f %f %f\n", microseconds()-t, temp, dbm, freq0, freq4, v0, a0, w0, v1, a1, w1);
    }
}

float Telemetry::getval(int f, int type)
{
    char buf[bufsize];
    lseek(f, 0, SEEK_SET);
    int bytes = read(f, buf, bufsize - 1);
    if (bytes < 0)
    {
        //printf("Failed to read from %d", f);
        return -1;
    }
    buf[bytes] = 0;
    float val = -1;
    char dummy[bufsize];
    if (type == 0)
        sscanf(buf, "%f", &val);
    else if (type == 1)
    {
        // Each temperature sensor is on a separate line, in form:
        // <name> : <value>
        // The value is in thousandths of a degree, but with a precision of degrees.
        // The sensor on the third line always reads the highest value, we return this.
        sscanf(buf, "%s : %s %s : %s %s : %f", dummy, dummy, dummy, dummy, dummy, &val);
        val /= 1000;
    }
    else if (type == 2)
    {
        // Output is in the form:
        //Inter-| sta-|   Quality        |   Discarded packets               | Missed | WE
        // face | tus | link level noise |  nwid  crypt   frag  retry   misc | beacon | 22
        // wlan0: 0000   45.  -65.  -256        0      0      0     36     22        0
        char *p = buf;
        // Skip first two lines then get fourth word as value
        for (int line = 0; (p < buf + bufsize) && (line < 2); line += *p++ == '\n');
        sscanf(p, "%s %s %s %f", dummy, dummy, dummy, &val);
        return val;
    }
    return val;
}
