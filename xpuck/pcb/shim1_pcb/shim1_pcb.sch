EESchema Schematic File Version 4
LIBS:shim1-cache
EELAYER 29 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 1 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 1100 1000 1400 1100
U 5634FE72
F0 "Connectors and shifters" 50
F1 "connectors_and_shifters.sch" 50
$EndSheet
$Sheet
S 1100 3000 1400 1100
U 5641E6F9
F0 "USB and FPGA configuration" 60
F1 "usb_fpga_config.sch" 60
$EndSheet
$Sheet
S 4100 1000 1400 1100
U 5641EAA8
F0 "FPGA and interfaces" 60
F1 "fpga_interfaces.sch" 60
$EndSheet
$Sheet
S 4100 3000 1400 1100
U 5641ED43
F0 "Power supply" 60
F1 "power_supply.sch" 60
$EndSheet
$Comp
L shim1:MOUNT M1
U 1 1 566B0F95
P 6700 1750
F 0 "M1" H 6700 1850 60  0000 C CNN
F 1 "MOUNT" H 6700 1650 60  0000 C CNN
F 2 "custom:M2.5" H 6700 1750 60  0000 C CNN
F 3 "" H 6700 1750 60  0000 C CNN
	1    6700 1750
	1    0    0    -1  
$EndComp
$Comp
L shim1:MOUNT M2
U 1 1 566B0FBC
P 6700 1300
F 0 "M2" H 6700 1400 60  0000 C CNN
F 1 "MOUNT" H 6700 1200 60  0000 C CNN
F 2 "custom:M2.5" H 6700 1300 60  0000 C CNN
F 3 "" H 6700 1300 60  0000 C CNN
	1    6700 1300
	1    0    0    -1  
$EndComp
$Comp
L shim1:MOUNT M3
U 1 1 566B0FF1
P 6700 2200
F 0 "M3" H 6700 2300 60  0000 C CNN
F 1 "MOUNT" H 6700 2100 60  0000 C CNN
F 2 "custom:M2.5" H 6700 2200 60  0000 C CNN
F 3 "" H 6700 2200 60  0000 C CNN
	1    6700 2200
	1    0    0    -1  
$EndComp
NoConn ~ 6700 1300
NoConn ~ 6700 1750
NoConn ~ 6700 2200
$EndSCHEMATC
