EESchema Schematic File Version 4
LIBS:shim1-cache
EELAYER 29 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 2 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L shim1:CONN_20X2 P1
U 1 1 56350BBD
P 3000 2150
F 0 "P1" H 3000 3200 60  0000 C CNN
F 1 "CONN_20X2" V 3000 2150 50  0000 C CNN
F 2 "custom:BTE-020-03" H 3000 2150 60  0001 C CNN
F 3 "" H 3000 2150 60  0000 C CNN
F 4 "Samtec" H 3000 2150 60  0001 C CNN "Manuf"
F 5 "BTE-020-03" H 3000 2150 60  0001 C CNN "MPN"
F 6 "BTE-020-02 for 8mm board sep, BTE-020-03 for 11mm board sep" H 3000 2150 60  0001 C CNN "Notes"
	1    3000 2150
	1    0    0    -1  
$EndComp
Text GLabel 2100 1400 0    50   Input ~ 0
epuck_vccbatt
Text GLabel 2100 1600 0    50   Input ~ 0
epuck_gndbatt
Text GLabel 2100 2000 0    50   Input ~ 0
epuck_vcc3v3
Text GLabel 2100 1900 0    50   Input ~ 0
epuck_gnd3v3
Text GLabel 2100 2100 0    50   Input ~ 0
epuck_cam_clk
Text GLabel 2100 1800 0    50   Input ~ 0
epuck_scl
Text GLabel 2100 1700 0    50   Input ~ 0
epuck_sda
Text GLabel 3900 2100 2    50   Input ~ 0
epuck_y0
Text GLabel 3900 2200 2    50   Input ~ 0
epuck_y1
Text GLabel 3900 2300 2    50   Input ~ 0
epuck_y2
Text GLabel 3900 2400 2    50   Input ~ 0
epuck_y3
Text GLabel 3900 2500 2    50   Input ~ 0
epuck_y4
Text GLabel 3900 2600 2    50   Input ~ 0
epuck_y5
Text GLabel 3900 2700 2    50   Input ~ 0
epuck_y6
Text GLabel 3900 2800 2    50   Input ~ 0
epuck_y7
Text GLabel 2100 2400 0    50   Input ~ 0
epuck_href
Text GLabel 2100 2300 0    50   Input ~ 0
epuck_vsync
Text GLabel 2100 2200 0    50   Input ~ 0
epuck_cam_reset
Text GLabel 2100 2800 0    50   Input ~ 0
epuck_enable_ldo
Text GLabel 2100 2600 0    50   Input ~ 0
epuck_pclk
Text GLabel 3900 1400 2    50   Input ~ 0
epuck_vccbatt
Text GLabel 3900 1600 2    50   Input ~ 0
epuck_gndbatt
Text GLabel 3900 2000 2    50   Input ~ 0
epuck_vcc3v3
Text GLabel 3900 1900 2    50   Input ~ 0
epuck_gnd3v3
$Comp
L shim1:CONN_20X2 P3
U 1 1 56350C14
P 7800 2150
F 0 "P3" H 7800 3200 60  0000 C CNN
F 1 "CONN_20X2" V 7800 2150 50  0000 C CNN
F 2 "custom:BTE-020-03" H 7800 2150 60  0001 C CNN
F 3 "" H 7800 2150 60  0000 C CNN
F 4 "Samtec" H 7800 2150 60  0001 C CNN "Manuf"
F 5 "BTE-020-03" H 7800 2150 60  0001 C CNN "MPN"
	1    7800 2150
	1    0    0    -1  
$EndComp
Text GLabel 6900 1400 0    50   Input ~ 0
epuck_sel0_spiclk
Text GLabel 6900 1500 0    50   Input ~ 0
epuck_sel1_spimosi
Text GLabel 8700 1400 2    50   Input ~ 0
epuck_sel2_spimiso
Text GLabel 8700 1500 2    50   Input ~ 0
epuck_sel3_spicsn
Text GLabel 6900 3100 0    50   Input ~ 0
epuck_vcc3v3
Text GLabel 8700 3100 2    50   Input ~ 0
epuck_gnd3v3
Text Notes 6750 950  0    50   ~ 0
'X' signals connect to peripherals, non-'X' connect to\nthe PIC. Pairs should be shorted unless the shim board\nwants to control that peripheral directly
Text Notes 1850 900  0    50   ~ 0
Batt and vccb are  pre- and post-power switch connection \nto the LiIon cell positive.\ngndb is post undervoltage protection connection to LiIon negative
Wire Wire Line
	2100 1200 2600 1200
Wire Wire Line
	2100 1300 2600 1300
Wire Wire Line
	2100 1400 2600 1400
Wire Wire Line
	2100 1500 2600 1500
Wire Wire Line
	2100 1600 2600 1600
Wire Wire Line
	2100 1700 2600 1700
Wire Wire Line
	2100 1800 2600 1800
Wire Wire Line
	2100 1900 2600 1900
Wire Wire Line
	2100 2000 2600 2000
Wire Wire Line
	2100 2100 2600 2100
Wire Wire Line
	2100 2200 2600 2200
Wire Wire Line
	2100 2300 2600 2300
Wire Wire Line
	2100 2400 2600 2400
Wire Wire Line
	2100 2500 2600 2500
Wire Wire Line
	2100 2600 2600 2600
Wire Wire Line
	2100 2700 2600 2700
Wire Wire Line
	2100 2800 2600 2800
Wire Wire Line
	2100 2900 2600 2900
Wire Wire Line
	2100 3000 2600 3000
Wire Wire Line
	2100 3100 2600 3100
Wire Wire Line
	3400 1200 3900 1200
Wire Wire Line
	3400 1300 3900 1300
Wire Wire Line
	3400 1400 3900 1400
Wire Wire Line
	3400 1500 3900 1500
Wire Wire Line
	3400 1600 3900 1600
Wire Wire Line
	3400 1700 3900 1700
Wire Wire Line
	3400 1800 3900 1800
Wire Wire Line
	3400 1900 3900 1900
Wire Wire Line
	3400 2000 3900 2000
Wire Wire Line
	3400 2100 3900 2100
Wire Wire Line
	3400 2200 3900 2200
Wire Wire Line
	3400 2300 3900 2300
Wire Wire Line
	3400 2400 3900 2400
Wire Wire Line
	3400 2500 3900 2500
Wire Wire Line
	3400 2600 3900 2600
Wire Wire Line
	3400 2700 3900 2700
Wire Wire Line
	3400 2800 3900 2800
Wire Wire Line
	3400 2900 3900 2900
Wire Wire Line
	3400 3000 3900 3000
Wire Wire Line
	3400 3100 3900 3100
Wire Wire Line
	6900 1200 7350 1200
Wire Wire Line
	6900 1300 7350 1300
Wire Wire Line
	6900 1400 7400 1400
Wire Wire Line
	6900 1500 7400 1500
Wire Wire Line
	6900 1600 7350 1600
Wire Wire Line
	6900 1700 7350 1700
Wire Wire Line
	6900 1800 7350 1800
Wire Wire Line
	6900 1900 7350 1900
Wire Wire Line
	6900 2000 7350 2000
Wire Wire Line
	6900 2100 7350 2100
Wire Wire Line
	6900 2200 7350 2200
Wire Wire Line
	6900 2300 7350 2300
Wire Wire Line
	6900 2400 7350 2400
Wire Wire Line
	6900 2500 7350 2500
Wire Wire Line
	6900 2600 7350 2600
Wire Wire Line
	6900 2700 7350 2700
Wire Wire Line
	6900 2800 7350 2800
Wire Wire Line
	6900 2900 7350 2900
Wire Wire Line
	6900 3000 7350 3000
Wire Wire Line
	6900 3100 7400 3100
Wire Wire Line
	8200 1200 8250 1200
Wire Wire Line
	8200 1300 8250 1300
Wire Wire Line
	8200 1400 8700 1400
Wire Wire Line
	8200 1500 8700 1500
Wire Wire Line
	8200 1600 8250 1600
Wire Wire Line
	8200 1700 8250 1700
Wire Wire Line
	8200 1800 8250 1800
Wire Wire Line
	8200 1900 8250 1900
Wire Wire Line
	8200 2000 8250 2000
Wire Wire Line
	8200 2100 8250 2100
Wire Wire Line
	8200 2200 8250 2200
Wire Wire Line
	8200 2300 8250 2300
Wire Wire Line
	8200 2400 8250 2400
Wire Wire Line
	8200 2500 8250 2500
Wire Wire Line
	8200 2600 8250 2600
Wire Wire Line
	8200 2700 8250 2700
Wire Wire Line
	8200 2800 8250 2800
Wire Wire Line
	8200 2900 8250 2900
Wire Wire Line
	8200 3000 8250 3000
Wire Wire Line
	8200 3100 8700 3100
Wire Wire Line
	7350 1200 7350 1150
Wire Wire Line
	7350 1150 8250 1150
Wire Wire Line
	8250 1150 8250 1200
Connection ~ 8250 1200
Connection ~ 7350 1200
Wire Wire Line
	7350 1300 7350 1250
Wire Wire Line
	7350 1250 8250 1250
Wire Wire Line
	8250 1250 8250 1300
Connection ~ 8250 1300
Connection ~ 7350 1300
Wire Wire Line
	7350 2200 7350 2150
Wire Wire Line
	7350 2150 8250 2150
Wire Wire Line
	8250 2150 8250 2200
Connection ~ 8250 2200
Connection ~ 7350 2200
Wire Wire Line
	7350 2700 7350 2650
Wire Wire Line
	7350 2650 8250 2650
Wire Wire Line
	8250 2650 8250 2700
Connection ~ 8250 2700
Connection ~ 7350 2700
Wire Wire Line
	7350 1800 7350 1750
Wire Wire Line
	7350 1750 8250 1750
Wire Wire Line
	8250 1750 8250 1800
Connection ~ 8250 1800
Connection ~ 7350 1800
Wire Wire Line
	7350 1900 7350 1850
Wire Wire Line
	7350 1850 8250 1850
Wire Wire Line
	8250 1850 8250 1900
Connection ~ 8250 1900
Connection ~ 7350 1900
Wire Wire Line
	7350 2000 7350 1950
Wire Wire Line
	7350 1950 8250 1950
Wire Wire Line
	8250 1950 8250 2000
Connection ~ 8250 2000
Connection ~ 7350 2000
Wire Wire Line
	7350 2100 7350 2050
Wire Wire Line
	7350 2050 8250 2050
Wire Wire Line
	8250 2050 8250 2100
Connection ~ 8250 2100
Connection ~ 7350 2100
Wire Wire Line
	7350 1600 7350 1550
Wire Wire Line
	7350 1550 8250 1550
Wire Wire Line
	8250 1550 8250 1600
Connection ~ 8250 1600
Connection ~ 7350 1600
Wire Wire Line
	7350 1700 7350 1650
Wire Wire Line
	7350 1650 8250 1650
Wire Wire Line
	8250 1650 8250 1700
Connection ~ 8250 1700
Connection ~ 7350 1700
Wire Wire Line
	7350 2300 7350 2250
Wire Wire Line
	7350 2250 8250 2250
Wire Wire Line
	8250 2250 8250 2300
Connection ~ 8250 2300
Connection ~ 7350 2300
Wire Wire Line
	7350 2400 7350 2350
Wire Wire Line
	7350 2350 8250 2350
Wire Wire Line
	8250 2350 8250 2400
Connection ~ 8250 2400
Connection ~ 7350 2400
Wire Wire Line
	7350 2500 7350 2450
Wire Wire Line
	7350 2450 8250 2450
Wire Wire Line
	8250 2450 8250 2500
Connection ~ 8250 2500
Connection ~ 7350 2500
Wire Wire Line
	7350 2600 7350 2550
Wire Wire Line
	7350 2550 8250 2550
Wire Wire Line
	8250 2550 8250 2600
Connection ~ 8250 2600
Connection ~ 7350 2600
Wire Wire Line
	7350 2800 7350 2750
Wire Wire Line
	7350 2750 8250 2750
Wire Wire Line
	8250 2750 8250 2800
Connection ~ 8250 2800
Connection ~ 7350 2800
Wire Wire Line
	7350 2900 7350 2850
Wire Wire Line
	7350 2850 8250 2850
Wire Wire Line
	8250 2850 8250 2900
Connection ~ 8250 2900
Connection ~ 7350 2900
Wire Wire Line
	7350 3000 7350 2950
Wire Wire Line
	7350 2950 8250 2950
Wire Wire Line
	8250 2950 8250 3000
Connection ~ 8250 3000
Connection ~ 7350 3000
Text Notes 7000 3450 0    60   ~ 0
!!!TODO!!! Maybe break out mclk here so we\ncan drive it from the FPGA
Text GLabel 2100 9000 0    50   Input ~ 0
xu4_uart_0.ctsn
Text GLabel 2100 9100 0    50   Input ~ 0
xu4_spi_1.mosi
Text GLabel 2100 9200 0    50   Input ~ 0
xu4_spi_1.miso
Text GLabel 2100 9300 0    50   Input ~ 0
xu4_spi_1.csn
Text GLabel 2100 9400 0    50   Input ~ 0
xu4_xe.int13
Text GLabel 2100 9500 0    50   Input ~ 0
xu4_xe.int10
Text GLabel 2100 9600 0    50   Input ~ 0
xu4_xe.int14
Text GLabel 2100 9700 0    50   Input ~ 0
xu4_xe.int22
Text GLabel 2100 9800 0    50   Input ~ 0
xu4_xe.int21
Text GLabel 2100 10000 0    50   Input ~ 0
xu4_xe.int15
Text GLabel 2100 10100 0    50   Input ~ 0
xu4_xe.int25
Text GLabel 2100 10200 0    50   Input ~ 0
xu4_vdd_io
Text GLabel 3900 8900 2    50   Input ~ 0
xu4_uart_0.rtsn
Text GLabel 3900 9000 2    50   Input ~ 0
xu4_uart_0.rxd
Text GLabel 3900 9100 2    50   Input ~ 0
xu4_uart_0.txd
Text GLabel 3900 9200 2    50   Input ~ 0
xu4_spi_1.clk
Text GLabel 3900 9400 2    50   Input ~ 0
xu4_i2c_1.scl
Text GLabel 3900 9500 2    50   Input ~ 0
xu4_i2c_1.sda
Text GLabel 3900 9600 2    50   Input ~ 0
xu4_xe.int11
Text GLabel 3900 9700 2    50   Input ~ 0
xu4_xe.int20
Text GLabel 3900 9800 2    50   Input ~ 0
xu4_xe.int23
Text GLabel 3900 9900 2    50   Input ~ 0
xu4_xe.int17
Text GLabel 3900 10000 2    50   Input ~ 0
xu4_xe.int16
Text GLabel 3900 8800 2    50   Input ~ 0
xu4_gnd
Text GLabel 3900 10100 2    50   Input ~ 0
xu4_gnd
Text GLabel 3900 10200 2    50   Input ~ 0
xu4_gnd
Wire Wire Line
	2100 8800 2600 8800
Wire Wire Line
	2100 8900 2600 8900
Wire Wire Line
	2100 9000 2600 9000
Wire Wire Line
	2100 9100 2600 9100
Wire Wire Line
	2100 9200 2600 9200
Wire Wire Line
	2100 9300 2600 9300
Wire Wire Line
	2100 9400 2600 9400
Wire Wire Line
	2100 9500 2600 9500
Wire Wire Line
	2100 9600 2600 9600
Wire Wire Line
	2100 9700 2600 9700
Wire Wire Line
	2100 9800 2600 9800
Wire Wire Line
	2100 9900 2600 9900
Wire Wire Line
	2100 10000 2600 10000
Wire Wire Line
	2100 10100 2600 10100
Wire Wire Line
	2100 10200 2600 10200
Wire Wire Line
	3900 8800 3400 8800
Wire Wire Line
	3900 8900 3400 8900
Wire Wire Line
	3900 9000 3400 9000
Wire Wire Line
	3900 9100 3400 9100
Wire Wire Line
	3900 9200 3400 9200
Wire Wire Line
	3900 9300 3400 9300
Wire Wire Line
	3900 9400 3400 9400
Wire Wire Line
	3900 9500 3400 9500
Wire Wire Line
	3900 9600 3400 9600
Wire Wire Line
	3900 9700 3400 9700
Wire Wire Line
	3900 9800 3400 9800
Wire Wire Line
	3900 9900 3400 9900
Wire Wire Line
	3900 10000 3400 10000
Wire Wire Line
	3900 10100 3400 10100
Wire Wire Line
	3900 10200 3400 10200
Text GLabel 2600 5850 0    50   Input ~ 0
xu4_spi_1.clk
Text GLabel 2600 5950 0    50   Input ~ 0
xu4_spi_1.mosi
Text GLabel 2600 6050 0    50   Output ~ 0
xu4_spi_1.miso
Text GLabel 2600 6150 0    50   Input ~ 0
xu4_spi_1.csn
Wire Wire Line
	2600 5850 4000 5850
Wire Wire Line
	2600 5950 4000 5950
Wire Wire Line
	2600 6150 4000 6150
Text GLabel 8100 5850 2    50   Output ~ 0
epuck_sel0_spiclk
Text GLabel 8100 5950 2    50   Output ~ 0
epuck_sel1_spimosi
Text GLabel 8100 6050 2    50   Input ~ 0
epuck_sel2_spimiso
Text GLabel 8100 6150 2    50   Output ~ 0
epuck_sel3_spicsn
Wire Wire Line
	5000 5850 7100 5850
Wire Wire Line
	5000 5950 7100 5950
Wire Wire Line
	5000 6050 7600 6050
Wire Wire Line
	5000 6150 7100 6150
$Comp
L shim1:R_Small R8
U 1 1 564B2583
P 3800 6450
F 0 "R8" H 3830 6470 50  0000 L CNN
F 1 "10k" H 3830 6410 50  0000 L CNN
F 2 "IPC7351-Nominal:RESC1608X50" H 3800 6450 60  0001 C CNN
F 3 "" H 3800 6450 60  0000 C CNN
F 4 "Res 0603 1%" H 3800 6450 60  0001 C CNN "MPN"
	1    3800 6450
	1    0    0    -1  
$EndComp
Wire Wire Line
	3800 6250 4000 6250
Wire Wire Line
	3800 4950 3800 5350
$Comp
L shim1:GND #PWR01
U 1 1 564B258C
P 3800 6750
F 0 "#PWR01" H 3800 6500 50  0001 C CNN
F 1 "GND" H 3800 6600 50  0000 C CNN
F 2 "" H 3800 6750 60  0000 C CNN
F 3 "" H 3800 6750 60  0000 C CNN
	1    3800 6750
	1    0    0    -1  
$EndComp
Wire Wire Line
	3800 6550 3800 6750
$Comp
L shim1:GND #PWR02
U 1 1 564B2593
P 5200 6750
F 0 "#PWR02" H 5200 6500 50  0001 C CNN
F 1 "GND" H 5200 6600 50  0000 C CNN
F 2 "" H 5200 6750 60  0000 C CNN
F 3 "" H 5200 6750 60  0000 C CNN
	1    5200 6750
	1    0    0    -1  
$EndComp
Wire Wire Line
	5000 6250 5200 6250
Wire Wire Line
	5200 6250 5200 6750
$Comp
L shim1:xu4_1v8 #PWR03
U 1 1 564B259B
P 3800 4950
F 0 "#PWR03" H 3800 4800 50  0001 C CNN
F 1 "xu4_1v8" H 3800 5090 50  0000 C CNN
F 2 "" H 3800 4950 60  0000 C CNN
F 3 "" H 3800 4950 60  0000 C CNN
	1    3800 4950
	1    0    0    -1  
$EndComp
Wire Wire Line
	3800 5350 4000 5350
$Comp
L shim1:epuck_3v3 #PWR04
U 1 1 564B25A2
P 5200 4950
F 0 "#PWR04" H 5200 4800 50  0001 C CNN
F 1 "epuck_3v3" H 5200 5090 50  0000 C CNN
F 2 "" H 5200 4950 60  0000 C CNN
F 3 "" H 5200 4950 60  0000 C CNN
	1    5200 4950
	1    0    0    -1  
$EndComp
Wire Wire Line
	5000 5350 5200 5350
Wire Wire Line
	5200 5350 5200 4950
$Comp
L shim1:C_Small C4
U 1 1 564B25AA
P 5600 5100
F 0 "C4" H 5610 5170 50  0000 L CNN
F 1 "100n" H 5610 5020 50  0000 L CNN
F 2 "IPC7351-Nominal:CAPC1608X90" H 5600 5100 60  0001 C CNN
F 3 "" H 5600 5100 60  0000 C CNN
F 4 "Cap 0603 X7R 10%" H 5600 5100 60  0001 C CNN "MPN"
	1    5600 5100
	1    0    0    -1  
$EndComp
$Comp
L shim1:epuck_3v3 #PWR05
U 1 1 564B25B1
P 5600 4950
F 0 "#PWR05" H 5600 4800 50  0001 C CNN
F 1 "epuck_3v3" H 5600 5090 50  0000 C CNN
F 2 "" H 5600 4950 60  0000 C CNN
F 3 "" H 5600 4950 60  0000 C CNN
	1    5600 4950
	1    0    0    -1  
$EndComp
$Comp
L shim1:GND #PWR06
U 1 1 564B25B7
P 5600 5250
F 0 "#PWR06" H 5600 5000 50  0001 C CNN
F 1 "GND" H 5600 5100 50  0000 C CNN
F 2 "" H 5600 5250 60  0000 C CNN
F 3 "" H 5600 5250 60  0000 C CNN
	1    5600 5250
	1    0    0    -1  
$EndComp
Wire Wire Line
	5600 4950 5600 5000
Wire Wire Line
	5600 5200 5600 5250
$Comp
L shim1:C_Small C3
U 1 1 564B25BF
P 3400 5100
F 0 "C3" H 3410 5170 50  0000 L CNN
F 1 "100n" H 3410 5020 50  0000 L CNN
F 2 "IPC7351-Nominal:CAPC1608X90" H 3400 5100 60  0001 C CNN
F 3 "" H 3400 5100 60  0000 C CNN
F 4 "Cap 0603 X7R 10%" H 3400 5100 60  0001 C CNN "MPN"
	1    3400 5100
	1    0    0    -1  
$EndComp
$Comp
L shim1:GND #PWR07
U 1 1 564B25C6
P 3400 5250
F 0 "#PWR07" H 3400 5000 50  0001 C CNN
F 1 "GND" H 3400 5100 50  0000 C CNN
F 2 "" H 3400 5250 60  0000 C CNN
F 3 "" H 3400 5250 60  0000 C CNN
	1    3400 5250
	1    0    0    -1  
$EndComp
Wire Wire Line
	3400 4950 3400 5000
Wire Wire Line
	3400 5200 3400 5250
$Comp
L shim1:xu4_1v8 #PWR08
U 1 1 564B25CE
P 3400 4950
F 0 "#PWR08" H 3400 4800 50  0001 C CNN
F 1 "xu4_1v8" H 3400 5090 50  0000 C CNN
F 2 "" H 3400 4950 60  0000 C CNN
F 3 "" H 3400 4950 60  0000 C CNN
	1    3400 4950
	1    0    0    -1  
$EndComp
Connection ~ 3800 5350
Connection ~ 3800 6250
Wire Wire Line
	3000 5450 4000 5450
Wire Wire Line
	3000 5550 4000 5550
Wire Wire Line
	5000 5450 6000 5450
Wire Wire Line
	5000 5550 6000 5550
Wire Wire Line
	5000 5650 6000 5650
Wire Wire Line
	5000 5750 6000 5750
Text GLabel 3000 5450 0    50   Input ~ 0
xu4_i2c_1.scl
Text GLabel 3000 5550 0    50   Input ~ 0
xu4_i2c_1.sda
Text GLabel 6000 5450 2    50   Input ~ 0
epuck_scl
Text GLabel 6000 5550 2    50   Input ~ 0
epuck_sda
Text Notes 3700 7300 0    60   ~ 0
TXS0108E suitable for open-drain\nand push pull
$Comp
L shim1:CONN_15X2 P2
U 1 1 564B2B85
P 3000 9750
F 0 "P2" H 3000 10800 60  0000 C CNN
F 1 "CONN_15X2" V 3000 9750 50  0000 C CNN
F 2 "conn-2mm:CONN-2MM-M-2x15" H 3000 9750 60  0001 C CNN
F 3 "" H 3000 9750 60  0000 C CNN
F 4 "Harwin" H 3000 9750 60  0001 C CNN "Manuf"
F 5 "M22-2021505" H 3000 9750 60  0001 C CNN "MPN"
F 6 "2mm pitch DIL vertical pin header" H 3000 9750 60  0001 C CNN "Notes"
	1    3000 9750
	1    0    0    -1  
$EndComp
$Comp
L shim1:R_Small R1
U 1 1 564B3C3A
P 3100 6050
F 0 "R1" V 3150 6150 50  0000 L CNN
F 1 "0R" V 3100 6000 50  0000 L CNN
F 2 "IPC7351-Nominal:RESC1608X50" H 3100 6050 60  0001 C CNN
F 3 "" H 3100 6050 60  0000 C CNN
F 4 "Res 0603 1%" H 3100 6050 60  0001 C CNN "MPN"
	1    3100 6050
	0    1    1    0   
$EndComp
Wire Wire Line
	2600 6050 3000 6050
Wire Wire Line
	3200 6050 4000 6050
$Comp
L shim1:R_Small R5
U 1 1 564B43F3
P 7200 6150
F 0 "R5" V 7250 6250 50  0000 L CNN
F 1 "0R" V 7200 6100 50  0000 L CNN
F 2 "IPC7351-Nominal:RESC1608X50" H 7200 6150 60  0001 C CNN
F 3 "" H 7200 6150 60  0000 C CNN
F 4 "Res 0603 1%" H 7200 6150 60  0001 C CNN "MPN"
	1    7200 6150
	0    1    1    0   
$EndComp
$Comp
L shim1:R_Small R4
U 1 1 564B444A
P 7200 5950
F 0 "R4" V 7250 6050 50  0000 L CNN
F 1 "0R" V 7200 5900 50  0000 L CNN
F 2 "IPC7351-Nominal:RESC1608X50" H 7200 5950 60  0001 C CNN
F 3 "" H 7200 5950 60  0000 C CNN
F 4 "Res 0603 1%" H 7200 5950 60  0001 C CNN "MPN"
	1    7200 5950
	0    1    1    0   
$EndComp
$Comp
L shim1:R_Small R3
U 1 1 564B44B5
P 7200 5850
F 0 "R3" V 7250 5950 50  0000 L CNN
F 1 "0R" V 7200 5800 50  0000 L CNN
F 2 "IPC7351-Nominal:RESC1608X50" H 7200 5850 60  0001 C CNN
F 3 "" H 7200 5850 60  0000 C CNN
F 4 "Res 0603 1%" H 7200 5850 60  0001 C CNN "MPN"
	1    7200 5850
	0    1    1    0   
$EndComp
Wire Wire Line
	7300 5850 7800 5850
Wire Wire Line
	7300 5950 7700 5950
Wire Wire Line
	7300 6150 7500 6150
Text GLabel 7300 4450 0    50   Input ~ 0
fpga_spiclk
Text GLabel 7300 4550 0    50   Input ~ 0
fpga_spimosi
Text GLabel 7300 4650 0    50   Output ~ 0
fpga_spimiso
Text GLabel 7300 4750 0    50   Input ~ 0
fpga_spicsn
Wire Wire Line
	7300 4750 7500 4750
Wire Wire Line
	7500 4750 7500 6150
Connection ~ 7500 6150
Wire Wire Line
	7300 4650 7600 4650
Wire Wire Line
	7600 4650 7600 6050
Connection ~ 7600 6050
Wire Wire Line
	7300 4550 7700 4550
Wire Wire Line
	7700 4550 7700 5950
Connection ~ 7700 5950
Wire Wire Line
	7300 4450 7800 4450
Wire Wire Line
	7800 4450 7800 5850
Connection ~ 7800 5850
Text Label 7000 1200 0    50   ~ 0
xrs_tx
Text Label 8300 1200 0    50   ~ 0
rs_tx
Text Label 7000 1300 0    50   ~ 0
xrs_rx
Text Label 8300 1300 0    50   ~ 0
rs_rx
Text Label 7000 1600 0    50   ~ 0
xbodyled
Text Label 7000 1700 0    50   ~ 0
xaudio_on
Text Label 7000 1800 0    50   ~ 0
xfrontled
Text Label 7000 1900 0    50   ~ 0
xbt_rx
Text Label 7000 2000 0    50   ~ 0
xbt_tx
Text Label 7000 2100 0    50   ~ 0
xbatt_low
Text Label 7000 2200 0    50   ~ 0
xled3
Text Label 7000 2300 0    50   ~ 0
xsdi
Text Label 7000 2400 0    50   ~ 0
xsdo
Text Label 7000 2500 0    50   ~ 0
xfsync
Text Label 7000 2600 0    50   ~ 0
xmclk
Text Label 7000 2700 0    50   ~ 0
xled5
Text Label 7000 2800 0    50   ~ 0
xaxex
Text Label 7000 2900 0    50   ~ 0
xaxey
Text Label 7000 3000 0    50   ~ 0
xaxez
Text Label 8300 1600 0    50   ~ 0
bodyled
Text Label 8300 1700 0    50   ~ 0
audio_on
Text Label 8300 1800 0    50   ~ 0
frontled
Text Label 8300 1900 0    50   ~ 0
bt_rx
Text Label 8300 2000 0    50   ~ 0
bt_tx
Text Label 8300 2100 0    50   ~ 0
batt_low
Text Label 8300 2200 0    50   ~ 0
led3
Text Label 8300 2300 0    50   ~ 0
sdi
Text Label 8300 2400 0    50   ~ 0
sdo
Text Label 8300 2500 0    50   ~ 0
fsync
Text Label 8300 2600 0    50   ~ 0
mclk
Text Label 8300 2700 0    50   ~ 0
led5
Text Label 8300 2800 0    50   ~ 0
axex
Text Label 8300 2900 0    50   ~ 0
axey
Text Label 8300 3000 0    50   ~ 0
axez
Text Label 2200 1300 0    50   ~ 0
pgc
Text Label 2200 1500 0    50   ~ 0
mclr
Text Label 2200 2500 0    50   ~ 0
mic3
Text Label 2200 2700 0    50   ~ 0
pic_reset
Text Label 2200 3000 0    50   ~ 0
rs_rx_10v
Text Label 3450 3000 0    50   ~ 0
rs_tx_10v
Text Label 3450 1800 0    50   ~ 0
spk-
Text Label 3450 1700 0    50   ~ 0
spk+
Text Label 3450 1500 0    50   ~ 0
remote
Text Label 3450 1300 0    50   ~ 0
pgd
NoConn ~ 3900 1300
NoConn ~ 3900 1500
NoConn ~ 3900 1700
NoConn ~ 3900 1800
NoConn ~ 2100 1300
NoConn ~ 2100 1500
NoConn ~ 2100 2500
NoConn ~ 2100 2700
NoConn ~ 2100 3000
NoConn ~ 3900 3000
NoConn ~ 6000 5650
NoConn ~ 6000 5750
Text Label 3450 9300 0    50   ~ 0
xu4_pwron
Text Label 2150 8900 0    50   ~ 0
xu4_adc_0.ain0
Text Label 2150 9900 0    50   ~ 0
xu4_adc_0.ain3
NoConn ~ 3900 9300
NoConn ~ 2100 8900
NoConn ~ 2100 9900
Wire Wire Line
	4000 5750 3400 5750
Wire Wire Line
	4000 5650 3400 5650
Text Label 2150 8800 0    60   ~ 0
xu4_p5v0
NoConn ~ 2100 8800
NoConn ~ 3400 5650
NoConn ~ 3400 5750
$Comp
L shim1:TXS0108 U2
U 1 1 564B25E4
P 4500 5200
F 0 "U2" H 4500 5200 60  0000 C CNN
F 1 "TXS0108" H 4500 4000 60  0000 C CNN
F 2 "IPC7351-Nominal:SOP65P640X120-20" H 4500 5200 60  0001 C CNN
F 3 "" H 4500 5200 60  0000 C CNN
F 4 "TI" H 4500 5200 60  0001 C CNN "Manuf"
F 5 "TXS0108EPWR" H 4500 5200 60  0001 C CNN "MPN"
	1    4500 5200
	1    0    0    -1  
$EndComp
NoConn ~ 3900 1200
NoConn ~ 2100 1200
Text Label 2200 1200 0    50   ~ 0
batt_1
Text Label 3450 1200 0    50   ~ 0
batt_2
Text Label 2200 3100 0    50   ~ 0
epuck_gnda_1
Text Label 3450 3100 0    50   ~ 0
epuck_gnda_2
Text Label 3450 2900 0    50   ~ 0
epuck_vcca_2
Text Label 2200 2900 0    50   ~ 0
epuck_vcca_1
NoConn ~ 3900 2900
NoConn ~ 3900 3100
NoConn ~ 2100 2900
NoConn ~ 2100 3100
Wire Wire Line
	8250 1200 8700 1200
Wire Wire Line
	7350 1200 7400 1200
Wire Wire Line
	8250 1300 8700 1300
Wire Wire Line
	7350 1300 7400 1300
Wire Wire Line
	8250 2200 8700 2200
Wire Wire Line
	7350 2200 7400 2200
Wire Wire Line
	8250 2700 8700 2700
Wire Wire Line
	7350 2700 7400 2700
Wire Wire Line
	8250 1800 8700 1800
Wire Wire Line
	7350 1800 7400 1800
Wire Wire Line
	8250 1900 8700 1900
Wire Wire Line
	7350 1900 7400 1900
Wire Wire Line
	8250 2000 8700 2000
Wire Wire Line
	7350 2000 7400 2000
Wire Wire Line
	8250 2100 8700 2100
Wire Wire Line
	7350 2100 7400 2100
Wire Wire Line
	8250 1600 8700 1600
Wire Wire Line
	7350 1600 7400 1600
Wire Wire Line
	8250 1700 8700 1700
Wire Wire Line
	7350 1700 7400 1700
Wire Wire Line
	8250 2300 8700 2300
Wire Wire Line
	7350 2300 7400 2300
Wire Wire Line
	8250 2400 8700 2400
Wire Wire Line
	7350 2400 7400 2400
Wire Wire Line
	8250 2500 8700 2500
Wire Wire Line
	7350 2500 7400 2500
Wire Wire Line
	8250 2600 8700 2600
Wire Wire Line
	7350 2600 7400 2600
Wire Wire Line
	8250 2800 8700 2800
Wire Wire Line
	7350 2800 7400 2800
Wire Wire Line
	8250 2900 8700 2900
Wire Wire Line
	7350 2900 7400 2900
Wire Wire Line
	8250 3000 8700 3000
Wire Wire Line
	7350 3000 7400 3000
Wire Wire Line
	3800 5350 3800 6250
Wire Wire Line
	3800 6250 3800 6350
Wire Wire Line
	7500 6150 8100 6150
Wire Wire Line
	7600 6050 8100 6050
Wire Wire Line
	7700 5950 8100 5950
Wire Wire Line
	7800 5850 8100 5850
$EndSCHEMATC
