

#include "p30F6014A.h"
#include <stdint.h>
#include "spi.h"
#include "epuck_ports.h"

uint16_t    inbuf[BUFSIZE];
uint16_t    outbuf[BUFSIZE] = {
        0x0000, 0x0001, 0x0002, 0x0003,
        0x0004, 0x0005, 0x0006, 0x0007,
        0x0008, 0x0009, 0x000a, 0x000b,
        0x000c, 0x000d, 0x000e, 0x000f,
        0x0010, 0x0011, 0x0012, 0x0013,
        0x0014, 0x0015, 0x0016, 0x0017,
        0x0018
    };
int ptr  = 0;


//--------------------------------------------------------------------
// Set up so that each packet of 16 bits causes an interrupt.
// The packet interrupt grabs the incoming data into inbuf, and gets
// the outgoing data from outbuf. outbuf[0] not used
//
// Doesn't seem possible to have an interrupt as the Slave Select line
// goes high, so set up a change notify interrupt, this sets the flip bit
// that is used in the main loop. Using the Slave Select line for this means
// there should be no problems getting out of sync
//--------------------------------------------------------------------
//
//
//
//
// Useful stuff grabbed from http://www.microchip.com/forums/m409742.aspx
void init_spi(void) {

    // SPI2 Pin setup
    TRISGbits.TRISG9 = 1; // SPI2 Slave Select  (input)
    TRISGbits.TRISG6 = 1; // SPI2 CLK   (input)
    TRISGbits.TRISG7 = 1; // SPI2 SDI/MOSI  (input)
    TRISGbits.TRISG8 = 0; // SPI2 SDO/MISO  (output)

    // SPI2 Register setup, Slave mode, SS1 disabled
    SPI2BUF             = 0;                    //Datasheet specifies the following order for setting SPI1 in slave mode
    IFS1bits.SPI2IF     = 0;                    //Clear interrupt flag (no interrupt)
    IEC1bits.SPI2IE     = 1;                    //Enable SPI1 interrupts
    IPC6bits.SPI2IP     = 6;                    //#Set SPI1 interrupt priority pretty high, higher than all other user interrupts
    //SPI2CON             = 0b0000010100011010;   //Setup for slave (0,0) mode with SS1 enabled.
    SPI2CON             = 0b0000000000000000;
    SPI2STAT            = 0b0000000000000000;
    SPI2CONbits.MODE16  = 1;
    SPI2CONbits.SMP     = 0;                    //Datasheet specifies this must be cleared.
    SPI2CONbits.CKE     = 1;                    //Mode (0,0)
    SPI2CONbits.SSEN    = 1;                    //Do
    SPI2CONbits.CKP     = 0;
    SPI2STATbits.SPIROV = 0;                    //Make sure no errors
    SPI2STATbits.SPIEN  = 1;                    //Start SPI1

    // Prime the output buffer
    while(SPI2STATbits.SPITBF);
    SPI2BUF             = outbuf[0];

    // Set up change notification on the Slave Select line
    // SS2N/CN11/RG9
    //CNEN1bits.CN10IE    = 1;
    CNEN1bits.CN11IE    = 1;
    IFS0bits.CNIF       = 0;
    IPC3bits.CNIP       = 7;
    IEC0bits.CNIE       = 1;

}
 

void __attribute__((interrupt, shadow, no_auto_psv)) _SPI2Interrupt(void)
{
    IFS1bits.SPI2IF     = 0;                    //Clear the interrupt flag
    SPI2STATbits.SPIROV = 0;                    //Clear any errors
    inbuf[ptr] = SPI2BUF;
    if (ptr < (BUFSIZE - 1))
        ptr++;
    //SPI2BUF = 0x55aa;
    SPI2BUF = outbuf[ptr];
}
 
volatile unsigned char flip = 0;
void __attribute((interrupt, no_auto_psv)) _CNInterrupt(void)
{
    IFS0bits.CNIF       = 0;                    //Clear the interrupt flag
    // Read from the port to clear CN condition
    uint16_t ssn = PORTG;
    ptr = 0;

    if (ssn&0x200)
        flip = 1;
    LED7 = ssn&0x200 ? 1 : 0;
    
}
 

