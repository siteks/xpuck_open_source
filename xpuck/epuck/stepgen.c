

#include "epuck_ports.h"
#include "stepgen.h"


// 
// We want ~1% jitter on 1000Hz steps
// Lets use:
//  velocity in steps/s
//  16 bit signed fixed point 11.5
//  Max pos frequency 01111111111.11111 = 1023.96875 Hz
//  Max neg frequency 10000000000.00000 = -1024.0 Hz
// Use power-of-two for steploop frequency, 
// 64   = 65536 Hz = 15.25878 us    
// 128  = 131072Hz = 7.629394 us
//
// Master clock is 7.3728 MHz. Instruction cycle is 14.7456 MHz = 67.81684 ns
// Timer periods    64  : 225 - 1       <== this one
//                  128 : 112.5 - 1
//
// Bit that toggles after 65
// 01 1111 1111 1111 1100 0000
// 00 0000 0111 1111 1111 1111 = 0x007fff
// 10 0000 0111 1111 1011 1111 = 0x207fbf
// ^
// | = bit 21



#define PICKOFF     21

// We make the right hand sequence backward WRT the left so that 
// velocities and raw step accumulations are both positive for forward
// motion
const char seql[4] = {0x05, 0x06, 0x0a, 0x09};
const char seqr[4] = {0x90, 0xa0, 0x60, 0x50};

int32_t             accum0, accum1;
int16_t             addval0, addval1;
int16_t             deltalim;
int16_t             target_addval0, target_addval1;
int32_t             rawcount0 = 0;
int32_t             rawcount1 = 0;
volatile char       step = 0;

void init_stepper_timer()
{
    // Set up timer 1 to interrupt 
    T1CONbits.TSIDL = 0;    // timer must count while CPU idle
    T1CONbits.TCKPS = 0;    // No prescale
    T1CONbits.TCS   = 0;    // Internal clock (fosc/4)
    IEC0bits.T1IE   = 1;
    IPC0bits.T1IP   = 0;
    PR1             = 224;   // Set period
    T1CONbits.TON   = 1;    // Start timer
}

// interrupt vector names defined in /opt/microchip/xc16/v1.25/support/dsPIC30F/gld/p30F6014A.gld
static inline void stepgen();
void __attribute((interrupt, no_auto_psv)) _T1Interrupt(void)
{
    IFS0bits.T1IF   = 0;
    //LED1 = 1;
    step = 1;
    stepgen();
    //LED1 = 0;
}
 


//unsigned char last_motor;
unsigned char motor = 0;
static inline void stepgen()
{
    // Implement acceleration 
    int32_t old_addval = addval0;
    if (target_addval0 > old_addval + deltalim)
        addval0 = old_addval + deltalim;
    else if (target_addval0 < old_addval - deltalim)
        addval0 = old_addval - deltalim;
    else
        addval0 = target_addval0;

    int8_t step_now = *((int8_t*)(&accum0) + 2);
    accum0          += addval0;
    step_now        ^= *((int8_t*)(&accum0) + 2);
    step_now        &= (1L << (PICKOFF-16));

    int curr_dir = addval0 > 0 ? 1 : -1;
    if (step_now)
        rawcount0   += curr_dir;
    
    old_addval = addval1;
    if (target_addval1 > old_addval + deltalim)
        addval1 = old_addval + deltalim;
    else if (target_addval1 < old_addval - deltalim)
        addval1 = old_addval - deltalim;
    else
        addval1 = target_addval1;

    step_now        = *((int8_t*)(&accum1) + 2);
    accum1          += addval1;
    step_now        ^= *((int8_t*)(&accum1) + 2);
    step_now        &= (1L << (PICKOFF-16));

    curr_dir = addval1 > 0 ? 1 : -1;
    if (step_now)
        rawcount1   += curr_dir;
 
    // Powersave by only applying power when any phase changes
    //last_motor = motor;   
    motor = seqr[rawcount1 & 3] | seql[rawcount0 & 3];
    if (!step_now && addval0 > -8 && addval0 < 8 && addval1 > -8 && addval1 < 8)
        LATD = 0;
    else
        LATD = motor;

    // MOTOR1_PHA = s & 1; s >>= 1;
    // MOTOR1_PHB = s & 1; s >>= 1;
    // MOTOR1_PHC = s & 1; s >>= 1;
    // MOTOR1_PHD = s & 1;
    //s = seq[state[1]];
    //MOTOR2_PHA = s & 1; s >>= 1;
    //MOTOR2_PHB = s & 1; s >>= 1;
    //MOTOR2_PHC = s & 1; s >>= 1;
    //MOTOR2_PHD = s & 1;

}


void setvel(int16_t vel_left, int16_t vel_right)
{
    target_addval0 = vel_left;
    // Negate the right hand velocity so setting both to positive moves forward
    target_addval1 = vel_right;
    // The smallest acceleration we can have is 1.
    // This will take 32768 loops = half a second to reach full speed
    deltalim = 50;
}




