

#ifndef _ADC_H
#define _ADC_H


#include <stdint.h>
#include "epuck_ports.h"

void init_adc();
void trigger_adc();


extern volatile int16_t accel[3];
extern volatile int16_t prox_ir_off[8];
extern volatile int16_t prox_ir_on[8];


#endif


