

#include "p30F6014A.h"
#include "adc.h"


void init_adc()
{
    // We want to scan the 3 accelerometer and the 8 IR sensors.
    // IR sensors need to be scanned with and without the IR emitter
    // turned on.
    //
    //
    // From datasheet:
    //
    // T_ad = T_cy * 0.5 * (ADCS<5:0> + 1)
    // T_ad must be >667ns
    // T_cy = 1/14.745600MHz = 67.82ns
    //
    // ADCS = 667 * 2 / 67.82 - 1
    //      = 18.67
    //      = 19
    //
    // T_ad = 67.82 * 0.5 * 20
    //      = 678.2 ns
    //
    // Using SSRC=7, SAMC=1
    // Sampling time = 15 * T_ad
    //               = 10173 ns
    // Sampling rate = 98.3 kHz
    //
    // For all inputs = 8.9 kHz
    //
    // To run with stepgen, which loops at 65536 Hz
    //
    // Inputs are:
    // 0
    // 1
    // 2    mic 0
    // 3    mic 1
    // 4    mic 2
    // 5    accel x
    // 6    accel y
    // 7    accel z
    // 8    ir0
    // 9    ir1
    // 10   ir2
    // 11   ir3
    // 12   ir4
    // 13   ir5
    // 14   ir6
    // 15   ir6
    ADCON1              = 0;
    ADCON2              = 0;
    ADCON3              = 0;
    ADCHS               = 0;

    // All ADC ports as analog inputs
    ADPCFG              = 0x0003;
    ADCON1bits.SSRC     = 7;
    ADCON3bits.ADCS     = 19;
    ADCON3bits.SAMC     = 1;
    // From Table 18.2 dsPIC Family reference
    ADCON2bits.SMPI     = 10;       // Interrupt after 11 samples
    ADCON2bits.CSCNA    = 1;        // Scan CH0+ inputs
    ADCSSL              = 0xffe0;   // accel and IR
    
    // Setup interrupts
    IFS0bits.ADIF = 0;  // Clear interrupt flag
    IEC0bits.ADIE = 1;  // Enable interrupts

}

volatile char toggle_irled = 0;
void trigger_adc()
{
    PULSE_IR0 = toggle_irled;
    PULSE_IR1 = toggle_irled;
    PULSE_IR2 = toggle_irled;
    PULSE_IR3 = toggle_irled;
    toggle_irled = (toggle_irled & 1) ^ 1;
    ADCON1bits.ADON     = 1;
    ADCON1bits.ASAM     = 1;
    IFS0bits.ADIF       = 0;  // Clear interrupt flag
    IEC0bits.ADIE       = 1;  // Enable interrupts
    //LED7 = 1;
}

volatile int16_t accel[3];
volatile int16_t prox_ir_off[8];
volatile int16_t prox_ir_on[8];

void __attribute((interrupt, no_auto_psv)) _ADCInterrupt(void)
{
    IFS0bits.ADIF = 0;  // Clear interrupt flag
    IEC0bits.ADIE = 0;  // Disable interrupts
    // Turn off ADC conversion until next triggered
    ADCON1bits.ADON     = 0;
    ADCON1bits.ASAM     = 0;

    PULSE_IR0 = 0;
    PULSE_IR1 = 0;
    PULSE_IR2 = 0;
    PULSE_IR3 = 0;


    accel[0] = ADCBUF0;
    accel[1] = ADCBUF1;
    accel[2] = ADCBUF2;
    if (toggle_irled)
    {
        prox_ir_off[0] = ADCBUF3;
        prox_ir_off[1] = ADCBUF4;
        prox_ir_off[2] = ADCBUF5;
        prox_ir_off[3] = ADCBUF6;
        prox_ir_off[4] = ADCBUF7;
        prox_ir_off[5] = ADCBUF8;
        prox_ir_off[6] = ADCBUF9;
        prox_ir_off[7] = ADCBUFA;
    }
    else
    {
        prox_ir_on[0] = ADCBUF3;
        prox_ir_on[1] = ADCBUF4;
        prox_ir_on[2] = ADCBUF5;
        prox_ir_on[3] = ADCBUF6;
        prox_ir_on[4] = ADCBUF7;
        prox_ir_on[5] = ADCBUF8;
        prox_ir_on[6] = ADCBUF9;
        prox_ir_on[7] = ADCBUFA;
    }
}


