

//#include <spi.h>
#include "p30F6014A.h"
#include <stdint.h>
#include "epuck_ports.h"
#include "init_ports.h"


#include "spi.h"
#include "stepgen.h"
#include "adc.h"


//---------------------------------------------------------------
// E-puck pin assignments
//---------------------------------------------------------------
// 
// 01   COFS/RG15                   fsync
// 02   T2CK/RC1                    frontled
// 03   T3CK/RC2                    bodyled
// 04   T4CK/RC3                    href
// 05   T5CK/RC4                    vsync
// 06   SCK2/CN8/RG6                sel0
// 07   SDI2/CN9/RG7                sel1
// 08   SDO2/CN10/RG8               sel2
// 09   nMCLR               
// 10   nSS2/CN11/RG9               sel3
// 11   VSS
// 12   VDD
// 13   INT1/RA12                   led3
// 14   INT2/RA13                   led5
// 15   AN5/CN7/RB5                 axis_x
// 16   AN4/CN6/RB4                 mic3
// 17   AN3/CN5/RB3                 mic2
// 18   AN2/nSS1/LVDIN/CN4/RB2      mic1
// 19   PGC/EMUC/AN1/CN3/RB1
// 20   PGD/EMUD/AN0/CN2/RB0
// 21   AN6/OCFA/RB6                axis_y
// 22   AN7/RB7                     axis_z
// 23   VREF-/RA9                   led2
// 24   VREF+/RA10                  led4
// 25   AVDD
// 26   AVSS
// 27   AN8/RB8                     ir0
// 28   AN9/RB9                     ir1
// 29   AN10/RB10                   ir2
// 30   AN11/RB11                   ir3
// 31   VSS
// 32   VDD
// 33   AN12/RB12                   ir4
// 34   AN13/RB13                   ir5
// 35   AN14/RB14                   ir6
// 36   AN15/OCFB/CN12/RB15         ir7
// 37   IC7/CN20/RD14               y6
// 38   IC8/CN21/RD15               y7
// 39   U2RX/CN17/RF4               rs_rx
// 40   U2TX/CN18/RF5               rs_tx
// 41   U1TX/RF3                    bt_tx
// 42   U1RX/RF2                    bt_rx
// 43   EMUD3/SDO1/RF8              pulse1
// 44   SDI1/RF7                    pulse0
// 45   EMUC3/SCK1/INT0/RF6         remote
// 46   SDA/RG3                     sda
// 47   SCL/RG2                     scl
// 48   VDD
// 49   OSC1/CLK1
// 50   OSC2/CLK0/RC15
// 51   VSS
// 52   INT3/RA14                   led6
// 53   INT4/RA15                   led7
// 54   IC1/RD8                     y0
// 55   IC2/RD9                     y1
// 56   IC3/RD10                    y2
// 57   IC4/RD11                    y3
// 58   EMUC2/OC1/RD0               mot1a
// 59   EMUD1/SOSCI/CN1/RC13        cam_reset
// 60   EMUC1/SOSCO/T1CK/RC14       pclk
// 61   EMUD2/OC2/RD1               mot1b
// 62   OC3/RD2                     mot1c
// 63   OC4/RD3                     mot1d
// 64   IC5/RD12                    y4
// 65   IC6/CN19/RD13               y5
// 66   OC5/CN13/RD4                mot2a
// 67   OC6/CN14/RD5                mot2b
// 68   OC7/CN15/RD6                mot2c
// 69   OC8/CN16/RD7                mot2d
// 70   VSS
// 71   VDD
// 72   C1RX/RF0
// 73   C1TX/RF1                    batt_low
// 74   C2TX/RG1                    pulse3
// 75   C2RX/RG0                    pulse2
// 76   CN22/RA6                    led0
// 77   CN23/RA7                    led1
// 78   CSCK/RG14                   mclk
// 79   CSDI/RG12                   sdo
// 80   CSDO/RG13                   sdi



// Steppers are two coil, driven by H-bridge
// coil1    ab
// coil2    cd










//---------------------------------------------------------------
// Configuration bits - in motor_led/e_init_port.c
_FOSC(CSW_FSCM_OFF & XT_PLL8);
_FBORPOR(PBOR_OFF & MCLR_EN);
//---------------------------------------------------------------



extern volatile char       step;

int main()
{
    init_ports();
    init_stepper_timer();
    init_spi();
    init_adc();

    setvel(0, 0);

    // Allow camera out of reset
	CAM_RESET=1;

    int i;
    for (i = 0; i < BUFSIZE; i++)
        inbuf[i] = outbuf[i] = 0;

    int32_t sptr = 0;
    int state = 0;

    trigger_adc();
    while(1)
    {
        IDLE();
        
        if (step)
        {
            // Step is triggered at 65536 Hz
            step = 0;
            sptr++;

            if ((sptr % 128) == 0)
            {
                // Arrive here at 512Hz
                // trigger_adc alternates between sampling with the IR emitter
                // off then on
                trigger_adc();
            }
        }

        if (flip)
        {
            flip = 0;
            outbuf[1]   = accel[0];
            outbuf[2]   = accel[1];
            outbuf[3]   = accel[2];
            outbuf[4]   = prox_ir_off[0];
            outbuf[5]   = prox_ir_off[1];
            outbuf[6]   = prox_ir_off[2];
            outbuf[7]   = prox_ir_off[3];
            outbuf[8]   = prox_ir_off[4];
            outbuf[9]   = prox_ir_off[5];
            outbuf[10]  = prox_ir_off[6];
            outbuf[11]  = prox_ir_off[7];
            outbuf[12]  = prox_ir_on[0];
            outbuf[13]  = prox_ir_on[1];
            outbuf[14]  = prox_ir_on[2];
            outbuf[15]  = prox_ir_on[3];
            outbuf[16]  = prox_ir_on[4];
            outbuf[17]  = prox_ir_on[5];
            outbuf[18]  = prox_ir_on[6];
            outbuf[19]  = prox_ir_on[7];

            outbuf[20]  = rawcount0 & 0xffff;
            outbuf[21]  = rawcount0 >> 16;
            outbuf[22]  = rawcount1 & 0xffff;
            outbuf[23]  = rawcount1 >> 16;
            outbuf[24]  = BATT_LOW ^ 1;
            //LED1        = ~BATT_LOW;

            setvel(inbuf[0], inbuf[1]);
        }
    }
}
