

#ifndef _SPI_H
#define _SPI_H

#define SSN     _RG9
#define BUFSIZE 32
extern uint16_t    inbuf[BUFSIZE];
extern uint16_t    outbuf[BUFSIZE];
void init_spi(void);

extern volatile unsigned char flip;

#endif

