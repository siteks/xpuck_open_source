# Xpuck Open Source

This repository contains the design files necessary to build the Xpuck robot, and the software 
for the robot simulator. It is licensed under the MIT license, the text of which is in the file LICENSE.txt
and reproduced below:

**License below line**

---

Copyright (c) 2019 Simon Jones simon.jones@brl.ac.uk

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

---

**License above line**


Directory structure
-------------------

    xps             C++ and OpenCL parallel simulator and BT evolver
        assets      OpenCL code
        include     Headers
        src         Main source code

    xpuck           Hardware and firmware design files
        docs        BOM, original spec and Frontiers paper
        epuck       New firmware for base epuck
        pcb         Kicad design files and plots for interface PCB
        src         Example snippets of code showing interfacing
        stl         Meshes for 3D printed components
        usbfpga     FPGA bitstream, FT2232 EEPROM config, and source



