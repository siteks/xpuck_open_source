

//#define DEBUG
//#define DEBUG2
#define CHECKS
 
#include "btcommon.h"


#ifdef CHECKS
#define CHECKNAN(m,x) if (isnan(x)){printf("gid:%d %s nan\n",get_global_id(0),m);}
#else
#define CHECKNAN(m,x)
#endif


//#ifdef DEBUG
#define HEXDUMP(start, length)      \
{global uchar *p = start;            \
for (int i = 0; i < 2048; i+= 16)    \
{                                   \
    printf("(%3lu) %04x  %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x\n", get_global_id(0), i, p[0],p[1],p[2],p[3],p[4],p[5],p[6],p[7],p[8],p[9],p[10],p[11],p[12],p[13],p[14],p[15]);         \
    p += 16;\
    if (p - start > length) \
        break;\
}}
//#else
//#define HEXDUMP(start,length)
//#endif

#define SIN(x) (sintab[as_uchar(x)])
#define COS(x) (costab[as_uchar(x)])

#ifdef _ROBOT
#define global

#define as_uchar(x) (*(uchar*)(&x))

// length(), fast_length(), atan2pi, and normalize() are OpenCL standard functions that
// need to define when compiled as C. convert_prox() is written in a very vectorised form
// for OpenCL, and here written in serialised form
float length(float2 v)
{
    return sqrt(v.x * v.x + v.y * v.y);
}
float fast_length(float2 v)
{
    return length(v);
}
float2 normalize(float2 v)
{
    float l = length(v);
    return (float2)INIT2(v.x / l, v.y / l);
}
float atan2pi(float y, float x)
{
    return atan2(y, x) / M_PI;
}

static float2 convert_prox(global ushort *p, global struct Params *pr)
{
    float2 v = (float2)INIT2(0,0);
    for(int i = 0; i < 8; i++)
    {
        float s = (pr->sensor_max_radius - (float)p[i] * 0.00001f) / (pr->sensor_max_radius - pr->sensor_min_radius);
        v.x += cos_sa[i] * s;
        v.y += sin_sa[i] * s;
    }
    return v;
}

static float2 fmin(float2 x, float y)
{
    return (float2)INIT2((float)fmin(x.x, y), (float)fmin(x.y, y));
}
static float2 fmax(float2 x, float y)
{
    return (float2)INIT2((float)fmax(x.x, y), (float)fmax(x.y, y));
}

#else

static float2 convert_prox(global ushort *p, global struct Params *pr)
{
    float8 prox = (pr->sensor_max_radius - convert_float8(vload8(0, p)) * 0.00001f)
                / (pr->sensor_max_radius - pr->sensor_min_radius);
    float2 v = (float2)(dot(prox.s0123, cos_sa.s0123) + dot(prox.s4567, cos_sa.s4567),
                      dot(prox.s0123, sin_sa.s0123) + dot(prox.s4567, sin_sa.s4567));
    
    DB2PRINT10("%f %f %f %f %f %f %f %f v:%f %f\n",prox.s0,prox.s1,prox.s2,prox.s3,prox.s4,prox.s5,prox.s6,prox.s7,v.x,v.y);
    return v;
}

#endif


#define MYRAND_MAX 2147483647
//https://en.wikipedia.org/wiki/Lehmer_random_number_generator
static uint rand(global uchar *bt)
{
    global uint *seed = (global uint*)bt;
    uint const a = 16807; //ie 7**5
    uint const m = 2147483647; //ie 2**31-1
    
    *seed = ((ulong)*seed * a) % m;
    DBPRINT1("seed:%u\n",*seed);
    //printf("%lu %u\n", get_global_id(0), *seed);
    return(*seed);
}


static float rand_realrange(global uchar *bt, float low, float high)
{
    float r = ((double)rand(bt)/(double)MYRAND_MAX) * (high - low) + low;
    //if(get_global_id(0)==15)printf("%f\n", r);
    return r;
}

static int rand_intrange(global uchar *bt, int low, int high)
{
    int r = (int)rand_realrange(bt, low, high + 1);
    //if(get_global_id(0)==14)printf("%d\n", r);
    return r;
}

#ifndef _ROBOT
static float rand_norm(global uchar *bt, float variance)
{
//    return normtab[*bt++ % 256] * variance;
    return variance * (double)rand(bt)/(double)MYRAND_MAX - 0.5;
//    
//    float f0 = (double)rand(bt)/(double)MYRAND_MAX - 0.5;
//    float f1 = (double)rand(bt)/(double)MYRAND_MAX - 0.5;
//    float f2 = (double)rand(bt)/(double)MYRAND_MAX - 0.5;
//    float f3 = (double)rand(bt)/(double)MYRAND_MAX - 0.5;
//    float4 f = (float4)(f0, f1, f2, f3);
//    
//    //printf("%f %f %f %f : %f\n", f.s0,f.s1,f.s2,f.s3, f.s0 + f.s1 + f.s2 + f.s3);
//    return  dot((float4)variance, f);
}



#endif




//----------------------------------------------------------------------------------
// Composition nodes
//----------------------------------------------------------------------------------
static void update_seq(global uchar *bt, ushort **sp, global Node *n, global uchar *vs, uchar count)
{
    DBPRINT3("enter update_seq ptr:%d stat:%d idx:%d\n", n->ptr, vs[n->ptr], vs[n->ptr+1]);
    if ((vs[n->ptr] == BT_INVALID) || (vs[n->ptr] == BT_RUNNING_STALE))
    {
        vs[n->ptr] = BT_RUNNING;
        vs[n->ptr + 1] = 0;
        (*sp)++;
        *(*sp)++ = n->data.sm4.op[0];
    }
    else if (vs[n->ptr] == BT_RUNNING)
    {
        // children have been refreshed
        global Node *c = (global Node*)(bt + n->data.sm4.op[vs[n->ptr + 1]]);
        DBPRINT2("child %d status %d\n", vs[n->ptr + 1], vs[c->ptr]);
        if (vs[c->ptr] == BT_FAILURE)
        {
            // Child has finished processing and failed, this node
            // must now finish and return failure
            vs[n->ptr] = BT_FAILURE;
        }
        else if (vs[c->ptr] == BT_SUCCESS)
        {
            // Child finished with success, we move on to the next
            // child if it exists, if there are none, we return success
            if (++vs[n->ptr + 1] < count)
            {
                (*sp)++;
                *(*sp)++ = n->data.sm4.op[vs[n->ptr + 1]];
                DBPRINT1("started child %d\n", vs[n->ptr + 1]);
            }
            else
            {
                vs[n->ptr] = BT_SUCCESS;
            }
        }
    }
    DBPRINT1("return update_seq %d\n", vs[n->ptr]);
}

static void update_seqm(global uchar *bt, ushort **sp, global Node *n, global uchar *vs, uchar count)
{
    global uchar *pstate    = vs + n->ptr;
    global uchar *pidx      = vs + n->ptr + 1;
    DBPRINT3("enter update_seqm ptr:%d stat:%d idx:%d\n", n->ptr, *pstate, *pidx);
    if (vs[n->ptr] == BT_INVALID)
    {
        DBPRINT1("INVALID pushing first child %04x\n", n->data.sm4.op[0]);
        *pstate = BT_RUNNING;
        *pidx = 0;
        (*sp)++;
        *(*sp)++ = n->data.sm4.op[0];
    }
    else if (vs[n->ptr] == BT_RUNNING_STALE)
    {
        // Children need to be refreshed
        DBPRINT1("RUNNING_STALE pushing next child %04x\n", n->data.sm4.op[*pidx]);
        *pstate = BT_RUNNING;
        (*sp)++;
        *(*sp)++ = n->data.sm4.op[*pidx];
    }
    else if (*pstate == BT_RUNNING)
    {
        // children have been refreshed
        global Node *c = (global Node*)(bt + n->data.sm4.op[*pidx]);
        DBPRINT3("RUNNING child %d %04x status %d\n", *pidx, n->data.sm4.op[*pidx], *pstate);
        if (vs[c->ptr] == BT_FAILURE)
        {
            // Child has finished processing and failed, this node
            // must now finish and return failure
            *pstate = BT_FAILURE;
        }
        else if (vs[c->ptr] == BT_SUCCESS)
        {
            // Child finished with success, we move on to the next
            // child if it exists, if there are none, we return success
            if (++*pidx < count)
            {
                (*sp)++;
                *(*sp)++ = n->data.sm4.op[*pidx];
                DBPRINT2("child SUCCESS pushing next child %d %04x\n", *pidx, n->data.sm4.op[*pidx]);
            }
            else
            {
                DBPRINT0("child SUCCESS, all done, returning SUCCESS\n");
                *pstate = BT_SUCCESS;
            }
        }
    }
    DBPRINT1("return update_seqm %d\n", *pstate);
}

static void update_sel(global uchar *bt, ushort **sp, global Node *n, global uchar *vs, uchar count)
{
    DBPRINT3("enter update_sel ptr:%d stat:%d idx:%d\n", n->ptr, vs[n->ptr], vs[n->ptr+1]);
    if ((vs[n->ptr] == BT_INVALID) || (vs[n->ptr] == BT_RUNNING_STALE))
    {
        vs[n->ptr] = BT_RUNNING;
        vs[n->ptr + 1] = 0;
        (*sp)++;
        *(*sp)++ = n->data.sm4.op[0];
    }
    else if (vs[n->ptr] == BT_RUNNING)
    {
        // children have been refreshed
        global Node *c = (global Node*)(bt + n->data.sm4.op[vs[n->ptr + 1]]);
        DBPRINT2("child %d status %d\n", vs[n->ptr + 1], vs[c->ptr]);
        if (vs[c->ptr] == BT_SUCCESS)
        {
            // Child has finished processing and succeeded, this node
            // must now finish and return success
            vs[n->ptr] = BT_SUCCESS;
        }
        else if (vs[c->ptr] == BT_FAILURE)
        {
            // Child finished with failure, we move on to the next
            // child if it exists, if there are none, we return failure
            if (++vs[n->ptr + 1] < count)
            {
                (*sp)++;
                *(*sp)++ = n->data.sm4.op[vs[n->ptr + 1]];
            }
            else
            {
                vs[n->ptr] = BT_FAILURE;
            }
        }
    }
    DBPRINT1("return update_sel %d\n", vs[n->ptr]);
}

static void update_selm(global uchar *bt, ushort **sp, global Node *n, global uchar *vs, uchar count)
{
    DBPRINT3("enter update_selm ptr:%d stat:%d idx:%d\n", n->ptr, vs[n->ptr], vs[n->ptr+1]);
    if (vs[n->ptr] == BT_INVALID)
    {
        vs[n->ptr] = BT_RUNNING;
        vs[n->ptr + 1] = 0;
        (*sp)++;
        *(*sp)++ = n->data.sm4.op[0];
    }
    else if (vs[n->ptr] == BT_RUNNING_STALE)
    {
        // Children need to be refreshed
        vs[n->ptr] = BT_RUNNING;
        (*sp)++;
        *(*sp)++ = n->data.sm4.op[vs[n->ptr + 1]];
    }
    else if (vs[n->ptr] == BT_RUNNING)
    {
        // children have been refreshed
        global Node *c = (global Node*)(bt + n->data.sm4.op[vs[n->ptr + 1]]);
        DBPRINT2("child %d status %d\n", vs[n->ptr + 1], vs[c->ptr]);
        if (vs[c->ptr] == BT_SUCCESS)
        {
            // Child has finished processing and succeeded, this node
            // must now finish and return success
            vs[n->ptr] = BT_SUCCESS;
        }
        else if (vs[c->ptr] == BT_FAILURE)
        {
            // Child finished with failure, we move on to the next
            // child if it exists, if there are none, we return failure
            if (++vs[n->ptr + 1] < count)
            {
                (*sp)++;
                *(*sp)++ = n->data.sm4.op[vs[n->ptr + 1]];
            }
            else
            {
                vs[n->ptr] = BT_FAILURE;
            }
        }
    }
    DBPRINT1("return update_selm %d\n", vs[n->ptr]);
}

static void update_repeati(global uchar *bt, ushort **sp, global Node *n, global uchar *vs)
{
    DBPRINT4("enter  update_repeati ptr:%d stat:%d rep:%d count:%d\n", n->ptr, vs[n->ptr], n->data.rep.repeat, vs[n->ptr + 1]);
    if (vs[n->ptr] == BT_INVALID)
    {
        vs[n->ptr + 1]  = n->data.rep.repeat;
        vs[n->ptr] = BT_RUNNING;
        // unpop repeat and push child
        (*sp)++;
        *(*sp)++ = n->data.rep.op;
    }
    else if (vs[n->ptr] == BT_RUNNING)
    {
        //uchar cs = ((global Node*)(bt + n->data.rep.op))->status;
        global Node *c = (global Node*)(bt + n->data.rep.op);
        DBPRINT1("RUNNING: status of child %d\n", vs[c->ptr]);
        if (vs[c->ptr] == BT_FAILURE)
        {
            vs[n->ptr] = BT_FAILURE;
        }
        else if (vs[c->ptr] == BT_RUNNING_STALE)
        {
            // Keep repeat and child node for next iteration
            (*sp)++;
            *(*sp)++ = n->data.rep.op;
        }
        else if (vs[c->ptr] == BT_SUCCESS)
        {
            DBPRINT0("About to dec count\n");
            vs[n->ptr + 1]--;
            if (vs[n->ptr + 1] == 0)
            {
                vs[n->ptr] = BT_SUCCESS;
            }
        }
    }
    else if (vs[n->ptr] == BT_RUNNING_STALE)
    {
        DBPRINT0("RUNNING_STALE: refresh child status: become RUNNING\n");
        (*sp)++;
        *(*sp)++ = n->data.rep.op;
        vs[n->ptr] = BT_RUNNING;
    }
    DBPRINT2("return update_repeati stat:%d count:%d\n", vs[n->ptr], vs[n->ptr + 1]);
}

static void update_repeatr(global uchar *bt, ushort **sp, global Node *n, global uchar *vs, global uchar *bb)
{
    //DBPRINT3("enter  update_repeatr %d, %d %d\n", n->status, n->data.rep.repeat, n->data.rep.count);
    if (vs[n->ptr] == BT_INVALID)
    {
        vs[n->ptr + 1]  = rand_intrange(bb - 8, 1, n->data.rep.repeat);
        vs[n->ptr] = BT_RUNNING;
        // unpop repeat and push child
        (*sp)++;
        *(*sp)++ = n->data.rep.op;
    }
    else if (vs[n->ptr] == BT_RUNNING)
    {
        global Node *c = (global Node*)(bt + n->data.rep.op);
        //DBPRINT1("status of child %d\n", cs);
        if (vs[c->ptr] == BT_FAILURE)
        {
            vs[n->ptr] = BT_FAILURE;
        }
        else if (vs[c->ptr] == BT_RUNNING_STALE)
        {
            // Keep repeat and child node for next iteration
            (*sp)++;
            *(*sp)++ = n->data.rep.op;
        }
        else if (vs[c->ptr] == BT_SUCCESS)
        {
            vs[n->ptr + 1]--;
            if (vs[n->ptr + 1] == 0)
            {
                vs[n->ptr] = BT_SUCCESS;
            }
        }
    }
    else if (vs[n->ptr] == BT_RUNNING_STALE)
    {
        (*sp)++;
        *(*sp)++ = n->data.rep.op;
        vs[n->ptr] = BT_RUNNING;
    }
    DBPRINT3("update_repeatr max:%d count:%d res:%d\n", n->data.rep.repeat, vs[n->ptr + 1], vs[n->ptr]);
}

static void update_successd(global uchar *bt, ushort **sp, global Node *n, global uchar *vs)
{
    DBPRINT0("enter  update_successd\n");
    if (vs[n->ptr] == BT_INVALID)
    {
        vs[n->ptr] = BT_RUNNING;
        (*sp)++;
        *(*sp)++ = n->data.fix.op;
    }
    else if (vs[n->ptr] == BT_RUNNING)
    {
        global Node *c = (global Node*)(bt + n->data.fix.op);
        if (vs[c->ptr] == BT_RUNNING_STALE)
        {
            (*sp)++;
            *(*sp)++ = n->data.fix.op;
        }
        else
        {
            vs[n->ptr] = BT_SUCCESS;
        }
    }
    else if (vs[n->ptr] == BT_RUNNING_STALE)
    {
        (*sp)++;
        *(*sp)++ = n->data.fix.op;
        vs[n->ptr] = BT_RUNNING;
    }
    DBPRINT1("return  update_successd %d\n", vs[n->ptr]);
}

static void update_failured(global uchar *bt, ushort **sp, global Node *n, global uchar *vs)
{
    DBPRINT0("enter  update_failured\n");
    if (vs[n->ptr] == BT_INVALID)
    {
        vs[n->ptr] = BT_RUNNING;
        (*sp)++;
        *(*sp)++ = n->data.fix.op;
        DBPRINT1("failure set to running, push node %04x\n", n->data.fix.op);
    }
    else if (vs[n->ptr] == BT_RUNNING)
    {
        global Node *c = (global Node*)(bt + n->data.fix.op);
        if (vs[c->ptr] == BT_RUNNING_STALE)
        {
            (*sp)++;
            *(*sp)++ = n->data.fix.op;
            DBPRINT1("failure already running and child running, push node %04x\n", n->data.fix.op);
            
        }
        else
        {
            vs[n->ptr] = BT_FAILURE;
            DBPRINT0("failure already running and child finished\n");
        }
    }
    else if (vs[n->ptr] == BT_RUNNING_STALE)
    {
        (*sp)++;
        *(*sp)++ = n->data.fix.op;
        vs[n->ptr] = BT_RUNNING;
    }
    DBPRINT1("return  update_failured %d\n", vs[n->ptr]);
}

static void update_invert(global uchar *bt, ushort **sp, global Node *n, global uchar *vs)
{
    DBPRINT0("enter  update_invert\n");
    if (vs[n->ptr] == BT_INVALID)
    {
        vs[n->ptr] = BT_RUNNING;
        (*sp)++;
        *(*sp)++ = n->data.fix.op;
    }
    else if (vs[n->ptr] == BT_RUNNING)
    {
        global Node *c = (global Node*)(bt + n->data.fix.op);
        if (vs[c->ptr] == BT_RUNNING_STALE)
        {
            (*sp)++;
            *(*sp)++ = n->data.fix.op;
        }
        else
        {
            vs[n->ptr] = (vs[c->ptr] == BT_SUCCESS) ? BT_FAILURE : BT_SUCCESS;
        }
    }
    else if (vs[n->ptr] == BT_RUNNING_STALE)
    {
        (*sp)++;
        *(*sp)++ = n->data.fix.op;
        vs[n->ptr] = BT_RUNNING;
    }
    DBPRINT1("return  update_invert %d\n", vs[n->ptr]);
}

//----------------------------------------------------------------------------------
// Action nodes
//----------------------------------------------------------------------------------
static void update_movcs(global uchar *bt, global Node *n, global uchar *vs, global uchar *bb)
{
    // dest <- (float)i8
    //
    // General purpose scalar move constant, but need to special case if we are
    // targetting the motor goal vector (or perhaps any other interface
    // to reality that takes time to operate) so that it doesn't succeed
    // if a previous motor command is still in action.
    // Physical blackboard entries are set to NaN at the start of the cycle,
    // if they are not NaN, they cannot be updated this cycle.
    global float *vars = (global float*)bb;
    char dest = n->data.mvc.dest;
    if (physical[dest] && !isnan(vars[dest]))
        vs[n->ptr] = BT_RUNNING;
    else
    {
        if (writeable[dest])
            vars[dest] = (float)n->data.mvc.i;
        vs[n->ptr] = BT_SUCCESS;
    }
    DBPRINT2("update_movcs bb[%d]<=%d\n", n->data.mvc.dest, n->data.mvc.i);
}

static void update_movcv(global uchar *bt, global Node *n, global uchar *vs, global uchar *bb)
{
    // dest <- vec(1, angle (float)i8/128)
    //
    // General purpose vector move constant, but need to special case if we are
    // targetting the motor goal vector (or perhaps any other interface
    // to reality that takes time to operate) so that it doesn't succeed
    // if a previous motor command is still in action.
    // Physical blackboard entries are set to NaN at the start of the cycle,
    // if they are not NaN, they cannot be updated this cycle.
    char dest = n->data.mvc.dest & 0xfe;
    char i = n->data.mvc.i;
    global float2 *var = (global float2*)(bb + (dest << 2));
    if (physical[dest] && !isnan((*var).x))
        vs[n->ptr] = BT_RUNNING;
    else
    {
        vs[n->ptr] = BT_SUCCESS;
        if (writeable[dest])
        {
            *var = (float2)INIT2(COS(i), SIN(i));
        }
    }
    DBPRINT4("update_movcv bb[%d]<=(%f,%f) i:%d\n", n->data.mvc.dest,
             (*var).x, (*var).y, n->data.mvc.i);
}

static void update_mulas(global uchar *bt, global Node *n, global uchar *vs, global uchar *bb)
{
    // dest <- src1 + f32 * src2
    //
    // Scalar scale and add
    global float *vars = (global float*)bb;
    char dest = n->data.mvc.dest;
    if (physical[dest] && !isnan(vars[dest]))
        vs[n->ptr] = BT_RUNNING;
    else
    {
        if (writeable[dest])
        {
            vars[dest] = vars[n->data.mac.src1] + vars[n->data.mac.src2] * n->data.mac.f;
        }
        vs[n->ptr] = BT_SUCCESS;
    }
    DBPRINT8("update_mulas st:%d bb[%d]<=%f src1:%d:%f f:%f src2:%d:%f\n", vs[n->ptr], dest, vars[dest],
             n->data.mac.src1, vars[n->data.mac.src1], n->data.mac.f, n->data.mac.src2, vars[n->data.mac.src2]);
}

static void update_mulav(global uchar *bt, global Node *n, global uchar *vs, global uchar *bb)
{
    // dest <- src1 + f32 * src2
    //
    // Vector scale and add
    global float2 *vars = (global float2*)bb;
    char dest = n->data.mac.dest & 0xfe;
    if (physical[dest] && !isnan(vars[dest >> 1].x))
        vs[n->ptr] = BT_RUNNING;
    else
    {
        if (writeable[dest])
        {
            DBPRINT1("writeable %d\n", dest);
#ifndef _ROBOT
            vars[dest >> 1] = vars[(n->data.mac.src1) >> 1] + vars[(n->data.mac.src2) >> 1] * n->data.mac.f;
#else
            vars[dest >> 1].x = vars[(n->data.mac.src1) >> 1].x + vars[(n->data.mac.src2) >> 1].x * n->data.mac.f;
            vars[dest >> 1].y = vars[(n->data.mac.src1) >> 1].y + vars[(n->data.mac.src2) >> 1].y * n->data.mac.f;
#endif
        }
        vs[n->ptr] = BT_SUCCESS;
    }
    DBPRINT11("update_mulav st:%d bb[%d]<=(%f,%f) src1:%d (%f,%f) f:%f src2:%d (%f,%f)\n", vs[n->ptr], n->data.mac.dest,
              vars[(n->data.mac.dest) >> 1].x, vars[(n->data.mac.dest) >> 1].y,
              n->data.mac.src1, vars[(n->data.mac.src1) >> 1].x, vars[(n->data.mac.src1) >> 1].y,
              n->data.mac.f,
              n->data.mac.src2, vars[(n->data.mac.src2) >> 1].x, vars[(n->data.mac.src2) >> 1].y);
    
}

static void update_rotav(global uchar *bt, global Node *n, global uchar *vs, global uchar *bb)
{
    // dest <- src1 + R(pi*i/128) * src2
    //
    // Vector rotate and add
    global float2 *vars = (global float2*)bb;
    char dest = n->data.rac.dest & 0xfe;
    if (physical[dest] && !isnan(vars[dest >> 1].x))
        vs[n->ptr] = BT_RUNNING;
    else
    {
        if (writeable[dest])
        {
            DBPRINT1("writeable %d\n", dest);
            float th = M_PI * (float)n->data.rac.i / 128;
            float sth = sin(th);
            float cth = cos(th);
            float2 s2 = vars[(n->data.rac.src2) >> 1];
#ifndef _ROBOT
            vars[dest >> 1] = vars[(n->data.rac.src1) >> 1] + (float2)(cth * s2.x - sth * s2.y, sth * s2.x + cth * s2.y);
#else
            vars[dest >> 1].x = vars[(n->data.rac.src1) >> 1].x + cth * s2.x - sth * s2.y;
            vars[dest >> 1].y = vars[(n->data.rac.src1) >> 1].y + sth * s2.x + cth * s2.y;
#endif
        }
        vs[n->ptr] = BT_SUCCESS;
    }
    DBPRINT10("update_rotav bb[%d]<=(%f,%f) src1:%d (%f,%f) i:%d src2:%d (%f,%f)\n", n->data.rac.dest,
              vars[(n->data.rac.dest) >> 1].x, vars[(n->data.rac.dest) >> 1].y,
              n->data.rac.src1, vars[(n->data.rac.src1) >> 1].x, vars[(n->data.rac.src1) >> 1].y,
              n->data.rac.i,
              n->data.rac.src2, vars[(n->data.rac.src2) >> 1].x, vars[(n->data.rac.src2) >> 1].y);
    
}

static void update_ifquad(global uchar *bt, global Node *n, global uchar *vs, global uchar *bb)
{
    // See if a vector is in a particular quad
    global float2 *vars = (global float2*)bb;
    float2 v        = vars[n->data.ifq.src >> 1];
    float len       = fast_length(v);
    Status result    = BT_FAILURE;
    // Quadrants are numbered anticlockwise starting from 1 .
    // Quadrant 0 is a special case and is true if the length of the vector is less than 0.1
    int quad = abs(n->data.ifq.i) % 5;
    quad = n->data.ifq.i < 0 ? 5 - quad : quad;
    if (len > 0.1f)
    {
        if (    ((quad == 1) && (v.x >= 0) && (v.y >= 0))
            ||  ((quad == 2) && (v.x <  0) && (v.y >= 0))
            ||  ((quad == 3) && (v.x <  0) && (v.y <  0))
            ||  ((quad == 4) && (v.x >= 0) && (v.y <  0)))
            result = BT_SUCCESS;
    }
    else if (quad == 0)
        result = BT_SUCCESS;
    DBPRINT6("update_ifquad src:%d (%f,%f:%f) quad:%d res:%d\n", n->data.ifq.src, v.x, v.y, atan2pi(v.y, v.x), n->data.ifq.i, result);
    vs[n->ptr] = result;
}

static void update_ifsect(global uchar *bt, global Node *n, global uchar *vs, global uchar *bb)
{
    // See if a vector is in a particular quad
    global float2 *vars = (global float2*)bb;
    float2 v        = vars[n->data.ifs.src >> 1];
    float len       = fast_length(v);
    int i = n->data.ifs.i;
    float sectangle = M_PI * (float)n->data.ifs.i / 128;
    float sectwidth = M_PI * (float)n->data.ifs.j / 256;
    Status result    = BT_FAILURE;

    if (len > 0.1f)
    {
        float angle = atan2(v.y, v.x);
        if (fabs(angle - sectangle) < sectwidth)
            result = BT_SUCCESS;
    }
    else if (sectwidth == 0)
        result = BT_SUCCESS;
    
    
    DBPRINT9("update_ifsect src:%d (%f,%f:%f) %d %d angle:%f width:%f res:%d\n", n->data.ifs.src, v.x, v.y, atan2pi(v.y, v.x), 
        n->data.ifs.i, n->data.ifs.j, sectangle, sectwidth, result);
    vs[n->ptr] = result;
}

static void update_ifprob(global uchar *bt, global Node *n, global uchar *vs, global uchar *bb)
{
    // success if
    global float *vars = (global float*)bb;
    float src   = vars[n->data.ifp.src];
    float k     = (float)n->data.ifp.k / 8;
    float l     = (float)n->data.ifp.l / 8;
    float p     = 1 / (1 + exp(k * (l - src)));
    float r     = rand_realrange(bb - 8, 0, 1);
    Status result    = r < p ? BT_SUCCESS : BT_FAILURE;
    DBPRINT6("update_ifprob src:%f k:%f l:%f p:%f r:%f res:%d\n", src, k, l, p, r, result);
    vs[n->ptr] = result;
}



static void update(global uchar *bt, ushort **sp, global Node *n, global uchar *vs, global uchar *bb)
{
    
    switch (n->type)
    {
            // composition
        case SEQ2:      update_seq(bt, sp, n, vs, 2); return;
        case SEQ3:      update_seq(bt, sp, n, vs, 3); return;
        case SEQ4:      update_seq(bt, sp, n, vs, 4); return;
        case SEL2:      update_sel(bt, sp, n, vs, 2); return;
        case SEL3:      update_sel(bt, sp, n, vs, 3); return;
        case SEL4:      update_sel(bt, sp, n, vs, 4); return;
        case SEQM2:     update_seqm(bt, sp, n, vs, 2); return;
        case SEQM3:     update_seqm(bt, sp, n, vs, 3); return;
        case SEQM4:     update_seqm(bt, sp, n, vs, 4); return;
        case SELM2:     update_selm(bt, sp, n, vs, 2); return;
        case SELM3:     update_selm(bt, sp, n, vs, 3); return;
        case SELM4:     update_selm(bt, sp, n, vs, 4); return;
        case REPEATI:   update_repeati(bt, sp, n, vs); return;
        case REPEATR:   update_repeatr(bt, sp, n, vs, bb); return;
        case SUCCESSD:  update_successd(bt, sp, n, vs); return;
        case FAILURED:  update_failured(bt, sp, n, vs); return;
        case INVERT:    update_invert(bt, sp, n, vs); return;
            // action
        case MOVCS:     update_movcs(bt, n, vs, bb); return;
        case MOVCV:     update_movcv(bt, n, vs, bb); return;
        case MULAS:     update_mulas(bt, n, vs, bb); return;
        case MULAV:     update_mulav(bt, n, vs, bb); return;
        case ROTAV:     update_rotav(bt, n, vs, bb); return;
        case IFPROB:    update_ifprob(bt, n, vs, bb); return;
        case IFQUAD:    update_ifquad(bt, n, vs, bb); return;
        case IFSECT:    update_ifsect(bt, n, vs, bb); return;
        case SUCCESSL:  vs[n->ptr] = BT_SUCCESS; DBPRINT0("return successl\n"); return;
        case FAILUREL:  vs[n->ptr] = BT_FAILURE; DBPRINT0("return failurel\n"); return;
        default:
            DBPRINT0("Failure - unhandled update case\n");
            return;
    }
}


static void reset_tree(global uchar *vs, ushort nodes)
{
    // Traverse the tree and reset all node statuses to INVALID except
    // for nodes that are RUNNING, which are set to RUNNING_STALE
#ifdef DEBUG
    DBPRINT0("Node status before reset\n");
    HEXDUMP(vs, nodes << 1);
#endif
    global uchar *vp = vs;
    for (int i = 0; i < nodes; i++)
    {
        if (*vp == BT_RUNNING)
            *vp = BT_RUNNING_STALE;
        else
            *vp = BT_INVALID;
        vp += 2;
    }
#ifdef DEBUG
    DBPRINT0("Node status after reset\n");
    HEXDUMP(vs, nodes << 1);
#endif

}

#ifdef DEBUG
#ifndef __APPLE__
#ifndef _ROBOT
constant char names[32][16] = {
        "SEQ2",       // 0
        "SEQ3",       // 1
        "SEQ4",       // 2
        "SEL2",       // 3
        "SEL3",       // 4
        "SEL4",       // 5
        "SEQM2",      // 6
        "SEQM3",      // 7
        "SEQM4",      // 8
        "SELM2",      // 9
        "SELM3",      // a
        "SELM4",      // b
        "REPEATI",    // 1a
        "REPEATR",    // 1a
        "SUCCESSD",   // 1b
        "FAILURED",   // 1c
        "INVERT",     // 1d
        "MOVCS",       // 12
        "MOVCV",       // 13
        "MULAS",
        "MULAV",
        "ROTAV",
        "IFPROB",
        "IFQUAD",     // 14
        "IFSECT",     // 14
        "SUCCESSL",   // 1e
        "FAILUREL"    // 1f
    };
#endif
#endif
#endif

// There are various sets of data involved in a behaviour tree:
// a) The tree structure itself - this can be shared between all the agents running the
// same tree
// b) The per agent data - the blackboard
// c) The per node status used to actually traverse the tree
// The blackboard must be local to each agent. The per node data can either be localised
// in the tree structure or separate per agent. If we have a homogenous swarm, we save
// resources by sharing the tree structure among all agents running the same tree.
// In our case though, we are likely to want to run simulations with two controllers
// (the two teams) and possibly heterogeneous populations. If this is the case, we don't
// really gain from separating data and tree
static void tick(global uchar *bt, ushort root, global uchar *vs, global uchar *bb, int bbentries)
{
    // Declaring this globally as constant char would segfault
    // when running on GPU OSX (nivdia driver), declared locally as below causes
    // CL_INVALID_PROGRAM_EXECUTABLE on Mali, even though no build problems
#ifdef DEBUG
#if defined(__APPLE__) || defined(_ROBOT)
    const char names[32][16] = {
        "SEQ2",       // 0
        "SEQ3",       // 1
        "SEQ4",       // 2
        "SEL2",       // 3
        "SEL3",       // 4
        "SEL4",       // 5
        "SEQM2",      // 6
        "SEQM3",      // 7
        "SEQM4",      // 8
        "SELM2",      // 9
        "SELM3",      // a
        "SELM4",      // b
        "REPEATI",    // 1a
        "REPEATR",    // 1a
        "SUCCESSD",   // 1b
        "FAILURED",   // 1c
        "INVERT",     // 1d
        "MOVCS",       // 12
        "MOVCV",       // 13
        "MULAS",
        "MULAV",
        "ROTAV",
        "IFPROB",
        "IFQUAD",     // 14
        "IFSECT",
        "SUCCESSL",   // 1e
        "FAILUREL"    // 1f
    };
#endif
#endif

    // Rewrite to avoid recursion
    //
    // Rather than keeping track on the stack, we explicity use the status byte.
    // The state of a tree starts at BT_INVALID and ends at BT_SUCCESS or BT_FAILURE.
    // This may take many calls of the tick() function. Before propagating tick, reset
    // all nodes that are not RUNNING to INVALID, and ones that are RUNNING to RUNNING_STALE
    DBPRINT2("Tick event=================== bt:%x root:%x\n", bt, root);
    //reset_tree(bt, offset);
    ushort nodes = *(global ushort *)(bt + 4);
    reset_tree(vs, nodes);
    DBPRINT0("after reset\n");
#ifdef DEBUG
    DBPRINT1("BB contents after reset: %d entries\n", bbentries);
    HEXDUMP(bb - 8, (bbentries << 2) + 8);
#endif
    //return;
    
    ushort stack[256];
    ushort *sp = stack;
    *sp++ = root;
    //printf("set stack entry\n");
    while (sp != stack)
    {
        // Pop the node off the stack
        global Node *n = (global Node*)(bt + *--sp);
        //printf("got node addr %x data %x %s\n", n, n->type, names[n->type]);
        
        //if ((((ulong)n)&3) != 0)
        //    DBPRINT1("=============>>>misalignment!! %llx\n", (long long)n);
        // Run the update on it and all nodes below
        DBPRINT3("========Got node %llx %2x %s\n", (long long)n, n->type, names[n->type]);
        update(bt, &sp, n, vs, bb);
        if ((sp - stack) > 256)
            printf("Stack overflow!!!! %x\n", sp - stack);
        DBPRINT1("after update, stack is %d\n",(uint)(sp-stack));
    }
}

static void pt_nr(global uchar *bt, ushort offset)
{
    ushort stack[1024];
    ushort *sp = stack;
    *sp++ = offset;
    while (sp != stack)
    {
        global Node *n = (global Node*)(bt+*(--sp));
        // Push children onto stack in reverse order!
        switch(n->type)
        {
            case SEQ2:
            {
                printf("%3d seq2  %04x %04x\n",(uint)(sp-stack), n->data.sm3.op[0], n->data.sm3.op[1]);
                *sp++ = n->data.sm2.op[1];
                *sp++ = n->data.sm2.op[0];
                break;
            }
            case SEQ3:
            {
                printf("%3d seq3  %04x %04x %04x\n",(uint)(sp-stack), n->data.sm3.op[0], n->data.sm3.op[1], n->data.sm3.op[2]);
                *sp++ = n->data.sm3.op[2];
                *sp++ = n->data.sm3.op[1];
                *sp++ = n->data.sm3.op[0];
                break;
            }
            case SEQ4:
            {
                printf("%3d seq4  %04x %04x %04x %04x\n",(uint)(sp-stack), n->data.sm3.op[0], n->data.sm3.op[1], n->data.sm3.op[2], n->data.sm3.op[2]);
                *sp++ = n->data.sm4.op[3];
                *sp++ = n->data.sm4.op[2];
                *sp++ = n->data.sm4.op[1];
                *sp++ = n->data.sm4.op[0];
                break;
            }
            case SEL2:
            {
                printf("%3d sel2  %04x %04x\n",(uint)(sp-stack), n->data.sm3.op[0], n->data.sm3.op[1]);
                *sp++ = n->data.sm2.op[1];
                *sp++ = n->data.sm2.op[0];
                break;
            }
            case SEL3:
            {
                printf("%3d sel3 %04x %04x %04x\n",(uint)(sp-stack), n->data.sm3.op[0], n->data.sm3.op[1], n->data.sm3.op[2]);
                *sp++ = n->data.sm3.op[2];
                *sp++ = n->data.sm3.op[1];
                *sp++ = n->data.sm3.op[0];
                break;
            }
            case SEL4:
            {
                printf("%3d sel4  %04x %04x %04x %04x\n",(uint)(sp-stack), n->data.sm3.op[0], n->data.sm3.op[1], n->data.sm3.op[2], n->data.sm3.op[2]);
                *sp++ = n->data.sm4.op[3];
                *sp++ = n->data.sm4.op[2];
                *sp++ = n->data.sm4.op[1];
                *sp++ = n->data.sm4.op[0];
                break;
            }
            case SEQM2:
            {
                printf("%3d seqm2 %04x %04x\n",(uint)(sp-stack), n->data.sm3.op[0], n->data.sm3.op[1]);
                *sp++ = n->data.sm2.op[1];
                *sp++ = n->data.sm2.op[0];
                break;
            }
            case SEQM3:
            {
                printf("%3d seqm3 %04x %04x %04x\n",(uint)(sp-stack), n->data.sm3.op[0], n->data.sm3.op[1], n->data.sm3.op[2]);
                *sp++ = n->data.sm3.op[2];
                *sp++ = n->data.sm3.op[1];
                *sp++ = n->data.sm3.op[0];
                break;
            }
            case SEQM4:
            {
                printf("%3d seqm4 %04x %04x %04x %04x\n",(uint)(sp-stack), n->data.sm3.op[0], n->data.sm3.op[1], n->data.sm3.op[2], n->data.sm3.op[2]);
                *sp++ = n->data.sm4.op[3];
                *sp++ = n->data.sm4.op[2];
                *sp++ = n->data.sm4.op[1];
                *sp++ = n->data.sm4.op[0];
                break;
            }
            case SELM2:
            {
                printf("%3d selm2 %04x %04x\n",(uint)(sp-stack), n->data.sm3.op[0], n->data.sm3.op[1]);
                *sp++ = n->data.sm2.op[1];
                *sp++ = n->data.sm2.op[0];
                break;
            }
            case SELM3:
            {
                printf("%3d selm3 %04x %04x %04x\n",(uint)(sp-stack), n->data.sm3.op[0], n->data.sm3.op[1], n->data.sm3.op[2]);
                *sp++ = n->data.sm3.op[2];
                *sp++ = n->data.sm3.op[1];
                *sp++ = n->data.sm3.op[0];
                break;
            }
            case SELM4:
            {
                printf("%3d selm4 %04x %04x %04x %04x\n",(uint)(sp-stack), n->data.sm3.op[0], n->data.sm3.op[1], n->data.sm3.op[2], n->data.sm3.op[2]);
                *sp++ = n->data.sm4.op[3];
                *sp++ = n->data.sm4.op[2];
                *sp++ = n->data.sm4.op[1];
                *sp++ = n->data.sm4.op[0];
                break;
            }
            case REPEATI:
            {
                printf("%3d repeati %d\n",(uint)(sp-stack), n->data.rep.repeat);
                *sp++ = n->data.rep.op;
                break;
            }
            case REPEATR:
            {
                printf("%3d repeatr %d\n",(uint)(sp-stack), n->data.rep.repeat);
                *sp++ = n->data.rep.op;
                break;
            }
            case SUCCESSD:
            {
                printf("%3d successd\n",(uint)(sp-stack));
                *sp++ = n->data.fix.op;
                break;
            }
            case FAILURED:
            {
                printf("%3d failured\n",(uint)(sp-stack));
                *sp++ = n->data.fix.op;
                break;
            }
            case INVERT:
            {
                printf("%3d invert\n",(uint)(sp-stack));
                *sp++ = n->data.fix.op;
                break;
            }
            case MOVCS:
            {
                printf("%3d movcs %d %d\n",(uint)(sp-stack), n->data.mvc.dest, n->data.mvc.i);
                break;
            }
            case MOVCV:
            {
                printf("%3d movcv %d %d\n",(uint)(sp-stack), n->data.mvc.dest, n->data.mvc.i);
                break;
            }
            case MULAS:
            {
                printf("%3d mulas %d %d %f %d\n",(uint)(sp-stack), n->data.mac.dest, n->data.mac.src1, n->data.mac.f, n->data.mac.src2);
                break;
            }
            case MULAV:
            {
                printf("%3d mulav %d %d %f %d\n",(uint)(sp-stack), n->data.mac.dest, n->data.mac.src1, n->data.mac.f, n->data.mac.src2);
                break;
            }
            case ROTAV:
            {
                printf("%3d rotav %d %d %d %d\n",(uint)(sp-stack), n->data.rac.dest, n->data.rac.src1, n->data.rac.i, n->data.rac.src2);
                break;
            }
            case IFPROB:
            {
                printf("%3d ifprob %d %d %d\n",(uint)(sp-stack), n->data.ifp.src, n->data.ifp.k, n->data.ifp.l);
                break;
            }
            case IFQUAD:
            {
                printf("%3d ifquad %d %d\n",(uint)(sp-stack), n->data.ifq.src, n->data.ifq.i);
                break;
            }
            case IFSECT:
            {
                printf("%3d ifsect %d %d %d\n",(uint)(sp-stack), n->data.ifq.src, n->data.ifq.i, n->data.ifp.l);
                break;
            }
            case SUCCESSL:
            {
                printf("%3d successl\n",(uint)(sp-stack));
                break;
            }
            case FAILUREL:
            {
                printf("%3d failurel\n",(uint)(sp-stack));
                break;
            }
                
                
        }
        
        
    }
    
}

#define WRAPPI(x) atan2(sin(x),cos(x))

static void ptree(global uchar *t)
{
    ushort r = *(global ushort *)t;

    //if (idx == 0)
    {
        printf("Root: %x sizeof(NodeType)%d\n", r,sizeof(Nodetype));
        global unsigned char *p = t;
        for (int i = 0; i < 256; i+= 16)
        {
            printf("%04x  ", i);
            for (int j = 0; j < 16; j++)
                printf("%02x ", *p++);
            printf("\n");
        }

        pt_nr(t, r);
    }
}

static void controller
(
 global uchar                *t,
 global uchar                *v,
 global struct SceneSense    *p,
 global struct SceneControl  *c,
 global struct Params        *pr,
 uint                        idx,
 float                       home_x,
 float                       home_y,
 float                       home_th
#ifdef _ROBOT
    ,
    bool                    verbose
#endif
 )
{
#ifdef DEBUG
    DBPRINT6("gid:%d t:%lx v:%lx p:%lx c:%lx idx:%x\n", get_global_id(0), (ulong)t, (ulong)v, (ulong)p, (ulong)c, idx);
//    if ((get_global_id(0) == 0))
//    {
        ushort tsize = *(global ushort *)(t+2);
        HEXDUMP(t, tsize)
    
//        ushort tsize = *(global ushort *)(t+2);
//        DBPRINT4("Root: %x size: %d nodes: %d bb: %d\n", *(global ushort *)t,
//            tsize, *(global ushort *)(t+4), *(global ushort *)(t+6));
//        global uchar *p = t;
//        for (int i = 0; i < 256; i+= 16)
//        {
//            DB2PRINT1("%04x  ", i);
//            for (int j = 0; j < 16; j++)
//            {
//                DB2PRINT1("%02x ", *p++);
//                if (p - t == tsize)
//                {
//                    i = 256;
//                    break;
//                }
//            }
//            DB2PRINT0("\n");
//        }
//    }
#endif
    //return;
    
    // Pointer t points to this scenes Behaviour Tree memory. The memory contains the root, and
    // the tree itself
    // Byte address
    //  0x0     root (ushort)
    //  0x2     total size (ushort)
    //  0x4     number of nodes (ushort)
    //  0x6     number of blackboard entries (ushort)
    //  0x8     tree
    //
    // Pointer v points to this agents variable tree storage. This contains the random seed, the
    // blackboard variables, and two chars per tree node, one for status, one for idx or count for
    // the nodes that need it.
    // Byte address
    //  0x0         random seed 0
    //  0x8         bb entry 0  goal_vector.x
    //  0xc                     goal_vector.y
    //  0x
    //  ...
    //  vstart      node 0 status
    //  vstart+2    node 1 status
    //  ...
    //
    ushort r = *(global ushort *)t;
    ushort bbentries = *(global ushort *)(t + 6);
    global uchar *vs = v + 4 * bbentries + 8;
    global struct BB *bb = (global struct BB*)(v + 8);
    
    // Make sure zero register is zero
    bb->vzero = (float2)INIT2(0.0f, 0.0f);
    
    // Clear goal vector before tick. If a move action node is active, the
    // revised vector will be placed in here
    //printf("%x\n", (global uchar*)&(bb->goal) - t);
    bb->vgoal = (float2)INIT2(NAN, NAN);
    
    // Construct the proximity sensor vector
    global struct PoseSense *sense = p->sense + idx;
    // sense->prox is an array of integer values representing mm*100, ranging from MIN_PROX_RANGE
    // to MAX_PROX_RANGE. Turn these into values between 0 and 1, where 0 is nothing
    // within range and 1 is adjacent to the sensor, create a vector of that length and
    // with the angle of the sensor, and sum the vectors
    DB2PRINT8("%3d %3d %3d %3d %3d %3d %3d %3d\n",sense->prox[0],sense->prox[1],sense->prox[2],sense->prox[3],sense->prox[4],sense->prox[5],sense->prox[6],sense->prox[7]);
    bb->vprox = convert_prox(sense->prox, pr);
    
    // Construct upfield vector
    bb->vup = (float2)INIT2((float)cos(-sense->theta), (float)sin(-sense->theta));
    
    // Construct the attraction vector. When no neighbours, just point forward, this makes
    // the attract macro easier to write
    // ##FIXME## write this in a more vector friendly way, define rmin somewhere better
    float rmin = 0.035;
    if (sense->n == 0)
        bb->vattr = (float2)INIT2(1.0f, 0.0f);
    else
    {
        bb->vattr = sense->vattr;
        //bb->vattr = (float2)INIT2(0.0f, 0.0f);
        //for (int i = 0; i < sense->n; i++)
        //{
        //    float len = rmin / sense->range[i];
#ifndef _ROBOT
        //    bb->vattr += len * (float2)(cos(sense->bearing[i]), sin(sense->bearing[i]));
#else
        //    bb->vattr.x += len * cos(sense->bearing[i]);
        //    bb->vattr.y += len * sin(sense->bearing[i]);
#endif
        //}
    }
    // Construct the blob detect vectors
    //if (get_global_id(0)==0)printf("blobs l:%d c:%d r:%d\n",
    //                               (sense->blobs >> 6) & 7, (sense->blobs >> 3) & 7, sense->blobs & 7);
    
    int bl = (sense->blobs >> 6) & 1;
    int bc = (sense->blobs >> 3) & 1;
    int br = (sense->blobs >> 0) & 1;
#ifndef _ROBOT
    bb->vred    = bl * camleft + bc * camcentre + br * camright;
#else
    bb->vred.x  = bl * camleft.x + bc * camcentre.x + br * camright.x;
    bb->vred.y  = bl * camleft.y + bc * camcentre.y + br * camright.y;
#endif
    bl = (sense->blobs >> 7) & 1;
    bc = (sense->blobs >> 4) & 1;
    br = (sense->blobs >> 1) & 1;
#ifndef _ROBOT
    bb->vgreen  = bl * camleft + bc * camcentre + br * camright;
#else
    bb->vgreen.x    = bl * camleft.x + bc * camcentre.x + br * camright.x;
    bb->vgreen.y    = bl * camleft.y + bc * camcentre.y + br * camright.y;
#endif
    bl = (sense->blobs >> 8) & 1;
    bc = (sense->blobs >> 5) & 1;
    br = (sense->blobs >> 2) & 1;
#ifndef _ROBOT
    bb->vblue   = bl * camleft + bc * camcentre + br * camright;
#else
    bb->vblue.x = bl * camleft.x + bc * camcentre.x + br * camright.x;
    bb->vblue.y = bl * camleft.y + bc * camcentre.y + br * camright.y;
#endif
    
    // Set neighbour count
    bb->sn = (float)sense->n;

    // Construct home vector and distance
    // This is based on privileged information i.e the real pose. The real pose
    // is in the world frame, so rotate into the robot frame to give a robot-relative
    // vector
#if 0
    float2 hv = (float2)INIT2(home_x - sense->position.x, home_y - sense->position.y);
    bb->vhome.x =  cos(-sense->theta) * hv.x - sin(-sense->theta) * hv.y;
    bb->vhome.y =  sin(-sense->theta) * hv.x + cos(-sense->theta) * hv.y ;
#else
    bb->vhome.x = sense->position.x;
    bb->vhome.y = sense->position.y;
#endif
    // We construct a vector such that when it is used as a velocity goal
    // vector, pointing behind the robot, it induces the correct on-the-spot
    // turning to reduce the angle error
    float dth = WRAPPI(home_th - sense->theta);
    bb->vth.x   = sense->theta;
    bb->vth.y   = dth;

    
    // Tick the behaviour tree
    tick(t, r, vs, (global uchar *)bb, bbentries);
    

    


// Limit for writeable BB entries
#define BBLIMIT (1e9)
    
    // Access the control for this agent
    global struct Control *ctrl = c->control + idx;
    //global struct Control *ctrl = c->control;
    
    // See if we need to do some motor actions. Since NaN is used to indicate an unwritten
    // goal vector, if there is a NaN present, the goal cannot have been written and
    // so the wheel velocity must be set to zero
    if (isnan(bb->vgoal.x))  bb->vgoal.x = 0.0f;
    if (isnan(bb->vgoal.y))  bb->vgoal.y = 0.0f;
    
    // Ensure that exponentially increasing values don't propagate into goal.
    // This ensures that we don't get infinities in the physics, which can cause
    // illegal accesses
    bb->vgoal = fmax(fmin(bb->vgoal, (float)BBLIMIT), (float)-BBLIMIT);
    // Also clamp the other writeable BB registers
    bb->vscr = fmax(fmin(bb->vscr, (float)BBLIMIT), (float)-BBLIMIT);
    bb->sscr = fmax(fmin(bb->sscr, (float)BBLIMIT), (float)-BBLIMIT);



    // Dump the blackboard registers
#ifdef _ROBOT
#undef DB2PRINT2
#define DB2PRINT2(fmt, ...) printf(fmt, ##__VA_ARGS__)
#undef DB2PRINT3
#define DB2PRINT3(fmt, ...) printf(fmt, ##__VA_ARGS__)
    if (verbose)
    {
#endif
    DB2PRINT3("%2d  2 goal:%8.5f %8.5f\n", idx, bb->vgoal.x, bb->vgoal.y);
    DB2PRINT3("%2d  4 prox:%8.5f %8.5f\n", idx, bb->vprox.x, bb->vprox.y);
    DB2PRINT3("%2d  6   up:%8.5f %8.5f\n", idx, bb->vup.x, bb->vup.y);
    DB2PRINT2("%2d theta  :%8.5f\n",       idx, sense->theta);
    DB2PRINT3("%2d  8 attr:%8.5f %8.5f\n", idx, bb->vattr.x, bb->vattr.y);
    DB2PRINT3("%2d  8 attr:%8.5f %8.5f\n", idx, sense->vattr.x, sense->vattr.y);
    DB2PRINT3("%2d 10  red:%8.5f %8.5f\n", idx, bb->vred.x, bb->vred.y);
    DB2PRINT3("%2d 12  grn:%8.5f %8.5f\n", idx, bb->vgreen.x, bb->vgreen.y);
    DB2PRINT3("%2d 14 blue:%8.5f %8.5f\n", idx, bb->vblue.x, bb->vblue.y);
    DB2PRINT2("%2d 16   sn:%8f\n",         idx, bb->sn);
    DB2PRINT2("%2d 17 sscr:%8.5f\n",       idx, bb->sscr);
    DB2PRINT3("%2d 18 vscr:%8.5f %8.5f\n", idx, bb->vscr.x, bb->vscr.y);
    DB2PRINT3("%2d 20 vhom:%8.5f %8.5f\n", idx, bb->vhome.x, bb->vhome.y);
    DB2PRINT3("%2d 22  vth:%8.5f %8.5f\n", idx, bb->vth.x, bb->vth.y);
    DB2PRINT2("%2d fitness:%8.5f\n",       idx, sense->fitness);
#ifdef _ROBOT
    }
#endif



    // Now work out the motor action and local fitness
    float scale = 0;
    float capped_vel_mag = 0;
    if ((bb->vgoal.x != 0.0f) || (bb->vgoal.y != 0.0f))
    {
        // Cap velocity vector at length 1, if x is negative, map to x=0, y=mag*sign(y).
        // This results in the behaviour that a goal velocity pointing behind xpuck will cause
        // xpuck to rotate until facing right way.
        // We limit the rotation speed to a proportion of the max possible because of
        // problems using the camera in high slew rates
        // clip x to positive, rotate 45 degrees, normalise and scale
        //float2 rscale = half_rsqrt(bb->vgoal);
        //float2 capped_vel = rscale < 1.0f ? bb->vgoal * rscale : bb->vgoal;
        scale = fast_length(bb->vgoal);

            float2 capped_vel = scale > 1.0f ? (float2)INIT2(bb->vgoal.x / scale, bb->vgoal.y / scale) : bb->vgoal;
        CHECKNAN("bt capped_vel", capped_vel.x)
        CHECKNAN("bt capped_vel", capped_vel.y)
        capped_vel_mag = length(capped_vel);
        CHECKNAN("bt l", capped_vel_mag)
        //if (isnan(l))printf("scale %f %f %f\n",scale, bb->vgoal.x, bb->vgoal.y);
        float2 mappos = capped_vel.x < 0.0f ? (float2)INIT2(0.0f, capped_vel.y > 0 ? capped_vel_mag : -capped_vel_mag) : capped_vel;
        float2 rotv = (float2)INIT2(mappos.x * (float)M_SQRT1_2 - mappos.y * (float)M_SQRT1_2,
                                    mappos.x * (float)M_SQRT1_2 + mappos.y * (float)M_SQRT1_2);
        ctrl->motor.x = pr->max_velocity * rotv.x;
        ctrl->motor.y = pr->max_velocity * rotv.y;
        //float mx = pr->max_velocity * rotv.x;
        //float my = pr->max_velocity * rotv.y;
        //float omega_ratio = fabs((my - mx) / (2 * pr->wheelbase_radius * pr->max_omega));
        //ctrl->motor.x = omega_ratio > 1.0 ? mx / omega_ratio : mx;
        //ctrl->motor.y = omega_ratio > 1.0 ? my / omega_ratio : my;
        //DB2PRINT3("Motor %d %f %f\n", (int)get_global_id(0), ctrl->motor.x, ctrl->motor.y);
        //}
    }
    else
        ctrl->motor = (float2)INIT2(0.0f, 0.0f);
    CHECKNAN("bt motor l", ctrl->motor.x)
    CHECKNAN("bt motor r", ctrl->motor.y)
    // At this point, calculate the change to the local fitness. This is based
    // only on the robot perceptions and actuations, ie the contents of the
    // blackboard.
    //
    // For pushing the frisbee to the left, we make the local fitness change positive if:
    //  facing west
    //  moving forward
    //  seeing blue in all segments (>2 in x only met in this case)
    float push_const        = 0.2;
    float approach_const    = 0.01;
    float move_const        = 0.001;
    float diff_const        = 0.1;
    float dt = 0.1;
    bool distant_blue = (bb->vblue.x > 0) && (bb->vblue.x <= 2);
    bool close_blue = bb->vblue.x > 2.0;
    bool moving_forwards = (capped_vel_mag > 0.5) && (fabs(atan2(bb->vgoal.y, bb->vgoal.x)) < 0.25 * M_PI);
    bool facing_west = fabs(sense->theta) > 0.5 * M_PI;
    {
        float f_incr = 0;
        if (distant_blue && moving_forwards)
            f_incr = approach_const;
        else if (close_blue && moving_forwards && facing_west)
            f_incr = push_const;
        else if (close_blue && moving_forwards && !facing_west)
            f_incr = -push_const;
        else if (moving_forwards)
            f_incr = move_const;
        
        

        sense->fitness += (// Fitness
                           +f_incr
                           // Diffusion term
                           - diff_const * (sense->n * sense->fitness - sense->adj_fitness)
                           ) * dt;
    }
    
    
    
}


#ifdef _ROBOT
// Undefine this, used to make the OpenCL into legal C++, otherwise it will
// conflict with later stuff
#undef global
#endif





