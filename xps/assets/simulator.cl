
#include "bthelpers.h"

//#define DEBUG
//#define DEBUG2



#define STRINGIFY2(x) #x
#define STRINGIFY(x) STRINGIFY2(x)


//------------------------------------------------------------------------------
// Datatype definitions
//------------------------------------------------------------------------------
constant float EPSILON = 0.0001f;

typedef float4 Mat2;
typedef float2 Vec2;


#define SENSE_SCALE         100000

// Camera horizontal FOV 56 degrees
#define FOV                 0.97738
#define BINS                15
#define BINSIZE             (FOV/BINS)
#define BIN0                (FOV/2)
#define BIN1                (BIN0 - BINSIZE)
#define BIN2                (BIN1 - BINSIZE)
#define BIN3                (BIN2 - BINSIZE)
#define BIN4                (BIN3 - BINSIZE)
#define BIN5                (BIN4 - BINSIZE)
#define BIN6                (BIN5 - BINSIZE)
#define BIN7                (BIN6 - BINSIZE)
#define BIN8                (BIN7 - BINSIZE)
#define BIN9                (BIN8 - BINSIZE)
#define BIN10               (BIN9 - BINSIZE)
#define BIN11               (BIN10 - BINSIZE)
#define BIN12               (BIN11 - BINSIZE)
#define BIN13               (BIN12 - BINSIZE)
#define BIN14               (BIN13 - BINSIZE)
#define BIN15               (BIN14 - BINSIZE)

#define BYTE2RAD            (M_PI/128.0f)






//------------------------------------------------------------------------------
// Main code
//------------------------------------------------------------------------------
// Helper functions
static Mat2 rotm(float radians)
{
    float c = half_cos(radians);
    float s = half_sin(radians);
    return (Mat2)(c, -s, s, c);
}
static Mat2 multmm(Mat2 a, Mat2 b)
{
    return (Mat2)(a.s0 * b.s0 + a.s1 * b.s2,
                  a.s0 * b.s1 + a.s1 * b.s3,
                  a.s2 * b.s0 + a.s3 * b.s2,
                  a.s2 * b.s1 + a.s3 * b.s3);
}
static Vec2 multmv(Mat2 a, Vec2 b)
{
    return (Vec2)(a.s0 * b.x + a.s1 * b.y,
                  a.s2 * b.x + a.s3 * b.y);
}
static Mat2 transpose(Mat2 m)
{
    return (Mat2)(m.s0, m.s2,
                  m.s1, m.s3);
}
static float dist_sqr(Vec2 a, Vec2 b)
{
    return dot(a - b, a - b);
}
static Vec2 crossvf(const Vec2 v, float a)
{
    return (Vec2)(a * v.y, -a * v.x);
}

static Vec2 crossfv(float a, const Vec2 v)
{
    return (Vec2)(-a * v.y, a * v.x);
}
static float crossvv(const Vec2 a, const Vec2 b)
{
    return a.x * b.y - a.y * b.x;
}

// Functions operating on body
static void apply_force(global struct Body *b, Vec2 f)
{
    b->force += f;
}
static void body_apply_impulse(global struct Body *b, Vec2 impulse, Vec2 contact_vector)
{
    b->velocity += b->im * impulse;
    b->omega    += b->iI * crossvv(contact_vector, impulse);
    
    //float cvv =crossvv(contact_vector, impulse);
    //printf("delta omega %f imp:(%f %f) cv:(%f %f) cvv:%f\n", b->iI * crossvv(contact_vector, impulse),impulse.x,impulse.y,contact_vector.x,contact_vector.y, cvv);

    //if (isnan(b->velocity.x))
    //    printf("%f %f %f\n", b->im, impulse.x, impulse.y);
}

// Collision functions
static uint  circle_to_circle(global struct Scene *s, struct Manifold *m, uint a, uint b)
{
    global struct Body  *ab = s->bodies + a;
    global struct Body  *bb = s->bodies + b;
    // Calculate translational vector, which is normal
    Vec2 normal     = bb->position - ab->position;
    
    //float dist_sqr  = normal.LenSqr( );
    float dist_sqr  = dot(normal, normal);
    float radius    = ab->radius + bb->radius;
    //printf("checking circle to circle %f %f\n", dist_sqr, radius);
    
    // Not in contact
    if (dist_sqr >= radius * radius)
    {
        return 0;
    }
    
    float distance  = half_sqrt(dist_sqr);
    //printf("dist %f\n", distance);
    
    
    //if(distance == 0.0f)
    //{
    //    m->penetration  = ab->radius;
    //    m->normal       = (Vec2)(1, 0);
    //    m->contact      = ab->position;
    //}
    //else
    {
        m->penetration  = radius - distance;
        m->normal       = normal / distance; // Faster than using Normalized since we already performed sqrt
        m->contact      = m->normal * ab->radius + ab->position;
    }
    return 1;
}

static uint  circle_to_circle_sensor(global struct Scene *s, struct Manifold *m, uint a, uint b, float sr)
{
    global struct Body  *ab = s->bodies + a;
    global struct Body  *bb = s->bodies + b;
    // Calculate translational vector, which is normal
    Vec2 normal     = bb->position - ab->position;
    
    //float dist_sqr  = normal.LenSqr( );
    float dist      = fast_length(normal);
    float radius    = sr + bb->radius;
    //printf("checking circle to circle %f %f\n", dist_sqr, radius);
    
    // Not in contact
    if (dist >= radius)
    {
        return 0;
    }
    
    //float distance  = half_sqrt(dist_sqr);
    //printf("dist %f\n", distance);
    
    
    //if(distance == 0.0f)
    //{
    //    m->penetration  = sr;
    //    m->normal       = (Vec2)(1, 0);
    //    m->contact      = ab->position;
    //}
    //else
    {
        m->penetration  = radius - dist;
        m->normal       = normal / dist; // Faster than using Normalized since we already performed sqrt
        m->contact      = m->normal * (dist - bb->radius) + ab->position;
    }
    //if (isnan(m->normal.x)||isnan(m->normal.y))
    //    printf("%f %f %f %f %f\n", ab->position.x,ab->position.y,bb->position.x,bb->position.y, dist_sqr);
    return 1;
}

static uint  circle_to_line(global struct Scene *s, struct Manifold *m, uint a, uint b)
{
    global struct Body  *ab = s->bodies + a;
    global struct Body  *bb = s->bodies + b;
    // Calculate normal from circle to line
    Vec2 normal     = (bb->type == HLINE    ? (Vec2)(ab->position.x, bb->position.y)
                                            : (Vec2)(bb->position.x, ab->position.y)) - ab->position;
    
    float distance  = bb->type == HLINE     ? fabs(ab->position.y - bb->position.y)
                                            : fabs(ab->position.x - bb->position.x);
    float radius    = ab->radius;
    
    // Not in contact
    if (distance >= radius)
    {
        return 0;
    }
    
    
    if(distance == 0.0f)
    {
        m->penetration  = radius;
        m->normal       = (Vec2)(1, 0);
        m->contact      = ab->position;
    }
    else
    {
        m->penetration  = radius - distance;
        m->normal       = normal / distance; // Faster than using Normalized since we already performed sqrt
        m->contact      = m->normal * radius + ab->position;
    }
    return 1;
}

static uint  circle_to_line_sensor(global struct Scene *s, struct Manifold *m, uint a, uint b, float sr)
{
    global struct Body  *ab = s->bodies + a;
    global struct Body  *bb = s->bodies + b;
    // Calculate normal from circle to line
    Vec2 normal     = (bb->type == HLINE    ? (Vec2)(ab->position.x, bb->position.y)
                                            : (Vec2)(bb->position.x, ab->position.y)) - ab->position;
    
    float distance  = bb->type == HLINE     ? fabs(ab->position.y - bb->position.y)
                                            : fabs(ab->position.x - bb->position.x);
    float radius    = sr;
    
    // Not in contact
    if (distance >= radius)
    {
        return 0;
    }
    
    
    if(distance == 0.0f)
    {
        m->penetration  = radius;
        m->normal       = (Vec2)(1, 0);
        m->contact      = ab->position;
    }
    else
    {
        m->penetration  = radius - distance;
        m->normal       = normal / distance; // Faster than using Normalized since we already performed sqrt
        m->contact      = m->normal * distance + ab->position;
    }
    return 1;
}

static int clip(Vec2 n, float c, Vec2 *face)
{
    uint sp = 0;
    Vec2 out[2] = {
        face[0],
        face[1]
    };
    
    // Retrieve distances from each endpoint to the line
    // d = ax + by - c
    float d1 = dot(n, face[0]) - c;
    float d2 = dot(n, face[1]) - c;
    
    // If negative (behind plane) clip
    if (d1 <= 0.0f) out[sp++] = face[0];
    if (d2 <= 0.0f) out[sp++] = face[1];
    
    // If the points are on different sides of the plane
    if(d1 * d2 < 0.0f) // less than to ignore -0.0f
    {
        // Push interesection point
        float alpha = d1 / (d1 - d2);
        out[sp] = face[0] + alpha * (face[1] - face[0]);
        ++sp;
    }
    
    // Assign our new converted values
    face[0] = out[0];
    face[1] = out[1];
    
    //assert( sp != 3 );
    return sp;
}

//----------------------------------------------------------------------
// Integration is carried out using the leapfrog method
// https://en.wikipedia.org/wiki/Leapfrog_integration
// Compared to Euler, this is stable and conserves energy (symplectic), but
// uses the same number of calculations
//
// x(i+1) = x(i) + v(i)dt + 1/2 a(i)dt^2
// v(i+1) = v(i) + 1/2(a(i) + a(i+1))dt
//
// Call sequence is:
// integrate_forces()
// integrate_velocity()
// integrate_forces()
//
static void integrate_forces(global struct Body *b, float dt )
{
    if(b->im == 0.0f)
        return;
    CHECKNAN("forcex", b->force.x)
    CHECKNAN("forcey", b->force.y)

    //printf("%f %f %f %f %f %f\n",b->velocity.x,b->velocity.y,b->force.x, b->force.y,b->im, dt);
    b->velocity += b->force * b->im * dt;
    b->omega    += b->torque * b->iI * dt;

    //if (isnan(b->velocity.x))
    //    printf("integrate force %f %f %f %f %f\n",dt, b->force.x, b->force.y, b->im, b->iI );

}

static void integrate_velocity(global struct Scene *s, uint b, global struct Params *p)
{
    global struct Body  *bb = s->bodies + b;
    
    if(bb->im == 0.0f)
        return;
    float dt = p->dt;
    //printf("integrate vel %d %f %f %f\n",b, dt, bb->velocity.x, bb->velocity.y );
    
    // Apply noise model. This is as described in Thrun - Probabilistic Robotics
    // p98. Noise added is gaussian, with mean zero and variance related to the current
    // linear and angular velocity. Three pairs of parameters describe the noise
    // components variances. Noise is added to linear and angular velocities, and to
    // the pose angle.
    // v'           = v + sample(a1 * |v| + a2 * |omega|)
    // omega'       = omega + sample(a3 * |v| + a4 * |omega|)
    // gamma        = sample(a5 * |v| + a6 * |omega|)
    // position'    = position + v' * dt
    // theta'       = theta + (omega' + gamma) * dt
    
    float   fv      = fast_length(bb->velocity);


#if 0
    float4  fz      =   (float4)(bb->velocity, bb->omega, 0) *
                        (float4)((float2)(  1 + rand_norm((global uchar *)s, p->alpha1)),
                                            1 + rand_norm((global uchar *)s, p->alpha3),
                                            0) * dt;
    bb->position    += fz.s01;
    bb->theta       += fz.s2;
#else
    // This formulation factionally faster
    //bb->position    += bb->velocity * (1 + rand_norm((global uchar *)s, p->alpha1)) * dt;
    //bb->theta       += bb->omega * (1 + rand_norm((global uchar *)s, p->alpha3)) * dt;
    float n1 = normtab[(*(global uint *)s)++ & 0xff];
    float n2 = normtab[(*(global uint *)s)++ & 0xff];
    float n3 = normtab[(*(global uint *)s)++ & 0xff];
    bb->position    += bb->velocity * (1 + n1 * p->alpha1) * dt;
    bb->theta       += (bb->omega * (1  + n2 * p->alpha2)
                            + fv * n3 * p->alpha3) * dt;
    CHECKNAN("positionx",bb->position.x)
    CHECKNAN("positiony",bb->position.y)
    CHECKNAN("velocityx",bb->velocity.x)
    CHECKNAN("velocityy",bb->velocity.y)
#endif

    
}

static void integrate_velocity_fit(global struct Scene *s, global struct SceneSense *p, uint b, global struct Params *pr)
{
    global struct Body  *bb = s->bodies + b;
    global struct PoseSense *ps = p->sense + b;

    if(bb->im == 0.0f)
        return;
    //printf("integrate vel %d %f %f %f\n",b, dt, bb->velocity.x, bb->velocity.y );
    float dt = pr->dt;
    
    float2 ds       = bb->velocity * dt;
    bb->position    += ds;
    bb->theta       += bb->omega * dt;
    
    ps->fitness     -= ds.x;
    
    CHECKNAN("positionx",bb->position.x)
    CHECKNAN("positiony",bb->position.y)
    CHECKNAN("velocityx",bb->velocity.x)
    CHECKNAN("velocityy",bb->velocity.y)
    
    
}



static void apply_impulse(struct Manifold *m, uint i, global struct Params *params)
{
    // Early out and positional correct if both objects have infinite mass
    struct Manifold *p = m + i;
    global struct Body *A = p->A;
    global struct Body *B = p->B;
    
    if (fabs(A->im + B->im) <= EPSILON)
    {
        A->velocity = (float2)(0.0f, 0.0f);
        B->velocity = (float2)(0.0f, 0.0f);
        return;
    }
    
    // Calculate radii from COM to contact
    float2 ra = p->contact - A->position;
    float2 rb = ((B->type == HLINE) || (B->type == VLINE)) ? (float2)(0,0) : p->contact - B->position;
    
    // Relative velocity
    float2 rv = B->velocity + crossfv(B->omega, rb) - A->velocity - crossfv(A->omega, ra);
    
    // Relative velocity along the normal
    float contact_vel = dot(rv, p->normal);
    
    //if(get_global_id(0)==0)printf("%d %10f %10f %10f %10f %10f\n", i, contact_vel, A->position.x, A->position.y, B->position.x, B->position.y);
    
    // Do not resolve if velocities are separating
    if(contact_vel > 0)
        return;
    

    float ra_cross_n = crossvv(ra, p->normal);
    float rb_cross_n = crossvv(rb, p->normal);
    float inv_mass_sum = A->im + B->im + ra_cross_n * ra_cross_n * A->iI + rb_cross_n * rb_cross_n * B->iI;
    
    //if (get_global_id(0)==0)printf("av:(%f %f) ao:%f bv:(%f %f) bo:%f raxn:%f rbxn:%f ims:%f cv:%f\n", A->velocity.x, A->velocity.y, A->omega,B->velocity.x, B->velocity.y, B->omega, ra_cross_n, rb_cross_n, inv_mass_sum, contact_vel);

    // Calculate impulse scalar
    float j = -(1.0f + params->restitution) * contact_vel;
    j /= inv_mass_sum;
    //if (isnan(j))
    //    printf("%f %f %f %f\n", A->im, B->im, p->normal.x, p->normal.y);
    
    // Apply impulse
    float2 impulse = p->normal * j;
    // if force < mu N, we dont change the velocity
    float f = fast_length(impulse) / params->dt;
    //if (f > A->mu / A->im * 9.8f)
        body_apply_impulse(A, -impulse, ra);
    //if (f > B->mu / B->im * 9.8f)
        body_apply_impulse(B, impulse, rb);
    //printf("imp:% 10f % 10f  f:% 10f  A:% 10f B:% 10f\n", impulse.x, impulse.y, f, A->mu / A->im * 9.8f,  B->mu / B->im * 9.8f);
    
    //if (get_global_id(0)==0)printf("av:(%f %f) ao:%f bv:(%f %f) bo:%f\n", A->velocity.x, A->velocity.y, A->omega,B->velocity.x, B->velocity.y, B->omega);

    // Friction impulse
    //rv = B->velocity + crossfv(B->omega, rb) - A->velocity - crossfv(A->omega, ra);
    
    // tangential relative velocity
    float2 trv = rv - (p->normal * dot(rv, p->normal));
    float2 t = normalize(trv);
    
    // j tangent magnitude
    float jt = -dot(rv, t) / inv_mass_sum;
    
    // Don't apply tiny friction impulses
    if(fabs(jt) <= EPSILON)
        return;
    
    // Coulumb's law
    float2 tangent_impulse;
    //if(fabs(jt) < j * params->dynamic_friction)
    //    tangent_impulse = t * jt;
    //else
        tangent_impulse = t * -j * (params->dynamic_friction * tanh(params->k2 * fast_length(trv)));
    // Apply friction impulse
    //body_apply_impulse(A, -tangent_impulse, ra);
    //body_apply_impulse(B, tangent_impulse, rb);
    
    // This is a bit of a hack to stop the situation where the friction impulse causes
    // unphysical changes in the rotational velocity. We just limit the maximum rotational
    // acceleration. In the real world, an xpuck can optimistically change from 4 to -4 rad/s
    // in one control cycle of 0.1s, so 80 rad/s/s. At a physics update period of 0.025,
#define MAX_OMEGA_DOT 2.0f
    A->velocity += A->im * -tangent_impulse;
    A->omega    += clamp(A->iI * crossvv(ra, -tangent_impulse), -MAX_OMEGA_DOT, MAX_OMEGA_DOT);
    
    B->velocity += B->im * tangent_impulse;
    B->omega    += clamp(B->iI * crossvv(rb, tangent_impulse), -MAX_OMEGA_DOT, MAX_OMEGA_DOT);
    
    //float2 rv2 = B->velocity + crossfv(B->omega, rb) - A->velocity - crossfv(A->omega, ra);


    //if (get_global_id(0)==0)printf("t:(%f %f) j:%f rv:(%f %f) ti:(%f %f) ra:%f rb:%f av:(%f %f) ao:%f bv:(%f %f) bo:%f AiI:%f nx:(%f %f) rv2:(%f %f)\n", t.x,t.y,j,rv.x,rv.y,tangent_impulse.x, tangent_impulse.y,ra,rb, A->velocity.x, A->velocity.y, A->omega,B->velocity.x, B->velocity.y, B->omega, A->iI, trv.x, trv.y, rv2.x,rv2.y);

}

static void positional_correction(struct Manifold *m, uint i)
{
    struct Manifold     *p          = m + i;
    global struct Body  *A          = p->A;
    global struct Body  *B          = p->B;
    const float         k_slop      = 0.001f; // Penetration allowance
    const float         percent     = 0.4f; // Penetration percentage to correct
    float2              correction  = (max(p->penetration - k_slop, 0.0f) / (A->im + B->im)) * p->normal * percent;
    
    //if(get_global_id(0)==0)printf("%d %10f %10f\n", i, p->penetration, correction);
    A->position -= correction * A->im;
    B->position += correction * B->im;

}

static uint bin_and_prune(global struct Scene *s, uint *bodies, float binsize, bool sensor, global struct Params *params)
{
    // A collision may exist if the x pos of two bodies is in the same or adjacent bins.
    // The maximum number of separate bins we can have is the maximum body count
    int bins[MAX_BODIES];
    // Assume 32 or less bodies (true at the moment) and use bitfield to represent if body
    // present in this bin
    //uint bodies[MAX_BODIES];
    for (uint i = 0; i < MAX_BODIES; i++)
        bins[i] = -999999;
    uint b_count = 0;
    // if this is a sensor check, we don't detect objects
    uint num_bodies = params->robot_count + (sensor ? 0 : params->object_count);
    for (uint i = 0; i < num_bodies; i++)
    {
        // break out if H or VLINE type, there will be no further
        // CIRCLEs
        if ((s->bodies[i].type == HLINE) || (s->bodies[i].type == VLINE))
            break;
        int bin = floor(s->bodies[i].position.x / binsize);
        char found = false;
        char insert = false;
        uint p = 0;
        for (uint j = 0; j < b_count; j++)
        {
            if (bins[j] == bin)
            {
                // bin exists, set bit corresponding to body
                bodies[j] |= 1 << i;
                found = true;
                break;
            }
            if (bins[j] > bin)
            {
                // gone beyond our bin value, save the insertion point
                p = j;
                insert = true;
                break;
            }
        }
        if (!found)
        {
            // we didn't find the bin, so do an insertion sort,
            // shuffle higher values along
            if (insert)
            {
                for (uint j = b_count; j > p; j--)
                {
                    bodies[j]   = bodies[j - 1];
                    bins[j]     = bins[j - 1];
                }
            }
            else
            {
                // Append to end
                p = b_count;
            }
            // insert value
            bins[p]     = bin;
            bodies[p]   = 1 << i;
            b_count++;
        }
        //if (get_global_id(0)==0)printf("%2d %10f %2d %2d %2d\n", i, s->bodies[i].position.x, bin, p, found);
    }
    return b_count;
}

static void generate_contacts(global struct Scene *s, uint b_count, uint *bodies, uint *m_count, struct Manifold *m, char sensor, float sr)
{
//    if (get_global_id(0)==0)
//    {
//        for(uint i = 0; i < b_count; i++)
//            printf("%2d %08x\n", i, bodies[i]);
//    }
    // bins are now in sorted order,
    // scan and generate possible colliding pairs
    //
    // b_count is the number of bins
    // bodies[] contains the sorted bins. Each entry is a 32 bit vector with  bits
    // set according to the bodies present in this bin
    uint match;
    for (uint i = 0; i < b_count; i++)
    {
        match = bodies[i] | ((i == b_count - 1) ? 0 : bodies[i + 1]);

        // Only here if there are two or more hits in this and adjacent bins, ie potential collisions,
        // so now check the collision for real
        while (popcount(match) > 1)
        {
            uint a  = 31 - clz(match);
            match   &= ~(1 << a);
            
            uint match_inner = match;
            while (popcount(match_inner) > 0)
            {
                uint b      = 31 - clz(match_inner);
                match_inner &= ~(1 << b);
                
                uint contact_count = sensor ?   circle_to_circle_sensor(s, m + *m_count, a, b, sr)
                                            :   circle_to_circle(s, m + *m_count, a, b);
                if (contact_count)
                {
                    // Only take notice of this manifold if there are actually
                    // contacts in it
                    m[*m_count].A = s->bodies + a;
                    m[*m_count].a = a;
                    m[*m_count].B = s->bodies + b;
                    (*m_count)++;
                }
                if (sensor)
                {
                    // Perform check the other way round to get sensing in both directions
                    contact_count = circle_to_circle_sensor(s, m + *m_count, b, a, sr);
                    if (contact_count )
                    {
                        // Only take notice of this manifold if there are actually
                        // contacts in it
                        m[*m_count].A = s->bodies + b;
                        m[*m_count].a = b;
                        m[*m_count].B = s->bodies + a;
                        (*m_count)++;
                    }
                }
            }
        }
    }
    
}

static void generate_line_contacts(global struct Scene *s, uint *m_count, struct Manifold *m, char sensor, global struct Params *params)
{
    // Now generate contacts with the HLINE and VLINE types
    // These are not binned, so need to be handled separately
    // dummy has infinite mass as physical properties
    global struct Body *bc = s->bodies;
    global struct Body *dummy = s->bodies + params->robot_count + params->object_count;
    for (uint i = 0; i < params->robot_count + params->object_count; i++)
    {

        Vec2 pos = bc->position;
        float radius = sensor ? params->sensor_max_radius : bc->radius;

        
        // Explicit early out
        if ((pos.x + radius >= params->border.x) || (pos.y + radius >= params->border.y) ||
            (pos.x - radius <= -params->border.x) || (pos.y - radius <= -params->border.y))
        {

            // Calculate distances and normals
            // +x,+y,-x,-y
            float4 distance     = fabs((float4)(pos, pos) -  (float4)(params->border, -params->border));
            // +x,+y,-y,-y
            // only the non-zero component
            float4 normal       = (float4)(params->border.x, params->border.y, -params->border.x, -params->border.y)
                                - (float4)(pos.x, pos.y, pos.x, pos.y);
//            if (isnan(normal.s0)||isnan(normal.s1)||isnan(normal.s2)||isnan(normal.s3)
//                ||isnan(distance.s0)||isnan(distance.s1)||isnan(distance.s2)||isnan(distance.s3)
//                ||(distance.s0==0)||(distance.s1==0)||(distance.s2==0)||(distance.s3==0))
//                printf("dist:%8f,%8f,%8f,%8f nx:%8f,%8f,%8f,%8f %d\n",
//                       distance.s0,distance.s1,distance.s2,distance.s3,
//                       normal.s0,normal.s1,normal.s2,normal.s3, dummy->type);
            
//            if(get_global_id(0)==0)printf("dist:%8f,%8f,%8f,%8f nx:%8f,%8f,%8f,%8f %d\n",
//                                          distance.s0,distance.s1,distance.s2,distance.s3,
//                                          normal.s0,normal.s1,normal.s2,normal.s3, dummy->type
//                                          );
            if ((distance.s0 < radius))
            {
                // positive x
                m[*m_count].penetration = radius - distance.s0;
                m[*m_count].normal      = (float2)(normal.s0, 0) / distance.s0;
                m[*m_count].contact     = (float2)(normal.s0, 0) + pos;
                m[*m_count].A = bc;
                m[*m_count].a = i;
                m[*m_count].B = dummy;
                (*m_count)++;
            }
            else if ((distance.s2 < radius))
            {
                // negative x
                m[*m_count].penetration = radius - distance.s2;
                m[*m_count].normal      = (float2)(normal.s2, 0) / distance.s2;
                m[*m_count].contact     = (float2)(normal.s2, 0) + pos;
                m[*m_count].A = bc;
                m[*m_count].a = i;
                m[*m_count].B = dummy;
                (*m_count)++;
            }

            if ((distance.s1 < radius))
            {
                // positive y
                m[*m_count].penetration = radius - distance.s1;
                m[*m_count].normal      = (float2)(0, normal.s1) / distance.s1;
                m[*m_count].contact     = (float2)(0, normal.s1) + pos;
                m[*m_count].A = bc;
                m[*m_count].a = i;
                m[*m_count].B = dummy;
                (*m_count)++;
            }
            else if ((distance.s3 < radius))
            {
                // negative y
                m[*m_count].penetration = radius - distance.s3;
                m[*m_count].normal      = (float2)(0, normal.s3) / distance.s3;
                m[*m_count].contact     = (float2)(0, normal.s3) + pos;
                m[*m_count].A = bc;
                m[*m_count].a = i;
                m[*m_count].B = dummy;
                (*m_count)++;
            }
        }
        bc++;
    }
    
}

// This is hardwired to just larger than robot diameter, arena is assumed to be no larger than
// 2m x 2m
#define CBINSIZE 0.08f
//#define NUM_CBINS ((int)((2.0 / CBINSIZE) + 1))
#define NUM_CBINS (26)
#define ROBOTOBJSEP (CBINSIZE * 4)
static uint generate_robot_contacts(global struct Scene *s, uint *m_count, struct Manifold *m, global struct Params *params)
{
// binning  = 24.6
// naive    = 54.9

    // A collision may exist if the x pos of two bodies is in the same or adjacent bins.
    uint xbins[NUM_CBINS];
    uint ybins[NUM_CBINS];
    uint rcontacts = 0;
    uint nrobots = params->robot_count;
    for (uint i = 0; i < NUM_CBINS; i++)
        xbins[i] = ybins[i] = 0;
    
    for (uint i = 0; i < nrobots; i++)
    {
        int xbin = floor(s->bodies[i].position.x / CBINSIZE) + NUM_CBINS / 2;
        int ybin = floor(s->bodies[i].position.y / CBINSIZE) + NUM_CBINS / 2;
#if 0
        if ((xbin < 0) || (xbin > 25) || (ybin < 0) || (ybin > 25))
        {
            printf("bad bin %d %d %d %f %f\n", i, xbin, ybin, s->bodies[i].position.x, s->bodies[i].position.y);
        }
#endif
        xbins[xbin] |= 1 << i;
        ybins[ybin] |= 1 << i;
    }
    // Potential collisions will have two or more bits set in the same or adjacent bins
    uint match;
    for (uint i = 0; i < NUM_CBINS - 1; i++)
    {
        match = xbins[i] | xbins[i + 1];
        while (popcount(match) > 1)
        {
            uint a  = 31 - clz(match);
            float ay = s->bodies[a].position.y;
            match   &= ~(1 << a);
            
            uint match_inner = match;
            while (popcount(match_inner) > 0)
            {
                uint b      = 31 - clz(match_inner);
                match_inner &= ~(1 << b);
                // quick check to early out
                if (fabs(ay - s->bodies[b].position.y) > CBINSIZE)
                    continue;
                
                uint contact_count = circle_to_circle(s, m + *m_count, a, b);
                if (contact_count)
                {
                    // Only take notice of this manifold if there are actually
                    // contacts in it
                    m[*m_count].A = s->bodies + a;
                    m[*m_count].a = a;
                    m[*m_count].B = s->bodies + b;
                    (*m_count)++;
                }
            }
        }
    }
    rcontacts = *m_count;
#if 1
    // now check the line contacts
    global struct Body *dummy = s->bodies + params->robot_count + params->object_count;
    match = xbins[0] | xbins[NUM_CBINS-1] | ybins[3] | ybins[4] | ybins[NUM_CBINS-5] | ybins[NUM_CBINS-4] | (1 << params->robot_count);
    while (popcount(match) > 0)
    {
        uint i = 31 - clz(match);
        match   &= ~(1 << i);

        float2 pos = s->bodies[i].position;
        float radius = s->bodies[i].radius;
        float4 distance     = fabs((float4)(pos, pos) -  (float4)(params->border, -params->border));
        // +x,+y,-x,-y
        // only the non-zero component
        float4 normal       = (   (float4)( params->border.x,   params->border.y,   -params->border.x,  -params->border.y)
                                - (float4)( pos.x,              pos.y,              pos.x,              pos.y)
                                ) / distance;

        if ((distance.s0 < radius))
        {
            // positive x
            m[*m_count].penetration = radius - distance.s0;
            m[*m_count].normal      = (float2)(normal.s0, 0);
            m[*m_count].contact     = (float2)(normal.s0, 0) + pos;
            m[*m_count].A = s->bodies + i;
            m[*m_count].a = i;
            m[*m_count].B = dummy;
            (*m_count)++;
        }
        else if ((distance.s2 < radius) )
        {
            // negative x
            m[*m_count].penetration = radius - distance.s2;
            m[*m_count].normal      = (float2)(normal.s2, 0);
            m[*m_count].contact     = (float2)(normal.s2, 0) + pos;
            m[*m_count].A = s->bodies + i;
            m[*m_count].a = i;
            m[*m_count].B = dummy;
            (*m_count)++;
        }
        
        if ((distance.s1 < radius))
        {
            // positive y
            m[*m_count].penetration = radius - distance.s1;
            m[*m_count].normal      = (float2)(0, normal.s1);
            m[*m_count].contact     = (float2)(0, normal.s1) + pos;
            m[*m_count].A = s->bodies + i;
            m[*m_count].a = i;
            m[*m_count].B = dummy;
            (*m_count)++;
        }
        else if ((distance.s3 < radius))
        {
            // negative y
            m[*m_count].penetration = radius - distance.s3;
            m[*m_count].normal      = (float2)(0, normal.s3);
            m[*m_count].contact     = (float2)(0, normal.s3) + pos;
            m[*m_count].A = s->bodies + i;
            m[*m_count].a = i;
            m[*m_count].B = dummy;
            (*m_count)++;
        }

    }
#endif



    // Assumes only a single object, which is 3x the size of xpuck
    float2 obj = s->bodies[nrobots].position;
    for (uint a = 0; a < nrobots; a++)
    {
        float2 sep = fabs(s->bodies[a].position - obj);
        if ((sep.x < ROBOTOBJSEP) && (sep.y < ROBOTOBJSEP))
        {
            uint contact_count = circle_to_circle(s, m + *m_count, a, nrobots);
            if (contact_count && (*m_count < MAX_PAIRS))
            {
                // Only take notice of this manifold if there are actually
                // contacts in it
                m[*m_count].A = s->bodies + a;
                m[*m_count].a = a;
                m[*m_count].B = s->bodies + nrobots;
                (*m_count)++;
            }

        }
    }
    return rcontacts;
}

static uint generate_sensor_contacts(global struct Scene *s, global struct SceneSense *p, uint *m_count, struct Manifold *m, global struct Params *params)
{
#define SBINSIZE 0.18f
//#define NUM_SBINS ((int)((2.0 / SBINSIZE) + 1))
#define NUM_SBINS 12
    // A collision may exist if the x pos of two bodies is in the same or adjacent bins.
    uint xbins[NUM_SBINS];
    uint ybins[NUM_SBINS];
    uint rcontacts;
    uint nrobots = params->robot_count;
    float sr = params->sensor_max_radius;
    for (uint i = 0; i < NUM_SBINS; i++)
        xbins[i] = ybins[i] = 0;
    
    for (uint i = 0; i < nrobots; i++)
    {
        int xbin = floor(s->bodies[i].position.x / SBINSIZE) + NUM_SBINS / 2;
        xbins[xbin] |= 1 << i;
        int ybin = floor(s->bodies[i].position.y / SBINSIZE) + NUM_SBINS / 2;
        ybins[ybin] |= 1 << i;
    }
    // Potential collisions will have two or more bits set in the same or adjacent bins
    uint match;
    for (uint i = 0; i < NUM_SBINS - 1; i++)
    {
        match = xbins[i] | xbins[i + 1];
        while (popcount(match) > 1)
        {
            uint a  = 31 - clz(match);
            float ay = s->bodies[a].position.y;
            match   &= ~(1 << a);
            
            uint match_inner = match;
            while (popcount(match_inner) > 0)
            {
                uint b      = 31 - clz(match_inner);
                match_inner &= ~(1 << b);
                // quick check to early out
                if (fabs(ay - s->bodies[b].position.y) > SBINSIZE)
                    continue;
                
                //uint contact_count = circle_to_circle_sensor(s, m + *m_count, a, b, params->sensor_radius);
                global struct Body  *ab = s->bodies + a;
                global struct Body  *bb = s->bodies + b;
                // Calculate translational vector, which is normal
                float2 v = bb->position - ab->position;
                float2 normal   = fast_normalize(v);
                float distance  = fast_length(v);
                float radius    = sr + bb->radius;
                //printf("checking circle to circle %f %f\n", dist_sqr, radius);
                
                // Not in contact
                if (distance >= radius)
                    continue;

                m[*m_count].penetration = radius - distance;
                m[*m_count].normal      = normal;
                m[*m_count].contact     = normal * (distance - bb->radius) + ab->position;
                m[*m_count].A           = s->bodies + a;
                m[*m_count].a           = a;
                m[*m_count].B           = s->bodies + b;
                (*m_count)++;
                m[*m_count].penetration = radius - distance;
                m[*m_count].normal      = -normal;
                m[*m_count].contact     = -normal * (distance - bb->radius) + bb->position;
                m[*m_count].A           = s->bodies + b;
                m[*m_count].a           = b;
                m[*m_count].B           = s->bodies + a;
                (*m_count)++;

                
            }
        }
    }
    rcontacts = *m_count;
#if 1
    global struct Body *dummy = s->bodies + params->robot_count + params->object_count;
    match = xbins[0] | xbins[NUM_SBINS-1] | ybins[2] | ybins[NUM_SBINS-3] ;
    //match = 0xffff;
    while (popcount(match) > 0)
    {
        uint i = 31 - clz(match);
        match   &= ~(1 << i);

        float2 pos = s->bodies[i].position;
        float radius = params->sensor_max_radius;
        float4 distance     = fabs((float4)(pos, pos) -  (float4)(params->border, -params->border));
        // +x,+y,-x,-y
        // only the non-zero component
        float4 normal       = (   (float4)( params->border.x,   params->border.y,   -params->border.x,  -params->border.y)
                                - (float4)( pos.x,              pos.y,              pos.x,              pos.y)
                                );

        if ((distance.s0 < radius))
        {
            // positive x
            m[*m_count].penetration = sr - distance.s0;
            m[*m_count].normal      = (float2)(normal.s0, 0) / distance.s0;
            m[*m_count].contact     = (float2)(normal.s0, 0) + pos;
            m[*m_count].A = s->bodies + i;
            m[*m_count].a = i;
            m[*m_count].B = dummy;
            (*m_count)++;
        }
        else if ((distance.s2 < radius) )
        {
            // negative x
            m[*m_count].penetration = sr - distance.s2;
            m[*m_count].normal      = (float2)(normal.s2, 0) / distance.s2;
            m[*m_count].contact     = (float2)(normal.s2, 0) + pos;
            m[*m_count].A = s->bodies + i;
            m[*m_count].a = i;
            m[*m_count].B = dummy;
            (*m_count)++;
        }
        
        if ((distance.s1 < radius))
        {
            // positive y
            m[*m_count].penetration = sr - distance.s1;
            m[*m_count].normal      = (float2)(0, normal.s1) / distance.s1;
            m[*m_count].contact     = (float2)(0, normal.s1) + pos;
            m[*m_count].A = s->bodies + i;
            m[*m_count].a = i;
            m[*m_count].B = dummy;
            (*m_count)++;
        }
        else if ((distance.s3 < radius))
        {
            // negative y
            m[*m_count].penetration = sr - distance.s3;
            m[*m_count].normal      = (float2)(0, normal.s3) / distance.s3;
            m[*m_count].contact     = (float2)(0, normal.s3) + pos;
            m[*m_count].A = s->bodies + i;
            m[*m_count].a = i;
            m[*m_count].B = dummy;
            (*m_count)++;
        }

    }
#endif
    return rcontacts;
}


static void physics(global struct Scene *s, global struct SceneSense *p, global struct Params *params)
{
    //printf("physics\n");
    struct Manifold m[MAX_PAIRS];
    uint m_count = 0;
    uint bodies[MAX_BODIES];
    uint b_count;

    float dt = params->dt;
    // Find collisions

    // 3.39
    // Use binning for robots (all the same size), and AABB for robot vs object
    generate_robot_contacts(s, &m_count, m, params); // 7.56 +4.17
    //generate_line_contacts(s, &m_count, m, false, params); // 8.75 +1.19
    barrier(CLK_GLOBAL_MEM_FENCE);

    //printf("gpu %d %d %f\n", s->body_count, m_count, dt);
    // Integrate forces 10.27 +1.52
    for(uint i = 0; i < params->body_count; ++i)
        integrate_forces(s->bodies + i, dt);
    barrier(CLK_GLOBAL_MEM_FENCE);

    // Initialize collision 10.04 +0.0
    //for(uint i = 0; i < m_count; ++i)
    //    initialise(m, i);

    // Solve collisions 10.75 +0.71
    for(uint j = 0; j < params->iterations; ++j)
        for(uint i = 0; i < m_count; ++i)
            apply_impulse(m, i, params);
    barrier(CLK_GLOBAL_MEM_FENCE);
    
    // Integrate velocities 12.72 +1.97
#if 0
    for(uint i = 0; i < params->body_count; ++i)
        integrate_velocity(s, i, params);
#else
    for(uint i = 0; i < params->robot_count; ++i)
        integrate_velocity(s, i, params);
    integrate_velocity_fit(s, p, params->robot_count, params);
#endif
    barrier(CLK_GLOBAL_MEM_FENCE);

    // Correct positions 13.11 +0.39
    for(uint i = 0; i < m_count; ++i)
        positional_correction(m, i);
    
    // Clear all forces 13.37 +0.26
    for(uint i = 0; i < params->body_count; ++i)
    {
        s->bodies[i].force = (Vec2)(0.0f, 0.0f);
        s->bodies[i].torque = 0.0f;
    }
    barrier(CLK_GLOBAL_MEM_FENCE);

}

static void kinematics(global struct Scene *s, global struct SceneControl *c, float dt, global struct Params *params)
{
    //printf("kinematics\n");
    for (uint i = 0; i < params->robot_count + params->object_count; i++)
    {
        global struct Body      *b      = s->bodies + i;
        global struct Control   *ctrl   = c->control + i;
        //printf("kinematics:%d:%d %f %f\n", get_global_id(0), i, ctrl->motor.x, ctrl->motor.y);
        
        // Radius is different between frisbee object and xpuck robots, the xpucks
        // wheels are not on the dircumference, so we use the wheelbase radius to place
        // then inside the actual radius. The frisbee actually effectively makes contact with
        // the arena in a ring around the actual radius, so using two points with zero
        // motor velocity is a reasonable approximation
        float radius = i < params->robot_count ? params->wheelbase_radius : b->radius * params->k3;
        //float radius = params->wheelbase_radius;
        // Objects with infinite mass are fixed
        if (b->im == 0)
            continue;
        // Basis of the model is two-wheel drive with tyres modelled as point contact.
        // Use wheel, linear, and angular velocity to get resultant velocity vectors
        // for each tyre. These translate to resultant forces, giving updates to linear and
        // angular velocities
        //
        // Friction is modelled as coulomb friction with mu altered close to zero
        // to avoid discontinuity:
        // \cite{williams2002dynamic}
        // mu = mu_max * 2/pi * arctan(k * v)
        // "We chose k=1000 by eye and to ensure numerical stability in Simulink"
        // They measured mu on different surfaces and report mu_max = 0.26 (in transverse
        // direction it is much lower, but they are using omniwheels).
        // Measuring actual epucks on workbench with force gauge gave mu = 0.4
        //float k = 0.1;
        float k = params->k1;
        


        // Add wheel velocity and velocity due to rotation in robot frame, then
        // transform resultant velocity to world frame
        CHECKNAN("wheel l", ctrl->motor.x)
        CHECKNAN("wheel r", ctrl->motor.y)
        Mat2 transform          = rotm(b->theta);
        Vec2 wheel_left_vel     = multmv(transform, (Vec2)(-ctrl->motor.x - b->omega * radius, 0.0f));
        Vec2 wheel_right_vel    = multmv(transform, (Vec2)(-ctrl->motor.y + b->omega * radius, 0.0f));
        

        // Now add robot linear velocity
        wheel_left_vel  += b->velocity;
        wheel_right_vel += b->velocity;
        //if (i==1)printf("v:(% 10f % 10f % 10f) ", wheel_left_vel.x, wheel_left_vel.y, b->omega);

        

        // Get unit velocity vectors
        float ll = fast_length(wheel_left_vel);
        float lr = fast_length(wheel_right_vel);

    
        //printf("%f %f\n", wheel_left_vel.x, wheel_left_unit_vel.x);

        Vec2 wheel_left_force   = 0.0f;
        Vec2 wheel_right_force  = 0.0f;
        
        // Factor of 0.5 because each wheel provides half the normal force
        if (ll > 0.0 && lr > 0.0)
        {
            Vec2 wheel_left_unit_vel    = normalize(wheel_left_vel);
            Vec2 wheel_right_unit_vel   = normalize(wheel_right_vel);
            wheel_left_force   = - b->mu / b->im * 9.8f * 0.5f * wheel_left_unit_vel * tanh(k * ll);
            wheel_right_force  = - b->mu / b->im * 9.8f * 0.5f * wheel_right_unit_vel * tanh(k * lr);
        }
        //printf("%f %f %f %f\n", b->mu, b->im, wheel_left_unit_vel.x, wheel_left_unit_vel.y);
        //printf("f:(% 10f % 10f % 10f % 10f)\n", wheel_left_force.x, wheel_left_force.y, wheel_right_force.x, wheel_right_force.y);

#if 1
        // If the force due to friction such that it would result in a 'large' change in velocity, reduce
        // so that we don't get unphysical effects. This is intended to deal with the problem with descrete
        // time simulation of friction. Friction should only ever reduce the relative velocity magnitude,
        // but if we are integrating force, we need to ensure that force is never large enough to do that.
        // This is an approximation; we only know about the wheels and assume that if the force is larger
        // than some proportion of the force required to achieve the velocity magnitude, we reduce the force
        // applied. The previous fix to make the sim stable was the reduction of k to a low value, 0.3, which resulted
        // in very unrealistically low friction forces at low velocities.
        Vec2 maxfl = wheel_left_vel / (b->im * dt);
        Vec2 maxfr = wheel_right_vel / (b->im * dt);
        float scalel = fast_length(wheel_left_force) / fast_length(maxfl);
        float scaler = fast_length(wheel_right_force) / fast_length(maxfr);
        float k2 = 4.0f;
        if (scalel > 1.0f / k2) wheel_left_force    /= k2 * scalel;
        if (scaler > 1.0f / k2) wheel_right_force   /= k2 * scaler;
#endif
        // Resultant force
        Vec2 resultant_force = wheel_left_force + wheel_right_force;

        // Resultant torque, transform to robot frame and take moments
        Mat2 rtransform = rotm(-b->theta);
        Vec2 lf_robot = multmv(rtransform, wheel_left_force);
        Vec2 rf_robot = multmv(rtransform, wheel_right_force);
        

        float tau = (rf_robot.x - lf_robot.x) * radius;

        // integrate force in physics
        b->force = resultant_force;
        b->torque = tau;
        
//        {
//            Vec2 maxfl = wheel_left_vel / (b->im * dt);
//            Vec2 maxfr = wheel_right_vel / (b->im * dt);
//            float scalel = fast_length(wheel_left_force) / fast_length(maxfl);
//            float scaler = fast_length(wheel_right_force) / fast_length(maxfr);
//            Vec2 delta_v        = resultant_force * b->im * dt;
//            if (i==1)printf("lenf:% 10f % 10f  vel:% 10f % 10f dvl:% 10f % 10f sc:% 10f % 10f\n",
//                        resultant_force.x, resultant_force.y, b->velocity.x, b->velocity.y, delta_v.x, delta_v.y, scalel, scaler);
//        }

    }
}

static float determinant(Vec2 v1, Vec2 v2) {
    return v1.x * v2.y - v1.y * v2.x;
}

static bool are_same_sign(float a, float b, float c) {
    if (a>0) return b>0 && c>0;
    if (a<0) return b<0 && c<0;
    return false;
}

static bool is_vector_between(Vec2 target, Vec2 edge1, Vec2 edge2) {
    return are_same_sign(determinant(edge1, target),
                         determinant(target, edge2),
                         determinant(edge1, edge2));
}


static void sensor_process_pair(global struct Scene *s, global struct SceneSense *p, global struct Params *params, struct Manifold *m)
{
    // Given a vector from the centre of the robot to the contact point, we need to work
    // out which two sensors it is between, then perform the intersection for both these sensors
    uint                    a       = m->a;
    global struct PoseSense *ps     = p->sense + a;
    float2                  v       = m->contact - s->bodies[a].position;
    float                   theta   = s->bodies[a].theta;
    float                   r       = s->bodies[a].radius;

    enum Stype              t       = m->B->type;
    float                   sr      = params->sensor_max_radius;
    float2 s1, s2;
    for (uint j = 0; j < NUM_SENSORS; j++)
    {
        if (j == 0)
            s1 = (Vec2)(cos(sensor_angles[j] + theta), sin(sensor_angles[j] + theta));
        else
            s1 = s2;
        s2 = (Vec2)(cos(sensor_angles[(j + 1) % NUM_SENSORS] + theta), sin(sensor_angles[(j + 1) % NUM_SENSORS] + theta));
        if (is_vector_between(v, s2, s1))
        {
            float m = length(v);
            float a1 = acos(dot(s1, v) / m);
            float a2 = acos(dot(v, s2) / m);
            float h1, h2;
            float sa1 = sin(a1);
            float sa2 = sin(a2);
            h1 = sr * SENSE_SCALE;
            h2 = sr * SENSE_SCALE;
            if (r / (m + r) > sa1)
                h1 = (m + r * sa1) * SENSE_SCALE;
            if (r / (m + r) > sa2)
                h2 = (m + r * sa2) * SENSE_SCALE;
            if (ps->prox[j] > h1)
            {
                ps->prox[j] = h1;
                //if (get_global_id(0) == 0)
                //    printf("%d %d %d %f %f %f %f %f %f\n",a, t, j, m, r, a1, a2, h1, h2);
            }
            if (ps->prox[(j + 1) % NUM_SENSORS] > h2)
            {
                ps->prox[(j + 1) % NUM_SENSORS] = h2;
                //if (get_global_id(0) == 0)
                //    printf("%d %d %d %f %f %f %f %f %f\n",a, t, j, m, r, a1, a2, h1, h2);
            }
        }
    }
}

static void sensors2(global struct Scene *s, global struct SceneSense *p, global struct Params *params)
{
    struct Manifold m;
    uint xbins[NUM_SBINS];
    uint ybins[NUM_SBINS];
    uint nrobots = params->robot_count;
    for (uint i = 0; i < NUM_SBINS; i++)
        xbins[i] = ybins[i] = 0;
    // First, clear all sensors to max distance
    for(uint a = 0; a < params->robot_count; a++)
    {
        global struct PoseSense *ps = p->sense + a;
        for (uint i = 0; i < NUM_SENSORS; i++)
        {
            ps->prox[i] = params->sensor_max_radius * SENSE_SCALE;
        }
    }
    for (uint i = 0; i < nrobots; i++)
    {
        int xbin = floor(s->bodies[i].position.x / SBINSIZE) + NUM_SBINS / 2;
        xbins[xbin] |= 1 << i;
        int ybin = floor(s->bodies[i].position.y / SBINSIZE) + NUM_SBINS / 2;
        ybins[ybin] |= 1 << i;
    }
    // Potential collisions will have two or more bits set in the same or adjacent bins
    uint match;
    for (uint i = 0; i < NUM_SBINS - 1; i++)
    {
        match = xbins[i] | xbins[i + 1];
        while (popcount(match) > 1)
        {
            uint a  = 31 - clz(match);
            float ay = s->bodies[a].position.y;
            match   &= ~(1 << a);
            
            uint match_inner = match;
            while (popcount(match_inner) > 0)
            {
                uint b      = 31 - clz(match_inner);
                match_inner &= ~(1 << b);
                // quick check to early out
                if (fabs(ay - s->bodies[b].position.y) > SBINSIZE)
                    continue;
                
                uint contact_count = circle_to_circle_sensor(s, &m, a, b, params->sensor_max_radius);
                if (contact_count)
                {
                    // Only take notice of this manifold if there are actually
                    // contacts in it
                    m.A = s->bodies + a;
                    m.a = a;
                    m.B = s->bodies + b;
                    sensor_process_pair(s, p, params, &m);
                    // Perform check the other way round to get sensing in both directions
                    circle_to_circle_sensor(s, &m, b, a, params->sensor_max_radius);
                    // Generate a manifold in both directions because both robots can sense each other
                    m.A = s->bodies + b;
                    m.a = b;
                    m.B = s->bodies + a;
                    sensor_process_pair(s, p, params, &m);
                }
            }
        }
    }


}

static void sensors(global struct Scene *s, global struct SceneSense *p, global struct Params *params)
{
    struct Manifold m[MAX_PAIRS];
    uint m_count = 0;
    uint bodies[MAX_BODIES];
    uint b_count;
    
    // Find collisions of bodies with sensors
    float sr = params->sensor_max_radius;
    float binsize = sr * 2.1f;
    
    //b_count = bin_and_prune(s, bodies, binsize, true, params);
    //generate_contacts(s, b_count, bodies, &m_count, m, true, sr);
    //generate_line_contacts(s, &m_count, m, true, params);
    uint rcontacts = generate_sensor_contacts(s, p, &m_count, m, params); // 7.56 +4.17


    // Array m now contains a set of pairs of bodies. Each body of each pair
    // must now have all its sensors updated.
    //
    // First, clear all sensors to max distance
    global struct PoseSense *ps = p->sense;
    ushort d = sr * SENSE_SCALE;
    for(uint i = 0; i < params->robot_count; i++)
    {
        vstore8((ushort8)d, 0, ps[i].prox);
        ps[i].position = s->bodies[i].position;
        ps[i].theta = s->bodies[i].theta = WRAPPI(s->bodies[i].theta);
        //ps++;
    }
    // Now iterate through pairs
    float2 s1, s2;
    for (uint i = 0; i < rcontacts; i++)
    {
        uint                a       = m[i].a;
        global struct PoseSense *ps = p->sense + a;
        float2              v       = m[i].contact - s->bodies[a].position;
        float               theta   = s->bodies[a].theta;
        float               r       = s->bodies[a].radius;
        float               m       = fast_length(v);
        float               rmr     = r / (m + r);
        
        //enum Stype          t       = m[i].B->type;
        s1 = (float2)(half_cos(sensor_angles[0] + theta), half_sin(sensor_angles[0] + theta));
        for (uint j = 0; j < NUM_SENSORS; j++)
        {
            // This code is a rough approximation for interesection of sensor
            // rays with a circle. There are a number of assumptions:
            // 1) The proximity sensors can be modelled with a ray
            // 2) Only the two sensors either side of the vector between the centres of
            // the circles are affected. This is true for the epuck sensor angles and radius. In general,
            // if two similar circles are touching at one sensor, an adjacent sensor will not intersect if
            // the angle between the sensors is greater than acos(sqrt(3)/2) = 30 deg = 0.524 rad. The smallest
            // angle between sensors is 0.558 rad
            //

            s2 = (float2)(half_cos(sensor_angles[(j + 1) & 7] + theta), half_sin(sensor_angles[(j + 1) & 7] + theta));
            if (is_vector_between(v, s2, s1))
            {
                //float2 a1 = (float2)(acos(dot(s1, v) / m), acos(dot(v, s2) / m));
                float2 sa1 = (float2)(half_sin(acos(dot(s1, v) / m)), half_sin(acos(dot(v, s2) / m)));
                int2 rmrgtsa = (float2)rmr > sa1;
                if (rmrgtsa.x)
                {
                    ps->prox[j] = (m + r * sa1.x) * SENSE_SCALE;
                }
                if (rmrgtsa.y)
                {
                    ps->prox[(j + 1) & 7] = (m + r * sa1.y) * SENSE_SCALE;
                }
                break;
            }
            s1 = s2;
        }
    }
    for (uint i = rcontacts; i < m_count; i++)
    {
        uint                a       = m[i].a;
        global struct PoseSense *ps = p->sense + a;
        Vec2                v       = m[i].contact - s->bodies[a].position;
        float               theta   = s->bodies[a].theta;
        
        s1 = (float2)(half_cos(sensor_angles[0] + theta), half_sin(sensor_angles[0] + theta));
        for (uint j = 0; j < NUM_SENSORS; j++)
        {
            s2 = (float2)(half_cos(sensor_angles[(j + 1) & 7] + theta), half_sin(sensor_angles[(j + 1) & 7] + theta));

            // These are line contacts
            float m2 = dot(v, v);
            float h = m2 / fmax((float)0, (float)dot(s1, v)) * SENSE_SCALE;
            //if(get_global_id(0)==0)printf("%d, %8f %8f %8f %8f\n", i, v.x, v.y, m[i].penetration, h);

            if (ps->prox[j] > h)
                ps->prox[j] = h;
            s1 = s2;
        }



    }
}

#define WRAPPI(x) atan2(sin(x),cos(x))

static void range_and_bearing(global struct Scene *s, global struct SceneSense *p, global struct Params *params)
{
    // Step through all robots and get the number and the range and bearing of their neighbours.
    // Brute force at first, optimise later
    global struct PoseSense *ps = p->sense;
    // Clear all values first, and use this opportunity to wrap the pose angle
    for (uint i = 0; i < params->robot_count; i++)
    {
        ps[i].n = 0;
        ps[i].adj_fitness = 0;
        ps[i].vattr = (float2)0;
        //ps[i].position = s->bodies[i].position;
        //ps[i].theta = s->bodies[i].theta = WRAPPI(s->bodies[i].theta);
    }
    //printf("blob=====\n");
    int cam_range = params->camera_radius / 2.0 * 255;
    for (uint i = 0; i < params->robot_count; i++)
    {
        global struct Body *bi = s->bodies + i;
        float2 pi = bi->position;
        uchar16 cbuf = (uchar16)(0);
        //uchar16 zbuf = (uchar16)(255);
        uchar16 zbuf = (uchar16)(cam_range);
        for (uint j = 0; j < params->robot_count + params->object_count; j++)
        {
            // Include objects as the second item, to do the camera blob detection.
            // Since all objects that can be detected with the camera are circles,
            // calculate the bearing and the angle subtended to determine which
            // camera segment the robot or object is within.
            if (i == j)
                // Can't see ourselves
                continue;
            
            
            global struct Body *bj = s->bodies + j;
            float2 pj     = bj->position;
            float2 v      = pj - pi;
            
            float a1    = WRAPPI(atan2(v.y, v.x) - bi->theta);
            float range = fast_length(v);
            float as    = asin(bj->radius / range);
            float lh    = a1 + as;
            float rh    = a1 - as;

        
            if ((lh > -FOV/2) && (rh < FOV/2))
            {
                // This object is visible, so render it into buffer. We implement a very simple Z buffer,
                // with a resolution of 15 elements across the visual field. If a seen object is present within
                // one of the buffer elements, and the Z value is lower than the current value, the colour and
                // Z are written to the buffers
                uchar16 map = (uchar16)
                (
                    (lh > BIN1) && (rh < BIN0),
                    (lh > BIN2) && (rh < BIN1),
                    (lh > BIN3) && (rh < BIN2),
                    (lh > BIN4) && (rh < BIN3),
                    (lh > BIN5) && (rh < BIN4),
                    (lh > BIN6) && (rh < BIN5),
                    (lh > BIN7) && (rh < BIN6),
                    (lh > BIN8) && (rh < BIN7),
                    (lh > BIN9) && (rh < BIN8),
                    (lh > BIN10) && (rh < BIN9),
                    (lh > BIN11) && (rh < BIN10),
                    (lh > BIN12) && (rh < BIN11),
                    (lh > BIN13) && (rh < BIN12),
                    (lh > BIN14) && (rh < BIN13),
                    (lh > BIN15) && (rh < BIN14),
                    0
                );
                uchar col = bj->colour;
                // make range a uchar: longest distance = 2m
                uchar rng = (uchar)(range * 255 / 2.0f);
                uchar16 r16 = (uchar16)(rng);
                uchar16 c16 = (uchar16)(col);
                char16 op = (map == true) && (rng < zbuf);
                if (j >= params->robot_count)
                    // objects do not occlude robots because low in height
                    //##FIXME## rather than having this test here, it may be faster to do the inner loop
                    // as two separate loops
                    cbuf = op ? c16 | cbuf : cbuf;
                    //cbuf = op ? c16 : cbuf;
                else
                    cbuf = op ? c16 : cbuf;
                zbuf = op ? r16 : zbuf;
                
            }
            
#if 1
            if ((j > i) && (j < params->robot_count) && (range < params->message_radius))
            {

                // fitness diffusion
                // We want to implement the network Laplacian
                ps[i].n++;
                ps[i].adj_fitness += ps[j].fitness;
                
                float rmin  = 0.035;
                ps[i].vattr += (rmin / range) * (float2)(half_cos(a1), half_sin(a1));
                float a2    = atan2(-v.y, -v.x) - bj->theta;
                ps[j].vattr += (rmin / range) * (float2)(half_cos(a2), half_sin(a2));
                
                ps[j].n++;
                ps[j].adj_fitness += ps[i].fitness;
            }
#endif
        }
        // At this point, the colour buffer has a depth sorted rendering of the field of view.
        // Each segment is 5 elements of the buffer. Or together to get the colours present in each segment
        ps[i].blobs = (     ((cbuf.s0 | cbuf.s1 | cbuf.s2 | cbuf.s3 | cbuf.s4) << 6)
                       |    ((cbuf.s5 | cbuf.s6 | cbuf.s7 | cbuf.s8 | cbuf.s9) << 3)
                       |    ((cbuf.sa | cbuf.sb | cbuf.sc | cbuf.sd | cbuf.se) << 0));
        
        

        
#if 0
        // Moving this code here into a separate loop structure results in much worse performance
        for (uint i = 0; i < params->robot_count; i++)
        {
            global struct Body *bi = s->bodies + i;
            float2 pi = bi->position;
            for (uint j = i + 1; j < params->robot_count; j++)
            {
                global struct Body *bj = s->bodies + j;
                float2 pj     = bj->position;
                float2 v      = pj - pi;
                float range   = fast_length(v);
                if (range < params->message_radius)
                {
                    
                    // fitness diffusion
                    // We want to implement the network Laplacian
                    ps[i].n++;
                    ps[i].adj_fitness += ps[j].fitness;
                    
                    float rmin  = 0.035;
                    float a1    = atan2(v.y, v.x) - bi->theta;
                    ps[i].vattr += (rmin / range) * (float2)(half_cos(a1), half_sin(a1));
                    float a2    = atan2(-v.y, -v.x) - bj->theta;
                    ps[j].vattr += (rmin / range) * (float2)(half_cos(a2), half_sin(a2));
                    
                    ps[j].n++;
                    ps[j].adj_fitness += ps[i].fitness;

                }
            }
        }
            
#endif
        
        
        //printf("%3d %2x%2x%2x%2x%2x%2x%2x%2x%2x%2x%2x%2x%2x%2x%2x%2x\n",i,cbuf.s0,cbuf.s1,cbuf.s2,cbuf.s3,cbuf.s4,cbuf.s5,cbuf.s6,cbuf.s7,
        //       cbuf.s8,cbuf.s9,cbuf.sa,cbuf.sb,cbuf.sc,cbuf.sd,cbuf.se,cbuf.sf);
        
        //ps[i].blobs |= bj->colour << 3;

        
    }
}

static void check_relocate_frisbee(global struct Scene *s, global struct Params *params)
{
    if (params->fitness_mode < 2)
        return;
    
    global struct Body *b = s->bodies + params->robot_count;
    float margin = b->radius * 2;
    if (fabs(b->position.x) > 0.95f - b->radius)
    {
        // If the frisbee is within 5cm of the left end arena, relocate it to the right hand end
        float2 npos;
        while (true)
        {
            npos = (float2)(rand_realrange((global uchar *)s, -0.1f, 0.1f),
                            rand_realrange((global uchar *)s, -0.6f, 0.6f));
            bool clash = false;
            for (uint i = 0; i < params->robot_count; i++)
            {
                global struct Body *br = s->bodies + i;
                if (fast_length(br->position - npos) < margin)
                {
                    clash = true;
                    break;
                }
            }
            if (!clash)
                break;
        }
        b->position = npos;
    }
}

// Forward declare
//static void controller(global uchar *t, ushort root, global struct Scene *s, global struct SceneSense *p, global struct SceneControl *c, uint tsize, float home_x, float home_y, float home_th);

//------------------------------------------------------------------------
__kernel void simstep
(
    global struct Scene             *scene,
    global struct SceneSense        *senses,
    global struct SceneControl      *control,
    global uchar                    *tree,
    global uchar                    *tree_vars,
    global struct Params            *params
)
{
    uint idx = get_global_id(0);
    global struct Scene         *s = scene + idx;
    global struct SceneSense    *p = senses + idx;
    global struct SceneControl  *c = control + idx;
    global struct Params        *pr = params + idx;
    global uchar                *t = tree + idx * pr->max_bt_size * pr->robot_count;
    global uchar                *tv = tree_vars + idx * pr->max_btvars_size * pr->robot_count;
    //ushort                      r  = root[idx];
    // Assumes number of agents, body count, is the same for each scene. This
    // is currently true
    

    DB2PRINT8("Simstep gid:%d gs:%d scene:%llx senses:%llx control:%llx tree:%llx tree_vars:%llx params:%llx\n", idx,get_global_size(0),
    scene, senses,control, tree, tree_vars, pr);
    //printf("bodies %d robots %d objects %d\ndt:%f cp:%d it:%d sr:%f mr:%f bdr:%f,%f\n", s->body_count, s->robot_count, s->object_count, params->dt,params->ctrl_period, params->iterations, params->sensor_radius, params->message_radius, params->border.x, params->border.y);

    //printf("%3d %f\n",idx,pr->)

//    if (idx == 0)
//    {
//
//        printf("In simstep %3d:%3d (%f,%f) (%f,%f) (%f,%f) (%f,%f) %f %f %lu %lu %lu %lu\n%3d %3d %3d %3d %3d %3d %3d %3d\n%3d %3d %3d %3d %3d %3d %3d %3d\n%3d %3d %3d %3d %3d %3d %3d %3d\n%3d %3d %3d %3d %3d %3d %3d %3d\n",
//            (uint)get_global_size(0), s->body_count,
//            s->bodies[0].position.x, s->bodies[0].position.y,
//            s->bodies[0].velocity.x, s->bodies[0].velocity.y,
//            s->bodies[1].position.x, s->bodies[1].position.y,
//            s->bodies[1].velocity.x, s->bodies[1].velocity.y,
//            s->bodies[0].radius,     s->bodies[1].radius,
//            sizeof(struct Scene), sizeof(struct Body), sizeof(struct SceneSense), sizeof(struct SceneControl),
//            
//               p->sense[0].prox[0], p->sense[0].prox[1], p->sense[0].prox[2], p->sense[0].prox[3],
//               p->sense[0].prox[4], p->sense[0].prox[5], p->sense[0].prox[6], p->sense[0].prox[7],
//               p->sense[1].prox[0], p->sense[1].prox[1], p->sense[1].prox[2], p->sense[1].prox[3],
//               p->sense[1].prox[4], p->sense[1].prox[5], p->sense[1].prox[6], p->sense[1].prox[7],
//               p->sense[2].prox[0], p->sense[2].prox[1], p->sense[2].prox[2], p->sense[2].prox[3],
//               p->sense[2].prox[4], p->sense[2].prox[5], p->sense[2].prox[6], p->sense[2].prox[7],
//               p->sense[3].prox[0], p->sense[3].prox[1], p->sense[3].prox[2], p->sense[3].prox[3],
//               p->sense[3].prox[4], p->sense[3].prox[5], p->sense[3].prox[6], p->sense[3].prox[7]
//               
//               );
//
//        for (uint i = 0; i < s->body_count; i++)
//        {
//            p->sense[i].position.x = s->bodies[i].position.x;
//            p->sense[i].position.y = s->bodies[i].position.y;
//            p->sense[i].theta = s->bodies[i].theta;
//        }
//    }

    //printf("%lu %lx %x\n", get_global_id(0), s, *(global uint *)s);
    //if (idx==0)HEXDUMP((global uchar *)s, 1024);
    
    for (int cs = 0; cs < pr->ctrl_steps; cs++)
    {
        for (uint st = 0; st < pr->ctrl_period; st++)
        {
            kinematics(s, c, pr->dt, pr);
            barrier(CLK_GLOBAL_MEM_FENCE);
            physics(s, p, pr);
            barrier(CLK_GLOBAL_MEM_FENCE);
        }
        sensors(s, p, pr);
        barrier(CLK_GLOBAL_MEM_FENCE);
        
        // Only run the camera every other control cycle
        if (cs % 2 == 0)
        {
            // See if frisbee needs relocating
            check_relocate_frisbee(s, pr);
            range_and_bearing(s, p, pr);
            barrier(CLK_GLOBAL_MEM_FENCE);
        }
        
        for (int i = 0; i < pr->robot_count; i++)
        {

            //printf("Scene %d agent %d global %d tree %d root %d\n", sidx, i, tidx, t, r);
            
            global uchar *v = tv + i * pr->max_btvars_size;
            global uchar *lt = pr->heterogeneous ? t + i * pr->max_bt_size : t;
            
            controller(lt, v, p, c, pr, i, 0.15 * (i % 3) - 0.5, 0.15 * (i / 3) - 0.15, 0);
            barrier(CLK_GLOBAL_MEM_FENCE);
        }
    }
}



__kernel void hello_world(void)
{
    printf("Hello world\n");
    return;
    printf("Scene           %d\n", sizeof(global struct Scene));
    printf("SceneSense      %d\n", sizeof(global struct SceneSense));
    printf("SceneControl    %d\n", sizeof(global struct SceneControl));
    printf("PoseSense       %d\n", sizeof(global struct PoseSense));
}






