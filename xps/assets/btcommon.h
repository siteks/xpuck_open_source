#ifndef _BT_COMMON
#define _BT_COMMON

// This file is designed to be included either within an OpenCL
// file or within a C99 file. This is in order to generate
// code that will run a BT on a real robot, as well as within the
// simulation using identical source.


// Major defines controlling memory used
#define MAX_BODIES          32
#define MAX_VERTEX_COUNT    4
#define MAX_PAIRS           256
#define NUM_SENSORS         8
// Proximity sensor ranges - should go somewhere better, (mm)
//#define MIN_PROX_RANGE      38.0f
//#define MAX_PROX_RANGE      78.0f
// Motor velocity (m/s)
#define MAX_VELOCITY        0.13f

#ifdef _ROBOT
// Here we define the missing or different bits when building
// C99
#include <stdint.h>
#include <math.h>
#ifdef __APPLE__
//#include "cl.h"
#include <OpenCL/cl.h>
#else
#define CL_PRINTF_CALLBACK_ARM    0x40B0
#define CL_PRINTF_BUFFERSIZE_ARM  0x40B1
#include <CL/cl.h>
#endif
//typedef cl_float2 float2;

typedef union
{
    struct{ cl_float  x, y; };
    struct{ cl_float  s0, s1; };
    struct{ cl_float  lo, hi; };
} float2;
typedef union
{
    struct{ cl_float  x, y, z, w; };
    struct{ cl_float  s0, s1, s2, s3; };
    struct{ cl_float2 lo, hi; };
} float4;
typedef union
{
    struct{ cl_float  x, y, z, w; };
    struct{ cl_float  s0, s1, s2, s3, s4, s5, s6, s7; };
    struct{ cl_float4 lo, hi; };
} float8;

#define constant const
#define global
#define get_global_id(x)    0
#define INIT2(x,y) {{x,y}}

typedef cl_char     schar;
typedef cl_uchar    uchar;
typedef cl_ushort   ushort;
typedef cl_uint     uint;
#ifndef __linux__
typedef cl_ulong    ulong;
#endif

constant float cos_sa[NUM_SENSORS] = {0.95622, 0.65622, 0.0, -0.866, -0.866, 0.0, 0.65622, 0.95622};
constant float sin_sa[NUM_SENSORS] = {0.29265, 0.75457, 1.0, 0.5, -0.5, -1.0, -0.75457, -0.29265};

#else

typedef char        schar;
#define INIT2(x,y) (x,y)

#endif


constant float sensor_angles[NUM_SENSORS] = {0.297, 0.855, 1.571, 2.618, -2.618, -1.571, -0.855, -0.297};


#ifndef _ROBOT
constant float8 cos_sa = (float8)(0.95622, 0.65622, 0.0, -0.866, -0.866, 0.0, 0.65622, 0.95622);
constant float8 sin_sa = (float8)(0.29265, 0.75457, 1.0, 0.5, -0.5, -1.0, -0.75457, -0.29265);
#endif

constant float2 camleft     = (float2)INIT2(0.94733f, 0.32026);
constant float2 camcentre   = (float2)INIT2(1.0f, 0.0f);
constant float2 camright    = (float2)INIT2(0.94733f, -0.32026);



// Struct to hold various parameters, to get round the restriction on
// the number of kernel arguments and to be a bit neater
struct Params
{
    float   dt;
    uint    ctrl_period;
    uint    ctrl_steps;
    
    uint    iterations;
    float   sensor_max_radius;
    float   sensor_min_radius;
    float   message_radius;
    float   wheelbase_radius;
    float   body_radius;
    float   camera_radius;
    
    float   max_velocity;
    float   max_omega;
    float   dynamic_friction;
    float   restitution;
    float   k1;
    float   k2;
    float   k3;
    
    //uint                seed;
    uint    body_count;
    uint    robot_count;
    uint    object_count;
    uint    max_bt_size;
    uint    max_btvars_size;
    
    
    float   alpha1;
    float   alpha2;
    float   alpha3;
    float   alpha4;
    float   alpha5;
    float   alpha6;
    float2  border;             // Symmetric about origin
    int     fitness_mode;
    bool    heterogeneous;
};



#ifdef DEBUG
#ifdef __APPLE__
// Sigh.. for some reason, string literal concatenation doesn't work with mali opencl compiler
#define DBPRINT0(fmt) printf("(%3d)" fmt,get_global_id(0))
#define DBPRINT1(fmt, a1) printf("(%3d)" fmt,get_global_id(0), a1)
#define DBPRINT2(fmt, a1,a2) printf("(%3d)" fmt,get_global_id(0), a1,a2)
#define DBPRINT3(fmt, a1,a2,a3) printf("(%3d)" fmt,get_global_id(0), a1,a2,a3)
#define DBPRINT4(fmt, a1,a2,a3,a4) printf("(%3d)" fmt,get_global_id(0), a1,a2,a3,a4)
#define DBPRINT5(fmt, a1,a2,a3,a4,a5) printf("(%3d)" fmt,get_global_id(0), a1,a2,a3,a4,a5)
#define DBPRINT6(fmt, a1,a2,a3,a4,a5,a6) printf("(%3d)" fmt,get_global_id(0), a1,a2,a3,a4,a5,a6)
#define DBPRINT7(fmt, a1,a2,a3,a4,a5,a6,a7) printf("(%3d)" fmt,get_global_id(0), a1,a2,a3,a4,a5,a6,a7)
#define DBPRINT8(fmt, a1,a2,a3,a4,a5,a6,a7,a8) printf("(%3d)" fmt,get_global_id(0), a1,a2,a3,a4,a5,a6,a7,a8)
#define DBPRINT9(fmt, a1,a2,a3,a4,a5,a6,a7,a8,a9) printf("(%3d)" fmt,get_global_id(0), a1,a2,a3,a4,a5,a6,a7,a8,a9)
#define DBPRINT10(fmt, a1,a2,a3,a4,a5,a6,a7,a8,a9,a10) printf("(%3d)" fmt,get_global_id(0), a1,a2,a3,a4,a5,a6,a7,a8,a9,a10)
#define DBPRINT11(fmt, a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11) printf("(%3d)" fmt,get_global_id(0), a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11)
#else
#define DBPRINT0(fmt) printf(fmt)
#define DBPRINT1(fmt, a1) printf(fmt, a1)
#define DBPRINT2(fmt, a1,a2) printf(fmt, a1,a2)
#define DBPRINT3(fmt, a1,a2,a3) printf(fmt, a1,a2,a3)
#define DBPRINT4(fmt, a1,a2,a3,a4) printf(fmt, a1,a2,a3,a4)
#define DBPRINT5(fmt, a1,a2,a3,a4,a5) printf(fmt, a1,a2,a3,a4,a5)
#define DBPRINT6(fmt, a1,a2,a3,a4,a5,a6) printf(fmt, a1,a2,a3,a4,a5,a6)
#define DBPRINT7(fmt, a1,a2,a3,a4,a5,a6,a7) printf(fmt, a1,a2,a3,a4,a5,a6,a7)
#define DBPRINT8(fmt, a1,a2,a3,a4,a5,a6,a7,a8) printf(fmt, a1,a2,a3,a4,a5,a6,a7,a8)
#define DBPRINT9(fmt, a1,a2,a3,a4,a5,a6,a7,a8,a9) printf(fmt, a1,a2,a3,a4,a5,a6,a7,a8,a9)
#define DBPRINT10(fmt, a1,a2,a3,a4,a5,a6,a7,a8,a9,a10) printf(fmt, a1,a2,a3,a4,a5,a6,a7,a8,a9,a10)
#define DBPRINT11(fmt, a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11) printf(fmt, a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11)
#endif
#else
#define DBPRINT0(fmt)
#define DBPRINT1(fmt, a1)
#define DBPRINT2(fmt, a1,a2)
#define DBPRINT3(fmt, a1,a2,a3)
#define DBPRINT4(fmt, a1,a2,a3,a4)
#define DBPRINT5(fmt, a1,a2,a3,a4,a5)
#define DBPRINT6(fmt, a1,a2,a3,a4,a5,a6)
#define DBPRINT7(fmt, a1,a2,a3,a4,a5,a6,a7)
#define DBPRINT8(fmt, a1,a2,a3,a4,a5,a6,a7,a8)
#define DBPRINT9(fmt, a1,a2,a3,a4,a5,a6,a7,a8,a9)
#define DBPRINT10(fmt, a1,a2,a3,a4,a5,a6,a7,a8,a9,a10)
#define DBPRINT11(fmt, a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11)
#endif
#ifdef DEBUG2
#define DB2PRINT0(fmt) printf(fmt)
#define DB2PRINT1(fmt, a1) printf(fmt, a1)
#define DB2PRINT2(fmt, a1,a2) printf(fmt, a1,a2)
#define DB2PRINT3(fmt, a1,a2,a3) printf(fmt, a1,a2,a3)
#define DB2PRINT4(fmt, a1,a2,a3,a4) printf(fmt, a1,a2,a3,a4)
#define DB2PRINT5(fmt, a1,a2,a3,a4,a5) printf(fmt, a1,a2,a3,a4,a5)
#define DB2PRINT6(fmt, a1,a2,a3,a4,a5,a6) printf(fmt, a1,a2,a3,a4,a5,a6)
#define DB2PRINT7(fmt, a1,a2,a3,a4,a5,a6,a7) printf(fmt, a1,a2,a3,a4,a5,a6,a7)
#define DB2PRINT8(fmt, a1,a2,a3,a4,a5,a6,a7,a8) printf(fmt, a1,a2,a3,a4,a5,a6,a7,a8)
#define DB2PRINT9(fmt, a1,a2,a3,a4,a5,a6,a7,a8,a9) printf(fmt, a1,a2,a3,a4,a5,a6,a7,a8,a9)
#define DB2PRINT10(fmt, a1,a2,a3,a4,a5,a6,a7,a8,a9,a10) printf(fmt, a1,a2,a3,a4,a5,a6,a7,a8,a9,a10)
#else
#define DB2PRINT0(fmt)
#define DB2PRINT1(fmt, a1)
#define DB2PRINT2(fmt, a1,a2)
#define DB2PRINT3(fmt, a1,a2,a3)
#define DB2PRINT4(fmt, a1,a2,a3,a4)
#define DB2PRINT5(fmt, a1,a2,a3,a4,a5)
#define DB2PRINT6(fmt, a1,a2,a3,a4,a5,a6)
#define DB2PRINT7(fmt, a1,a2,a3,a4,a5,a6,a7)
#define DB2PRINT8(fmt, a1,a2,a3,a4,a5,a6,a7,a8)
#define DB2PRINT9(fmt, a1,a2,a3,a4,a5,a6,a7,a8,a9)
#define DB2PRINT10(fmt, a1,a2,a3,a4,a5,a6,a7,a8,a9,a10)
#endif


// All information about shape
enum Stype
{
    ROBOT,
    OBJECT,
    HLINE,
    VLINE
};

// All information about body
struct Body
{
    float2          position;   //0
    float2          velocity;   //8
    float2          force;      //16
    
    float           theta;      //24
    float           omega;      //28
    float           torque;     //32
    
    float           im;         //36 inverse mass
    float           iI;         //40 inverse moment of inertia
    
    float           mu;         //44
    
    enum Stype      type;       //48
    float           radius;     //52
    uint            colour;     //56
    //float           fitness;    //60
};

struct Manifold
{
    global struct Body     *A;
    global struct Body     *B;
    uint            a;
    float           penetration;
    float2          normal;
    float2          contact;
    //float           e;  // mixed restitution
    //float           df; // mixed dynamic friction
    //float           sf; // mixed static friction
};


struct PoseSense
{
    float2      position;               //0
    float       theta;                  //8
    ushort      prox[NUM_SENSORS];      //12
    uint        n;                      //28
    float2      vattr;                  //32
    //float       range[MAX_BODIES];
    //float       bearing[MAX_BODIES];
    float       fitness;                //40
    float       adj_fitness;            //44
    ushort      blobs;                  //48
    
};

struct Control
{
    float2      motor;
};

// The maximum number of pairs of colliding objects. Theoretically, could be:
// let n=MAX_SHAPES, p=pairs
// p = (n-1) + (n-2) + ... + 1 = (n-1)*n/2 = (n^2-n)/2
// p = 2016
// In reality, this could only arise if all the objects were initialised on top
// of each other. In sensible configurations, the number of pairs is much lower.
//
// Bodies: 16 robots, 1 object, 1 dummy object
//
struct Scene
{
    uint                seed;
    //uint                body_count;
    //uint                robot_count;
    //uint                object_count;
    struct Body         bodies[MAX_BODIES];
};
struct SceneSense
{
    struct PoseSense    sense[MAX_BODIES];
};
struct SceneControl
{
    struct Control      control[MAX_BODIES];
};


// Force type and status enums to be represented with smallest
// possible size ie 1 byte
typedef enum __attribute__((packed))
{
    SEQ2,       // 0
    SEQ3,       // 1
    SEQ4,       // 2
    SEL2,       // 3
    SEL3,       // 4
    SEL4,       // 5
    SEQM2,      // 6
    SEQM3,      // 7
    SEQM4,      // 8
    SELM2,      // 9
    SELM3,      // a
    SELM4,      // b
    REPEATI,    // c
    REPEATR,    // d
    SUCCESSD,   // e
    FAILURED,   // f
    INVERT,     // 10
    MOVCS,      // 11
    MOVCV,      // 12
    MULAS,      // 13
    MULAV,      // 14
    ROTAV,
    IFPROB,     // 15
    IFQUAD,     // 16
    IFSECT,     // 17
    SUCCESSL,   // 18
    FAILUREL,    // 19
    
    //=====================
    EXPLORE,
    UPFIELD,
    ATTRACT,
    RED,
    GREEN,
    BLUE,
    NEIGHBOUR,
    FIXEDPROB,
    AVOIDING,
    CLEFT,
    CRIGHT,
    CFRONT,
    BSEARCH,
    RWALK,
    SETFLAG,
    CLEARFLAG,
    TESTFLAG
    
    
} Nodetype;






// Blackboard variables
struct BB
{
    float2      vzero;  //  0
    float2      vgoal;  //  2
    float2      vprox;  //  4
    float2      vup;    //  6
    float2      vattr;  //  8
    float2      vred;   //  10
    float2      vgreen; //  12
    float2      vblue;  //  14
    float       sn;     //  16
    float       sscr;   //  17
    float2      vscr;   //  18
    float2      vhome;  //  20
    float2      vth;    //  22
};

// A mask of which blackboard registers have physical effect and need to be
// protected from more than one write per tick.
// We see if they have been written by checking for NaN.
constant char physical[24]  = {0,0,1,1, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0};
constant char writeable[24] = {0,0,1,1, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,1,1,1, 0,0,0,0};

constant float sintab[256] = {
    0.000000,  0.024541,  0.049068,  0.073565,  0.098017,  0.122411,  0.146730,  0.170962,  0.195090,  0.219101,  0.242980,  0.266713,  0.290285,  0.313682,  0.336890,  0.359895,  0.382683,  0.405241,  0.427555,  0.449611,  0.471397,  0.492898,  0.514103,  0.534998,  0.555570,  0.575808,  0.595699,  0.615232,  0.634393,  0.653173,  0.671559,  0.689541,  0.707107,  0.724247,  0.740951,  0.757209,  0.773010,  0.788346,  0.803208,  0.817585,  0.831470,  0.844854,  0.857729,  0.870087,  0.881921,  0.893224,  0.903989,  0.914210,  0.923880,  0.932993,  0.941544,  0.949528,  0.956940,  0.963776,  0.970031,  0.975702,  0.980785,  0.985278,  0.989177,  0.992480,  0.995185,  0.997290,  0.998795,  0.999699,  1.000000,  0.999699,  0.998795,  0.997290,  0.995185,  0.992480,  0.989177,  0.985278,  0.980785,  0.975702,  0.970031,  0.963776,  0.956940,  0.949528,  0.941544,  0.932993,  0.923880,  0.914210,  0.903989,  0.893224,  0.881921,  0.870087,  0.857729,  0.844854,  0.831470,  0.817585,  0.803208,  0.788346,  0.773010,  0.757209,  0.740951,  0.724247,  0.707107,  0.689541,  0.671559,  0.653173,  0.634393,  0.615232,  0.595699,  0.575808,  0.555570,  0.534998,  0.514103,  0.492898,  0.471397,  0.449611,  0.427555,  0.405241,  0.382683,  0.359895,  0.336890,  0.313682,  0.290285,  0.266713,  0.242980,  0.219101,  0.195090,  0.170962,  0.146730,  0.122411,  0.098017,  0.073565,  0.049068,  0.024541,  0.000000,  -0.024541,  -0.049068,  -0.073565,  -0.098017,  -0.122411,  -0.146730,  -0.170962,  -0.195090,  -0.219101,  -0.242980,  -0.266713,  -0.290285,  -0.313682,  -0.336890,  -0.359895,  -0.382683,  -0.405241,  -0.427555,  -0.449611,  -0.471397,  -0.492898,  -0.514103,  -0.534998,  -0.555570,  -0.575808,  -0.595699,  -0.615232,  -0.634393,  -0.653173,  -0.671559,  -0.689541,  -0.707107,  -0.724247,  -0.740951,  -0.757209,  -0.773010,  -0.788346,  -0.803208,  -0.817585,  -0.831470,  -0.844854,  -0.857729,  -0.870087,  -0.881921,  -0.893224,  -0.903989,  -0.914210,  -0.923880,  -0.932993,  -0.941544,  -0.949528,  -0.956940,  -0.963776,  -0.970031,  -0.975702,  -0.980785,  -0.985278,  -0.989177,  -0.992480,  -0.995185,  -0.997290,  -0.998795,  -0.999699,  -1.000000,  -0.999699,  -0.998795,  -0.997290,  -0.995185,  -0.992480,  -0.989177,  -0.985278,  -0.980785,  -0.975702,  -0.970031,  -0.963776,  -0.956940,  -0.949528,  -0.941544,  -0.932993,  -0.923880,  -0.914210,  -0.903989,  -0.893224,  -0.881921,  -0.870087,  -0.857729,  -0.844854,  -0.831470,  -0.817585,  -0.803208,  -0.788346,  -0.773010,  -0.757209,  -0.740951,  -0.724247,  -0.707107,  -0.689541,  -0.671559,  -0.653173,  -0.634393,  -0.615232,  -0.595699,  -0.575808,  -0.555570,  -0.534998,  -0.514103,  -0.492898,  -0.471397,  -0.449611,  -0.427555,  -0.405241,  -0.382683,  -0.359895,  -0.336890,  -0.313682,  -0.290285,  -0.266713,  -0.242980,  -0.219101,  -0.195090,  -0.170962,  -0.146730,  -0.122411,  -0.098017,  -0.073565,  -0.049068,  -0.024541};

constant float costab[256] = {1.000000,  0.999699,  0.998795,  0.997290,  0.995185,  0.992480,  0.989177,  0.985278,  0.980785,  0.975702,  0.970031,  0.963776,  0.956940,  0.949528,  0.941544,  0.932993,  0.923880,  0.914210,  0.903989,  0.893224,  0.881921,  0.870087,  0.857729,  0.844854,  0.831470,  0.817585,  0.803208,  0.788346,  0.773010,  0.757209,  0.740951,  0.724247,  0.707107,  0.689541,  0.671559,  0.653173,  0.634393,  0.615232,  0.595699,  0.575808,  0.555570,  0.534998,  0.514103,  0.492898,  0.471397,  0.449611,  0.427555,  0.405241,  0.382683,  0.359895,  0.336890,  0.313682,  0.290285,  0.266713,  0.242980,  0.219101,  0.195090,  0.170962,  0.146730,  0.122411,  0.098017,  0.073565,  0.049068,  0.024541,  0.000000,  -0.024541,  -0.049068,  -0.073565,  -0.098017,  -0.122411,  -0.146730,  -0.170962,  -0.195090,  -0.219101,  -0.242980,  -0.266713,  -0.290285,  -0.313682,  -0.336890,  -0.359895,  -0.382683,  -0.405241,  -0.427555,  -0.449611,  -0.471397,  -0.492898,  -0.514103,  -0.534998,  -0.555570,  -0.575808,  -0.595699,  -0.615232,  -0.634393,  -0.653173,  -0.671559,  -0.689541,  -0.707107,  -0.724247,  -0.740951,  -0.757209,  -0.773010,  -0.788346,  -0.803208,  -0.817585,  -0.831470,  -0.844854,  -0.857729,  -0.870087,  -0.881921,  -0.893224,  -0.903989,  -0.914210,  -0.923880,  -0.932993,  -0.941544,  -0.949528,  -0.956940,  -0.963776,  -0.970031,  -0.975702,  -0.980785,  -0.985278,  -0.989177,  -0.992480,  -0.995185,  -0.997290,  -0.998795,  -0.999699,  -1.000000,  -0.999699,  -0.998795,  -0.997290,  -0.995185,  -0.992480,  -0.989177,  -0.985278,  -0.980785,  -0.975702,  -0.970031,  -0.963776,  -0.956940,  -0.949528,  -0.941544,  -0.932993,  -0.923880,  -0.914210,  -0.903989,  -0.893224,  -0.881921,  -0.870087,  -0.857729,  -0.844854,  -0.831470,  -0.817585,  -0.803208,  -0.788346,  -0.773010,  -0.757209,  -0.740951,  -0.724247,  -0.707107,  -0.689541,  -0.671559,  -0.653173,  -0.634393,  -0.615232,  -0.595699,  -0.575808,  -0.555570,  -0.534998,  -0.514103,  -0.492898,  -0.471397,  -0.449611,  -0.427555,  -0.405241,  -0.382683,  -0.359895,  -0.336890,  -0.313682,  -0.290285,  -0.266713,  -0.242980,  -0.219101,  -0.195090,  -0.170962,  -0.146730,  -0.122411,  -0.098017,  -0.073565,  -0.049068,  -0.024541,  -0.000000,  0.024541,  0.049068,  0.073565,  0.098017,  0.122411,  0.146730,  0.170962,  0.195090,  0.219101,  0.242980,  0.266713,  0.290285,  0.313682,  0.336890,  0.359895,  0.382683,  0.405241,  0.427555,  0.449611,  0.471397,  0.492898,  0.514103,  0.534998,  0.555570,  0.575808,  0.595699,  0.615232,  0.634393,  0.653173,  0.671559,  0.689541,  0.707107,  0.724247,  0.740951,  0.757209,  0.773010,  0.788346,  0.803208,  0.817585,  0.831470,  0.844854,  0.857729,  0.870087,  0.881921,  0.893224,  0.903989,  0.914210,  0.923880,  0.932993,  0.941544,  0.949528,  0.956940,  0.963776,  0.970031,  0.975702,  0.980785,  0.985278,  0.989177,  0.992480,  0.995185,  0.997290,  0.998795,  0.999699};

// generated with:
//import random
//import numpy as np
//s = 100
//seed = 1
//while abs(s) > 0.1:
//    np.random.seed(seed)
//    r = np.random.normal(size=256)
//    s = np.sum(r)
//    print 'seed:%d sum:%f' % (seed, np.sum(r))
//    seed += 1
// seed:102 sum:-0.068067
constant float normtab[256] = { 1.668068,  0.925862,  1.057997,  -0.920339,  1.299748,  0.331183,  -0.509845,  -0.903099,  -0.130016,  -2.238203,  0.973165,  -0.024185,  -0.484928,  -1.109264,  -0.558975,  1.042387,  -1.712263,  0.136120,  -0.464444,  0.050980,  1.447899,  0.593138,  -0.755616,  -0.627166,  0.884035,  0.162718,  -2.502813,  0.410791,  0.728750,  0.029140,  -0.291052,  0.314293,  0.997190,  3.428563,  -0.578567,  -0.204163,  -1.867169,  1.443434,  0.403380,  -0.690765,  1.172287,  -1.571128,  0.801992,  -0.571973,  1.553394,  0.701220,  1.014993,  -0.118042,  -1.063891,  1.176691,  1.347619,  -0.794300,  -1.279943,  0.556753,  -0.048404,  0.234364,  -0.983657,  1.093005,  -1.251296,  1.481600,  -0.227293,  -0.063780,  -0.176185,  -2.172674,  -1.588957,  -1.292981,  -0.547431,  -0.008797,  -0.478540,  -0.359362,  0.609151,  1.038694,  0.098843,  -1.109817,  -0.754384,  1.017106,  -1.749905,  -1.350573,  0.033912,  0.424447,  -0.429403,  -0.223170,  0.231589,  -0.895379,  -1.000759,  1.169370,  0.786860,  0.563006,  -1.270066,  -0.284836,  0.268541,  -0.750508,  0.480002,  1.938019,  -0.704193,  -0.230239,  -0.013516,  -0.490594,  0.156247,  -0.402487,  0.278943,  -0.064388,  1.274979,  0.912503,  -0.186412,  0.058773,  0.789484,  0.979825,  -2.650835,  -0.306270,  -0.719729,  0.117394,  -0.462441,  -0.940543,  -1.778986,  0.529387,  0.831352,  -0.258645,  0.167274,  -0.593892,  -0.563070,  0.882805,  0.311495,  -0.844298,  0.833962,  1.391924,  0.431659,  0.577680,  0.533017,  0.714847,  -0.267228,  1.198386,  1.971195,  -0.264506,  -0.292710,  -1.575398,  0.438984,  -0.199203,  0.159284,  1.303704,  0.001817,  -3.274464,  0.602087,  -0.179885,  -1.505570,  0.274341,  1.468625,  1.121132,  1.799841,  2.351742,  0.756814,  -0.126035,  0.945210,  0.854252,  -1.770798,  1.198418,  -1.309283,  1.315358,  0.923098,  0.164003,  0.854101,  -1.307599,  -1.485952,  0.870746,  0.575038,  0.246979,  -0.601873,  -0.465131,  0.183339,  0.826680,  -1.362105,  -0.216016,  0.206353,  -1.641117,  -1.666657,  0.034567,  -0.329217,  0.936132,  0.726568,  0.646532,  0.350097,  2.382684,  0.538469,  -1.100072,  -0.076063,  1.655305,  0.892536,  0.495940,  1.750032,  0.384477,  1.202931,  -0.363174,  0.453507,  0.740486,  -0.827448,  -1.655083,  -1.505504,  -0.983285,  0.866741,  -0.321151,  0.192540,  -0.621747,  -0.151523,  0.349400,  -0.697888,  -0.256482,  -0.189662,  -0.615678,  -0.395839,  0.259253,  -2.721608,  1.370591,  2.010820,  -0.175887,  -0.973576,  -0.269937,  0.612257,  0.582097,  -1.776359,  0.100261,  -0.235891,  1.327439,  -0.942637,  0.439008,  -0.830918,  -0.687747,  -0.251633,  0.704485,  1.000721,  -1.361738,  -1.437768,  -0.953233,  0.835028,  -0.826619,  1.446182,  -2.387377,  -0.127622,  -0.050570,  -0.327073,  0.066223,  1.401605,  -0.694592,  -0.204245,  -0.430897,  -1.141404,  1.269289,  0.997247,  -0.275718,  0.080340,  1.360363,  -0.927473,  0.197464,  0.724022,  -1.176494,  -0.506585,  -0.461296}  ;

typedef enum __attribute__((packed))
{
    BT_FAILURE = 0,
    BT_SUCCESS,
    BT_RUNNING,
    BT_RUNNING_STALE,
    BT_INVALID
} Status;


struct SM2
{
    //uchar       idx;
    ushort      op[2];
};

struct SM3
{
    //uchar       idx;
    ushort      op[3];
};

struct SM4
{
    //uchar       idx;
    ushort      op[4];
};

struct MVC
{
    schar        dest;
    schar        i;
};

struct IFQ
{
    schar        src;
    schar        i;
};

struct IFS
{
    schar        src;
    schar        i;
    uchar       j;
};

struct MAC
{
    uchar       dest;
    uchar       src1;
    uchar       src2;
    float       f;
};

struct RAC
{
    uchar       dest;
    uchar       src1;
    uchar       src2;
    schar       i;
};

struct IFP
{
    uchar       src;
    schar        k;
    schar        l;
};

struct REP
{
    //uchar       count;
    uchar       repeat;
    ushort      op;
};

struct FIX
{
    ushort      op;
};


union Ndata
{
    
    struct SM2  sm2;
    struct SM3  sm3;
    struct SM4  sm4;
    struct MVC  mvc;
    struct IFQ  ifq;
    struct IFS  ifs;
    struct MAC  mac;
    struct RAC  rac;
    struct IFP  ifp;
    struct REP  rep;
    struct FIX  fix;
    
};

struct Node
{
    Nodetype    type;
    //Status      status;
    ushort      ptr;
    union Ndata data;
};
typedef struct Node Node;

struct Nodestat
{
    uchar       status;
    uchar       idx;
};


#ifdef _ROBOT
// Undefine this, used to make the OpenCL into legal C++, otherwise it will
// conflict with later stuff
#undef constant
#undef global
#endif

#endif



