
#ifndef CLOCK_H
#define CLOCK_H


#include <chrono>

typedef std::chrono::high_resolution_clock  hr_clock;
typedef std::chrono::nanoseconds            clock_freq;


class Clock
{
public:
    Clock();
    ~Clock(){};
    
    void        start();
    void        stop();
    long long   elapsed();
    long long   lap();
    long long   difference();
    long long   current();


private:
    hr_clock::time_point m_start;
    hr_clock::time_point m_stop;
    hr_clock::time_point m_current;
    hr_clock::time_point m_last;

};

#endif

