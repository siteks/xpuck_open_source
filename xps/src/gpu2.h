#ifndef _GPU2_H
#define _GPU2_H

//#define DEBUG

#ifdef DEBUG
#define DBPRINTF(...) printf(__VA_ARGS__)
#else
#define DBPRINTF(...)
#endif

#define __CL_ENABLE_EXCEPTIONS

#include <fstream>
#include <iostream>
#include <iterator>
#include <queue>
#ifdef __APPLE__
//#include "cl.h"
#include <OpenCL/cl.h>
#else
#define CL_PRINTF_CALLBACK_ARM    0x40B0
#define CL_PRINTF_BUFFERSIZE_ARM  0x40B1
#include <CL/cl.h>
#endif



#include "vcd.h"
#include "bt.h"
#include <random>
#include "json11.hpp"
#include <zmq.hpp>
#include <array>


#define ZMQPORT_CONTROL_SENSORS     "5700"
#define ZMQPORT_CONTROL_ACTUATORS   "5701"


void zmqlog(zmq::socket_t *s, std::string m);


using namespace json11;
//float randrange(float low, float high, int seed = 1);


class Simparams
{
public:
    float       simtime             = 60;
    int         steps;
    int         robots              = 16;
    int         parallel            = 256;
    int         seed                = 1;
    int         verbose             = 0;
    int         fitness_mode        = 0;
    float       dyn_mu              = 0.1;     //
    float       restitution         = 0.1;      //
    float       xp_mu               = 0.65;
    float       fr_mu               = 0.25;      //
    float       fr_radius           = 0.095;
    float       fr_mass             = 0.05;
    float       max_velocity        = 0.13;     //
    float       max_omega           = 4.0;
    float       wheelbase_radius    = 0.027;
    float       sensor_max_radius   = 0.06;
    float       sensor_min_radius   = 0.03;
    float       camera_radius       = 1.9;
    float       body_radius         = 0.04;
    float       message_radius      = 0.5;
    float       k1                  = 1000.0;
    float       k2                  = 100.0;
    float       k3                  = 1;
    int         wfindex             = 0;
    bool        controlled_sim      = false;
    bool        heterogeneous       = false;
    std::string genepool            = "";
    int         xp_to_idx[16]       = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
    int         idx_to_xp[16]       = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};

    float       fr_mass_alt[2]      = {0.03, 0.3};
    int         flip_params         = 0;
};


namespace gpusim
{

#define MAX_BODIES          32
#define MAX_VERTEX_COUNT    4
#define NUM_SENSORS         8
    
#define COLOUR_RED          1
#define COLOUR_GREEN        2
#define COLOUR_BLUE         4
//#define MAX_NODE_SIZE       2048
    
    
    
//const char *err_text(int err);
//const char *cmd_text(cl_command_type ctype);
//void check_err(cl_int err);
//char *getfile(const char *name);
//void printf_callback(const char *buffer, size_t len, size_t complete, void *user_data);
//void context_callback(const char *errinfo, const void *private_info, size_t cb, void *user_data);
    
    
// Control whether OpenCL kernel printfs active
const cl_uint print = false;

// Angles of the epuck proximity sensors
const cl_float sensor_angles[NUM_SENSORS] = {0.297, 0.855, 1.571, 2.618, -2.618, -1.571, -0.855, -0.297};
const cl_float camera_angles[4] = {-0.489, -0.163, 0.163, 0.481};


// Definitions mirroring the OpenCL ones
struct Vec2
{
    Vec2() : x(0), y(0) {}
    Vec2(cl_float _x, cl_float _y) : x(_x), y(_y) {}
    cl_float    x;
    cl_float    y;
};
typedef struct
{
    union
    {
        struct
        {
            cl_float    m00,m01;
            cl_float    m10,m11;
        };
    };
} Mat2;

    
// Possible shapes a body can take.
// Only circles can move and have controllers
enum clStype
{
    ROBOT,     // robots
    OBJECT,     // moveable objects
    HLINE,      // arena walls
    VLINE       // arena walls
};

// All information about body
struct clBody
{
    Vec2            position                __attribute__ ((aligned(8)));
    Vec2            velocity                __attribute__ ((aligned(8)));
    Vec2            force                   __attribute__ ((aligned(8)));
    
    float           theta;
    float           omega;
    float           torque;
    
    //float           m;  // mass
    float           im; // inverse mass
    //float           I;  // moment of inertia
    float           iI; // inverse moment of inertia
    
    //float           static_friction;
    //float           dynamic_friction;
    //float           restitution;
    float           mu;
    
    enum clStype    type                    __attribute__ ((aligned(4)));
    float           radius                  __attribute__ ((aligned(4)));
    cl_uint         colour;
    //float           fitness;
    
};


// Both Nvidia and Mali use 64 bit alignment, without the attributes, the bodies array
// ends up on a 32 bit boundary
// All circle types are stored first.
struct clScene
{
    cl_uint         seed                            __attribute__ ((aligned(8)));
    //cl_uint         body_count;
    //cl_uint         robot_count;
    //cl_uint         object_count;
    struct clBody   bodies[MAX_BODIES]              __attribute__ ((aligned(8)));
};


struct clPoseSense
{
    Vec2        position                            __attribute__ ((aligned(8)));
    float       theta                               __attribute__ ((aligned(4)));
    cl_ushort   prox[NUM_SENSORS]                   __attribute__ ((aligned(2)));
    cl_uint     n                                   __attribute__ ((aligned(4)));
    Vec2        vattr                               __attribute__ ((aligned(8)));
    float       fitness;
    float       adj_fitness;
    //cl_float    range[MAX_BODIES]                   __attribute__ ((aligned(8)));
    //cl_float    bearing[MAX_BODIES]                 __attribute__ ((aligned(8)));
    cl_ushort   blobs;
};

struct clSceneSense
{
    clPoseSense sense[MAX_BODIES];
};

struct clControl
{
    Vec2        motor;
};
struct clSceneControl
{
    clControl   control[MAX_BODIES];
};

// Struct to hold various parameters, to get round the restriction on
// the number of kernel arguments and to be a bit neater
struct clParams
{
    cl_float    dt;
    cl_uint     ctrl_period;
    cl_uint     ctrl_steps;
    cl_uint     iterations;
    cl_float    sensor_max_radius;
    cl_float    sensor_min_radius;
    cl_float    message_radius;
    cl_float    wheelbase_radius;
    cl_float    body_radius;
    cl_float    camera_radius;
    
    cl_float    max_velocity;
    cl_float    max_omega;
    cl_float    dynamic_friction;
    cl_float    restitution;
    cl_float    k1;
    cl_float    k2;
    cl_float    k3;
    
    //cl_uint                seed;
    cl_uint     body_count;
    cl_uint     robot_count;
    cl_uint     object_count;
    cl_uint     max_bt_size;
    cl_uint     max_btvars_size;

    cl_float    alpha1;
    cl_float    alpha2;
    cl_float    alpha3;
    cl_float    alpha4;
    cl_float    alpha5;
    cl_float    alpha6;
    Vec2        border                 __attribute__ ((aligned(8)));            // Symmetric about origin
    cl_int      fitness_mode;
    cl_bool     heterogeneous;
};
    

    
struct Physparam
{
    float   mass;
    float   radius;
    //float   static_friction;
    //float   dynamic_friction;
    //float   restitution;
    float   mu;
    uint    colour;
};
    

struct Pose
{
    Pose() : x(0), y(0), th(0) {}
    Pose(float _x, float _y, float _th)
    : x(_x), y(_y), th(_th) {}
    float x;
    float y;
    float th;
};

struct PoseMat
{
    PoseMat(int _zs, int _ys, int _xs)
    : zs(_zs), ys(_ys), xs(_xs), data(zs * ys * xs) {}
    int zs, ys, xs;
    std::vector<Pose> data;
    
    Pose operator() (int z, int y, int x) const {return data[z * ys * xs + y * xs + x];}
    Pose &operator() (int z, int y, int x) {return data[z * ys * xs + y * xs + x];}
};
//typedef std::vector< std::vector<Pose> > PoseMat;

class Evalcond
{
public:
    Evalcond() {};
    void add(float x, float y, float th)
    {
        coords.push_back(x); coords.push_back(y); coords.push_back(th);
    }
    std::vector<float>  coords;
    float               fitness;
    int                 seed;
};
    
    
    

class Sim
{
public:
    Sim(Simparams &_params, bool _vcdtrace = false);
    void run(int steps);
    void run_capture_all_fitness(int steps);
    void capture(int samples, PoseMat &m);
    void step();
    void evaluate_bt(std::vector<BT *> *t, int reps, int csteps, Json *wf = NULL);
    void postamble();
    void create_scene(Json *wf = NULL, std::string p = "");
    void create_scene(double *s);
    void process_events(bool output);
    void addbody(int sidx, float x, float y, float theta, Physparam &param, clStype type);
    void addfixedrect(int sidx, float x, float y, float xs, float ys);
    void addwall(int sidx);
    void render(bool update);
    void print_btstatus();
    void print_status();
    void set_bt(std::vector<BT *> *t, int loop, int reps);
    void set_bt(std::vector<BT *> *t, int loop);
    void print_bt();
    void set_motor(float wheel_left, float wheel_right);
    void get_btvars(double *btv);
    void get_pose(int scene, int obj, Pose &m);

    void dump_state();

    
    Simparams           &params;
    int                 num_scenes;
    bool                vcdtrace;
    bool                param_sweep     = false;
    bool                param_alternate = false;
    //------------------------------------------------------
    // Number of agents - the number of robots running a BT controller
    cl_uint             num_agents      = params.robots;
    // Number of inert objects
    cl_uint             num_objects     = 1;
    // Maximum size of each behaviour tree
    cl_uint             max_bt_size     = MAX_BT_SIZE;
    // Maximum size of the BT vars, holds random seed, root, size, blackboard, node status
    cl_uint             max_btvars_size = MAX_BTVARS_SIZE;
    // Null BT, does nothing
    BT                  *nulltree;
    //------------------------------------------------------
    // Robot characteristics
    Physparam robot_param =
    {
        .mass               = 0.3f,
        .radius             = 0.038f,
        .mu                 = params.xp_mu
    };
    // Object characteristics
    Physparam object_param =
    {
        .mass               = params.fr_mass,
        .radius             = params.fr_radius,
        .mu                 = params.fr_mu
    };
    //------------------------------------------------------
    // Simulator parameters
    clParams sim_param =
    {
        .dt             = 0.025f,
        .ctrl_period    = 4,
        .ctrl_steps     = 100,
        .iterations     = 10,
        //.sensor_radius  = robot_param.radius + 0.04f,
        .sensor_max_radius  = params.sensor_max_radius,
        .sensor_min_radius  = params.sensor_min_radius,
        .message_radius     = params.message_radius,
        .wheelbase_radius   = params.wheelbase_radius,
        .body_radius        = params.body_radius,
        .camera_radius      = params.camera_radius,
        
        .max_velocity       = params.max_velocity,
        .max_omega          = params.max_omega,
        .dynamic_friction   = params.dyn_mu,
        .restitution        = params.restitution,
        .k1                 = params.k1,
        .k2                 = params.k2,
        .k3                 = params.k3,
        
        .body_count         = num_agents + num_objects + 1,
        .robot_count        = num_agents,
        .object_count       = num_objects,
        .max_bt_size        = max_bt_size,
        .max_btvars_size    = max_btvars_size,
        
        // velocity noise   velocity component
        .alpha1         = 0.1f,
        // omega noise      omega component
        .alpha2         = 0.1f,
        // angle noise      velocity component
        .alpha3         = 0.1f
        
    };
    
    
    cl_ulong            kernel_execution_time;

    //-------------------------------------------------------
    // Public for gui access
    float               sim_wheel_left  = 0.0f;
    float               sim_wheel_right = 0.0f;
    cl_uint             sim_body_count;
    Vec2                sim_positions[MAX_BODIES];
    cl_int              sim_dragged = -1;
    bool                update_object = false;
    
    std::vector<float>  all_fitness;
    std::vector<float>  all_lfitness;
    std::vector<float>  all_parsimony;
    std::vector<float>  all_stddev;
    std::queue<float>   all_startcoord;
    std::vector<Evalcond>   all_evc;
    
    

    cl_uint             master_seed     = 1;
    cl_uint             stepnum         = 0;

    // ZMQ logging channel
    zmq::socket_t       *pub_log        = 0;

    std::vector<float>  lfitness;

private:
    void get_platform();
    void get_devices();
    void get_context();
    void make_kernel();
    void make_pipeline();
    void flip_buffers();
    void run_control();
    void finish_all();
    void barrier();
    void add_depend();
    void clear_depend();

    std::vector<float>  fitness;
    std::vector<float>  parsimony;
    std::vector<float>  stddev;

    
    template <typename T> void unmap(T *mapped_ptr, cl_mem clbuffer, std::vector<cl_event> *d = NULL);
    template <typename T> void map(T **mapped_ptr, cl_mem clbuffer, unsigned int size = 1, std::vector<cl_event> *d = NULL);
    template <typename T> T* get_item_ptr(T *p, unsigned int i, unsigned int size = 1);

    

    
    cl_platform_id      platform_id;
    cl_device_id        device_id;
    cl_context          context;
    cl_int              err;
    
    std::queue<cl_event>   kevents;


    
    cl_command_queue    queue;
    cl_kernel           simstep;
    cl_kernel           ctrlstep;
    cl_kernel           simple;
    cl_kernel           print_tree;
    
    
    cl_uint             scene_buf_size;
    cl_uint             senses_buf_size;
    cl_uint             control_buf_size;
    cl_uint             bt_buf_size;
    cl_uint             btvars_buf_size;
    cl_uint             param_buf_size;
    
    cl_mem              scene_buf;
    cl_mem              senses_buf;
    cl_mem              control_buf;
    cl_mem              bt_buf;
    cl_mem              btvars_buf;
    cl_mem              param_buf;
    
    clScene             *scene_ptr;
    clSceneSense        *senses_ptr;
    clSceneControl      *control_ptr;
    cl_uchar            *bt_ptr;
    cl_uchar            *btvars_ptr;
    clParams            *param_ptr;
    
    

    std::deque<float>   local_parsim;

    cl_uint             buf_ptr         = 0;
    
    int                 total_events    = 0;
    
    VCD                 *vcd;
    
    
    int                 body_count;
    int                 repeats;
    
    std::vector<cl_event>   depend;
    
    // Render control
    bool                render_blob_detect  = true;
    
    // External sim control --------------------
    // ZMQ sockets for external sim control
    zmq::context_t      zmqcontext;
    zmq::socket_t       *sub_control_actuators[16];
    zmq::socket_t       pub_control_sensors;
    void                get_control_commands();
    void                send_sensor_readings();

    float               mode2_fitness;
    
};

    
}


// Python interface
extern "C"
{
    gpusim::Sim *Sim_new(int scenes, int robots, int bb_entries);
    void setup_and_step(gpusim::Sim *sim, 
           double *scene, double *blackboard, float smin_r, float smax_r, float cam_radius);
}



#endif
