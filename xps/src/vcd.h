#ifndef _VCD_H
#define _VCD_H

// Simple VCD generator

#include <string>
#include <cstdint>
#include <map>
#include <vector>

class VCD
{
public:
    VCD(std::string _filename);
    
    void addsignal(std::string name, uint32_t width, uint32_t value);
    void addchange(std::string name, uint64_t timestamp, uint32_t value);
    void initevent(int queues);
    void addevent(std::string name, uint64_t t1, uint64_t t2, uint64_t t3, uint64_t t4);
    void dump();
    std::string tagstr(uint16_t tag);
    std::string binary(uint32_t x);

private:
    
    std::string                                         filename;
    uint16_t                                            tagidx = 0;
    int                                                 interleave = 4;

    typedef std::map<uint16_t, uint32_t>                Values;
    typedef std::map<uint64_t, Values>                  Trace;
    
    Trace                                               data;
    Values                                              current_state;
    Values                                              initial_state;
    std::map<uint16_t, uint32_t>                        signal_width;

    std::map<std::string, uint16_t>                     tags;
    

    std::map<std::string, int>                          current_signal;
    
};









#endif

