#include <stdio.h>

#define DEBUG
#include "bt.h"

// This include file is OpenCL, and relies on defines in bt.h to be legal C++.
// It defines the function controller(), which executes a tick of the behaviour tree
// each time it is called. 
#include "bthelpers.h"

struct Scene        s;
struct SceneSense   p;
struct SceneControl c;
struct Params       pr;

int main(int argc, char **argv)
{

    BT *bt = new BT(20, "movcv 2 0 ");
    bt->print_tree();
    pr.max_velocity = 0.13;

    unsigned char *t = bt->cltree.data();
    unsigned char v[4096];
    memset(v, 0, 4096);
    // tick the tree
    controller(t, v, &p, &c, &pr, 0, 0, 0, 0, 0);
    
}
