

#include "gpu2.h"
#include "glut.h"
#include <assert.h>
#include <typeinfo>


static std::mt19937 generator;
void randrange_seed(int seed)
{
    generator.seed(seed);
}
float randrange(float low, float high)
{
    //generator(seed);
    std::uniform_real_distribution<float> distribution(low, high);
    float r = distribution(generator);
    //printf("%f\n", r);
    return r;
}

float randnorm(float xbar, float sigma)
{
    //generator(seed);
    std::normal_distribution<float> distribution(xbar, sigma);
    float r = distribution(generator);
    //printf("%f\n", r);
    return r;
}


void dumphex2(unsigned char *p, int l, char *label)
{
    FILE *fp = fopen("dump.hex", "a");
    fprintf(fp, "# %10s size:%08x\n", label, l);
    for(int i = 0; i < l; i += 16)
    {
        fprintf(fp, "%08x ", i);
        for(int j = 0; j < 16; j++)
            if (i + j < l)
                fprintf(fp, "%02x ", p[i+j]);
        fprintf(fp, "\n");
    }
    fclose(fp);
}

void zmqlog(zmq::socket_t *s, std::string m)
{
    if (!s) return;
    zmq::message_t msg(m.size());
    memcpy(msg.data(), m.data(), m.size());
    s->send(msg);
}


using namespace gpusim;

const char *err_text(int err)
{
    switch (err)
    {
        case 0:return "CL_SUCCESS";
        case -1: return "CL_DEVICE_NOT_FOUND";
        case -2: return "CL_DEVICE_NOT_AVAILABLE";
        case -3: return "CL_COMPILER_NOT_AVAILABLE";
        case -4: return "CL_MEM_OBJECT_ALLOCATION_FAILURE";
        case -5: return "CL_OUT_OF_RESOURCES";
        case -6: return "CL_OUT_OF_HOST_MEMORY";
        case -7: return "CL_PROFILING_INFO_NOT_AVAILABLE";
        case -8: return "CL_MEM_COPY_OVERLAP";
        case -9: return "CL_IMAGE_FORMAT_MISMATCH";
        case -10: return "CL_IMAGE_FORMAT_NOT_SUPPORTED";
        case -11: return "CL_BUILD_PROGRAM_FAILURE";
        case -12: return "CL_MAP_FAILURE";
        case -13: return "CL_MISALIGNED_SUB_BUFFER_OFFSET";
        case -14: return "CL_EXEC_STATUS_ERROR_FOR_EVENTS_IN_WAIT_LIST";
        case -15: return "CL_COMPILE_PROGRAM_FAILURE";
        case -16: return "CL_LINKER_NOT_AVAILABLE";
        case -17: return "CL_LINK_PROGRAM_FAILURE";
        case -18: return "CL_DEVICE_PARTITION_FAILED";
        case -19: return "CL_KERNEL_ARG_INFO_NOT_AVAILABLE";
        case -30: return "CL_INVALID_VALUE";
        case -31: return "CL_INVALID_DEVICE_TYPE";
        case -32: return "CL_INVALID_PLATFORM";
        case -33: return "CL_INVALID_DEVICE";
        case -34: return "CL_INVALID_CONTEXT";
        case -35: return "CL_INVALID_QUEUE_PROPERTIES";
        case -36: return "CL_INVALID_COMMAND_QUEUE";
        case -37: return "CL_INVALID_HOST_PTR";
        case -38: return "CL_INVALID_MEM_OBJECT";
        case -39: return "CL_INVALID_IMAGE_FORMAT_DESCRIPTOR";
        case -40: return "CL_INVALID_IMAGE_SIZE";
        case -41: return "CL_INVALID_SAMPLER";
        case -42: return "CL_INVALID_BINARY";
        case -43: return "CL_INVALID_BUILD_OPTIONS";
        case -44: return "CL_INVALID_PROGRAM";
        case -45: return "CL_INVALID_PROGRAM_EXECUTABLE";
        case -46: return "CL_INVALID_KERNEL_NAME";
        case -47: return "CL_INVALID_KERNEL_DEFINITION";
        case -48: return "CL_INVALID_KERNEL";
        case -49: return "CL_INVALID_ARG_INDEX";
        case -50: return "CL_INVALID_ARG_VALUE";
        case -51: return "CL_INVALID_ARG_SIZE";
        case -52: return "CL_INVALID_KERNEL_ARGS";
        case -53: return "CL_INVALID_WORK_DIMENSION";
        case -54: return "CL_INVALID_WORK_GROUP_SIZE";
        case -55: return "CL_INVALID_WORK_ITEM_SIZE";
        case -56: return "CL_INVALID_GLOBAL_OFFSET";
        case -57: return "CL_INVALID_EVENT_WAIT_LIST";
        case -58: return "CL_INVALID_EVENT";
        case -59: return "CL_INVALID_OPERATION";
        case -60: return "CL_INVALID_GL_OBJECT";
        case -61: return "CL_INVALID_BUFFER_SIZE";
        case -62: return "CL_INVALID_MIP_LEVEL";
        case -63: return "CL_INVALID_GLOBAL_WORK_SIZE";
        case -64: return "CL_INVALID_PROPERTY";
        case -65: return "CL_INVALID_IMAGE_DESCRIPTOR";
        case -66: return "CL_INVALID_COMPILER_OPTIONS";
        case -67: return "CL_INVALID_LINKER_OPTIONS";
        case -68: return "CL_INVALID_DEVICE_PARTITION_COUNT";
        default: return "UNKNOWN";
            
    }
}
const char *cmd_text(cl_command_type ctype)
{
    switch (ctype)
    {
        case CL_COMMAND_NDRANGE_KERNEL:     return "ndrange_kernel";
        case CL_COMMAND_READ_BUFFER:        return "read_buffer";
        case CL_COMMAND_WRITE_BUFFER:       return "write_buffer";
        case CL_COMMAND_MAP_BUFFER:         return "map_buffer";
        case CL_COMMAND_UNMAP_MEM_OBJECT:   return "unmap_mem_object";
        case CL_COMMAND_BARRIER:            return "barrier";
        default: return "unknown";
    }
}

void check_err(cl_int err)
{
    if (err)
    {
        printf("%s\n", err_text(err));
        exit(1);
    }
}

char *getfile(const char *name)
{
    FILE *fp = fopen(name, "r");
    char *buffer = 0;
    if (fp)
    {
        fseek(fp, 0, SEEK_END);
        int length = ftell(fp);
        fseek(fp, 0, SEEK_SET);
        buffer = (char *)malloc(length + 1);
        if (buffer)
        {
            printf("Allocating %d bytes\n", length + 1);
            fread(buffer, 1, length, fp);
        }
        else
        {
            printf("Failed to allocate buffer for source\n");
            exit(1);
        }
        fclose(fp);
        buffer[length] = 0;
    }
    return buffer;
}


#ifndef __APPLE__
#define CL_PRINTF_CALLBACK_ARM    0x40B0
#define CL_PRINTF_BUFFERSIZE_ARM  0x40B1
void printf_callback(const char *buffer, size_t len, size_t complete, void *user_data)
{
    printf("%.*s", (int)len, buffer);
}
#endif


void context_callback(const char *errinfo, const void *private_info, size_t cb, void *user_data)
{
    printf("Context: %s\n", errinfo);
}


template <typename T> void Sim::unmap(T *mapped_ptr, cl_mem clbuffer, std::vector<cl_event> *d)
{
    cl_event e;
    DBPRINTF("unmap mem object %d\n", clbuffer);
    err = clEnqueueUnmapMemObject(queue, clbuffer, mapped_ptr,
                                  d ? d[0].size() : 0,
                                  d ? d[0].data() : 0, &e); check_err(err);
    clFlush(queue);
    cl_uint m1,m2;
    err = clGetMemObjectInfo(clbuffer, CL_MEM_MAP_COUNT, 4, &m1, 0); check_err(err);
    err = clGetMemObjectInfo(clbuffer, CL_MEM_REFERENCE_COUNT, 4, &m2, 0); check_err(err);
    //printf("unmap %d %d\n", m1,m2);
    kevents.push(e);
    
}
template <typename T> void Sim::map(T **mapped_ptr, cl_mem clbuffer, unsigned int size, std::vector<cl_event> *d)
{
    cl_event e;
    int s = sizeof(T) * num_scenes * size;
    DBPRINTF("map buffer %d\n", clbuffer);
    *mapped_ptr = (T*)clEnqueueMapBuffer(queue, clbuffer, CL_TRUE, CL_MAP_READ|CL_MAP_WRITE, 0, s,
                                           d ? d[0].size() : 0,
                                           d ? d[0].data() : 0, &e, &err); check_err(err);
    clFlush(queue);
    cl_uint m1,m2;
    err = clGetMemObjectInfo(clbuffer, CL_MEM_MAP_COUNT, sizeof(cl_uint), &m1, 0); check_err(err);
    err = clGetMemObjectInfo(clbuffer, CL_MEM_REFERENCE_COUNT, sizeof(cl_uint), &m2, 0); check_err(err);
    //printf("map   %d %d\n", m1,m2);
    kevents.push(e);
    //printf("device %d err %3d mapping %s to %lx\n", i, err, typeid(T).name(), (unsigned long)mapped_ptr[i]);
}
template <typename T> T* Sim::get_item_ptr(T *p, unsigned int i, unsigned int size)
{
    return &(p[i * size]);
}

void Sim::add_depend()
{
    // The last items added to the queue should be added to the dependency list.
    // We also increase the ref count of the event to retain it

    cl_event e = kevents.back();
    depend.push_back(e);
    DBPRINTF("retain event\n");
    err = clRetainEvent(e); check_err(err);
}

void Sim::clear_depend()
{
    // Clear the dependency list and release the events
    while (depend.size())
    {
        DBPRINTF("release event\n");
        err = clReleaseEvent(depend.back()); check_err(err);
        depend.pop_back();
    }
}


void Sim::finish_all()
{
    // Ensure all queues are flushed and completed
    cl_event    marker;
    
    DBPRINTF("enqueue marker with wait list\n");
    err = clEnqueueMarkerWithWaitList(queue, 0, NULL, &marker);
    check_err(err);
    DBPRINTF("flush\n");
    err = clFlush(queue); check_err(err);
    DBPRINTF("wait for events\n");
    err = clWaitForEvents(1, &marker); check_err(err);
    
}

void Sim::barrier()
{
    // Prevent following commands executing before preceeding ones complete    for (int i = 0; i < num_devices; i++)
    cl_event e;
    err = clEnqueueBarrierWithWaitList(queue, 0, NULL, &e);
    kevents.push(e);
}

void Sim::get_platform()
{
    printf("About to get platform..\n");
    err = clGetPlatformIDs(1, &platform_id, NULL);
    check_err(err);
}

void Sim::get_devices()
{
    printf("About to get devices..\n");
    //err = clGetDeviceIDs(platform_id, CL_DEVICE_TYPE_CPU, 1, &device_id, NULL);
    //err = clGetDeviceIDs(platform_id, CL_DEVICE_TYPE_GPU, 1, &device_id, NULL);
    err = clGetDeviceIDs(platform_id, CL_DEVICE_TYPE_ALL, 1, &device_id, NULL);
    check_err(err);

    char n[1024];
    cl_uint compute_units;
    err = clGetDeviceInfo(device_id, CL_DEVICE_NAME, 1024, n, NULL); check_err(err);
    err = clGetDeviceInfo(device_id, CL_DEVICE_MAX_COMPUTE_UNITS, sizeof(cl_uint), &compute_units, NULL); check_err(err);
    printf("Device name %s has %d compute units\n", n, compute_units);
}

void Sim::get_context()
{
    //-------------------------------------------------------------------------------------
    // Set up the callback for printf. This is only necessary with the T628
    cl_context_properties properties[] =
    {
#ifdef __arm__
        CL_PRINTF_CALLBACK_ARM,   (cl_context_properties) printf_callback,
        CL_PRINTF_BUFFERSIZE_ARM, (cl_context_properties) 0x100000,
#endif
        CL_CONTEXT_PLATFORM,      (cl_context_properties) platform_id,
        0
    };
    
    //-------------------------------------------------------------------------------------
    // Create context, command queues for each device, and build the program.
    // Check execution with hello world kernel
    context     = clCreateContext(properties, 1, &device_id, NULL, NULL, &err);
    check_err(err);
    queue       = clCreateCommandQueue(context, device_id, CL_QUEUE_PROFILING_ENABLE, &err);
    if (err)
    {
        printf("Failed to create queue %d: %s\n", 0, err_text(err));
        exit(1);
    }
    printf("Queue %ld\n", (long)queue);
}


#include  <sys/resource.h>
void Sim::make_kernel()
{
    char *prog[2];
#ifndef __APPLE__
    prog[0] = getfile("/home/simonj/new_world/xps/assets/simulator.cl");
    prog[1] = getfile("/home/simonj/new_world/xps/assets/bt.cl");
#else
    prog[0] = getfile("/Network/simonj/new_world/xps/assets/simulator.cl");
    prog[1] = getfile("/Network/simonj/new_world/xps/assets/bt.cl");
#endif
    //printf("%s\n", prog);
    cl_program program = clCreateProgramWithSource(context, 2, (const char **) &prog, NULL, &err);
    if (err != CL_SUCCESS)
        printf("CreateProgramWithSource failed %d\n", err);
#ifndef __APPLE__
    err = clBuildProgram(program, 0, NULL, "-I /home/simonj/new_world/xps/assets", NULL, NULL); //check_err(err);
#else
    //err = clBuildProgram(program, 0, NULL, "-I /Network/simonj/new_world/xps/assets -cl-opt-disable", NULL, NULL); //check_err(err);
    err = clBuildProgram(program, 0, NULL, "-I /Network/simonj/new_world/xps/assets", NULL, NULL); //check_err(err);
#endif
    if (err == CL_BUILD_PROGRAM_FAILURE)
    {
        char s[32768];
        clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG, 32768, s, NULL);
        printf("Build error\n%s\n", s);
        exit(1);
    }
    // Test with hello world
    printf("Creating kernel hello\n");
    cl_kernel hello   = clCreateKernel(program, "hello_world", NULL);
    // Now create the real kernel
    printf("Creating kernel simstep\n");
    simstep = clCreateKernel(program, "simstep", &err);
    check_err(err);
    printf("Creating kernel print_tree\n");
    print_tree = clCreateKernel(program, "print_tree", &err);
    check_err(err);
    //printf("Creating kernel ctrlstep\n");
    //ctrlstep = clCreateKernel(program, "ctrlstep", &err);
    //check_err(err);
    
    printf("clScene         %lu\n", sizeof(clScene));
    printf("clSceneSense    %lu\n", sizeof(clSceneSense));
    printf("clSceneControl  %lu\n", sizeof(clSceneControl));
    printf("clPoseSense     %lu\n", sizeof(clPoseSense));
    
    size_t  gsize = 1;
    clEnqueueNDRangeKernel(queue, hello, 1, NULL, &gsize, NULL, 0, NULL, NULL);
    clFinish(queue);
    return;
    
    struct rusage r;
    
    
    for(int j = 0; j < 1000000; j++)
    {
        //clEnqueueNDRangeKernel(queue[i], hello, 1, NULL, &global, NULL, 0, NULL, &e);
        clEnqueueNDRangeKernel(queue, hello, 1, NULL, &gsize, NULL, 0, NULL, NULL);
        clFlush(queue);
        
        getrusage(RUSAGE_SELF, &r);
        printf("mem:%lu %lu\n", r.ru_maxrss, r.ru_isrss);
    }
    printf("Finished kernel creation and initial test\n");
    exit(1);
    
    
    //simple  = clCreateKernel(program, "simple", NULL);
}

void Sim::process_events(bool output)
{
    // Handle queue of events, turning them into VCD or stdout. This must be called regularly,
    // otherwise kevents will grow unrestrained.
    //
    // Print the profile information associated with all complete groups of events on the event queueus
    // Get the status
    //printf("eq %d %d\n", kevents[0].size(), kevents[1].size());
    while (true)
    {

        if (!kevents.size())
            return;
        cl_int status;
        err = clGetEventInfo(kevents.front(), CL_EVENT_COMMAND_EXECUTION_STATUS, sizeof(cl_int), &status, NULL);
        check_err(err);
        if (status != CL_COMPLETE)
            return;
        
        // Events at the front of the queue is complete
        total_events++;
        if (output)
            printf("Event %5d ", total_events);

        cl_ulong t1,t2,t3,t4;
        cl_event e = kevents.front();
        cl_command_type ctype;
        err = clGetEventInfo(e, CL_EVENT_COMMAND_TYPE, sizeof(cl_command_type), &ctype, NULL); check_err(err);
        err = clGetEventProfilingInfo(e, CL_PROFILING_COMMAND_QUEUED,    sizeof(cl_ulong), &t1, NULL); check_err(err);
        err = clGetEventProfilingInfo(e, CL_PROFILING_COMMAND_SUBMIT,    sizeof(cl_ulong), &t2, NULL); check_err(err);
        err = clGetEventProfilingInfo(e, CL_PROFILING_COMMAND_START,     sizeof(cl_ulong), &t3, NULL); check_err(err);
        err = clGetEventProfilingInfo(e, CL_PROFILING_COMMAND_END,       sizeof(cl_ulong), &t4, NULL); check_err(err);
        if (output)
            printf("%4d %16s %12llu %9llu %9llu     ", (int)kevents.size(), cmd_text(ctype), t1, t4-t1, t4-t3);
        kernel_execution_time += t4 - t3;
        err = clReleaseEvent(e); check_err(err);
        auto signame = cmd_text(ctype) + std::to_string(0);
        if (vcdtrace)
            vcd->addevent(signame, t1, t2, t3, t4);
        kevents.pop();
        if (output)
            printf("\n");
        
    }
}


void Sim::make_pipeline()
{
    //-----------------------------------------------------------------------------------
    // We split the workload according to to the number of compute units in each device.
    // Each work item is a complete 2D physics simulation
    // There are three set of buffers:
    //  scene  - these contain all the information necessary for the physics simulation
    //  senses  - these contain the senses for each robot within the scenes
    //  control - these contain the control outputs for each robot within the scenes
    //
    // The scenes are set up once on the CPU and then unmapped to place in GPU land. They
    // are never transferred back
    //
    // Senses are derived from the physics simulation, and are transferred back to the cpu
    // for the input to the robot controller.
    // Control is the control output of the controllers, and is transferred to the GPU
    // to affect the physics model
    // Senses and control are double buffered:
    //
    //  T   cpu                     gpu
    //  0   R sense 0, W ctrl 0     R ctrl 1, W sense 1
    //  1   R sense 1, W ctrl 1     R ctrl 0, W sense 0
    //  2   R sense 0, W ctrl 0     R ctrl 1, W sense 1
    //  3   ...
    //
    // There are multiple simulation physics steps for each controller step. E.g
    // physics at 20ms period, controller at 100ms period

    // Calculate the sizes of the buffers
    scene_buf_size      = sizeof(clScene) * num_scenes;
    senses_buf_size     = sizeof(clSceneSense) * num_scenes;
    control_buf_size    = sizeof(clSceneControl) * num_scenes;
    if (params.heterogeneous)
        bt_buf_size         = max_bt_size * num_agents * num_scenes;
    else
        bt_buf_size         = max_bt_size * num_scenes;
    btvars_buf_size     = max_btvars_size * num_agents * num_scenes;
    param_buf_size      = sizeof(clParams) * num_scenes; //FIXME this is a hack because the map/unmap templates assume all buffers are per scene
    
    
    // Create opencl buffer objects. For the three buffer types, there is one for
    // each of the Opencl devices, or two for senses and control which are double buffered
    scene_buf           = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_ALLOC_HOST_PTR, scene_buf_size, NULL, &err); check_err(err);
    senses_buf          = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_ALLOC_HOST_PTR, senses_buf_size, NULL, &err); check_err(err);
    control_buf         = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_ALLOC_HOST_PTR, control_buf_size, NULL, &err); check_err(err);
    bt_buf              = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_ALLOC_HOST_PTR, bt_buf_size, NULL, &err); check_err(err);
    btvars_buf          = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_ALLOC_HOST_PTR, btvars_buf_size, NULL, &err); check_err(err);
    param_buf           = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_ALLOC_HOST_PTR, param_buf_size, NULL, &err); check_err(err);
    

        
        // Clear the accumulated kernel execution time
    kernel_execution_time = 0;

}




void Sim::postamble()
{
    // Get the scene buffer back into CPU space and examine it
    map(&scene_ptr, scene_buf);
    finish_all();
    for (int j = 0; j < ((num_scenes > 16) ? 16 : num_scenes); j++)
    {
        printf("%2d %f %f\n", j, get_item_ptr(scene_ptr, j)->bodies[0].position.x, get_item_ptr(senses_ptr, j)->sense[0].position.x);
    }
    process_events(false);
    if (vcdtrace)
        vcd->dump();
}

void Sim::set_motor(float wheel_left, float wheel_right)
{
    sim_wheel_left = wheel_left;
    sim_wheel_right = wheel_right;
}




void Sim::addbody(int sidx, float x, float y, float theta, Physparam &param, clStype type)
{
    //##FIXME## check that bodies added in correct order, must be robot->object->lines
    clBody *clb     = get_item_ptr(scene_ptr, sidx)->bodies + body_count;
    
    clb->position.x         = x;
    clb->position.y         = y;
    clb->velocity.x         = 0;
    clb->velocity.y         = 0;
    clb->theta              = theta;
    clb->omega              = 0;
    clb->torque             = 0;

    //clb->m                  = param.mass;
    clb->im                 = param.mass ? 1.0 / param.mass : 0.0;
    // Moment of inertia of disk I = 0.5mr^2
    //clb->I                  = 0.5 * param.mass * param.radius * param.radius;
    clb->iI                 = param.mass ? 1.0 / (0.5 * param.mass * param.radius * param.radius) : 0.0;

    //clb->static_friction    = param.static_friction;
    //clb->dynamic_friction   = param.dynamic_friction;
    //clb->restitution        = param.restitution;
    clb->mu                 = param.mu;
    clb->type               = type;
    clb->radius             = param.radius;
    clb->colour             = param.colour;
    //clb->fitness            = 0;
    
    body_count++;

    if (sidx == 0)
    {
        sim_body_count = body_count;
    }
    
    //printf("shape:%ul body:%ul\n",sizeof(clShape), sizeof(clBody));
}


void Sim::addwall(int sidx)
{
    // H and VLINE have the size of the line in the other position axis
    // i.e an HLINE is defined by the y position, but the x position contains its 'size'.
    // This has no meaning in terms of the physics, the lines are regarded as infinite,
    // but is used for visualisation
    clBody *clb             = get_item_ptr(scene_ptr, sidx)->bodies + body_count;
    clb->position.x         = 0;
    clb->position.y         = 0;
    //clb->m = clb->im = clb->I = clb->iI = 0;
    clb->im = clb->iI = 0;
    //clb->static_friction    = 0.5f;
    //clb->dynamic_friction   = 0.5f;
    //clb->restitution        = 0.5f;
    clb->mu                 = 0.5f;
    clb->type               = HLINE;
    body_count++;
}

void Sim::render(bool update)
{
    // We render only the first scene
    map(&scene_ptr, scene_buf);
    map(&senses_ptr, senses_buf);
    map(&control_ptr, control_buf);
    finish_all();
    
    // We use this point to update GPU-side parameters for GUI interactions
    // Copy in the motor commands
    //get_item_ptr(control_ptr, 0)->control[0].motor.x = sim_wheel_left;
    //get_item_ptr(control_ptr, 0)->control[0].motor.y = sim_wheel_right;
    
    // first do dragging
    for (int i = 0; i < sim_body_count; i++)
    {
        clBody      &r = get_item_ptr(scene_ptr, 0)->bodies[i];
        clPoseSense &s = get_item_ptr(senses_ptr, 0)->sense[i];
        // Save the poses so that the GUI can interact with them
        if (sim_dragged == i)
        {
            // If a robot got dragged, copy position back into GPU domain
            r.position.x = sim_positions[i].x;
            r.position.y = sim_positions[i].y;
            sim_dragged = -1;
        }
        sim_positions[i].x = r.position.x;
        sim_positions[i].y = r.position.y;
    }
    // update frisbee mass if necessary
    if (update_object)
    {
        clBody &r   = get_item_ptr(scene_ptr, 0)->bodies[sim_body_count - 1];
        r.im        = object_param.mass ? 1.0 / object_param.mass : 0.0;
        r.iI        = object_param.mass ? 1.0 / (0.5 * object_param.mass * object_param.radius * object_param.radius) : 0.0;
    }
    
    // Render camera visualisation
    for (int i = 0; i < num_agents; i++)
    {
        clBody      &r = get_item_ptr(scene_ptr, 0)->bodies[i];
        clPoseSense &s = get_item_ptr(senses_ptr, 0)->sense[i];
        if (r.type == ROBOT)
        {
            // Render sensor radii
            glColor3f(0.7f, 0.2f, 0.2f);
            glBegin(GL_LINES);
            for (int j = 0; j < NUM_SENSORS; j++)
            {
                glVertex2f(r.position.x, r.position.y);
                float dist = (float)s.prox[j] / 100000;
                Vec2 t(dist * std::cos(r.theta + sensor_angles[j]), dist * std::sin(r.theta + sensor_angles[j]));
                glVertex2f(r.position.x + t.x, r.position.y + t.y);
            }
            glEnd();
            
            if (render_blob_detect)
            {
                glShadeModel(GL_FLAT);
                glEnable (GL_BLEND);
                glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
                int rl, rc, rr, gl, gc, gr, bl, bc, br;
                rl = (s.blobs >> 6) & 1;
                rc = (s.blobs >> 3) & 1;
                rr = (s.blobs >> 0) & 1;
                gl = (s.blobs >> 7) & 1;
                gc = (s.blobs >> 4) & 1;
                gr = (s.blobs >> 1) & 1;
                bl = (s.blobs >> 8) & 1;
                bc = (s.blobs >> 5) & 1;
                br = (s.blobs >> 2) & 1;
                glBegin(GL_TRIANGLE_FAN);
                glVertex2f(r.position.x, r.position.y);
                float l = 0.5f;
                Vec2 v1(l * std::cos(r.theta + camera_angles[0]), l * std::sin(r.theta + camera_angles[0]));
                Vec2 v2(l * std::cos(r.theta + camera_angles[1]), l * std::sin(r.theta + camera_angles[1]));
                Vec2 v3(l * std::cos(r.theta + camera_angles[2]), l * std::sin(r.theta + camera_angles[2]));
                Vec2 v4(l * std::cos(r.theta + camera_angles[3]), l * std::sin(r.theta + camera_angles[3]));
                glVertex2f(r.position.x + v1.x, r.position.y + v1.y);
                glColor4f(rr, gr, br, 0.12f);
                glVertex2f(r.position.x + v2.x, r.position.y + v2.y);
                glColor4f(rc, gc, bc, 0.12f);
                glVertex2f(r.position.x + v3.x, r.position.y + v3.y);
                glColor4f(rl, gl, bl, 0.12f);
                glVertex2f(r.position.x + v4.x, r.position.y + v4.y);
                glEnd();
            }
        }
    }
    
    // Render the bodies
    for (int i = 0; i < sim_body_count; i++)
    {
        clBody      &r = get_item_ptr(scene_ptr, 0)->bodies[i];
        clPoseSense &s = get_item_ptr(senses_ptr, 0)->sense[i];

        if ((r.type == ROBOT) || (r.type == OBJECT))
        {
            if (0&&(update))printf("agent:%2d x:%10f y:%10f a:%10f  %10f %10f %3d %3d %3d %3d %3d %3d %3d %3d %lu\n", i,
                   r.position.x,
                   r.position.y,
                   r.theta,
                   sim_positions[i].x,
                   sim_positions[i].y,
                   s.prox[0],
                   s.prox[1],
                   s.prox[2],
                   s.prox[3],
                   s.prox[4],
                   s.prox[5],
                   s.prox[6],
                   s.prox[7], sizeof(clSceneSense)
                   );


            // Render a circle as filled with colour of object and
            // with an outer line
            if (r.type == OBJECT)
                glColor3f((float)(r.colour & 1)/2+0.5, (float)((r.colour >> 1) & 1)/2+0.5, (float)((r.colour >> 2) & 1)/2+0.5);
            else
            {
                // For robot, shade according to fitness
                float f = s.fitness;
                float red = (r.colour & 1) * (f + 0.5);
                float green = ((r.colour >> 1) & 1) * (f + 0.5);
                float blue = ((r.colour >> 2) & 1) * (f + 0.5);
                glColor3f(red, green, blue);
            }
            const int k_segments = 32;
            float inc = M_PI * 2.0f / k_segments;
            float theta = r.theta;
            glBegin(GL_TRIANGLE_FAN);
            glVertex2f(r.position.x, r.position.y);
            for(int i = 0; i <= k_segments; ++i)
            {
                theta += inc;
                Vec2 p(std::cos(theta), std::sin(theta));
                p.x *= r.radius;
                p.y *= r.radius;
                p.x += r.position.x;
                p.y += r.position.y;
                glVertex2f(p.x, p.y);
            }
            glEnd();
            
            glColor3f(0.1f, 0.5f, 0.2f);
            glBegin(GL_LINE_LOOP );
            theta = r.theta;
            for(int i = 0; i < k_segments; ++i)
            {
                theta += inc;
                Vec2 p(std::cos(theta), std::sin(theta));
                p.x *= r.radius;
                p.y *= r.radius;
                p.x += r.position.x;
                p.y += r.position.y;
                glVertex2f(p.x, p.y);
            }
            glEnd();
            
            // Render line within circle so orientation is visible
            glColor3f(1,1,1);
            glBegin( GL_LINE_STRIP );
            Vec2 t(std::cos(r.theta), std::sin(r.theta));
            t.x *= r.radius;
            t.y *= r.radius;
            t.x += r.position.x;
            t.y += r.position.y;
            glVertex2f(r.position.x, r.position.y);
            glVertex2f(t.x, t.y);
            glEnd();
        }
    }

    // Render attraction vectors
    for (int i = 0; i < num_agents; i++)
    {
        clBody      &r = get_item_ptr(scene_ptr, 0)->bodies[i];
        clPoseSense &s = get_item_ptr(senses_ptr, 0)->sense[i];

        // Render range and bearing as a triangle with base thickness depending on strength
        if (s.n)
        {
            float a = r.theta + atan2(s.vattr.y, s.vattr.x);
            float dist = 0.01 * sqrt(pow(s.vattr.x, 2) + pow(s.vattr.y, 2));
            glColor3f( 0.2f, 0.85f, 0.85f );
            glBegin(GL_TRIANGLES);
            glVertex2f(r.position.x + 0.1 * cos(a), r.position.y + 0.1 * sin(a));
            glVertex2f(r.position.x + dist * cos(a+M_PI/2), r.position.y + dist * sin(a+M_PI/2));
            glVertex2f(r.position.x + dist * cos(a-M_PI/2), r.position.y + dist * sin(a-M_PI_2));
            glEnd();
        }
    }
    
    // Render fitness
    float f = 0;
    for (int i = 0; i < sim_body_count; i++)
    {
        clBody      &r = get_item_ptr(scene_ptr, 0)->bodies[i];
        clPoseSense &s = get_item_ptr(senses_ptr, 0)->sense[i];

        // Render fitness by each agent
        
        {
            glColor3f(1.0f, 1.0f, 1.0f);
            glRasterPos2f(r.position.x, r.position.y);
            std::string fs = string_format("%4.2f", s.fitness);
            for( int i = 0; i < fs.size(); i++ )
                glutBitmapCharacter(GLUT_BITMAP_9_BY_15, fs[i]);
            
            f = s.fitness;
        }
        
    }
    
    
    {
        // Calc mode 2 fitness for display
        float time = stepnum * sim_param.ctrl_steps * sim_param.ctrl_period * sim_param.dt;
        float vel = f / time;
        float mode2_fitness = (vel + MAX_VELOCITY) / (2 * MAX_VELOCITY);
        glRasterPos2f(-1, 0.77);
        std::string fs = string_format("%4.2f", mode2_fitness);
        for( int i = 0; i < fs.size(); i++ )
            glutBitmapCharacter(GLUT_BITMAP_9_BY_15, fs[i]);
    }
    
    
    // Draw border
    // Render fixed polygons
    glColor3f( 0.1f, 0.2f, 0.3f );
    glBegin(GL_LINE_LOOP);
    glVertex2f(-sim_param.border.x, -sim_param.border.y);
    glVertex2f( sim_param.border.x, -sim_param.border.y);
    glVertex2f( sim_param.border.x,  sim_param.border.y);
    glVertex2f(-sim_param.border.x,  sim_param.border.y);
    glEnd();


    
    //print_btstatus();
    
    
    unmap(scene_ptr, scene_buf);
    unmap(senses_ptr, senses_buf);
    unmap(control_ptr, control_buf);

    process_events(false);
    finish_all();

}

void Sim::print_btstatus()
{
    int agent = 0;
    int scene = 0;
    map(&bt_ptr, bt_buf, max_bt_size);
    map(&btvars_ptr, btvars_buf, max_btvars_size * num_agents);
    finish_all();
    
    unsigned char *bt = get_item_ptr(bt_ptr, scene, max_bt_size);
    unsigned char *bv = get_item_ptr(btvars_ptr, scene * num_agents + agent, max_btvars_size);
    // Structure of bt
    // Byte address
    //  0x0     root (ushort)
    //  0x2     total size (ushort)
    //  0x4     number of nodes (ushort)
    //  0x6     number of blackboard entries (ushort)
    //  0x8     tree
    //
    // Variable tree storage. This contains the random seed, the
    // blackboard variables, and two chars per tree node, one for status, one for idx or count for
    // the nodes that need it.
    // Byte address
    //  0x0         random seed 0
    //  0x8         bb entry 0  goal_vector.x
    //  0xc                     goal_vector.y
    //  0x
    //  ...
    //  vstart      node 0 status
    //  vstart+2    node 1 status
    //  ...
    //
    uint16_t root = *(uint16_t*)bt;
    uint16_t bbsize = *(uint16_t*)(bt + 4);
    uint16_t bbentries = *(uint16_t*)(bt + 6);
    printf("bv:%08x root:%04x bbs:%04x bbe:%04x\n", bv, root, bbsize, bbentries);
    uchar *status = bv + 8 + bbentries * 4;
    
//    for(int i = 0; i < 0x100; i++)
//    {
//        if (i%16 == 0)
//            printf("\n%04x ", i);
//        printf("%02x ", status[i]);
//    }
//    printf("\n");
    
    BT t(bbentries, "successl ");
    t.cltree.resize(max_bt_size);
    memcpy(t.cltree.data(), bt, max_bt_size);
    t.print_tree(root, 0, status);
 
    
    unmap(bt_ptr, bt_buf);
    unmap(btvars_ptr, btvars_buf);
    finish_all();
}

void Sim::print_status()
{
    map(&senses_ptr, senses_buf);
    finish_all();
    for (int i = 0; i < num_agents; i++)
    {
        clPoseSense r = get_item_ptr(senses_ptr, 0)->sense[i];
        
        
        Vec2 vprox(0,0);
        {
            for(int i = 0; i < 8; i++)
            {
                float s = (params.sensor_max_radius - (float)r.prox[i] * 0.00001f) / (params.sensor_max_radius - params.sensor_min_radius);
                vprox.x += cos_sa[i] * s;
                vprox.y += sin_sa[i] * s;
            }
        }
        printf("agent:%2d x:%10f y:%10f a:%10f  %3d %3d %3d %3d %3d %3d %3d %3d  rnb:%2d % 8f % 8f  % 8f % 8f\n", i,
               r.position.x,
               r.position.y,
               r.theta,
               r.prox[0],
               r.prox[1],
               r.prox[2],
               r.prox[3],
               r.prox[4],
               r.prox[5],
               r.prox[6],
               r.prox[7],
               r.n, r.vattr.x, r.vattr.y, vprox.x, vprox.y
               );
    }
    unmap(senses_ptr, senses_buf);
    finish_all();
}

void Sim::create_scene(double *s)
{
    // Create a scene, designed to be called from Python. All scenes are
    // described in an array of doubles describing (x, y, th) for each robot and
    // object in each scene
    
    //printf("Create scene from python\n");
    // Map scene into CPU space
    if (params.verbose>1)printf("Creating scene..\n");
    map(&scene_ptr, scene_buf);
    // Map the sense and control buffers into CPU space
    map(&senses_ptr, senses_buf);
    map(&control_ptr, control_buf);
    map(&param_ptr, param_buf);
    if (params.verbose>1)printf("Finish mapping..\n");
    finish_all();
    
    //memset(scene_ptr, );

    // Set up the scenes and zero sensor inputs and control outputs
    if (params.verbose>1)printf("Set counts and zero sensors..\n");
    for(int i = 0; i < num_scenes; i++)
    {
        //get_item_ptr(scene_ptr, i)->body_count = 0;
        //get_item_ptr(scene_ptr, i)->robot_count = 0;
        //get_item_ptr(scene_ptr, i)->object_count = 0;
        for(int j = 0; j < num_agents; j++)
        {
            for(int k = 0; k < NUM_SENSORS; k++)
                get_item_ptr(senses_ptr, i)->sense[j].prox[k] = 0;
            get_item_ptr(senses_ptr, i)->sense[j].n = 0;
            get_item_ptr(senses_ptr, i)->sense[j].fitness = 0;
            get_item_ptr(control_ptr, i)->control[j].motor = Vec2(0,0);
        }
    }
    
    // Create the scene and replicate in all gpu sim scenes.
    // We use the Body class from the CPU sim then copy this across into the GPU world
    if (params.verbose>1)printf("Create bodies and walls..\n");
    float spacing = 0.1f;
    sim_param.border = Vec2(1.0f, 0.75f);
    for(int i = 0; i < num_scenes; i++)
    {
        double *scene_in = s + i * (num_agents + 1) * 3;
        memcpy(param_ptr + i, &sim_param, sizeof(clParams));
        body_count = 0;
        for (int j = 0; j < num_agents; j++)
        {
            double x    = scene_in[j * 3];
            double y    = scene_in[j * 3 + 1];
            double th   = scene_in[j * 3 + 2];
            robot_param.colour = COLOUR_RED;
            addbody(i, x, y, th, robot_param, ROBOT);
            //printf("%d:%d %llx %llx %10f %10f %10f\n", i, j, s, scene_in,x, y, th);
        }
        for (int j = 0; j < num_objects; j++)
        {
            double x    = scene_in[(num_agents + j) * 3];
            double y    = scene_in[(num_agents + j) * 3 + 1];
            double th   = scene_in[(num_agents + j) * 3 + 2];
            object_param.colour = COLOUR_BLUE;
            addbody(i, x, y, th, object_param, OBJECT);
            //printf("%d:%d %10f %10f %10f\n", i, j, x, y, th);
        }
        // Add a final dummy object with infinite mass that will be used for arena wall
        // collisions
        addwall(i);
        //printf("scene %d bodies %d\n", i, get_item_ptr(scene_ptr, i)->body_count);

    }
    
    // Give each sim its own random seed
    if (params.verbose>1)printf("Copy in parameters..\n");
    std::seed_seq seq{master_seed};
    std::vector<std::uint32_t> seeds(num_scenes);
    seq.generate(seeds.begin(), seeds.end());
    for(int i = 0; i < num_scenes; i++)
        get_item_ptr(scene_ptr, i)->seed = seeds[i];
    
    // After each scene, kick the seed, otherwise, the fittest solution always
    // gets evaluated again with the same seeds
    master_seed++;
    // Make the scene available to the GPU
    unmap(param_ptr, param_buf);
    unmap(scene_ptr, scene_buf);
    unmap(senses_ptr, senses_buf);
    unmap(control_ptr, control_buf);
    finish_all();
  
}


void Sim::create_scene(Json *wf, std::string p)
{
    // Create scene using json desciption if available, otherwise random
    
    // Map scene into CPU space
    if (params.verbose>1)printf("Creating scene..\n");
    map(&scene_ptr, scene_buf);
    // Map the sense and control buffers into CPU space
    map(&senses_ptr, senses_buf);
    map(&control_ptr, control_buf);
    map(&param_ptr, param_buf);
    if (params.verbose>1)printf("Finish mapping..\n");
    finish_all();
    
    // Set up the scenes and zero sensor inputs and control outputs
    if (params.verbose>1)printf("Set counts and zero sensors..\n");
    memset(scene_ptr, 0, scene_buf_size);
    memset(senses_ptr, 0, senses_buf_size);
    memset(control_ptr, 0, control_buf_size);
    // Create the scene and replicate in all gpu sim scenes.
    // We use the Body class from the CPU sim then copy this across into the GPU world
    // Make half the agents red, the others green, and the object blue
    if (params.verbose>1)printf("Create bodies and walls..\n");

    std::seed_seq seq{master_seed};
    std::vector<std::uint32_t> seeds(num_scenes);
    seq.generate(seeds.begin(), seeds.end());
    if (params.fitness_mode>=3)
    {
        randrange_seed(seeds[0]);
    }

    sim_param.border = Vec2(1.0f, 0.75f);
    for(int i = 0; i < num_scenes; i++)
    {
        // Record in detail the starting condition of each scene
        all_evc.push_back(Evalcond());

        // Copy in the simulation parameters
        if (param_sweep)
        {
            float v0 = (*wf)["params"][p][0].number_value();
            float v1 = (*wf)["params"][p][1].number_value();

            // Interpolate over parameter values
            float fraction = (float)i / (num_scenes - 1);
            float v = v0 + fraction * (v1 - v0);
            if (p == "dyn_mu")
                sim_param.dynamic_friction = v;
            else if (p == "restitution")
                sim_param.restitution = v;
            else if (p == "xp_mu")
                robot_param.mu = v;
            else if (p == "fr_mu")
                object_param.mu = v;
            else if (p == "fr_radius")
                object_param.radius = v;
            else if (p == "fr_mass")
                object_param.mass = v;
            else if (p == "max_velocity")
                sim_param.max_velocity = v;
            else if (p == "k1")
                sim_param.k1 = v;
            else if (p == "k2")
                sim_param.k2 = v;
            else if (p == "k3")
                sim_param.k3 = v;
            // Copy in the parameters
            printf("Param %s:%f\n", p.c_str(), v);
        }
        
        //-------------------------------------
        //#FIXME## This is all a nasty hack to get env sensing working quickly
        //object_param.mass = params.fr_mass;
        if (param_alternate)
        {
                object_param.mass = params.fr_mass_alt[1];
        }
        else
        {
                object_param.mass = params.fr_mass_alt[0];
        }
        //-------------------------------------
        
        memcpy(param_ptr + i, &sim_param, sizeof(clParams));
        
        // Hacky - get the loctaion of the frisbee here if we are using a JSON worldfile so that
        // we can ensure that the robot locations will not clash with it. Assumes only one frisbee.
        float2 json_frisbee;
        const Json *o;
        if (wf && (o = &(*wf)["scene"][params.wfindex]["frisbee"][0])->is_array())
        {
            json_frisbee.x = (*o)[0].number_value();
            json_frisbee.y = (*o)[1].number_value();
        }

        body_count = 0;
        std::vector<float2> rpos(num_agents);
        for (int j = 0; j < num_agents; j++)
        {
            if (wf && (o = &(*wf)["scene"][params.wfindex]["robot"][j])->is_array())
            {
                float x     = (*o)[0].number_value();
                float y     = (*o)[1].number_value();
                float th    = (*o)[2].number_value();
                robot_param.colour = COLOUR_RED;
                if ((*wf)["params"]["pnoise"].is_array())
                {
//                    printf("%f %f %f\n", (*wf)["params"]["pnoise"][0].number_value(),
//                           (*wf)["params"]["pnoise"][1].number_value(),
//                    (*wf)["params"]["pnoise"][2].number_value());
                    bool found = false;
                    float nx, ny, nth;
                    while (!found)
                    {
                        nx = randnorm(x, (*wf)["params"]["pnoise"][0].number_value());
                        ny = randnorm(y, (*wf)["params"]["pnoise"][1].number_value());
                        nth = randnorm(th, (*wf)["params"]["pnoise"][2].number_value());
                        //printf("%f %f %f\n", x, y, th);
                        bool clash = false;
                        // quick hack to stop frisbee overlap
                        if (sqrt(pow(nx - json_frisbee.x, 2) + pow(ny - json_frisbee.y, 2)) < 0.15)
                            clash = true;
                        for (int k = 0; k < j; k++)
                        {
                            float2 rp = rpos[k];
                            if (sqrt(pow(nx - rp.x, 2) + pow(ny - rp.y, 2)) < 0.1)
                            {
                                clash = true;
                                break;
                            }
                        }
                        found = !clash;
                        if ((nx < -0.9) || (nx > 0.9) || (ny < -0.7) || (ny > 0.7))
                            found = false;
                    }
                    x = nx;
                    y = ny;
                    th = nth;
                }
                rpos[j].x = x;
                rpos[j].y = y;
                addbody(i, x, y, th, robot_param, ROBOT);
                all_evc.back().add(x, y, th);
                if (params.verbose>1)printf("Created robot x:%f y:%f th:%f\n", x,y,th);
            }
            else
            {
                bool found = false;
                float x, y, th;
                while (!found)
                {
                    if ((params.fitness_mode == 3) || (params.fitness_mode == 5))
                    {
                        if (randrange(0,1) < 0.5)
                            x = randrange(-0.8, +0.8);
                        else
                            x = randrange(-0.9, -0.5);
                        y = randrange(-0.6, 0.6);
                    }
                    else if (params.fitness_mode == 4)
                    {
                        x = randrange(-0.8, +0.8);
                        y = randrange(-0.55, 0.55);
                    }
                    else
                    {
                        x = randrange(-0.9, -0.5);
                        y = randrange(-0.5, 0.5);
                    }
                    th = randrange(-M_PI, M_PI);
                    bool clash = false;
                    for (int k = 0; k < j; k++)
                    {
                        float2 rp = rpos[k];
                        if (sqrt(pow(x - rp.x, 2) + pow(y - rp.y, 2)) < 0.1)
                        {
                            clash = true;
                            break;
                        }
                    }
                    found = !clash;
                }
                if (params.verbose>1)printf("Created robot at x:%f y:%f th:%f\n", x, y, th);
                rpos[j].x = x;
                rpos[j].y = y;
                addbody(i, x, y, th, robot_param, ROBOT);
                all_evc.back().add(x, y, th);
            }
        }
        for (int j = 0; j < num_objects; j++)
        {
            const Json *o;
            if (wf && (o = &(*wf)["scene"][params.wfindex]["frisbee"][j])->is_array())
            {
                float x = (*o)[0].number_value();
                float y = (*o)[1].number_value();
                float th = (*o)[2].number_value();
                object_param.colour = COLOUR_BLUE;
                addbody(i, x, y, th, object_param, OBJECT);
                all_evc.back().add(x, y, 0);
                all_startcoord.push(x);
                if (params.verbose>1)printf("Created frisbee x:%f y:%f th:%f\n", x,y,th);

            }
            else
            {
                object_param.colour = COLOUR_BLUE;
                //addbody(i, 0.0, j * spacing, 0.5, object_param, OBJECT);
                float x, y;
                if (params.fitness_mode >= 3)
                {
                    x = randrange(-0.1, 0.1);
                    y = randrange(-0.6, 0.6);
                }
                else
                {
                    x = randrange(0.6, 0.88);
                    y = randrange(-0.6, 0.6);
                }
                addbody(i, x, y, 0, object_param, OBJECT);
                all_evc.back().add(x, y, 0);
                all_startcoord.push(x);
            }
        }
        // Add a final dummy object with infinite mass that will be used for arena wall
        // collisions
        addwall(i);
        //printf("scene %d bodies %d\n", i, get_item_ptr(scene_ptr, i)->body_count);

    }
    
    // Give each sim its own random seed
    if (params.verbose>1)printf("Copy in parameters..\n");
    
    int json_seed = 0;
    if (wf && (*wf)["scene"][params.wfindex]["seed"].is_number())
    {
        json_seed = (*wf)["scene"][params.wfindex]["seed"].number_value();
        printf("Setting specific simulation seed %d\n", json_seed);
    }

    for(int i = 0; i < num_scenes; i++)
        if (json_seed)
        {
            get_item_ptr(scene_ptr, i)->seed = json_seed;
            all_evc[all_evc.size() - num_scenes + i].seed = json_seed;
        }
        else
        {
            get_item_ptr(scene_ptr, i)->seed = seeds[i];
            //printf("s %d\n", seeds[i]);
            all_evc[all_evc.size() - num_scenes + i].seed = seeds[i];
            if (params.verbose>1)printf("Master seed %d derived seed %d\n\n", master_seed, seeds[i]);
        }

    // After each scene, kick the seed, otherwise, the fittest solution always
    // gets evaluated again with the same seeds
    master_seed++;
    

    

    
    // Make the scene available to the GPU
    unmap(param_ptr, param_buf);
    unmap(scene_ptr, scene_buf);
    unmap(senses_ptr, senses_buf);
    unmap(control_ptr, control_buf);
    finish_all();
  
}


void Sim::evaluate_bt(std::vector<BT *> *t, int reps, int csteps, Json *wf)
{
    repeats = reps;
    int num_bt = t->size();
    float fr = (float)num_bt * repeats / num_scenes;
    int runs = (int)fr;
    int ntrees = 0;
    if (params.heterogeneous)
    {
        runs = repeats / num_scenes;
    }
    else
    {
        while ((float)runs != fr)
        {
            // Stuff the sim with null trees to make an integer number of runs
            t->push_back(nulltree);
            ntrees++;
            num_bt++;
            fr = (float)num_bt * repeats / num_scenes;
            runs = (int)fr;
        }
    }
    
    if (params.verbose>1)printf("Evaluate bt:%d reps:%d scenes:%d runs:%d csteps:%d nulltrees:%d\n", num_bt, repeats, num_scenes, runs, csteps, ntrees);

    
    all_fitness.clear();
    all_lfitness.clear();
    all_startcoord = std::queue<float>();
    all_parsimony.clear();
    all_stddev.clear();
    all_evc.clear();
    for(int i = 0; i < runs; i++)
    {
        if (params.verbose>1)printf("Running set %d of %d\n", i + 1, runs);
        set_bt(t, i);
        if (wf)
            create_scene(wf);
        else
            create_scene();
        run_capture_all_fitness(csteps);
        all_fitness.insert(all_fitness.end(), fitness.begin(), fitness.end());
        all_lfitness.insert(all_lfitness.end(), lfitness.begin(), lfitness.end());
        all_parsimony.insert(all_parsimony.end(), parsimony.begin(), parsimony.end());
        all_stddev.insert(all_stddev.end(), stddev.begin(), stddev.end());
    }
    //return all_fitness;
}

void Sim::set_bt(std::vector<BT *> *t, int loop, int reps)
{
    repeats = reps;
    set_bt(t, loop);
}

void Sim::set_bt(std::vector<BT *> *t, int loop)
{
    if (params.verbose>1)printf("Setting bt\n");
    // Transfer a set of BTs to the GPU
    BT *bt;
    map(&bt_ptr, bt_buf, max_bt_size);
    map(&btvars_ptr, btvars_buf, max_btvars_size * num_agents);
    finish_all();
    
    int individual = loop * num_scenes / repeats;
    //repeats = num_scenes / t->size();
    // Give each agent its own random seed, ensure seeds are good
    // ##FIXME## We probably want to be able to specify this seed on the command line
    std::seed_seq seq{master_seed};
    std::vector<std::uint32_t> seeds(num_scenes * num_agents);
    seq.generate(seeds.begin(), seeds.end());
    //for(int i = 0; i < 100; i++)
    //    printf("s %d\n", seeds[i]);
    
    
    // Clear the state memory for the agents
    memset(btvars_ptr, 0, btvars_buf_size);
    // Clear the parsimony queue
    local_parsim.clear();
    for (int i = 0; i < num_scenes; i+= repeats)
    {
        
        // Copy the set of behaviour trees into the simulation setups, repeat
        // each one as many times as the population and num_scenes allows. Last
        // individual has excess copies which we will ignore ##FIXME## may be
        // faster to trim num_scenes, but probably best ensure population such
        // that all scenes are filled evenly.
        if (!params.heterogeneous)
        {
            bt = (*t)[individual];
            if (params.verbose>1)printf("Setting tree ind:%d >>%s<<\n", individual, bt->get_btstr().c_str());
            if (individual < t->size() - 1)
                individual++;
            // If the tree violates the resource limits, substitute a null tree that will cause a fitness of zero
            if (bt->parsimony < 0)
            {
                printf("BT resource requirement exceeded! parsimony:%f size:%d %d  nodes:%d %d\n", bt->parsimony,
                       (int)bt->cltree.size(), max_bt_size, bt->num_nodes, (max_btvars_size - BLACKBOARD_ENTRIES * 8 - 8)/2);
                bt = nulltree;
            }
        }
        for (int j = 0; j < repeats && j < num_scenes; j++)
        {
            if (!params.heterogeneous)
            {
                memcpy(get_item_ptr(bt_ptr, i + j, max_bt_size), bt->cltree.data(), max_bt_size);
                if (params.verbose>2)dumphex2(bt->cltree.data(), bt->cltree.size(), "tree");
            }
            for (int k = 0; k < num_agents; k++)
            {
                if (params.heterogeneous)
                {
                    bt = (*t)[individual];
                    if (params.verbose>1)printf("Setting tree ind:%d >>%s<<\n", individual, bt->get_btstr().c_str());
                    individual = (individual + 1) % t->size();
                    
                    memcpy(get_item_ptr(bt_ptr, (i + j) * num_agents + k, max_bt_size), bt->cltree.data(), max_bt_size);
                    
                }
                *(uint*)get_item_ptr(btvars_ptr, (i + j) * num_agents + k, max_btvars_size) = seeds[(i + j) * num_agents + k];
            }

        }

        local_parsim.push_back(bt->parsimony);
        
        
        // Shape of btvars
        //
        //  0   seed
        //  4   node 0 status
        //  5   (node 0 idx)
        //  6   node 1 status
        //  ...
        //printf("Setting random seeds for agents..\n");
        
    }
    unmap(bt_ptr, bt_buf);
    unmap(btvars_ptr, btvars_buf);
    finish_all();
    //finish_all();

}

void Sim::print_bt()
{
    if (params.verbose)printf("Attempting ocl print_tree\n");

//    size_t number = 1;
//    clSetKernelArg(print_tree, 0, sizeof(cl_mem), &bt_buf);
//    clSetKernelArg(print_tree, 1, sizeof(cl_mem), &btvars_buf);
//    clSetKernelArg(print_tree, 2, sizeof(cl_int), &max_bt_size);
//    err = clEnqueueNDRangeKernel(queue, print_tree, 1, NULL, &number, NULL, 0, NULL, NULL); check_err(err);
//    clFlush(queue);
    printf("Printed BT\n");
    finish_all();

}

void dump(FILE *fp, unsigned char *p, int l)
{
    fprintf(fp, "<size=%d hex uchar> ", l);
    for(int i = 0; i < l; i++)
        fprintf(fp, "%02x ",p[i]);
    fprintf(fp, "\n");
}

void dumphex(FILE *fp, unsigned char *p, int l, int s, char *label)
{
    fprintf(fp, "# %10s step:%4d size:%08x\n", label, s, l);
    for(int i = 0; i < l; i += 16)
        fprintf(fp, "%08x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x\n", i,
                p[i], p[i+1], p[i+2], p[i+3], p[i+4], p[i+5], p[i+6], p[i+7],
                p[i+8], p[i+9], p[i+10], p[i+11], p[i+12], p[i+13], p[i+14], p[i+15]
                );
}

void Sim::dump_state()
{
    FILE *fp = fopen("dump.hex","a");
    dumphex(fp, (unsigned char*)scene_ptr, (int)scene_buf_size, stepnum, "scene_buf");
    //dumphex(fp, (unsigned char*)senses_ptr, (int)senses_buf_size, stepnum, "senses_buf");
    //dumphex(fp, (unsigned char*)control_ptr, (int)control_buf_size, stepnum, "control_buf");
    //dumphex(fp, (unsigned char*)bt_ptr, (int)bt_buf_size, stepnum, "bt_buf");
    //dumphex(fp, (unsigned char*)btvars_ptr, *(uint16_t*)(btvars_ptr+2), stepnum, "btvars_buf");
    //dumphex(fp, (unsigned char*)param_ptr, (int)param_buf_size, stepnum, "param_buf");
    fclose(fp);
}


void Sim::step()
{
    //printf("Scene:%lu SceneSense:%lu SceneControl:%lu\n", sizeof(clScene),sizeof(clSceneSense), sizeof(clSceneControl));
    //printf("Step:%d\n", stepnum);
    cl_event e;
#ifndef __APPLE__
    //##FIXME## This needs to be tuned for the platform, but tie at one for now so that any number of agents can be used
    //size_t sswg_size = 2;
    //size_t btwg_size[2] = {4,2};
    size_t sswg_size[1];
    if (num_scenes <= 32)
        sswg_size[0] = {1};
    else
        sswg_size[0] = {64};
    //size_t btwg_size[2] = {16};
#else
    size_t sswg_size[1];
    if (num_scenes <= 32)
        sswg_size[0] = {1};
    else
        sswg_size[0] = {1};
#endif
    //finish_all();
    
    
    // Kernel signature is:
    //    __kernel void simstep
    //(
    //    global struct Scene             *scene,
    //    global struct SceneSense        *senses,
    //    global struct SceneControl      *control,
    //    global uchar                    *tree,
    //    global uchar                    *tree_vars,
    //    global struct Params            *params
    //)

#if 0
    // dump args for use with oclgrind-kernel
    printf("Stepnum %5d\n", stepnum);
    if (stepnum == 1262)
    {
        FILE *fp = fopen("dump.sim","w");
        fprintf(fp,"simulator.cl\nsimstep\n%d 1 1\n1 1 1\n", num_scenes);
        dump(fp, (unsigned char*)scene_ptr, (int)scene_buf_size);
        dump(fp, (unsigned char*)senses_ptr, (int)senses_buf_size);
        dump(fp, (unsigned char*)control_ptr, (int)control_buf_size);
        dump(fp, (unsigned char*)bt_ptr, (int)bt_buf_size);
        dump(fp, (unsigned char*)btvars_ptr, (int)btvars_buf_size);
        dump(fp, (unsigned char*)param_ptr, (int)param_buf_size);
        fclose(fp);
    }
#endif
    if (params.verbose>2)dump_state();
    if (params.controlled_sim)
        get_control_commands();
    
    
    size_t simrange[1] = {static_cast<size_t>(num_scenes)};
    clSetKernelArg(simstep, 0, sizeof(cl_mem), &scene_buf);
    clSetKernelArg(simstep, 1, sizeof(cl_mem), &senses_buf);
    clSetKernelArg(simstep, 2, sizeof(cl_mem), &control_buf);
    clSetKernelArg(simstep, 3, sizeof(cl_mem), &bt_buf);
    clSetKernelArg(simstep, 4, sizeof(cl_mem), &btvars_buf);
    //clSetKernelArg(simstep, 5, sizeof(cl_uint), &max_bt_size);
    clSetKernelArg(simstep, 5, sizeof(cl_mem), &param_buf);
    err = clEnqueueNDRangeKernel(queue, simstep, 1, 0, simrange, sswg_size, depend.size(), depend.data(), &e);
    check_err(err);
    kevents.push(e);
    //clFlush(queue);
    finish_all();

    if (params.controlled_sim)
        send_sensor_readings();
    
    //print_status();
    if (0)
    {
        for(int i = 0; i < num_scenes; i++)
            printf("scene %3d %8f %8f\n", i,
                   get_item_ptr(scene_ptr, i)->bodies[num_agents].position.x,
                   get_item_ptr(scene_ptr, i)->bodies[num_agents].position.y);
    }
    
    process_events(false);
    stepnum++;
}


void Sim::get_control_commands()
{
    for (int i = 0; i < 16; i++)
    {
        bool empty = false;
        while (!empty)
        {
            zmq::message_t  cmdm;
            sub_control_actuators[i]->recv(&cmdm, ZMQ_NOBLOCK);
            if (cmdm.size())
            {
                std::string cmds = std::string(static_cast<char *>(cmdm.data()), cmdm.size());
                int j;
                float ml, mr;
                //printf("Got message:%s\n", cmds.c_str());
                sscanf(cmds.c_str(), "xp%d %f %f", &j, &ml, &mr);
                // remap indices here
                int idx = params.xp_to_idx[j];
                if (idx < num_agents)
                {
                    get_item_ptr(control_ptr, 0)->control[idx].motor.x = ml;
                    get_item_ptr(control_ptr, 0)->control[idx].motor.y = mr;
                }
            }
            else
            {
                //printf("Got nothing\n");
                empty = true;
            }
        }
    }
}

void Sim::send_sensor_readings()
{
    for (int i = 0; i < num_agents + num_objects; i++)
    {
        cl_ushort *prox = get_item_ptr(senses_ptr, 0)->sense[i].prox;
        cl_ushort blobs = get_item_ptr(senses_ptr, 0)->sense[i].blobs;
        Vec2 position   = get_item_ptr(senses_ptr, 0)->sense[i].position;
        float theta     = get_item_ptr(senses_ptr, 0)->sense[i].theta;
        Vec2 velocity   = get_item_ptr(scene_ptr, 0)->bodies[i].velocity;
        // The velocity needs to be transformed into the robot frame and the X component
        // sent to the robot. This will be used to form the x velocity that is sent to
        // the controller when in fake mode. In real mode, xpuck_node will integrate the
        // readings from the Y accelerometer (which is aligned with what we call the X
        // axis of the robot
        float xvel      = velocity.x * cos(-theta) + velocity.y * sin(-theta);

        
        // The controller when wrapped for use in the robot expects the proximity
        // sensor results to be reverse order, and with the hardware response curve.
        // In the simulator, we have integer distance from the centre of the robot in units
        // of 10um
        //
        // Characterising the sensors gives this approximation:
        // A pretty good curve fit is:
        //
        // y' = g * ((ax + d)^-4 + o)
        // y =   0                   if y' < 0
        //       y'                  if y' < g
        //       sqrt(y' - g) + g    otherwise
        //
        // where
        // g = 3500
        // a = 40
        // d = 0.7
        // o = -0.06
        const float g = 3500.0f;
        const float a = 40.0f;
        const float d = 0.7f;
        const float o = -0.07f;
        float converted_prox[8];
        for (int j = 0; j < 8; j++)
        {
            converted_prox[j] = g * (pow(a * ((float)prox[j] * 1e-5 - params.sensor_min_radius) + d, -4) + o);
            if (converted_prox[j] < 0)
                converted_prox[j] = 0;
            else if (converted_prox[j] >= g)
                converted_prox[j] = sqrt(converted_prox[j] - g) + g;
            //if (i == 0) printf("%d %4d %4d\n", j, prox[j], (int)converted_prox[j]);
        }
        
        int idx = params.idx_to_xp[i];
        if (i == num_agents)
        {
            // Special case of frisbee, call this xp00
            idx = 0;
            position = get_item_ptr(scene_ptr, 0)->bodies[num_agents].position;
        }
        if (idx == -1)
            // Skip unmapped agents
            continue;
        std::string sensors = string_format("xp%02d %f %f %f %d %d %d %d %d %d %d %d %o %f",
                idx, position.x, position.y, theta,
                                            (int)converted_prox[7],
                                            (int)converted_prox[6],
                                            (int)converted_prox[5],
                                            (int)converted_prox[4],
                                            (int)converted_prox[3],
                                            (int)converted_prox[2],
                                            (int)converted_prox[1],
                                            (int)converted_prox[0],
                                            blobs, xvel);
        //printf("Send message:%s\n", sensors.c_str());
        zmq::message_t msg(sensors.size());
        memcpy((char*)msg.data(), &sensors[0], sensors.size());
        pub_control_sensors.send(msg);
    }
}

void Sim::capture(int samples, PoseMat &m)
{
    // Run the sim for the required number of steps, capturing the trajectories
    int stride = num_agents + 1;
    for (int i = 0; i < samples; i++)
    {
        if (params.verbose>1)printf("Iteration %5d  step %5d\n", i, stepnum);
        for(int j = 0; j < num_scenes; j++)
        {
            for(int k = 0; k < stride; k++)
            {
               m(i, j, k) = Pose(
//               i,j,k);
                   get_item_ptr(scene_ptr, j)->bodies[k].position.x,
                   get_item_ptr(scene_ptr, j)->bodies[k].position.y,
                   get_item_ptr(scene_ptr, j)->bodies[k].theta);
               //printf("%11g %11g %11g ", m(i,j,k).x, m(i,j,k).y, m(i,j,k).th);
            }
        }
        //printf("\n");
        step();
    }
}

void Sim::get_pose(int scene, int obj, Pose &m)
{
    m = Pose(get_item_ptr(scene_ptr, scene)->bodies[obj].position.x,
             get_item_ptr(scene_ptr, scene)->bodies[obj].position.y,
             get_item_ptr(scene_ptr, scene)->bodies[obj].theta);
}

void Sim::run(int steps)
{
    if(params.verbose>1)printf("steps:%d ctrl_steps:%d\n",steps,sim_param.ctrl_steps);
    for (int i = 0; i < steps / sim_param.ctrl_steps; i++)
    {
        if (params.verbose>1)printf("Iteration %5d  step %5d\r", i, stepnum);
        step();
    }
    
    // Calculate the fitnesses for the set of sims. Take the average of the fitness values
    // over repeats number of sims.
    //
    // Ftness in this sim is how far puck pushed towards left, but penalise if the
    // puck has not moved off the (0,0) position. f = x/2 + 1
    fitness.clear();
    stddev.clear();
    parsimony.clear();
    for (int i = 0; i < num_scenes; i += repeats)
    {
        std::vector<float> ltf;
        std::vector<float> gtf;
        float ps = local_parsim.front();
        local_parsim.pop_front();

        for(int j = 0; j < repeats; j++)
        {
            float x = get_item_ptr(scene_ptr, i + j)->bodies[num_agents].position.x;
            
            
            //if(params.verbose)printf("%3d %10f %10f\n",i+j, x, y);

            
            //float gf = -x / (1 - object_param.radius) * ((x >= 0)  ? 0 : 1) * (ps < 0.5 ? ps * 2 : 1);
            float sx = all_startcoord.front(); all_startcoord.pop();

            float gf = sx - x;
            //printf("%f %f %f\n", sx, x, gf);
            
            float f = 0;
            for (int k = 0; k < num_agents; k++)
            {
                float fl = get_item_ptr(senses_ptr, i + j)->sense[k].fitness;
                //if(params.verbose)printf("%3d:%2d %10f\n",i+j, k, fl);
                f += fl;
            }
            gf  *= (ps < 0.5 ? ps * 2 : 1);
            f   *= (ps < 0.5 ? ps * 2 : 1);
            if (gf == 0)   gf = -1;
            if (f == 0)   f = -1;

            gtf.push_back(gf);
            ltf.push_back(f);
            if(params.verbose>1)printf("Fitness scene: %d rep: %d gf:%10f lf:%10f parsimony:%10f\n", i, j, gf, f, ps);

            
        }

        float gmean = 0; for (auto &n : gtf)  gmean += n; gmean /= gtf.size();
        float gvar = 0; for (auto &n : gtf) gvar += pow((n - gmean), 2); gvar /= (gtf.size() - 1);
        float gsd = sqrt(gvar);
        float lmean = 0; for (auto &n : ltf)  lmean += n; lmean /= ltf.size();
        float lvar = 0; for (auto &n : ltf) lvar += pow((n - lmean), 2); lvar /= (ltf.size() - 1);
        float lsd = sqrt(lvar);
        
        
        float fit   = params.fitness_mode == 0 ? gmean : lmean;
        float sd    = params.fitness_mode == 0 ? gsd : lsd;
        
        fitness.push_back(fit);
        stddev.push_back(sd);
        parsimony.push_back(ps);
        if (params.verbose)printf("individual %3d gfitness:%10f  gstd:%10f lfitness:%10f lstd:%10f parsimony:%10f\n", i / repeats, gmean, gsd, lmean, lsd, ps);
    }
}

void Sim::run_capture_all_fitness(int steps)
{
    if(params.verbose>1)printf("steps:%d ctrl_steps:%d\n",steps,sim_param.ctrl_steps);
    for (int i = 0; i < steps / sim_param.ctrl_steps; i++)
    {
        if (params.verbose>1)printf("Iteration %5d  step %5d\r", i, stepnum);
        step();
        
    }
    
    // Calculate the fitnesses for the set of sims. Take the average of the fitness values
    // over repeats number of sims.
    //
    // Ftness in this sim is how far puck pushed towards left, but penalise if the
    // puck has not moved off the (0,0) position. f = x/2 + 1
    fitness.clear();
    lfitness.clear();
    parsimony.clear();
    for (int i = 0; i < num_scenes; i += repeats)
    {
        float ps = local_parsim.front();
        parsimony.push_back(ps);
        local_parsim.pop_front();
        
        for(int j = 0; j < repeats && j < num_scenes; j++)
        {
            float x = get_item_ptr(scene_ptr, i + j)->bodies[num_agents].position.x;
            float sx = all_startcoord.front(); all_startcoord.pop();
            
            float gf = sx - x;
            
            float lf = 0;
            for (int k = 0; k < num_agents; k++)
            {
                float f = get_item_ptr(senses_ptr, i + j)->sense[k].fitness;
                lf += f;
            }
            lf /= num_agents;
            lfitness.push_back(lf);
            float f2 = get_item_ptr(senses_ptr, i + j)->sense[num_agents].fitness;
            float f5 = f2;
            //f2  = f2 / (params.simtime * params.max_velocity );
            //f2  = f2 * 4 / (params.simtime * params.max_velocity);
            if (f2 == 0)
                f2 -= 1.0;
            
            f2  *= (ps < 0.5 ? ps * 2 : 1);
            gf  *= (ps < 0.5 ? ps * 2 : 1);
            lf  *= (ps < 0.5 ? ps * 2 : 1);
            

            float fit   = params.fitness_mode == 0 ? gf
                        : params.fitness_mode == 1 ? lf
                        : params.fitness_mode == 2 ? f2
                        : params.fitness_mode == 3 ? f2
                        : params.fitness_mode == 4 ? f2
                        : f5;
            //printf("%d %d %f\n", i, j, fit);
            // fitness exactly zero is bad, penalise
            //if (fit == 0)
            //    fit = -1;
            fitness.push_back(fit);
         
        }
    }
}




Sim::Sim(Simparams &_params, bool _vcdtrace) :
    params                  (_params),
    vcdtrace                (_vcdtrace),
    zmqcontext              (1),
    //sub_control_actuators   (zmqcontext, ZMQ_SUB),
    pub_control_sensors     (zmqcontext, ZMQ_PUB)
{
    
    // Copy params in !!FIXME tidy this up!!
    params.steps            = params.simtime / (sim_param.dt * sim_param.ctrl_period);
    //sim_param.robot_count   = params.robots;
    sim_param.body_count    = params.robots + num_objects + 1;
    //num_agents              = params.robots;
    num_scenes              = params.parallel;
    sim_param.fitness_mode  = params.fitness_mode;
    sim_param.heterogeneous = params.heterogeneous;
    
    
    
    master_seed             = params.seed;
    randrange_seed(master_seed);

    
    // Make context
    get_platform();
    
    // Get devices
    get_devices();
    
    // Context and command queues
    get_context();
    
    // Build kernels
    make_kernel();
    
    // Construct the buffer and pipeline structure
    make_pipeline();
    
    // Tracing
    if (vcdtrace)
    {
        vcd = new VCD("test.vcd");
        vcd->initevent(1);
    }

    // Set up the ZMQ sockets if we are a controlled sim
    if (params.controlled_sim)
    {
        
        for (int i = 0; i < 16; i++)
        {
            sub_control_actuators[i] = new zmq::socket_t(zmqcontext, ZMQ_SUB);
            const int confl = true;
            sub_control_actuators[i]->setsockopt(ZMQ_CONFLATE, &confl, sizeof(int));
            std::string subscr = string_format("xp%02d", i);
            sub_control_actuators[i]->setsockopt(ZMQ_SUBSCRIBE, subscr.c_str(), subscr.size());
            sub_control_actuators[i]->connect(string_format("tcp://%s:%s", params.genepool.c_str(), ZMQPORT_CONTROL_ACTUATORS).c_str());
        }
        
        pub_control_sensors.connect(string_format("tcp://%s:%s", params.genepool.c_str(), ZMQPORT_CONTROL_SENSORS).c_str());
        //pub_control_sensors.bind(string_format("tcp://*:%s", ZMQPORT_CONTROL_SENSORS).c_str());
    }
    
    
    
    
    // Do nothing tree for use when evolved tree is too large, to cause zero fitness
    nulltree = new BT(BLACKBOARD_ENTRIES, "successl ");
    
}

void Sim::get_btvars(double *btv)
{
    map(&btvars_ptr, btvars_buf, max_btvars_size * num_agents);
    finish_all();

    for(int i = 0; i < num_scenes; i++)
    {
        for(int j = 0; j < num_agents; j++)
        {
            float *f = (float *)get_item_ptr(btvars_ptr, i * num_agents + j, max_btvars_size);
            f += 2; // Skip seed
            for(int k = 0; k < 24; k++)
                *btv++ = (double)*f++;
        }
    }

    unmap(btvars_ptr, btvars_buf);
    finish_all();
}

extern "C"
{
    gpusim::Sim *Sim_new(int scenes, int robots, int bb_entries)
    {   
        static Simparams sp;
        sp.parallel = scenes;
        sp.robots   = robots;
        printf("Creating new Sim object with scenes:%d robots:%d bb_entries:%d\n", scenes, robots, bb_entries);
        gpusim::Sim *s = new gpusim::Sim(sp);
        printf("Created ptr to Sim %llx\n", (unsigned long long)s);

        return s;
    }
    void setup_and_step(gpusim::Sim *sim, double *scene, double *blackboard, float smin_r, float smax_r, float cam_radius)
    {
        //printf("Passed ptr to Sim %llx\n", (unsigned long long)sim);
        //printf("Passed ptr to scene %llx\n", (unsigned long long)scene);
        //printf("smin %12f smax %12f\n", smin_r, smax_r);
        sim->params.verbose = false;
        sim->sim_param.sensor_min_radius = smin_r;
        sim->sim_param.sensor_max_radius = smax_r;
        sim->sim_param.camera_radius = cam_radius;
        sim->create_scene(scene);
        //sim->print_status();
        sim->step();
        //sim->print_status();
        sim->get_btvars(blackboard);
    }
    void step(gpusim::Sim *sim, double *scene, double *blackboard)
    {
        sim->step();
        sim->get_btvars(blackboard);
    }
}





