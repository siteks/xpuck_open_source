
#include <unistd.h>
#include "system.h"
#include "gui.h"

using namespace gpusim;


void System::evolve()
{
    Sim *sim = new Sim(simparams);
    
    // Create GP and populate with individuals. Pre-seed with trees if provided
    GP  gp(evoparams, trees);
    
    gp.logfile = sysparams.logfile;
    gp.setup_evo(sim);
}

void System::evolve_mo()
{
    Sim *sim = new Sim(simparams);
    
    // Create GP and populate with individuals. Pre-seed with trees if provided
    GP  gp(evoparams, trees);
    
    gp.logfile = sysparams.logfile;
    gp.setup_moevo(sim);
}

void System::evolve_so()
{
    Sim *sim = new Sim(simparams);
    
    // Modify the logfile with hostname if running with genepool server.
    // This is so that cluster machines do not clash.
    // Also set up the ZMQ channel for logging
    if (evoparams.genepool.size())
    {
        sysparams.logfile = string_format("%s.%s", sysparams.logfile.c_str(), sysparams.hostname.c_str());
        zmqcontext = zmq::context_t(1);
        // Set up the PUB channel for sending log data
        pub_log = new zmq::socket_t(zmqcontext, ZMQ_PUB);
        pub_log->connect(string_format("tcp://%s:%s", evoparams.genepool.c_str(), ZMQPORT_GALOG).c_str());
    }
    
    // Create GP and populate with individuals. Pre-seed with trees if provided
    GP  gp(evoparams, trees);
    
    printf("Using logfile %s\n", sysparams.logfile.c_str());
    gp.logfile      = sysparams.logfile;
    gp.pub_log      = pub_log;
    sim->pub_log    = pub_log;
    zmqlog(pub_log, "Testing, testing..");
    zmqlog(pub_log, "Testing, testing..");
    zmqlog(pub_log, "Testing, testing..");
    gp.setup_soevo_island(sim);
    printf("Completed\n");
}


void System::gui()
{

    printf("Running GUI..\n");
    
    simparams.parallel = 1;
    Sim *sim = new Sim(simparams);

    if (sysparams.controlled_sim || sysparams.force_cam_update)
    {
        // When running a remote controlled sim, we just do a single control step.
        // Camera and range and bearing is calcualted every control step
        sim->sim_param.ctrl_steps = 1;
    }
    else
        // Normal GUI runs two steps, so that camera and r&b only every other step,
        // as used in the evolution sims
        sim->sim_param.ctrl_steps = 2;
    
    if (trees.size())
    {
        bts.push_back(new BT(BLACKBOARD_ENTRIES, trees[0]));
    }
    else if (!bts.size())
    {
        // bts may have been populated by read_json, if it is empty, add a null placeholder
        bts.push_back(new BT(BLACKBOARD_ENTRIES, "successl "));
    }
    sim->set_bt(&bts, 0, 1);
    if (wf.is_object())
        sim->create_scene(&wf);
    else
        sim->create_scene();
    Gui gui(sim);
    if (sysparams.controlled_sim)
    {
        // Start sim in running state when controlled
        gui.runstate = true;
    }
    glutMainLoop();
    
}

void System::capture_bb()
{
    
    printf("Running capture BB..\n");
    
    simparams.parallel = 1;
    Sim *sim = new Sim(simparams);

    sim->sim_param.ctrl_steps = 1;
    if (trees.size())
    {
        bts.push_back(new BT(BLACKBOARD_ENTRIES, trees[0]));
    }
    else
    {
        bts.push_back(new BT(BLACKBOARD_ENTRIES, "successl "));
    }
    sim->set_bt(&bts, 0, 1);
    if (wf.is_object())
        sim->create_scene(&wf);
    else
        sim->create_scene();
    
    FILE *fp = fopen(sysparams.capture_bb_file.c_str(), "w");
    const int bb_entries = 24;
    std::vector<double> blackboard(bb_entries * simparams.robots);
    for (int i = 0; i < simparams.steps; i++)
    {
        sim->step();
        sim->get_btvars(blackboard.data());
        for (int r = 0; r < simparams.robots; r++)
        {
            fprintf(fp, "%5d %2d ", i, r);
            for(int k = 2; k < bb_entries - 1; k++)
                fprintf(fp, "% 8f ", blackboard[r * bb_entries + k]);
            fprintf(fp, "\n");
        }
        Pose m;
        sim->get_pose(0, simparams.robots, m);
        fprintf(fp, "%5d %2d         0         0         0         0         0         0         0         0         0         0         0         0         0         0         0         0         0         0 % 8f % 8f % 8f\n", i, 99, m.x, m.y, m.th);
 
        
    }
    fclose(fp);
}


void System::read_json()
{
    std::string err;
    
    // Read in whole file
    // https://stackoverflow.com/questions/2602013/read-whole-ascii-file-into-c-stdstring
    std::ifstream t(sysparams.worldfile);
    std::string str((std::istreambuf_iterator<char>(t)), std::istreambuf_iterator<char>());
    for(int i = 0; i < str.size(); i++)
        if (str[i] == 10)
            str[i] = ' ';
    
    wf = Json::parse(str, err);
    
    
    // JSON schema:
    // {
    //      "params":
    //      {
    //          "simtime":<time>,
    //          "dyn_mu"
    //          ...
    //      }
    //      "scene":
    //      [
    //          {
    //              "robot":    [[<x>,<y>,<th>],[],..,[]] | "robot":    {"number":<num>},
    //              "frisbee":  [[],..,[]] | "frisbee":<num>},
    //              "seed":     <num>
    //      ]
    //      "tree":"<tree>"
    //}
    if (err != "")
    {
        printf("Error in worldfile\n%s\nerr:%s\n", str.c_str(), err.c_str());
        exit(1);
    }
    
    simparams.wfindex   = sysparams.wfindex;
    if (!wf["params"]["simtime"].is_null())
        simparams.simtime   = wf["params"]["simtime"].number_value();
    if (wf["params"]["fitness_mode"].is_number())
        simparams.fitness_mode = wf["params"]["fitness_mode"].int_value();
    printf("Using fitness mode %d\n", simparams.fitness_mode);
    if (!wf["scene"].is_null())
    {
//        printf("is_null   %d\n", wf["scene"][sysparams.wfindex]["robot"].is_null());
//        printf("is_number %d\n", wf["scene"][sysparams.wfindex]["robot"].is_number());
//        printf("is_bool   %d\n", wf["scene"][sysparams.wfindex]["robot"].is_bool());
//        printf("is_string %d\n", wf["scene"][sysparams.wfindex]["robot"].is_string());
//        printf("is_array  %d\n", wf["scene"][sysparams.wfindex]["robot"].is_array());
//        printf("is_object %d\n", wf["scene"][sysparams.wfindex]["robot"].is_object());
        // scene is always a list of objects, and is used to populate the actual scenes in a circular way.
        // Within each scene object, there are three fields:
        //  seed:       numeric value for seed (optional)
        //  robot:      list of coordinate tuples, or object {number:<number>}
        //  frisbee:    list of coordinate tuples, or object {number:<number>}
        if (wf["scene"][sysparams.wfindex]["robot"].is_array())
            simparams.robots    = wf["scene"][sysparams.wfindex]["robot"].array_items().size();
        else if (wf["scene"][sysparams.wfindex]["robot"].is_object())
            simparams.robots    = wf["scene"][sysparams.wfindex]["robot"]["number"].number_value();
        else
        {
            printf("Malformed JSON worldfile!\n");
            exit(1);
        }
    }
    

    if (!wf["params"]["dyn_mu"].is_null())
        simparams.dyn_mu = wf["params"]["dyn_mu"].number_value();
    if (!wf["params"]["restitution"].is_null())
        simparams.restitution = wf["params"]["restitution"].number_value();
    if (!wf["params"]["xp_mu"].is_null())
        simparams.xp_mu = wf["params"]["xp_mu"].number_value();
    if (!wf["params"]["fr_mu"].is_null())
        simparams.fr_mu = wf["params"]["fr_mu"].number_value();
    if (!wf["params"]["k1"].is_null())
        simparams.k1 = wf["params"]["k1"].number_value();
    if (!wf["params"]["k2"].is_null())
        simparams.k2 = wf["params"]["k2"].number_value();
    if (!wf["params"]["k3"].is_null())
        simparams.k3 = wf["params"]["k3"].number_value();
    
    
    
    if (wf["tree"].is_string())
    {
        printf("Single tree: %s\n", wf["tree"].string_value().c_str());
        bts.push_back(new BT(BLACKBOARD_ENTRIES, wf["tree"].string_value()));
        trees.push_back(wf["tree"].string_value());
    }
    else if (wf["tree"].is_array())
    {
        simparams.heterogeneous = true;
        auto t = wf["tree"].array_items();
        int idx = 0;
        for (auto i : t)
        {
            printf("Tree:%3d: %s\n", idx++, i.string_value().c_str());
            bts.push_back(new BT(BLACKBOARD_ENTRIES, i.string_value()));
            trees.push_back(i.string_value());
        }
    }
    
}

void System::measure_fitness()
{
    if (trees.size() && (bts.size() == 0))
    {
        bts.push_back(new BT(BLACKBOARD_ENTRIES, trees[0]));
    }
    else if (!bts.size())
    {
        bts.push_back(new BT(BLACKBOARD_ENTRIES, "successl "));
    }
    for(int i = 0; i < bts.size(); i++)
        printf("tree %d:%s\n", i, bts[i]->get_btstr().c_str());

    Sim *sim = new Sim(simparams);
    printf("Evaluating %d trees over %d simulations, each %d steps\n",
           (int)bts.size(), sysparams.eval_count, simparams.steps);
    if (simparams.steps < sim->sim_param.ctrl_steps)
    {
        printf("Adjusting ctrl_steps\n");
        sim->sim_param.ctrl_steps = simparams.steps;
    }
    if (wf.is_object())
        sim->evaluate_bt(&bts, sysparams.eval_count, simparams.steps, &wf);
    else
        sim->evaluate_bt(&bts, sysparams.eval_count, simparams.steps);
    
    if (simparams.verbose > 0)
        for (int i = 0; i < sim->all_fitness.size(); i++)
        printf("Fitness of run: %d is: % 12f\n", i, sim->all_fitness[i]);
    print_bar_chart(sim->all_fitness);
    
    if (wf.is_object() && !wf["params"]["logfile"].is_null())
    {
        std::string fname = wf["params"]["logfile"].string_value();
        printf("logging results to %s\n", fname.c_str());
        FILE *fp = fopen(fname.c_str(), "w");
        for (int i = 0; i < sim->all_fitness.size(); i++)
        {
            fprintf(fp, "% 10f ", sim->all_fitness[i]);
            for (int j = 0; j < simparams.robots + 1; j++)
            {
                fprintf(fp, "% 10f % 10f % 10f", sim->all_evc[i].coords[j*3],
                       sim->all_evc[i].coords[j*3+1],
                       sim->all_evc[i].coords[j*3+2]);
            }
            fprintf(fp, "\n");
        }
    }
    //for (int i = 0; i < sim->all_lfitness.size(); i++)
    //    printf("Local fitness of run: %d is: % 12f\n", i, sim->all_lfitness[i]);
    //print_bar_chart(sim->all_lfitness);
}

void System::run()
{
    switch (action)
    {
        case USEGUI:
            gui();
            break;
        case MEASURE_SIM:
            break;
        case MEASURE_FITNESS:
            measure_fitness();
            break;
        case EVOLVE:
            switch (evoparams.model)
            {
                case 0:
                    evolve();
                    break;
                case 1:
                    evolve_mo();
                    break;
                case 2:
                case 3:
                case 4:
                    evolve_so();
                    break;
            }
            break;
        case CAPTURE_BB:
            capture_bb();
            break;
        default:
            printf("No action defined!\n");
    }
}



System::System(int argc, char **argv)
{
    action = USEGUI;

    char hostname_cstr[1024];
    gethostname(hostname_cstr, 1024);
    sysparams.hostname = evoparams.hostname = std::string(hostname_cstr);



    for (int i = 0; i < argc; i++)
        printf("%s ", argv[i]);
    printf("\n");
    for(int i = 1; i < argc; i++)
    {
        if (!strcmp(argv[i], "-h"))
        {
            printf( "Usage: - xps [options]\n"
                   "   -h  print this message\n"
                   "   -b  benchmark tree\n"
                   "   -v  <level> verbosity level\n"
                   "   -t  <tree> in quotes. Can be used multiple times. default 'explore 5'\n"
                   "   -x  [1-9] example tree, 1 is explore 5\n"
                   "   -e  <pop,gens,reps,epochs,mig> run evolution\n"
                   "   -ep <gendepth,pmparam,pmpoint,pmsubtree,pmshuffle>\n"
                   "   -et <tsize>\n"
                   "   -m  <evo mode> 1 - actionatoms only, 2 - actionchunks only, 3 (default) all\n"
                   "   -z  <mass> mass of frisbee (default 0.05kg\n"
                   "   -fi <type> specify fitness measure. 0 - default global, 1 - local, 2 - frisbee velocity with relocate, 3 - as 2 with half and half random/left robot placement, central frisbee, 4 - as 2 with random robot, central frisbee, 5 - as 3 but no zero penalty\n"
                   "   -s  <secs> simulation time, default 60\n"
                   "   -p  <parallel> number of parallel sims to run, default 256\n"
                   "   -r  <seed> random number generator seed, default 1\n"
                   "   -a  <agents> number of robots, default 16\n"
                   "   -g  <IP address> in quotes. Connect to gene server at address every epoch\n"
                   "   -f  full log, when set, all generations individuals are logged, without only the last gen\n"
                   "   -n  <model> evolution model (default - standard) 1=multiobjective fitness/stddev, 2=new singleobjective, 3=SO with modified tournament, 4=3 with f95 sort\n"
                   "   -l  <file> logfile\n"
                   "   -gf <num> evaluate tree for global fitness <num> times with different seeds\n"
                   "   -w  <file> json worldfile\n"
                   "   -wi <index> json worldfile index when multiple starting conditions\n"
                   "   -d  dump json files of final population evaluations\n"
                   "   -c  Controlled mode. Respond to command channel, and send individuals over galog\n"
                   "   -cs <map> Controlled sim mode. Run single simulation at real-time speed and allow control of robots over zmq. Map is comma separated list of robot->agent mappings. '-' does default mapping\n"
                   "   -tf <treefile> filename for fittest tree to be stored in\n"
                   "   -cp <filename>  Full capture. Run the sim as a single thread, capture all inputs and outputs\n"
                   "   -fp <num> flip params on frisbee mass every <num> steps. If neg, start with param 1\n"
                   "   -rp listen to reality probe and change frisbee weight if above threshold\n"
                   "   -cm force camera and attraction updates every cycle in gui mode for debug\n"
                   );
            exit(1);
        }
        
        else if (!strcmp(argv[i], "-b"))
            action = MEASURE_SIM;
        else if (!strcmp(argv[i], "-f"))
            sysparams.full_log = true;
        else if (!strcmp(argv[i], "-cm"))
            sysparams.force_cam_update = true;
        else if (!strcmp(argv[i], "-cp"))
        {
            if (++i >= argc)
            {
                printf("Missing cpature file!\n");
                exit(1);
            }
            action = CAPTURE_BB;
            sysparams.capture_bb_file = std::string(argv[i]);
        }
        else if (!strcmp(argv[i], "-n"))
        {
            if (++i >= argc)
            {
                printf("Missing evolution model!\n");
                exit(1);
            }
            evoparams.model = atoi(argv[i]);
        }
        else if (!strcmp(argv[i], "-fp"))
        {
            if (++i >= argc)
            {
                printf("Missing flip param number!\n");
                exit(1);
            }
            simparams.flip_params = atoi(argv[i]);
        }
        else if (!strcmp(argv[i], "-fi"))
        {
            if (++i >= argc)
            {
                printf("Missing fitness mode!\n");
                exit(1);
            }
            simparams.fitness_mode = atoi(argv[i]);;
            printf("Using fitness mode %d\n", simparams.fitness_mode);
        }
        else if (!strcmp(argv[i], "-t"))
        {
            if (++i >= argc)
            {
                printf("Missing tree!\n");
                exit(1);
            }
            trees.push_back(std::string(argv[i]));
            //tprintf("Tree: %s\n", trees.back().c_str());
        }
        else if (!strcmp(argv[i], "-e"))
        {
            if (++i >= argc)
            {
                printf("Missing evo params pop,gens,reps!\n");
                exit(1);
            }
            action = EVOLVE;
            std::string s(argv[i]);
            int pos = 0;
            int commas = 0;
            while ((pos = s.find(",", pos + 1)) != std::string::npos) commas++;
            evoparams.popsize = std::stol(s.substr(0, pos = s.find(",")));
            s.erase(0, pos + 1);
            if (commas >= 1)
            {
                evoparams.generations = std::stol(s.substr(0, pos = s.find(",")));
                s.erase(0, pos + 1);
            }
            if (commas >= 2)
            {
                evoparams.repeats = std::stol(s.substr(0, pos = s.find(",")));
                s.erase(0, pos + 1);
            }
            if (commas >= 3)
                evoparams.epochs = std::stol(s.substr(0, pos = s.find(",")));
            
            if (commas > 3)
                evoparams.gsfittest = std::stol(s.substr(0, pos = s.find(",")));
            
            printf("Evo params pop:%d gens:%d reps:%d epochs:%d\n", evoparams.popsize, evoparams.generations,
                   evoparams.repeats, evoparams.epochs);
        }
        else if (!strcmp(argv[i], "-ep"))
        {
            if (++i >= argc)
            {
                printf("Missing evo params gendepth,pmparam,pmpoint,pmsubtree,pmshuffle\n");
                exit(1);
            }
            std::string s(argv[i]);
            int pos = 0;
            int commas = 0;
            while ((pos = s.find(",", pos + 1)) != std::string::npos) commas++;
            if ((commas != 3) && (commas != 4))
            {
                printf("Incorrect number of parameters; expecting gendepth,pmparam,pmpoint,pmsubtree,pmshuffle\n");
                exit(1);
            }
            evoparams.gendepth      = std::stol(s.substr(0, pos = s.find(","))); s.erase(0, pos + 1);
            evoparams.prob_mparam   = std::stof(s.substr(0, pos = s.find(","))); s.erase(0, pos + 1);
            evoparams.prob_mpoint   = std::stof(s.substr(0, pos = s.find(","))); s.erase(0, pos + 1);
            evoparams.prob_msubtree = std::stof(s.substr(0, pos = s.find(",")));
            if (commas == 4)
            {
                s.erase(0, pos + 1); evoparams.prob_mshuffle = std::stof(s.substr(0, pos = s.find(",")));
            }
            printf("Evo params2 gendepth:%d pmparam:%f pmpoint:%f pmsubtree:%f pmshuffle:%f\n",
                   evoparams.gendepth, evoparams.prob_mparam, evoparams.prob_mpoint, evoparams.prob_msubtree,
                   evoparams.prob_mshuffle);
        }
        else if (!strcmp(argv[i], "-et"))
        {
            if (++i >= argc)
            {
                printf("Missing tournament size!\n");
                exit(1);
            }
            evoparams.n_tsize = atoi(argv[i]);
        }
        else if (!strcmp(argv[i], "-rp"))
        {
            evoparams.listen_rp = true;
        }
        else if (!strcmp(argv[i], "-m"))
        {
            if (++i >= argc)
            {
                printf("Missing evo mode!\n");
                exit(1);
            }
            evoparams.mode = atoi(argv[i]);
        }
        else if (!strcmp(argv[i], "-s"))
        {
            if (++i >= argc)
            {
                printf("Missing sim time!\n");
                exit(1);
            }
            simparams.simtime = atoi(argv[i]);
        }
        else if (!strcmp(argv[i], "-z"))
        {
            if (++i >= argc)
            {
                printf("Missing frisbee mass!\n");
                exit(1);
            }
            simparams.fr_mass = atof(argv[i]);
            simparams.fr_mass_alt[0] = atof(argv[i]);
        }
        else if (!strcmp(argv[i], "-v"))
        {
            if (++i >= argc)
            {
                printf("Missing verbosity level!\n");
                exit(1);
            }
            sysparams.verbose = atoi(argv[i]);
            evoparams.verbose = simparams.verbose = sysparams.verbose;
        }
        else if (!strcmp(argv[i], "-gf"))
        {
            if (++i >= argc)
            {
                printf("Missing evaluation count!\n");
                exit(1);
            }
            action = MEASURE_FITNESS;
            sysparams.eval_count = atoi(argv[i]);
            printf("Setting parallelism to 1\n");
            simparams.parallel = 1;
        }
        else if (!strcmp(argv[i], "-x"))
        {
            if (++i >= argc)
            {
                printf("Missing example!\n");
                exit(1);
            }
            sysparams.example = atoi(argv[i]);
        }
        else if (!strcmp(argv[i], "-p"))
        {
            if (++i >= argc)
            {
                printf("Missing sim parallelism!\n");
                exit(1);
            }
            simparams.parallel = atoi(argv[i]);
        }
        else if (!strcmp(argv[i], "-r"))
        {
            if (++i >= argc)
            {
                printf("Missing random seed!\n");
                exit(1);
            }
            sysparams.seed = atoi(argv[i]);
            if (sysparams.hostname[0] == 'x')
            {
                // This is a cluster machine, set the seed to 100000 * robot number
                int i = std::stoi(sysparams.hostname.substr(2, 2)) * 100000;
                sysparams.seed += i;
            }
            simparams.seed = evoparams.seed = sysparams.seed;
            
            
            printf("Setting master random seed to %d %d %d\n", simparams.seed, evoparams.seed, sysparams.seed);
        }
        else if (!strcmp(argv[i], "-a"))
        {
            if (++i >= argc)
            {
                printf("Missing agent count!\n");
                exit(1);
            }
            simparams.robots = atoi(argv[i]);
        }
        else if (!strcmp(argv[i], "-g"))
        {
            if (++i >= argc)
            {
                printf("Missing IP address!\n");
                exit(1);
            }
            simparams.genepool = evoparams.genepool = std::string(argv[i]);
            printf("Connecting to gene server %s\n", evoparams.genepool.c_str());
        }
        else if (!strcmp(argv[i], "-l"))
        {
            if (++i >= argc)
            {
                printf("Missing log file name!\n");
                exit(1);
            }
            sysparams.logfile = std::string(argv[i]);
        }
        else if (!strcmp(argv[i], "-d"))
        {
            evoparams.dumppath = "/tmp";
            evoparams.dumpjson = true;
            evoparams.grabdetail = true;
        }
        else if (!strcmp(argv[i], "-c"))
        {
            sysparams.controlled = true;
            evoparams.controlled = true;
        }
        else if (!strcmp(argv[i], "-cs"))
        {
            printf("Controlled sim mode\n");
            simparams.controlled_sim = sysparams.controlled_sim = true;

            if (++i >= argc)
            {
                printf("Missing robot-agent map!\n");
                exit(1);
            }
            std::string s(argv[i]);
            if (s[0] == '-')
            {
                printf("Using default mapping\n");
            }
            else
            {
                int pos = 0;
                std::vector<int> mapping;
                while(1)
                {
                    int m = std::stol(s.substr(0, pos = s.find(",")));
                    mapping.push_back(m);
                    if (pos > s.size())
                        break;
                    s.erase(0, pos + 1);
                }
                for (int i = 0; i < 16; i++)
                    simparams.xp_to_idx[i] = simparams.idx_to_xp[i] = -1;
                for (int i = 0; i < mapping.size(); i++)
                {
                    // Create the agent to xpuck mapping
                    simparams.idx_to_xp[i] = mapping[i];
                    // Create the xpuck to agent mapping
                    simparams.xp_to_idx[mapping[i]] = i;
                }
            }
            printf("Mapping\n  r->a a->r\n");
            for(int i = 0; i < 16; i++)
                printf("%2d %4d %4d\n", i, simparams.xp_to_idx[i], simparams.idx_to_xp[i]);

        }
        else if (!strcmp(argv[i], "-w"))
        {
            if (++i >= argc)
            {
                printf("Missing world file name!\n");
                exit(1);
            }
            sysparams.worldfile = std::string(argv[i]);
            if (sysparams.worldfile != "")
                read_json();
        }
        else if (!strcmp(argv[i], "-tf"))
        {
            if (++i >= argc)
            {
                printf("Missing tree file name!\n");
                exit(1);
            }
            evoparams.treefile = std::string(argv[i]);
        }
        else if (!strcmp(argv[i], "-wi"))
        {
            if (++i >= argc)
            {
                printf("Missing worldfile index!\n");
                exit(1);
            }
            simparams.wfindex = sysparams.wfindex = atoi(argv[i]);
        }
    }
}


