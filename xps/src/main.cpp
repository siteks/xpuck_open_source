#include <cstdlib>
#include "gpu2.h"
#include "bt.h"
//#include "genetic.hpp"
#include "gp.h"
#include "clock.h"
#include "gui.h"
#include <unistd.h>
#include <zmq.hpp>
#include "json11.hpp"

#include "system.h"



#define ESC_KEY 27

using namespace gpusim;
using namespace json11;



int main(int argc, char** argv)
{
    
    System sys(argc, argv);
    
    sys.run();
    
    return 0;
    
}
    
