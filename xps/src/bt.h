

#ifndef _BT_H
#define _BT_H

//#define DEBUG


#define BLACKBOARD_ENTRIES  24
#define MAX_BT_SIZE         16384
#define MAX_BTVARS_SIZE     4096

#ifdef DEBUG
#define DBPRINT(fmt, ...) printf(fmt, ##__VA_ARGS__)
#else
#define DBPRINT(fmt, ...)
#endif


//#ifdef __APPLE__
//#include "cl.h"
//#else
//#define CL_PRINTF_CALLBACK_ARM    0x40B0
//#define CL_PRINTF_BUFFERSIZE_ARM  0x40B1
//#include <CL/cl.h>
//#endif

#define _ROBOT
#include "btcommon.h"



#include <string>
#include <regex>


// http://stackoverflow.com/questions/2342162/stdstring-formatting-like-sprintf
template<typename ... Args>
std::string string_format( const std::string& format, Args ... args )
{
    size_t size = snprintf( nullptr, 0, format.c_str(), args ... ) + 1; // Extra space for '\0'
    std::unique_ptr<char[]> buf( new char[ size ] );
    snprintf( buf.get(), size, format.c_str(), args ... );
    return std::string( buf.get(), buf.get() + size - 1 ); // We don't want the '\0' inside
}



//typedef enum __attribute__((packed))
//{
//    SEQ2,       // 0
//    SEQ3,       // 1
//    SEQ4,       // 2
//    SEL2,       // 3
//    SEL3,       // 4
//    SEL4,       // 5
//    SEQM2,      // 6
//    SEQM3,      // 7
//    SEQM4,      // 8
//    SELM2,      // 9
//    SELM3,      // a
//    SELM4,      // b
//    REPEATI,    // c
//    REPEATR,    // d
//    SUCCESSD,   // e
//    FAILURED,   // f
//    INVERT,     // 10
//    MOVCS,      // 11
//    MOVCV,      // 12
//    MULAS,      // 13
//    MULAV,      // 14
//    IFPROB,     // 15
//    IFQUAD,     // 16
//    SUCCESSL,   // 17
//    FAILUREL,    // 18
//    //=====================
//    EXPLORE,
//    UPFIELD,
//    ATTRACT,
//    RED,
//    GREEN,
//    BLUE,
//    NEIGHBOUR,
//    FIXEDPROB,
//    AVOIDING,
//    CLEFT,
//    CRIGHT,
//    MACRO8,
//    MACRO9,
//    MACRO10
//} Nodetype;

//typedef enum __attribute__((packed))
//{
//    BT_FAILURE = 0,
//    BT_SUCCESS,
//    BT_RUNNING,
//    BT_RUNNING_STALE,
//    BT_INVALID
//} Status;


//  INTx        Signed integer, x bits
//  Xi_j        Fixed point number i.j
//  IPx         Positive nonzero integer [1,x]
//  IPZx        Positive integer [0,x-1]
//  FPMx        Floating point number [-x,+x]
//  CVREG       Colour vector register

enum Ptype
{
    SREG,
    VREG,
    WSREG,
    WVREG,
    INT8,
    X5_3,
    FPM32,
    IP100,
    //======== Macro specific
    FPM5,
    IPZ3,
    CVREG
};

struct Ntype
{
    std::string         name;
    Nodetype            type;
    int                 c_arity;
    int                 p_arity;
    std::vector<Ptype>  ptype;
    std::string         macro;
};

typedef const std::vector<Ntype> Nodeset;
typedef std::vector<Ntype> VNodeset;

#define MAX_ARITY       4
Nodeset composition =
{
    {"seq2",        SEQ2,       2,  0},
    {"seq3",        SEQ3,       3,  0},
    {"seq4",        SEQ4,       4,  0},
    {"sel2",        SEL2,       2,  0},
    {"sel3",        SEL3,       3,  0},
    {"sel4",        SEL4,       4,  0},
    {"seqm2",       SEQM2,      2,  0},
    {"seqm3",       SEQM3,      3,  0},
    {"seqm4",       SEQM4,      4,  0},
    {"selm2",       SELM2,      2,  0},
    {"selm3",       SELM3,      3,  0},
    {"selm4",       SELM4,      4,  0},
    {"repeati",     REPEATI,    1,  1,  {IP100}},
    {"repeatr",     REPEATR,    1,  1,  {IP100}},
    {"successd",    SUCCESSD,   1,  0},
    {"failured",    FAILURED,   1,  0},
    {"invert",      INVERT,     1,  0}
};



Nodeset actionatom =
{
    {"movcs",       MOVCS,      0,  2,  {WSREG, INT8}},
    {"movcv",       MOVCV,      0,  2,  {WVREG, INT8}},
    {"mulas",       MULAS,      0,  4,  {WSREG, SREG,   FPM32,    SREG}},
    {"mulav",       MULAV,      0,  4,  {WVREG, VREG,   FPM32,    VREG}},
    {"rotav",       ROTAV,      0,  4,  {WVREG, VREG,   INT8,     VREG}},
    {"ifprob",      IFPROB,     0,  3,  {WSREG, X5_3,   X5_3}},
    {"ifquad",      IFQUAD,     0,  2,  {WVREG, INT8}},
    {"ifsect",      IFSECT,     0,  3,  {WVREG, INT8,   INT8}},
    {"successl",    SUCCESSL,   0,  0},
    {"failurel",    FAILUREL,   0,  0}
};


// To define a new action node, define here, btcommon.h, gp.h for whether allowed to be used in evo
Nodeset action =
{
    // Need extra space on end of macro string
    {"explore",     EXPLORE,    0,  1,  {IP100},        "sel3 seqm2 ifquad 4 1 repeatr %1$g movcv 2 -64 seqm2 ifquad 4 4 repeatr %1$g movcv 2 64 movcv 2 0 "},
    {"upfield",     UPFIELD,    0,  1,  {FPM5},         "mulav 2 0 %1$g 6 "},
    {"attract",     ATTRACT,    0,  1,  {FPM5},         "mulav 2 0 %1$g 8 "},
    //{"red",         RED,        0,  1,  {X5_3},         "seq2 sel2 ifquad 10 1 ifquad 10 4 ifprob 0 -0.25 %1$g "},
    //{"green",       GREEN,      0,  1,  {X5_3},         "seq2 sel2 ifquad 12 1 ifquad 12 4 ifprob 0 -0.25 %1$g "},
    //{"blue",        BLUE,       0,  1,  {X5_3},         "seq2 sel2 ifquad 14 1 ifquad 14 4 ifprob 0 -0.25 %1$g "},
    {"neighbour",   NEIGHBOUR,  0,  2,  {X5_3,  X5_3},  "ifprob 16 %g %g "},
    {"fixedprob",   FIXEDPROB,  0,  1,  {X5_3},         "ifprob 0 -0.25 %g "},
    //
    {"avoiding",    AVOIDING,   0,  0,  {},             "seq2 sel2 ifsect 4 32 64 ifsect 4 -32 64 mulav 2 0 -5 4 "},
    //{"cleft",       CLEFT,      0,  2,  {CVREG, X5_3},  "seq2 ifquad %1$g 1 ifprob 0 -0.25 %2$g "},
    //{"cright",      CRIGHT,     0,  2,  {CVREG, X5_3},  "seq2 ifquad %1$g 4 ifprob 0 -0.25 %2$g "},
    //{"cleft",       CLEFT,      0,  1,  {CVREG},        "ifquad %1$g 1 "},
    //{"cright",      CRIGHT,     0,  1,  {CVREG},        "ifquad %1$g 4 "},
    {"cleft",       CLEFT,      0,  0,  {},             "ifsect 14 64 115 "},
    {"cright",      CRIGHT,     0,  0,  {},             "ifsect 14 -64 115 "},
    {"cfront",      CFRONT,     0,  0,  {},             "ifsect 14 0 12 "},
    {"bsearch",     BSEARCH,    0,  1,  {INT8},         "sel2 ifsect 14 0 20 failured seq2 movcv 18 %1$g mulav 2 0 0.3 18 "},
    {"rwalk",       RWALK,      0,  0,  {},        "seqm2 selm2 seqm2 ifprob 0 0 0 repeatr 1 movcv 2 64 repeatr 1 movcv 2 -64 repeati 20 movcv 2 0 "},

    //{"setflag",     SETFLAG,    0,  0,  {},             "movcs 17 1 "},
    //{"clearflag",   CLEARFLAG,  0,  0,  {},             "movcs 17 0 "},
    //{"testflag",    TESTFLAG,   0,  0,  {},             "ifprob 17 15.875 0.5 "},
};



//struct Node;
//
//
//union Ndata
//{
//    
//    SM2  sm2;
//    SM3  sm3;
//    SM4  sm4;
//    MVC  mvc;
//    IFQ  ifq;
//    MAC  mac;
//    IFP  ifp;
//    REP  rep;
//    FIX  fix;
//    
//};
//
//struct Node
//{
//    Nodetype    type;
//    Status      status;
//    Ndata       data;
//};

//---------------------------------
// OpenCL forms
struct clSM2
{
    //cl_uchar    idx;
    cl_ushort   op[2];
};

struct clSM3
{
    //cl_uchar    idx;
    cl_ushort   op[3];
};

struct clSM4
{
    //cl_uchar    idx;
    cl_ushort   op[4];
};

struct clMVC
{
    cl_char     dest;
    cl_char     i;
};

struct clIFQ
{
    cl_char     src;
    cl_char     i;
};

struct clIFS
{
    cl_char     src;
    cl_char     i;
    cl_uchar    j;
};

struct clMAC
{
    cl_uchar       dest;
    cl_uchar       src1;
    cl_uchar       src2;
    cl_float       f;
};

struct clRAC
{
    cl_uchar       dest;
    cl_uchar       src1;
    cl_uchar       src2;
    cl_char        i;
};

struct clIFP
{
    cl_uchar       src;
    cl_char        k;
    cl_char        l;
};


struct clREP
{
    //cl_uchar    count;
    cl_uchar    repeat;
    cl_ushort   op;
};

struct clFIX
{
    cl_ushort   op;
};


union clNdata
{
    
    clSM2  sm2;
    clSM3  sm3;
    clSM4  sm4;
    clMVC  mvc;
    clIFQ  ifq;
    clIFS  ifs;
    clMAC  mac;
    clRAC  rac;
    clIFP  ifp;
    clREP  rep;
    clFIX  fix;
    
};

struct clNode
{
    Nodetype    type;
    Status      status;
    cl_ushort   ptr;
    clNdata     data;
};


void            set_vars(float *v);
struct Node     *newnode(Nodetype type,...);
Status          tick(struct Node *bt);
//void print_tree(struct Node *bt, int level);





class BT
{
public:
    BT(
       // Number of 32 bit floats to use for blackboard
       cl_uint _blackboard,
       // String containing BT source to be parsed into tree
       std::string _s
    );
    void            print_tree();
    void            print_tree(cl_ushort root, int level, uchar *status = 0);
    std::string     get_btstr();
    
    // Block of memory passed to GPU, containing blackboard,
    // behaviour tree, and other stuff..
    //  0                       - root
    //  4 ->                    - behviour tree nodes
    std::vector<unsigned char>  cltree;
    cl_ushort                   root;

    // Tree stats
    int             num_nodes   = 0;
    int             num_leaves  = 0;
    int             num_inner   = 0;
    float           parsimony   = 0;
    
private:
    cl_uint         blackboard;
    void            parse_tree(std::string s);
    std::string     check(std::string msg);
    void            get_tree_str(std::string &ts, cl_ushort root, int level);

    cl_ushort       clnewnode(Nodetype type, ...);
    cl_ushort       clpt(int d);
   
    std::string     s;

    // Regex for parsing BT string
    std::regex                                          e;
    
    // Default constructor
    std::regex_token_iterator<std::string::iterator>    tokens;
    std::vector<std::string>                            words;
    
    //
    VNodeset                                            allnodes;
    std::vector<std::vector<int> >                      aritymap;
    


};














#endif
