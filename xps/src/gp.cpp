

//#define DEBUG

#include "gp.h"
#include <algorithm>
#include <string>


// This is yuk, but a quick hack so we can seed the generator
std::mt19937 generator2;

// Another global, this for unique IDs
int uid = 1;
int getid()
{
    return uid++;
}

float randfloat(float low, float high, int seed)
{
    static std::mt19937 generator(seed);
    std::uniform_real_distribution<float> distribution(low, high);
    float n = distribution(generator);
    //DBPRINT("randfloat %f %f %f\n", low, high, n);
    //printf("garf %d %f\n", seed, n);
    return n;
}

int randint(int low, int high, int seed)
{
    static std::mt19937 generator(seed);
    //std::uniform_int_distribution<int32_t> distribution(low, high);
    // This is a nasty hack - it seems that, although the generator is defined tightly,
    // the ditributions are not, and so are not guaranteed to produce the same results
    // across libraries and compilers.
    // It seems from observation that uniform_real_distribution is the same
    // between arm32 gcc and x64 llvm
    // gcc (Ubuntu/Linaro 4.9.4-2ubuntu1~14.04.1) 4.9.4
    // Apple LLVM version 8.1.0 (clang-802.0.42) x86_64-apple-darwin16.7.0
    std::uniform_real_distribution<float> distribution(low, high + 1);
    int n = int(distribution(generator));
    //DBPRINT("randint %d %d %d\n", low, high, n);
    //printf("gari %d %d %d\n", low, high, n);
    return n;
}



float Individual::gen_param(Ptype t)
{
    float p = 0;
    const std::vector<int> ws = {2,3,17,18,19};
    const std::vector<int> wv = {2,18};
    // FIXME!! These should get here from the evoparams somehow, quick hack to test
    const std::vector<int> rs = {0,1,2,3,4,5,6,7,8,9,14,15,16,17,18,19};
    const std::vector<int> rv = {0,2,4,6,8,14,16,18};
    switch(t)
    {
        case SREG:
            p = rs[randint(0, rs.size() - 1)];
            break;
        case VREG:
            p = rv[randint(0, rv.size() - 1)];
            break;
        case WSREG:
            p = ws[randint(0, ws.size() - 1)];
            break;
        case WVREG:
            p = wv[randint(0, wv.size() - 1)];
            break;
        case CVREG:
            p = randint(0, 2) * 2 + 10;
            break;
        case INT8:
            p = randint(-128, 127);
            break;
        case X5_3:
            p = (float)randint(-128, 127) / 8.0f;
            break;
        case FPM32:
            p = randfloat(-32.0f, 32.0f);
            break;
        case IP100:
            p = randint(1, 100);
            break;
        case IPZ3:
            p = randint(0, 2);
            break;
        case FPM5:
            p = randfloat(-5.0f, 5.0f);
            break;
    }
    return p;
}

GPnode Individual::gen_node(Nodeset &set)
{
    // Generate a node randomly from the nodeset, with parameters filled in
    GPnode res;
    res.type = set[randint(0, set.size() - 1)];
    res.nodes_below = 0;
    res.params.resize(res.type.p_arity);
    for(int i = 0; i < res.type.p_arity; i++)
    {
        res.params[i] = gen_param(res.type.ptype[i]);
    }
    return res;
}



int Individual::gen_tree(Nodeset &composition, Nodeset &action, int current_depth, int depth, bool grow)
{
    // Generate a tree recursively to the depth given using grow if true else full.
    // grow chooses from entire nodeset, full chooses only from composition until
    // at depth, then only from action.
    // The tree is stored as a vector in depth-first lr ordering, allowing fast
    // manipulations necessary for the GP operations such as pick random node.
    //
    // Mostly from poli2008fielda
    int nodes_below = 0;
    int idx = tree.size();
    if (    (current_depth == depth)
        ||  (grow
             && (randfloat(0, 1)
                 <  ((float)action.size() / (action.size() + composition.size()))
                )
            )
        )
    {
        // Generate action node
        tree.push_back(gen_node(action));
        anodes.push_back(idx);
        tree.back().depth = current_depth;
    }
    else
    {
        // Generate composition node and fill in children recursively
        tree.push_back(gen_node(composition));
        cnodes.push_back(idx);
        for (int i = 0; i < tree[idx].type.c_arity; i++)
        {
            nodes_below += gen_tree(composition, action, current_depth + 1, depth, grow);
        }
        tree[idx].depth         = current_depth;
        tree[idx].nodes_below   = nodes_below;
    }
    return nodes_below + 1;
}


int Individual::gen_tree(Nodeset &allnodes, std::regex_token_iterator<std::string::iterator> &tokens, int current_depth)
{
    std::regex_token_iterator<std::string::iterator>    rend;
    int nodes_below = 0;
    std::string s = *tokens;
    tokens++;
    bool found = false;
    for(auto &n : allnodes)
    {
        if (s == n.name)
        {
            found = true;
            GPnode r;
            r.type = n;
            r.depth = current_depth;
            for(int i = 0; i < r.type.p_arity; i++)
            {
                r.params.push_back(stof(*tokens));
                tokens++;
            }
            tree.push_back(r);
            int idx = tree.size() - 1;
            for(int i = 0; i < r.type.c_arity; i++)
            {
                nodes_below += gen_tree(allnodes, tokens, current_depth + 1);
            }
            tree[idx].nodes_below = nodes_below;
            break;
        }
    }
    if (!found)
    {
        printf("Malformed tree string, token not found:%s\n", s.c_str());
        exit(1);
    }
    return nodes_below + 1;
}

void Individual::gen_tree_from_string(std::string t, Nodeset &allnodes)
{
    std::regex e("[\\s]+|\\(.*\\)");
    std::regex_token_iterator<std::string::iterator>  tokens = std::regex_token_iterator<std::string::iterator>(t.begin(), t.end(), e, -1);

    gen_tree(allnodes, tokens, 0);
    
    //print_tree();
}

std::string Individual::string_tree()
{
    return string_tree(0);
}
std::string Individual::string_tree(int idx)
{
    std::string s = "";
    for (int i = 0; i < tree[idx].nodes_below + 1; i++)
    {
        GPnode &n = tree[i + idx];
        std::string pad (n.depth * 2, ' ');
        s += string_format("%3d %2d %2d %s%-8s ", i, n.depth, n.nodes_below, pad.c_str(), n.type.name.c_str());
        for(auto i = 0; i < n.type.p_arity; i++)
        {
            switch(n.type.ptype[i])
            {
                case SREG: case VREG: case INT8: case IP100: case IPZ3: case WSREG: case WVREG: case CVREG:
                    s += string_format("%7d ", (int)n.params[i]);
                    break;
                case FPM32: case FPM5: case X5_3:
                    s += string_format("%7.3f ", n.params[i]);
                    break;
            }
        }
        s += "\n";
    }
    return s;
}
void Individual::print_tree()
{
    print_tree(0);
}

void Individual::print_tree(int idx)
{
    printf("%s\n", string_tree(idx).c_str());
}


void GP::fill_ramped_half_half()
{
    // Fill the population with individuals ranging across the
    // depth allowed, with half each grow and full (koza)
    float dsteps = (float)params.gendepth * 2 / params.popsize;
    float fd = 0;
    for(int i = 0; i < params.popsize; i++)
    {
        if (i == (params.popsize / 2))
            fd = 0;
        p[i] = new Individual((int)fd, i < params.popsize / 2, compnodes, actionnodes);
        fd += dsteps;

    }
}

void GP::print_pop()
{
    for (int i = 0; i < params.popsize; i++)
    {
        printf("Individual %d ===================\n", i);
        p[i]->print_tree();
    }
}

void Individual::update_cache()
{
    if (stale_nodecache)
    {
        cnodes.clear();
        anodes.clear();
        for(int i = 0; i < tree.size(); i++)
            if (tree[i].nodes_below)
                cnodes.push_back(i);
            else
                anodes.push_back(i);
        stale_nodecache = false;
    }
}

Individual *GP::crossover(Individual *p1, Individual *p2, bool use_all_p2)
{
    // Create a new individual from the two parents by crossover
    //
    // Pick two locations, one in each individuals tree, and create
    // the new tree by replacing everything at and below the first individuals
    // node with the subtree from the second individual
    //
    // Per Koza, chose composition nodes 90% of the time.
    //
    // Because all nodes in a behaviour tree are themselves legal subtrees,
    // and we are not representing parameters as terminals, this is always
    // a valid operation to perform.
    //
    // The tree is represented as a vector of depth-first traversal. This means
    // that a subtree is a contiguous block within the whole vector. We need to copy
    // from p1 up to but not including i1, then from p2 from i2 until
    // i2 + nodes_below + 1, then from p1 again, starting from after the origina
    // subtree ends
    Individual *i = new Individual;
    
    // Build lists of indices of composition and action nodes. This should
    // probably be done in a cached manner
    p1->update_cache();
    p2->update_cache();
    
    // Pick comp nodes 90% of the time. In the cases where there are only
    // comp nodes or only action nodes, just pick randomly from them.
    int i1, i2;
    int p1cs = p1->cnodes.size();
    int p1as = p1->anodes.size();
    int p2cs = p2->cnodes.size();
    int p2as = p2->anodes.size();
    
    if (p1as == 0)
        i1 = p1->cnodes[randint(0, p1cs - 1)];
    else if (p1cs == 0)
        i1 = p1->anodes[randint(0, p1as - 1)];
    else if (randfloat(0,1) < params.prob_compnode)
        i1 = p1->cnodes[randint(0, p1cs - 1)];
    else
        i1 = p1->anodes[randint(0, p1as - 1)];
    
    if (use_all_p2)
        i2 = 0;
    else
    {
        if (p2as == 0)
            i2 = p2->cnodes[randint(0, p2cs - 1)];
        else if (p2cs == 0)
            i2 = p2->anodes[randint(0, p2as - 1)];
        else if (randfloat(0,1) < params.prob_compnode)
            i2 = p2->cnodes[randint(0, p2cs - 1)];
        else
            i2 = p2->anodes[randint(0, p2as - 1)];
    }
    
    // Get size of original subtree
    int s1 = p1->tree[i1].nodes_below + 1;
    // Get size of replacement subtree
    int s2 = p2->tree[i2].nodes_below + 1;
    // Get size of new tree and resize target
    int sdiff = s2 - s1;
    int newsize = p1->tree.size() + sdiff;
    int d1 = p1->tree[i1].depth;
    int d2 = p2->tree[i2].depth;
    int ddiff = d2 - d1;
    i->tree.resize(newsize);

    // Copy in the bits
    std::copy(p1->tree.begin(), p1->tree.begin() + i1, i->tree.begin());
    std::copy(p2->tree.begin() + i2, p2->tree.begin() + i2 + s2, i->tree.begin() + i1);
    std::copy(p1->tree.begin() + i1 + s1, p1->tree.end(), i->tree.begin() + i1 + s2);
    
    // The copied tree needs to have its depth fixed up
    for(auto j = i->tree.begin() + i1; j != i->tree.begin() + i1 + s2; ++j)
        (*j).depth -= ddiff;
    
    // The nodes_below fields of all prior entries in the list that have
    // a lower depth needs to be fixed up
//    for(auto j = i->tree.begin(); j != i->tree.begin() + i1; ++j)
//        if ((*j).depth < d1)
//        {
//            d1 = (*j).depth;
//            (*j).nodes_below += sdiff;
//        }
    for(auto j = i1; j >= 0; --j)
        if (i->tree[j].depth < d1)
        {
            d1 = i->tree[j].depth;
            i->tree[j].nodes_below += sdiff;
        }
    
    
    // Give the individual a unique ID and copy in the parents
    i->p1id = p1->myid;
    i->p2id = p2->myid;
    
    //##FIXME## ADB stuff
    DBPRINT("Performing crossover p1c:%d p1a:%d p2c:%d p2a:%d at %d+%d, %d+%d of \n%s\nwith\n%s\n", p1cs,p1as,p2cs,p2as,i1,s1,i2,s2,p1->string_tree().c_str(),p2->string_tree().c_str());

    DBPRINT("Giving \n%s\n", i->string_tree().c_str());
    return i;
}

Individual *Individual::clone()
{
    // Create a copy of an individual, return pointer to it
    Individual *n = new Individual;
    *n = *this;
    return n;
}

BT *Individual::get_bt()
{
    if (stale_bt)
    {
        btstr = btprefix;
        for (int i = 0; i < tree[0].nodes_below + 1; i++)
        {
            GPnode &n = tree[i];
            btstr += string_format("%s ", n.type.name.c_str());
            for(auto i = 0; i < n.type.p_arity; i++)
            {
                switch(n.type.ptype[i])
                {
                    case SREG: case VREG: case INT8: case IP100: case IPZ3: case WSREG: case WVREG: case CVREG:
                        btstr += string_format("%d ", (int)n.params[i]);
                        break;
                    case FPM32: case FPM5: case X5_3:
                        btstr += string_format("%f ", n.params[i]);
                        break;
                }
            }
        }
        if (bt)
            delete(bt);
        DBPRINT("About to generate BT: tree:\n%swith string:>>%s<<\n", string_tree().c_str(), btstr.c_str());
        bt = new BT(BLACKBOARD_ENTRIES, btstr);
        //printf("%s\n%s\n", btstr.c_str(), bt->get_btstr().c_str());
        stale_bt = false;
    }
    return bt;
}

std::string Individual::get_btstr()
{
    get_bt();
    return btstr;
}


Individual::Individual(int depth, bool grow, Nodeset &compnodes, Nodeset &actionnodes)
{
    gen_tree(compnodes, actionnodes, 0, depth, grow);
    stale_nodecache = false;
    bt              = NULL;
    objectives.push_back(NAN);
    objectives.push_back(NAN);
    myid = getid();
}

Individual::Individual()
{
    objectives.push_back(NAN);
    objectives.push_back(NAN);
    myid = getid();
}

Individual::Individual(std::string tstr, Nodeset &allnodes)
{
    // When we generate a new individual, we must strip off any BT prefix, since this does not take
    // part in the evolutionary algorithm
    // All BTs consist of space-delimited words and numbers. Count the words in the btprefix and remove that
    // many from the input
    int prefix_length = btprefix.size();
    gen_tree_from_string(tstr.substr(prefix_length), allnodes);
    update_cache();
    objectives.push_back(NAN);
    objectives.push_back(NAN);
    myid = getid();
}


void GP::mutateparams(Individual *i)
{
    // Perform param mutation, iterate over all parameters in all nodes,
    // with certain probability mutate the parameter
    int total_params = 0;
    bool any_mutations_done = false;
    for(auto &n : i->tree)
        total_params += n.type.p_arity;
    int expected_muts = params.prob_mparam * (float)total_params;
    DBPRINT("Mut params usebias:%d bias:%f params:%d expected:%d\n", params.use_bias_mut, params.mparam_bias, total_params, expected_muts);
    
    if (!params.use_bias_mut)
    {

        int pos = 0;
        for(auto &n : i->tree)
        {
            if (n.type.p_arity)
            {
                for (int k = 0; k < n.type.p_arity; k++)
                {
                    if (randfloat(0, 1) < params.prob_mparam)
                    {
                        n.params[k] = i->gen_param(n.type.ptype[k]);
                        any_mutations_done = true;
                        DBPRINT("Mut at param %d\n", pos);
                    }
                    pos++;
                }
            }
        }
    }
    else
    {
        // Get total number of parameters and then an expected number of mutations.
        // We use the mutation probability times the bias and start mutating from the beginning
        // of the tree until we reach the expected number or the end of the tree.
        // If the bias is high, more mutations will occur near the start of the
        // list (which is depth first lr)
        int muts = 0;
        int pos = 0;
        for(auto &n : i->tree)
        {
            if (n.type.p_arity)
            {
                for (int k = 0; k < n.type.p_arity; k++)
                {
                    if (randfloat(0, 1) < params.prob_mparam * params.mparam_bias)
                    {
                        n.params[k] = i->gen_param(n.type.ptype[k]);
                        any_mutations_done = true;
                        muts++;
                        DBPRINT("Mut:%d at param %d\n", muts, pos);
                        if (muts >= expected_muts)
                            goto escape;
                    }
                    pos++;
                }
            }
        }
    escape:;
        // All mutations done

    }
    if (any_mutations_done)
    {
        i->mutations |= 1;
//        // Consider this a new individual, so give it a new id
//        i->p1id = i->myid;
//        i->p2id = -1;
//        i->myid = getid();
    }
}

void GP::mutatepoint(Individual *i)
{
    // Perform point mutation, iterate over all nodes, with certain probability replace
    // node with one of same child arity, randomly generating parameters
    int expected_muts = params.prob_mpoint * (float)i->tree.size();
    bool any_mutations_done = false;
    DBPRINT("Mut point usebias:%d bias:%f size:%d, expected:%d\n", params.use_bias_mut, params.mpoint_bias, (int)i->tree.size(), expected_muts);
    int pos = 0;
    if (!params.use_bias_mut)
    {
        for(auto &n : i->tree)
        {
            if (randfloat(0, 1) < params.prob_mpoint)
            {
                any_mutations_done = true;
                int arity   = n.type.c_arity;
                int pick    = randint(0, aritymap[arity].size() - 1);
                int node    = aritymap[arity][pick];
                DBPRINT("Replacing pos:%d %s:%d with %s:%d\n", pos, n.type.name.c_str(), n.type.p_arity, allnodes[node].name.c_str(), allnodes[node].p_arity);
                n.type      = allnodes[node];
                //DBPRINT("\n");
                n.params.resize(n.type.p_arity);
                for(int k = 0; k < n.type.p_arity; k++)
                {
                    Ptype ptype = n.type.ptype[k];
                    float p = i->gen_param(ptype);
                    n.params[k] = p;
                }
            }
            pos++;
        }
    }
    else
    {
        int muts = 0;
        for(auto &n : i->tree)
        {
            if (randfloat(0, 1) < params.prob_mpoint * params.mpoint_bias)
            {
                any_mutations_done = true;
                int arity   = n.type.c_arity;
                int pick    = randint(0, aritymap[arity].size() - 1);
                int node    = aritymap[arity][pick];
                DBPRINT("Replacing pos:%d %s:%d with %s:%d\n", pos, n.type.name.c_str(), n.type.p_arity, allnodes[node].name.c_str(), allnodes[node].p_arity);
                n.type      = allnodes[node];
                //DBPRINT("\n");
                n.params.resize(n.type.p_arity);
                for(int k = 0; k < n.type.p_arity; k++)
                {
                    Ptype ptype = n.type.ptype[k];
                    float p = i->gen_param(ptype);
                    n.params[k] = p;
                }
                muts++;
                if (muts >= expected_muts)
                    break;
            }
            pos++;
        }
    }
    if (any_mutations_done)
    {
        i->mutations |= 2;
//        // Consider this a new individual, so give it a new id
//        i->p1id = i->myid;
//        i->p2id = -2;
//        i->myid = getid();
    }
}

void GP::mutatesubtree(Individual  *&i)
{
    // Perform subtree mulation. Pick a node at random, then with probability prob_msubtree,
    // replace it with a new subtree. Pick composition nodes 90% of the time
    
    if (randfloat(0,1) < params.prob_msubtree)
    {
        DBPRINT("Mut subtree\n");
        // Use crossover, but with one parent generated especially
        Individual *n = new Individual(randint(0, params.gendepth), false, compnodes, actionnodes);
        // Force crossover to use whole of second parent
        Individual *m = crossover(i, n, true);
        // Get rid of old tree and replace with new, copying over parentage and mutation
        // record
        m->myid         = i->myid;
        m->p1id         = i->p1id;
        m->p2id         = i->p2id;
        m->mutations    = i->mutations | 4;
        delete(i);
        delete(n);
        i = m;
        //i->p2id = -3;
    }
}

void GP::mutateshuffle(Individual *&i)
{
    // Perform child mutation. Pick a composition node with arity > 1 and shuffle
    // the order of its children
    int total_multichild = 0;
    for(auto &n : i->tree)
        if (n.type.c_arity > 1)
            total_multichild++;
    int expected_muts = params.prob_mshuffle * (float)total_multichild;
    int muts = 0;
    DBPRINT("Mut shuffle bias:%f multichild:%d expected:%d\n", params.mparam_bias, total_multichild, expected_muts);

    i->update_cache();
    if (params.use_bias_mut)
    {
        int pos = 0;
        for(auto idx : i->cnodes)
        {
            auto &n = i->tree[idx];
            if ((n.type.c_arity > 1) && (randfloat(0, 1) < params.prob_mshuffle * params.mshuffle_bias))
            {
                // Get the indices of the child nodes
                DBPRINT("Starting with\n%s\n", i->string_tree().c_str());
                int child_idx = idx + 1;
                std::vector<int> children;
                std::vector<int> order;
                children.push_back(child_idx);
                order.push_back(0);
                for(int j = 1; j <= n.type.c_arity; j++)
                {
                    GPnode &child   = i->tree[child_idx];
                    child_idx       = child_idx + child.nodes_below + 1;
                    children.push_back(child_idx);
                    order.push_back(j);
                }
                // Now the children vector has the indices of each child, and of the
                // following node at the same level, so arity + 1 entries.
                //
                // Copy the child trees in shuffled order into a second vector, then copy the whole lot
                // back into the main tree
                Tree c;
                std::shuffle(order.begin(), order.end() - 1, generator2);
                for(auto o : order)
                    DBPRINT("%d ", o);
                DBPRINT("\n");
                for(int j = 0; j < n.type.c_arity; j++)
                {
                    int start   = children[order[j]];
                    int len     = children[order[j] + 1] - start;
                    for(int k = 0; k < len; k++)
                    {
                        c.push_back(i->tree[start + k]);
                    }
                }
                for(int j = 0; j < n.nodes_below; j++)
                {
                    i->tree[children[0] + j] = c[j];
                }
                muts++;
                DBPRINT("Giving\n%s\n", i->string_tree().c_str());

            }
        }

    }
    if (muts)
    {
        // Consider this a new individual, so give it a new id
        i->p1id = i->myid;
        i->p2id = -4;
        i->myid = getid();
    }

}


void GP::update_fitness(gpusim::Sim *sim, bool secondary)
{
    // Update fitness values on individuals. This function will be called
    // with a vector of fitnesses calculated by running the BTs in sim
    //
    // The order of individuals is as at creation time, so the first will be the elites.
    // We don't update the fitness values of elites, since these have already been
    // copied in from a previous trial, unless they have never been evaluated, i.e this is
    // the first generation
    //int actual_elites = params.popsize < params.n_elites ? params.popsize : params.n_elites;

    int repeats = sim->all_fitness.size() / params.popsize;
    for(int i = 0; i < params.popsize; i++)
    {
        
        // https://en.wikipedia.org/wiki/Algorithms_for_calculating_variance#Online_algorithm
        if (!p[i]->evaluations)
        {
            p[i]->fitness = 0;
            p[i]->lfitness = 0;
            p[i]->parsimony = sim->all_parsimony[i];
        }
        for (int j = 0; j < repeats; j++)
        {
            float f             = sim->all_fitness[i * repeats + j];
            p[i]->evaluations   += 1;
            float delta         = f - p[i]->fitness;
            p[i]->fitness       += delta / p[i]->evaluations;
            float delta2        = f - p[i]->fitness;
            p[i]->m2            += delta * delta2;

            
            float lf            = sim->all_lfitness[i * repeats + j];
            delta               = lf - p[i]->lfitness;
            p[i]->lfitness       += delta / p[i]->evaluations;

            if (0)
            {
                std::vector<float> &c =  sim->all_evc[i * repeats + j].coords;
                for(int k = 0; k < c.size(); k+=3)
                    zmqlog(pub_log, string_format("%s:evc:i:%d:(%f,%f,%f) fit:%f id:%d",
                                                  params.hostname.c_str(), i, c[k], c[k+1], c[k+2], f, p[i]->myid));
            }
            if (params.grabdetail)
            {
                p[i]->evc.push_back(sim->all_evc[i * repeats + j]);
                p[i]->evc.back().fitness = f;
            }
        }
        p[i]->variance  = p[i]->m2 / (p[i]->evaluations - 1);
        
        
        p[i]->objectives.clear();
        p[i]->objectives.push_back(p[i]->fitness);
        p[i]->objectives.push_back(1.0 - sqrt(p[i]->variance));
        
        // Get the lower bound 95% confidence fitness
        if (p[i]->evaluations >= 8)
            p[i]->fitness95 = p[i]->fitness - 1.96 * (sqrt(p[i]->variance) / sqrt(p[i]->evaluations));
        else
            p[i]->fitness95 = p[i]->fitness * ((float)p[i]->evaluations / 8);
        
        

        
        
        if (params.verbose>1)printf("ind:%4d eval:%3d nfit:%10f lfit:%10f nstd:%10f\n", i, p[i]->evaluations,
                                    p[i]->fitness, p[i]->lfitness, sqrt(p[i]->variance));
    }
}

int GP::tournament()
{
    int idx = 0;
    if (1)
    {
        // Tournament selection - pick tsize individuals with replacement
        // from the population and return an index to the fittest. Fitness is
        // assumed to be positive = better
        float f = -99999999;
        int tsize = params.n_tsize;
        bool use_f95 = true;
        std::vector<int> indices;
        for (int i = 0; i < tsize; i++)
        {
            indices.push_back(randint(0, params.popsize - 1));
            //if (p[indices.back()]->evaluations == 1)
            //    use_f95 = false;
        }
        //if (use_f95)printf("use_f95 %d %d\n", params.model, tsize, );
        for (int i = 0; i < tsize; i++)
        {
            int new_idx = indices[i];
            float new_f;
            if (params.model == 3)
            {
                printf("%d %d\n", new_idx, p[new_idx]->evaluations);
                if (p[new_idx]->evaluations > 1)
                    new_f = p[new_idx]->fitness95;
                else
                    new_f = p[new_idx]->fitness * 0.5;
            }
            else
                new_f = p[new_idx]->fitness;
            //float new_f = p[new_idx]->fitness - p[new_idx]->stddev;
            if (new_f > f)
            {
                f   = new_f;
                idx = new_idx;
            }
        }
    }
    else
    {
        // We want to maximise fitness while minimising stddev, so that solutions are consistant.
        // Pick tournament size individuals. If there is a single dominant individual, return that.
        // Otherwise, pick the fittest from the set of non-dominated individuals.
        //

        std::shuffle(indices.begin(), indices.end(), generator2);
        std::vector<int> dominated;
        for(int i = 0; i < params.n_tsize; i++)
        {
            for(int j = i + 1; j < params.n_tsize; j++)
            {
                if ((p[indices[i]]->fitness < p[indices[j]]->fitness) && (p[indices[i]]->variance > p[indices[j]]->variance))
                {
                    if (std::find(dominated.begin(), dominated.end(), i) == dominated.end())
                        dominated.push_back(i);
                    break;
                }
                if ((p[indices[j]]->fitness < p[indices[i]]->fitness) && (p[indices[j]]->variance > p[indices[i]]->variance))
                {
                    if (std::find(dominated.begin(), dominated.end(), j) == dominated.end())
                        dominated.push_back(j);
                }
            }
        }
        // At this point, the vector dominated has all the individuals that are weaker in fitness and stddev than at
        // least one other. All other individuals are non-dominant and on the pareto front. From these, choose
        // the fittest
        float f = -99999999;
        for(int i = 0; i < params.n_tsize; i++)
        {
            if (std::find(dominated.begin(), dominated.end(), i) == dominated.end())
            {
                // This individual is non-dominated
                float new_f = p[indices[i]]->fitness;
                if (new_f > f)
                {
                    f   = new_f;
                    idx = indices[i];
                }
            }
        }
//        for(int i = 0; i < params.popsize; i++)
//            printf("idx:%3d fit:% 8f std:% 8f\n", i, p[i]->fitness, p[i]->stddev);
//        printf("idx:%d\n", idx);
    }
    return idx;
        
}


void GP::sort_population(bool use_f95)
{
    auto sortrule = [](Individual *i1, Individual *i2) -> bool
    {
        return i1->fitness > i2->fitness;
    };
    auto sortrule_f95 = [](Individual *i1, Individual *i2) -> bool
    {
        return i1->fitness95 > i2->fitness95;
    };
    
    if (use_f95)
        std::sort(p.begin(), p.end(), sortrule_f95);
    else
        std::sort(p.begin(), p.end(), sortrule);
    
}



void GP::make_next_generation()
{
    // At this point, every individual in the population has a fitness, and the population
    // has been sorted
    // 2) copy fittest n_elites
    // 3) discard weakest n_weak, replace with new trees
    // 4) fill remaining by repeated tournament selection t=3 to get parents, crossover, then mutation
    DBPRINT("Make next generation..\n");

    for(int i = 0; i < params.popsize; i++)
    {
        DBPRINT("%d %8f %s\n", i, p[i]->fitness, p[i]->get_btstr().c_str());
    }

    if(params.verbose>1)printf("Fittest individual:\n%s\n%s\nfitness:%f\n", p[0]->string_tree().c_str(), p[0]->get_btstr().c_str(), p[0]->fitness);
    // Create new empty population
    Population np(params.popsize);

    // Copy elites
    int actual_elites = params.popsize < params.n_elites ? params.popsize : params.n_elites;
    for (int i = 0; i < actual_elites; i++)
    {
        DBPRINT("Individual %d by elitism\n", i);
        np[i] = p[i]->clone();
        if (params.model == 0)
            np[i]->evaluations = 0;
    }
    
    // Create replacements for weakest
    int actual_weakest = params.popsize < params.n_elites ? 0 : params.popsize < params.n_elites + params.n_weakest ? params.popsize - params.n_elites : params.n_weakest;
    for (int i = params.popsize - actual_weakest; i < params.popsize; i++)
    {
        DBPRINT("Individual %d by new tree\n", i);
        np[i] = new Individual(params.gendepth, false, compnodes, actionnodes);
    }

    
    // Do crossovers
    //int n_xover = (popsize - n_elites) * prop_xover;
    for (int i = actual_elites; i < params.popsize - actual_weakest; i++)
    {
        int p1 = tournament();
        int p2 = tournament();
        DBPRINT("Individual %d by crossing over %d and %d\n", i, p1, p2);
        np[i] = crossover(p[p1], p[p2]);
    }
    
    // Do mutations
    for (int i = actual_elites; i < params.popsize - actual_weakest; i++)
    {
        //int c = tournament(n_tsize);
        DBPRINT("Individual %d mutation\n", i);
        //np[i] = p[c]->clone();
        mutateparams(np[i]);
        mutatepoint(np[i]);
        mutatesubtree(np[i]);
        //mutateshuffle(np[i]);
    }

    // Free the old population
    for (int i = 0; i < params.popsize; i++)
        delete(p[i]);
    
    p = np;
  
    for(int i = 0; i < params.popsize; i++)
    {
        DBPRINT("\nIndividual %d stale:%d\nbtstr:%s\n",i, p[i]->stale_bt, p[i]->get_btstr().c_str());
    }

}

std::vector<BT*> *GP::bt()
{
    // Return a vector of behaviour tree structures representing the population
    b.clear();
    for (int i = 0; i < params.popsize; i++)
    {
        b.push_back(p[i]->get_bt());
    }
    return &b;
}

Individual::Individual(Json i, Nodeset &allnodes)
{

   // When we generate a new individual, we must strip off any BT prefix, since this does not take
    // part in the evolutionary algorithm
    // All BTs consist of space-delimited words and numbers. Count the words in the btprefix and remove that
    // many from the input
    
    int prefix_length = btprefix.size();
    gen_tree_from_string(i["tree"].string_value().substr(prefix_length), allnodes);
    update_cache();
    myid        = i["myid"].number_value();
    p1id        = i["p1id"].number_value();
    p2id        = i["p2id"].number_value();
    //fitness     = i["af"].number_value();
    //evaluations = i["ev"].number_value();
    // Imported genes start anew.
    fitness     = 0;
    evaluations = 0;
    objectives.push_back(fitness);
    objectives.push_back(NAN);

}

void GP::insert_genes(std::string &gs)
{
    // Given a set of genes from the geneserver, insert into the population, replacing the weakest.
    // The genes are expressed as a json array of json objects. Each object is an individual
    int idx = params.popsize - 1;

    std::string err;
    Json genes = Json::parse(gs, err);
    if (err.size())
    {
        printf("Malformed gene list from server %s\n", err.c_str());
        return;
    }
    if (!genes.is_array())
    {
        printf("Expecting array from geneserver!\n");
        return;
    }
    for (int i = 0; i < genes.array_items().size(); i++)
    {
        p[idx] = new Individual(genes[i], universenodes);
        p[idx]->get_bt();
        p[idx]->parsimony = p[idx]->bt->parsimony;
        idx--;
    }
}



GP::GP(Evoparams &_params, std::vector<std::string> &_trees)
:   params (_params), trees(_trees)
{

    // Initialise the unique ID source
    uid = 1;
    if (params.hostname[0] == 'x')
    {
        // This is a cluster machine, set the seed to 100000 * robot number
        int i = std::stoi(params.hostname.substr(2, 2)) * 100000;
        uid         += i;
    }
    printf("Using starting ID %d\n", uid);
    // Set the random number generator seed
    generator2.seed(params.seed);
    randfloat(0, 0, params.seed);
    randint(0, 0, params.seed);
    
    if(params.verbose)printf("Constructing GP mode:%d\n", params.mode);

    // Construct the composition and action node vectors that we use when building trees.
    //
    // This is a subset of the BT vocabulary
    for (auto i : composition)
    {
        if (find(params.allowed_comp.begin(), params.allowed_comp.end(), i.type) != params.allowed_comp.end())
        {
            compnodes.push_back(i);
        }
    }
    if (params.mode & 1)
        for (auto i : actionatom)
        {
            if (find(params.allowed_actionatom.begin(), params.allowed_actionatom.end(), i.type) != params.allowed_actionatom.end())
            {
                actionnodes.push_back(i);
            }
        }
    if (params.mode & 2)
        for (auto i : action)
        {
            if (find(params.allowed_action.begin(), params.allowed_action.end(), i.type) != params.allowed_action.end())
            {
                actionnodes.push_back(i);
            }
        }

    // Construct vector of all allowed node types
    allnodes.insert(allnodes.end(), compnodes.begin(), compnodes.end());
    allnodes.insert(allnodes.end(), actionnodes.begin(), actionnodes.end());
    
    
    // Construct the vector of the universe of nodes, this is the complete set, and allows creation of
    // individuals from strings that include forbidden nodes, eg the prefix sel2 avoiding
    universenodes.insert(universenodes.end(), composition.begin(), composition.end());
    universenodes.insert(universenodes.end(), actionatom.begin(), actionatom.end());
    universenodes.insert(universenodes.end(), action.begin(), action.end());
    
    // Build the arity map
    for(int i = 0; i <= MAX_ARITY; i++)
    {
        // For each arity value, build a vector of nodes that have that arity
        std::vector<int>    nodes;
        for(int j = 0; j < allnodes.size(); j++)
        {
            if (allnodes[j].c_arity == i)
                nodes.push_back(j);
        }
        aritymap.push_back(nodes);
    }
    
    // Build the indices array, used for shuffling to obtain a random set of
    // individuals without replacement
    for(int i = 0; i < params.popsize; i++)
        indices.push_back(i);
    
    
    // Make an empty population and fill the individuals using
    // ramped half and half
    p.resize(params.popsize);
    fill_ramped_half_half();
    
    
    
    // If we have seed trees, use these to overwrite some of the population
    int i = 0;
    if (trees.size() > 0)
        while (i < params.popsize)
        {
            for(auto &t : trees)
            {
                if (i >= params.popsize)
                    break;
                delete(p[i]);
                p[i] = new Individual(t, universenodes);
                i++;
            }
        }
    
}


void print_bar_chart(std::vector<float> f)
{
    float min = HUGE_VALF;
    float max = -HUGE_VALF;
    float mean = 0;
    float std = 0;
    int n = f.size();
    for (auto i : f)
    {
        if (i < min) min = i;
        if (i > max) max = i;
        mean += i;
    }
    mean /= n;
    for (auto i : f)
    {
        std += pow(i - mean, 2);
    }
    std = sqrt(std / (n - 1));
    const int bnum = 10;
    int bins[bnum] = {0};
    float bsize = (max - min) / bnum;
    int offset = min / bsize;
    for (auto i : f)
    {
        int b = i / bsize;
        bins[b - offset]++;
    }
    printf("Fitness: % 8f  std: % 8f  min: % 8f  max: % 8f\n", mean, std,
           min, max);
    for (int i = 0; i < bnum; i++)
    {
        printf("%2d %3d % 8f %s\n", i, bins[i], (float)(i + offset) * bsize, std::string(bins[i], '=').c_str());
    }

}


std::string GP::create_json(Individual *i, bool minimal)
{
    Json ji;
    if (minimal)
    {
        ji = Json::object {
            {"myid", i->myid},
            {"p1",  i->p1id},
            {"p2",  i->p2id},
            {"m",   i->mutations},
            {"ev",  i->evaluations},
            {"af",  i->fitness},
            {"lf",  i->lfitness},
            {"g", generation},
        };
    }
    else
    {
        std::vector<Json> scenes;
        if (i->evc.size())
        {
            for (int e = 0; e < i->evaluations; e++)
            {
                int robots = i->evc[0].coords.size() / 3 - 1;
                std::vector<float> &c = i->evc[e].coords;
                std::vector<Json> rposes;
                for (int j = 0; j < robots; j++)
                {
                    rposes.push_back(Json(std::vector<float>({c[j * 3], c[j * 3 + 1], c[j * 3 + 2]})));
                }
                std::vector<Json> fposes;
                fposes.push_back(Json(std::vector<float>({c[robots * 3], c[robots * 3 + 1], c[robots * 3 + 2]})));
                scenes.push_back(Json::object {
                    {"seed", i->evc[e].seed},
                    {"eval", e},
                    {"robot", Json(rposes)},
                    {"frisbee", Json(fposes)},
                    {"fitness", i->evc[e].fitness},
                });
                
            }
        }
        ji = Json::object {
            {"params", Json::object {
                {"simtime",  sim->params.simtime},
            }},
            {"scene", Json(scenes) },
            {"tree", Json(i->get_btstr())},
            {"myid", i->myid},
            {"p1",  i->p1id},
            {"p2",  i->p2id},
            {"m",   i->mutations},
            {"ev",  i->evaluations},
            {"af",  i->fitness},
            {"lf",  i->lfitness},
            {"g",   generation}
        };
    }
    return ji.dump();
}


void GP::dump_json(Individual *i)
{

    std::string fullfname = string_format("ind%08d.json", i->myid);
    if (params.dumppath != "")
        fullfname = string_format("%s/%s", params.dumppath.c_str(), fullfname.c_str());
    FILE *fp = fopen(fullfname.c_str(), "w");
    if (!fp)
    {
        printf("Failed to open %s\n", fullfname.c_str());
        exit(1);
    }
    std::string ds = create_json(i);
    fprintf(fp, "%s", ds.c_str());
    fclose(fp);
}

void die(char *s, int d)
{
    printf("%s (errno=%d)\n", s, d);
    exit(1);
}
void GP::report_population(bool meval)
{
    //if (params.controlled) return;
    FILE *lf = fopen(logfile.c_str(), "a");
    if (!lf)
    {
        printf("Something strange happened and the logfile: %s didn't open..\n", logfile.c_str());
        exit(1);
    }
    float mean = 0.0f;
    for (auto i : p)
        mean += i->fitness;
    mean /= p.size();
    int ind = 0;
    int evals = 0;
    // For new algo, reporting the 'fittest' is deceptive, since it may only have a few or a single evaluation.
    // For compatibility with previous methods, find the fittest in the top half of the elites that has at least
    // 8 evaluations, or, failing that, the individual with the most evaluations in the the top half of the elites.
    if (meval)
    {
        std::set<int> possibles = get_possibles(1);
        ind = *(possibles.begin());
    }
//        for (int i = 0; i < p.size() * params.prop_elites * 0.5; i++)
//            if (p[i]->evaluations > evals)
//            {
//                evals = p[i]->evaluations;
//                ind = i;
//                if (evals >= 8)
//                    break;
//            }
    std::string ls = string_format(
        "====Generation %3d of %3d, pop %d rep %d seed %d gd %d el %d ts %d fittest %8f f95 % 8f std %8f evals %4d mean % 8f parsimony %8f elapsed %8f ind %d\n",
                                   generation + 1,
                                   params.generations,
                                   params.popsize, params.repeats, params.seed,
                                   params.gendepth, params.n_elites,
                                   params.n_tsize, p[ind]->fitness, p[ind]->fitness95, sqrt(p[ind]->variance),
                                   p[ind]->evaluations, mean, p[ind]->parsimony, (double)lap_time/1e9, ind);
    printf("%s", ls.c_str());
    fprintf(lf, "%s", ls.c_str());
    fclose(lf);

    if (params.treefile != "")
    {
        // If there is a treefile specified, we save the BT string of the 'fittest' (pointed to by ind)
        // individual to that file. Since the file is read periodically by the BT controller running the robot,
        // the write needs to be done atomically. To do this, the following steps are needed:
        //  1) Write output to temporary file (in same filesystem)
        //  2) fsync to ensure written to file system
        //  3) close the file
        //  4) Rename the file to the name of the target
        // http://www.microhowto.info/howto/atomically_rewrite_the_content_of_a_file.html
        std::string tmp_treefile = string_format("%s~", params.treefile.c_str());
        int fd = open(tmp_treefile.c_str(), O_RDWR | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
        if (fd == -1)                                                       die("Failed to open treefile for writing", errno);
        std::string ts = p[ind]->get_btstr();
        ts = string_format("( Dumped by xps id:%d fit:%f ) %s ", p[ind]->myid, p[ind]->fitness, ts.c_str());
        if (write(fd, ts.c_str(), ts.size()) == -1)                         die("Failed to write treefile", errno);
        if (fsync(fd) == -1)                                                die("Failed to fsync treefile", errno);
        if (close(fd) == -1)                                                die("Failed to close treefile", errno);
        if (rename(tmp_treefile.c_str(), params.treefile.c_str()) == -1)    die("Failed to move treefile tmp", errno);

        // Keep the tree we output in the archive
        //archive.push_back(p[ind]->clone());
        
        // Now send the tree we saved
        //zmqlog(pub_log, string_format("%s:-2:%s", params.hostname.c_str(), create_json(p[ind], true).c_str()));
    }
}

void GP::output_population(bool final)
{
    if (params.controlled) return;
    FILE *lf = fopen(logfile.c_str(), "a");
    if (final)
    {
        std::string ls = string_format(
                         "====Final eval: pop %d rep %d seed %d gd %d el %d ts %d fittest %8f std %8f parsimony %8f elapsed %8f\n",
                         params.popsize, params.repeats,
                         params.seed, params.gendepth, params.n_elites, params.n_tsize,
                         p[0]->fitness, sqrt(p[0]->variance), p[0]->parsimony, (double)lap_time/1e9);
        printf("%s", ls.c_str());
        fprintf(lf, "%s", ls.c_str());
    }
    for(int j = params.popsize - 1; j >= 0; j--)
    {
        fprintf(lf, "%3d %10f %s\n", j, p[j]->fitness, p[j]->get_btstr().c_str());
        if (params.dumpjson)
            dump_json(p[j]);
    }
    fclose(lf);
}

void GP::setup_evo(gpusim::Sim *_sim)
{
    sim = _sim;
    //Clock clk;

    // The constructor has filled the initial population using ramped half-and-half, with
    // seed trees if supplied
    clk.start();
    for (generation = 0; generation < params.generations; generation++)
    {
        sim->evaluate_bt(bt(), params.repeats, sim->params.steps);
        update_fitness(sim);
        sort_population();
        lap_time = clk.lap();
        report_population();
        if (generation != params.generations - 1)
            make_next_generation();
    }
    sort_population();
    //output_population();
    
    // Make a final evaluation with many more repeats
    //int rep = 256;
    //sim->evaluate_bt(bt(), rep, sim->params.steps);
    //update_fitness(sim->all_fitness, sim->all_parsimony, sim->all_evc);
    //sort_population();
    //for (int i = 0; i < p.size(); i++)
    //    print_bar_chart(std::vector<float>(sim->all_fitness.begin() + i * rep, sim->all_fitness.begin() + i * rep + rep - 1));
    output_population(true);
}


void GP::sort_kc()
{
    auto sortrule = []( std::pair<int,int> i1, std::pair<int,int> i2) -> bool
    {
        return i1.second > i2.second;
    };
    std::sort(kc.begin(), kc.end(), sortrule);
    
}

int GP::calc_kmeans()
{
    kcluster.resize(p.size());
    // Initialise with random cluster assignment
    for (int i = 0; i < pf.size(); i++)
        kcluster[pf[i]] = randint(0, n_means - 1);
    bool still_moving = true;
    int km_iterations = 0;
    while (still_moving)
    {
        still_moving = false;
        // Update means
        std::fill(kmeans.begin(), kmeans.end(), Kmeans());
        for (int i = 0; i < pf.size(); i++)
        {
            kmeans[kcluster[pf[i]]].mean.x += p[pf[i]]->objectives[0];
            kmeans[kcluster[pf[i]]].mean.y += p[pf[i]]->objectives[1];
            kmeans[kcluster[pf[i]]].count++;
        }
        for (int i = 0; i < n_means; i++)
        {
            kmeans[i].mean.x /= kmeans[i].count;
            kmeans[i].mean.y /= kmeans[i].count;
        }
        // Reassign clusters
        for (int i = 0; i < pf.size(); i++)
        {
            float2 xy = (float2){{p[pf[i]]->objectives[0], p[pf[i]]->objectives[1]}};
            float min_dist = HUGE_VALF;
            int nearest_cluster = -1;
            for (int j = 0; j < n_means; j++)
            {
                float dist = pow(xy.x - kmeans[j].mean.x, 2) + pow(xy.y - kmeans[j].mean.y, 2);
                if (dist < min_dist)
                {
                    min_dist        = dist;
                    nearest_cluster = j;
                }
            }
            if (nearest_cluster != kcluster[pf[i]])
            {
                still_moving = true;
                kcluster[pf[i]] = nearest_cluster;
            }
        }
        km_iterations++;
    }
    // When we get here, the PF has been assigned to n_means clusters
    for (int i = 0; i < n_means; i++)
    {
        kc[i].first = i;
        kc[i].second = kmeans[i].count;
    }
    sort_kc();
    return km_iterations;
}


void GP::print_pop_mo()
{
    float th = 0.8;
    for (int i = 0; i < 64; i++)
    {
        bool d = dominated[i] != -1;
        char a0 = p[i]->objectives[0] > th ? '*' : ' ';
        char a1 = p[i + 64]->objectives[0] > th ? '*' : ' ';
        char a2 = p[i + 128]->objectives[0] > th ? '*' : ' ';
        char a3 = p[i + 192]->objectives[0] > th ? '*' : ' ';
        std::string s = string_format("id: %3d ev: %3d km: %2d % 9f%c% 9f ",
                                      i, p[i]->evaluations, d ? -1 : kcluster[i], p[i]->objectives[0], a0, p[i]->objectives[1]);
        s += string_format("id: %3d ev: %3d km: %2d % 9f%c% 9f ",
                           i + 64, p[i + 64]->evaluations, d ? -1 : kcluster[i + 64], p[i + 64]->objectives[0], a1, p[i + 64]->objectives[1]);
        s += string_format("id: %3d ev: %3d km: %2d % 9f%c% 9f ",
                           i + 128, p[i + 128]->evaluations, d ? -1 : kcluster[i + 128], p[i + 128]->objectives[0], a2, p[i + 128]->objectives[1]);
        s += string_format("id: %3d ev: %3d km: %2d % 9f%c% 9f ",
                           i + 192, p[i + 192]->evaluations, d ? -1 : kcluster[i + 192], p[i + 192]->objectives[0], a3, p[i + 192]->objectives[1]);
        
        
        printf("%s\n", s.c_str());
    }
    
}
void GP::print_pop_so()
{
    if (params.controlled) return;
    float th = 0.8;
    int ps = params.popsize;
    if (params.popsize < 64)
    {
        for (int i = 0; i < params.popsize; i++)
        {
            char a0 = p[i]->objectives[0] > th ? '*' : ' ';
            std::string s = string_format("id:%5d ev: %3d % 9f%c% 9f ",
                                          p[i]->myid, p[i]->evaluations, p[i]->fitness, a0, sqrt(p[i]->variance));
            printf("%s\n", s.c_str());
        }
    }
    else
    {
        char a0, a1, a2, a3;
        for (int i = 0; i < 64; i++)
        {
            a0 = p[i]->objectives[0] > th ? '*' : ' ';
            if(ps>64)   a1 = p[i + 64]->objectives[0] > th ? '*' : ' ';
            if(ps>128)  a2 = p[i + 128]->objectives[0] > th ? '*' : ' ';
            if(ps>192)  a3 = p[i + 192]->objectives[0] > th ? '*' : ' ';
            std::string s = string_format("id:%5d ev: %3d % 9f%c% 9f ",
                                          p[i]->myid, p[i]->evaluations, p[i]->fitness, a0, sqrt(p[i]->variance));
            if(ps>64)   s += string_format("id:%5d ev: %3d % 9f%c% 9f ",
                               p[i + 64]->myid, p[i + 64]->evaluations, p[i + 64]->fitness, a1, sqrt(p[i + 64]->variance));
            if(ps>128)  s += string_format("id:%5d ev: %3d % 9f%c% 9f ",
                               p[i + 128]->myid, p[i + 128]->evaluations, p[i + 128]->fitness, a2, sqrt(p[i + 128]->variance));
            if(ps>192)  s += string_format("id:%5d ev: %3d % 9f%c% 9f ",
                               p[i + 192]->myid, p[i + 192]->evaluations, p[i + 192]->fitness, a3, sqrt(p[i + 192]->variance));
            
            if (p.size() == 512)
            {
                char a4 = p[i + 256]->objectives[0] > th ? '*' : ' ';
                char a5 = p[i + 320]->objectives[0] > th ? '*' : ' ';
                char a6 = p[i + 384]->objectives[0] > th ? '*' : ' ';
                char a7 = p[i + 448]->objectives[0] > th ? '*' : ' ';
                s += string_format("id:%5d ev: %3d % 9f%c% 9f ",
                                   p[i + 256]->myid, p[i + 256]->evaluations, p[i + 256]->fitness, a4, sqrt(p[i + 256]->variance));
                s += string_format("id:%5d ev: %3d % 9f%c% 9f ",
                                   p[i + 320]->myid, p[i + 320]->evaluations, p[i + 320]->fitness, a5, sqrt(p[i + 320]->variance));
                s += string_format("id:%5d ev: %3d % 9f%c% 9f ",
                                   p[i + 384]->myid, p[i + 320]->evaluations, p[i + 384]->fitness, a6, sqrt(p[i + 384]->variance));
                s += string_format("id:%5d ev: %3d % 9f%c% 9f ",
                                   p[i + 448]->myid, p[i + 448]->evaluations, p[i + 448]->fitness, a7, sqrt(p[i + 448]->variance));
                

            }
            printf("%s\n", s.c_str());
        }
    }
}

void GP::print_dom()
{
    std::string s;
    for (int i = 0; i < p.size(); i++)
    {
        if (i % 64 == 0)
            s += "\n";
        s += dominated[i] != -1 ? "_" : "0";
    }
    printf("%s\n", s.c_str());
}

void GP::print_kmeans()
{
    const int xsize = 10, ysize = 10;
    const float xscale = 1.4, yscale = 1.0;
    char plt[ysize][xsize + 1];
    for(int y = 0; y < ysize; y++)
    {
        for (int x = 0; x < xsize; x++)
            plt[y][x] = '.';
    }
    for (int i = 0; i < n_means; i++)
    {
        if (kmeans[i].count)
        {
            int xc = kmeans[i].mean.x / xscale * xsize;
            int yc = kmeans[i].mean.y / yscale * ysize;
            if ((xc >= 0) && (xc < xsize) && (yc >= 0) and (yc < ysize))
                plt[ysize - yc - 1][xc] = 'o';
        }
    }
    for (int y = 0; y < ysize; y++)
        plt[y][xsize] = 0;
    
    for(int i = 0; i < n_means; i++)
    {
        int idx = kc[i].first;
        printf("kmeans %2d (% 9f % 9f) %3d   %s\n", idx, kmeans[idx].mean.x, kmeans[idx].mean.y, kmeans[idx].count, plt[i]);
    }

}

void GP::setup_moevo(gpusim::Sim *_sim)
{
    sim = _sim;
    //Clock clk;
    
    // Assumption - the population is a multiple 256 and the parallelism is 256
    // Population has been filled
    
    // Dominated has one entry per individual and it points to another individual that
    // dominates it, or -1 if is not dominated. All individuals with -1 are in the pareto front
    
    const int required_size_pf   = p.size() * prop_pf;
    kc.resize(n_means);
    kmeans.resize(n_means);
    dominated.resize(p.size());
                     
    sim->evaluate_bt(bt(), params.repeats, sim->params.steps);
    update_fitness(sim);
    for (generation = 0; generation < params.generations; generation++)
    {
        
        // Find non-dominated set (pareto front).
        std::fill(dominated.begin(), dominated.end(), -1);
        for (int i = 0; i < p.size(); i++)
        {
            for (int j = i + 1; j < p.size(); j++)
            {
                if ((p[i]->objectives[0] > p[j]->objectives[0]) && (p[i]->objectives[1] > p[j]->objectives[1]))
                {
                    dominated[j] = i;
                }
                if ((p[j]->objectives[0] > p[i]->objectives[0]) && (p[j]->objectives[1] > p[i]->objectives[1]))
                {
                    dominated[i] = j;
                }
            }
        }
        
        print_dom();
        // Find the PF and the dominated individuals as vectors of indices.
        std::vector<int> dom;
        pf.clear();
        for (int i = 0; i < p.size(); i++)
            if (dominated[i] == -1)
                pf.push_back(i);
        // Shuffle the PF so that there is no bias towards culling early index members of the PF
        std::shuffle(pf.begin(), pf.end(), generator2);
        

        
        // We want to maintain a certain proportion of the population to hold the pareto front.
        // When the number of non-dominated individuals exceeds this proportion, we cull the elite
        // such that the remaining ones are spread over the PF.
        //
        // Do this using k-means
        int km_iterations = calc_kmeans();
        
        if(params.verbose)print_kmeans();
        if(params.verbose)printf("Kmeans took %d iterations\n", km_iterations);

        
        
        // Now we cull the elite. We want to do this so that the individuals remain spead over the PF.
        // If they are evenly spread, we would expect to see the sizes of the clusters to be similar
        // and each holding pop * prop_pf / n_means. Successively remove individuals from the fullest clusters
        // until there are only pop * prop_pf left in the PF
        int reduce_by = pf.size() - required_size_pf;
        for (int i = 0; i < reduce_by; i++)
        {
            // Reduce from largest bin until equal to next largest, then resort
            for (bool culled = false; !culled;)
            {
                if (kc[0].second >= kc[1].second)
                {
                    kc[0].second--;
                    kmeans[kc[0].first].count--;
                    for (int j = 0; j < pf.size(); j++)
                        if (kcluster[pf[j]] == kc[0].first)
                        {
                            dominated[pf[j]] = 0;
                            pf.erase(pf.begin() + j);
                            culled = true;
                            break;
                        }
                }
                else
                {
                    sort_kc();
                }
            }
        }
        
        km_iterations = calc_kmeans();
        print_kmeans();
        printf("Kmeans took %d iterations\n", km_iterations);

        
        // Replace the dominated individuals using parents selected from the pareto front
        for (int i = 0; i < p.size(); i++)
            if (dominated[i] != -1)
                dom.push_back(i);

        // find the two least populated bins
        int bin1 = -1, bin2 = -1;
#if 0
        for (int i = kc.size() - 1; i >= 0; i--)
            if (bin1 < 0 && kc[i].second)
            {
                bin1 = kc[i].first;
            }
            else if (bin2 < 0 && kc[i].second)
            {
                bin2 = kc[i].first;
                break;
            }
#else
        // Find the two bins with highest fitness
        float fittest = -999;
        for (int i = 0; i < n_means; i++)
        {
            if ((kmeans[i].mean.x > fittest) && kmeans[i].count)
            {
                bin1 = i;
                fittest = kmeans[i].mean.x;
            }
        }
        fittest = -999;
        for (int i = 0; i < n_means; i++)
        {
            if ((kmeans[i].mean.x > fittest) && kmeans[i].count && (i != bin1))
            {
                bin2 = i;
                fittest = kmeans[i].mean.x;
            }
        }
#endif
        if (bin2 == -1)
            bin2 = bin1;

        printf("bin1:%d bin2:%d\n", bin1, bin2);
        
        std::vector<int> b1idx, b2idx;
        for (int i = 0; i < pf.size(); i++)
        {
            if (kcluster[pf[i]] == bin1)
                b1idx.push_back(pf[i]);
            if (kcluster[pf[i]] == bin2)
                b2idx.push_back(pf[i]);
        }
        for (int i = 0; i < dom.size(); i++)
        {
            if (randfloat(0, 1) < 0.5)
            {
                // Replace half of the dominated individuals with new individuals. This allows possible noise effects
                // to discover badly classified but actually good individuals in the dominated set.
                //
                // Possibly generate a new node by crossover, then mutate.
                // Preferentially choose from least full clusters
                if (randfloat(0, 1) < params.prob_xover)
                {
                    int p1 = b1idx[randint(0, b1idx.size() - 1)];
                    int p2 = b2idx[randint(0, b2idx.size() - 1)];
                    Individual *n = crossover(p[p1], p[p2]);
                    n->objectives.push_back(NAN);
                    n->objectives.push_back(NAN);
                    delete p[dom[i]];
                    p[dom[i]] = n;
                }
                else
                {
                    delete p[dom[i]];
                    p[dom[i]] = new Individual(5, true, compnodes, actionnodes);
                }
                mutateparams(p[dom[i]]);
                mutatepoint(p[dom[i]]);
                mutatesubtree(p[dom[i]]);
            }
        }
        

        printf("Pre evaluation with new population\n");
        if (params.verbose)print_pop_mo();

        
        printf("Gen: %3d PF: %3d\n", generation, (int)pf.size());
        //for ( int i = 0; i < pf.size(); i++)
        //    printf("pfid:%3d eval:%3d km:%2d % 9f % 9f\n", i, p[pf[i]]->evaluations, kcluster[i], p[pf[i]]->objectives[0], p[pf[i]]->objectives[1]);
        
        sim->evaluate_bt(bt(), params.repeats, sim->params.steps);
        update_fitness(sim);
        
        //print_pop_mo();
        
    }
    
    sort_population();
    output_population(true);
    int rep = 64;
    sim->evaluate_bt(bt(), rep, sim->params.steps);
    update_fitness(sim);
    sort_population();
    output_population(true);

}


int GP::so_select(int range)
{
    int idx = -1;

    // Tournament selection - pick tsize individuals with replacement
    // from the population and return an index to the fittest. Fitness is
    // assumed to be positive = better
    float f = -9999;
    int tsize = params.n_tsize;
    
    
    if (params.model == 2)
    {
        // Try tournament with requirement for multiple evaluations, if this fails,
        // run again without the requirememt
        for (int i = 0; i < tsize; i++)
        {
            int new_idx = randint(0, range - 1);
            float new_f = p[new_idx]->fitness;
            if ((new_f > f) && (p[new_idx]->evaluations > 1))
            {
                f   = new_f;
                idx = new_idx;
            }
        }
        if (idx != -1)
            return idx;
        for (int i = 0; i < tsize; i++)
        {
            int new_idx = randint(0, range - 1);
            float new_f = p[new_idx]->fitness;
            if (new_f > f)
            {
                f   = new_f;
                idx = new_idx;
            }
        }
    }
    else if (params.model == 4)
    {
        // Perform tournament with 95% likelihood fitness if already multiple evaluations, otherwise
        // shrink the fitness for the comparison
        for (int i = 0; i < tsize; i++)
        {
            int new_idx = randint(0, range - 1);
            float new_f;
            if (p[new_idx]->evaluations >= 8)
                new_f = p[new_idx]->fitness95;
            else
                new_f = p[new_idx]->fitness * ((float)p[new_idx]->evaluations / 8);
            
            //float new_f = p[new_idx]->fitness - p[new_idx]->stddev;
            if (new_f > f)
            {
                f   = new_f;
                idx = new_idx;
            }
        }
    }
    else
    {
        // Perform tournament with 95% likelihood fitness if already multiple evaluations, otherwise
        // shrink the fitness for the comparison
        for (int i = 0; i < tsize; i++)
        {
            int new_idx = randint(0, range - 1);
            float new_f;
            if (p[new_idx]->evaluations > 1)
                new_f = p[new_idx]->fitness95;
            else
                new_f = p[new_idx]->fitness * 0.5;
            
            //float new_f = p[new_idx]->fitness - p[new_idx]->stddev;
            if (new_f > f)
            {
                f   = new_f;
                idx = new_idx;
            }
        }
    }
    return idx;

}

void GP::setup_soevo(gpusim::Sim *_sim)
{
    sim = _sim;
    //Clock clk;
    
    // The constructor has filled the initial population using ramped half-and-half, with
    // seed trees if supplied
    clk.start();
    
    int n_elites = p.size() * params.prop_elites;
    for (generation = 0; generation < params.generations; generation++)
    {
        sim->evaluate_bt(bt(), params.repeats, sim->params.steps);
        update_fitness(sim);
        sort_population(params.model == 3);
        lap_time = clk.lap();
        report_population(true);
        if (generation != params.generations - 1)
        {
            
            for (int i = n_elites; i < p.size(); i++)
            {
                if (randfloat(0, 1) < 0.5)
                {
                    // Replace half of the non-elite individuals with new individuals. This allows possible noise effects
                    // to discover badly classified but actually good individuals in the dominated set.
                    //
                    // Possibly generate a new node by crossover, then mutate.
                    // Preferentially choose from least full clusters
                    if (randfloat(0, 1) < params.prob_xover)
                    {
                        int p1 = so_select(n_elites);
                        int p2 = so_select(n_elites);
                        Individual *n = crossover(p[p1], p[p2]);
                        n->objectives.push_back(NAN);
                        n->objectives.push_back(NAN);
                        delete p[i];
                        p[i] = n;
                    }
                    else
                    {
                        delete p[i];
                        p[i] = new Individual(4, true, compnodes, actionnodes);
                    }
                    mutateparams(p[i]);
                    mutatepoint(p[i]);
                    mutatesubtree(p[i]);
                }
            }
        }
        print_pop_so();
    }
    sort_population(params.model == 3);
    output_population();
    
    // Make a final evaluation with many more repeats
    int rep = 64;
    sim->evaluate_bt(bt(), rep, sim->params.steps);
    update_fitness(sim);
    sort_population(params.model == 3);
    //for (int i = 0; i < p.size(); i++)
    //    print_bar_chart(std::vector<float>(sim->all_fitness.begin() + i * rep, sim->all_fitness.begin() + i * rep + rep - 1));
    output_population(true);
}



bool GP::connect_to_geneserver()
{
    // The genepool server is always the controlling server, so we also connect to the
    // telemetry channel in order to get the command state
    bool connected_to_geneserver = false;
    if (params.genepool.size())
    {
        context = zmq::context_t(1);
        genesrv  = new zmq::socket_t(context, ZMQ_REQ);
        printf("Attempting connection to geneserver..\n");
        genesrv->connect(string_format("tcp://%s:%s", params.genepool.c_str(), ZMQPORT_GENESERVER).c_str());
        // send test query
        std::string msg = string_format("testing..:%s", params.hostname.c_str());
        zmq::message_t request(msg.size());
        memcpy(request.data(), msg.data(), msg.size());
        genesrv->send(request);
        usleep(1000000);
        zmq::message_t reply;
        if (!genesrv->recv(&reply, ZMQ_NOBLOCK))
        {
            printf("No connection to genepool! Running solo\n");
        }
        else
        {
            printf("Got connection! %s\n", std::string((char *)reply.data(), reply.size()).c_str());
            connected_to_geneserver = true;
        }
        
        // Now connect to telemetry in PUB-SUB, use conflate so we only get the
        // latest data
        const int confl = true;
        sub_telem = new zmq::socket_t(context, ZMQ_SUB);
        sub_telem->setsockopt(ZMQ_CONFLATE, &confl, sizeof(int));
        sub_telem->connect(string_format("tcp://%s:%s", params.genepool.c_str(), ZMQPORT_POSE).c_str());
        // find out who we are to filter messages

        sub_telem->setsockopt(ZMQ_SUBSCRIBE, params.hostname.c_str(), params.hostname.size());
        
        // Finally, set up the PUB channel for sending log data about the evolving population
        //pub_log = new zmq::socket_t(context, ZMQ_PUB);
        //pub_log->connect(string_format("tcp://%s:%s", params.genepool.c_str(), ZMQPORT_GALOG).c_str());
        
        // Finally, connect to the local fitness channel to receive information about the running controller
        printf("Connecting to local fitness reporting..\n");
        sub_localfit = new zmq::socket_t(context, ZMQ_SUB);
        sub_localfit->setsockopt(ZMQ_CONFLATE, &confl, sizeof(int));
        sub_localfit->connect(string_format("tcp://127.0.0.1:%s", ZMQPORT_LOCALFIT).c_str());
        // subscribe to all
        sub_localfit->setsockopt(ZMQ_SUBSCRIBE, "", 0);

        
        
    }
    return connected_to_geneserver;
}


std::set<int> GP::get_possibles(int num)
{
    std::set<int>       possibles;
    int min_evals = 8;
    while ((possibles.size() < num) && (min_evals > 0))
    {
        for (int i = 0; i < p.size() * params.prop_elites * 0.5; i++)
        {
            if (p[i]->evaluations >= min_evals)
            {
                possibles.insert(i);
                if (possibles.size() == num)
                    break;
            }
        }
        min_evals--;
    }
    return possibles;
}

void GP::send_pop_to_galogger()
{
    if (!params.controlled)
        return;
    if (!pub_log)
        return;
    for (int i = 0; i < p.size(); i++)
    {
        zmqlog(pub_log, string_format("%s:%d:%s", params.hostname.c_str(), i, create_json(p[i], true).c_str()));
    }
    std::set<int> possibles = get_possibles(1);
    zmqlog(pub_log, string_format("%s:-1:%s", params.hostname.c_str(), create_json(p[*(possibles.begin())], true).c_str()));
}


void GP::send_fittest_to_geneserver(int num)
{
    std::set<int>       possibles = get_possibles(num);
    
    std::string msg = string_format("%s:[", params.hostname.c_str());
    for (auto i : possibles)
    {
        std::string is = create_json(p[i]);
        msg += string_format("%s,", is.c_str());
    }
    msg = msg.substr(0, msg.size() - 1) + "]";
    if (params.verbose)printf("Message to geneserver:\n%s\n", msg.c_str());
    zmq::message_t request(msg.size());
    memcpy(request.data(), msg.data(), msg.size());
    genesrv->send(request);

}

void GP::get_response_from_geneserver()
{
    zmq::message_t reply;
    genesrv->recv(&reply);
    std::string rs((char *)reply.data(), reply.size());
    if (params.verbose)printf("Server sent %s\n", rs.c_str());
    // Implant the returned genes into the population
    // Replace the current weakest individuals, also updating fitness with value from
    // server, then re-sort
    insert_genes(rs);
    sort_population();
}

void GP::get_local_fitness()
{
    if (!sub_localfit)
        return;
    // Get the latest messages on the local fitness channel, which are used to update the
    // true measure of the individuals in the controller archive
    zmqlog(pub_log, string_format("%s:-9:Try and read from lfit channel..\n", params.hostname.c_str()));
    printf("Checking local fitness..\n");
    zmq::message_t lfit_msg;
    std::string message = "";
    sub_localfit->recv(&lfit_msg, ZMQ_NOBLOCK);
    {
        if (lfit_msg.size() == 0)
        {
            zmqlog(pub_log, string_format("%s:-11:No message from lfit\n", params.hostname.c_str()));
            return;
        }
        char buf[128];
        int     id;
        float   lfit;
        //float   rprobe;
        int     ticks;
        int64_t tsms = clk.current() / 1e6;
        message = std::string(static_cast<char*>(lfit_msg.data()), lfit_msg.size());
        sscanf(message.c_str(), "%d %f %f %d", &id, &lfit, &rprobe, &ticks);
        printf("Got message:%s\n", message.c_str());
        zmqlog(pub_log, string_format("%s:-3:{\"ts\":%" PRId64 ",\"myid\":%d,\"lf\":%f,\"rp\":%f,\"ticks\":%d}", params.hostname.c_str(), tsms, id, lfit, rprobe, ticks));

        
        //zmqlog(pub_log, string_format("%s:-9:", params.hostname.c_str(), id, idx));
        
//        for (int idx = 0; idx < archive.size(); idx++)
//        {
//            auto i = archive[idx];
//            if (i->myid == id)
//            {
//                i->real_lfitness = lfit;
//                zmqlog(pub_log, string_format("%s:-9:found %d in archive %d", params.hostname.c_str(), id, idx));
//            }
//        }
    }
    
}

void GP::check_cmd_state()
{
    if (!params.controlled)
        return;
    while (true)
    {
        printf("Checking cmd state..\n");
        zmq::message_t hub_msg;
        std::string message = "";
        sub_telem->recv(&hub_msg, ZMQ_NOBLOCK);
        if (hub_msg.size())
        {
            char buf[128]; uint64_t ts; float x, y, th; int32_t cmd;
            message = std::string(static_cast<char*>(hub_msg.data()), hub_msg.size());
            sscanf(message.c_str(), "%s %" PRIu64 " %f %f %f %d", buf, &ts, &x, &y, &th, &cmd);
            if (cmd == 400)
            {
                // Command state 400 is run
                printf("Cmd state is now run\n");
                break;
            }
        }
        usleep(100000);
    }
}


void GP::adapt_to_environment()
{

    // rprobe contains the latest reality probe figure from the robt running in the real world.
    // If it is above the threshold, we assume that the frisbee is now heavy, and modify the
    // simulated environment.
    //
    //printf("%f %f\n", rprobe, params.adapt_rp_thresh);
    bool last_alternate = sim->param_alternate;
    if (params.listen_rp && (rprobe > params.adapt_rp_thresh))
        sim->param_alternate = true;
    else
        sim->param_alternate = false;
    if (last_alternate != sim->param_alternate)
    {
        // reset eval counts
        for (int i = 0; i < p.size(); i++)
        {
            p[i]->evaluations = 1;
        }
    }
    //zmqlog(pub_log, string_format("%s:-9:Current param alternate %f %d", params.hostname.c_str(), rprobe, sim->param_alternate));
    int64_t tsms = clk.current() / 1e6;
    zmqlog(pub_log, string_format("%s:-10:{\"ts\":%" PRId64 ",\"myid\":%d,\"f95\":%f,\"rp\":%f,\"af\":%f,\"ev\":%d,\"g\":%d}", params.hostname.c_str(), tsms, p[0]->myid, p[0]->fitness95, rprobe, p[0]->fitness, p[0]->evaluations, generation));



}

void GP::setup_soevo_island(gpusim::Sim *_sim)
{
    sim = _sim;
    //Clock clk;

    zmqlog(pub_log, "in setup_soevo_lsland");
    
    usegenepool = connect_to_geneserver();
    
    // The constructor has filled the initial population using ramped half-and-half, with
    // seed trees if supplied
    clk.start();
    
    int num_elites = p.size() * params.prop_elites;
    for (generation = 0; generation < params.generations; generation++)
    {
        check_cmd_state();
        sim->evaluate_bt(bt(), params.repeats, sim->params.steps);
        update_fitness(sim);
        sort_population(params.model == 4);
        //sort_population();
        lap_time = clk.lap();
        send_pop_to_galogger();
        get_local_fitness();
        adapt_to_environment();
        
        report_population(true);
        if (generation != params.generations - 1)
        {
            if (usegenepool && ((generation % params.epochs) == 0))
            {
                // Send the fittest individuals with multiple evaluations to the gene server
                send_fittest_to_geneserver(params.gsfittest);
                // Get response and replace weakest individuals
                get_response_from_geneserver();
            }
            
            for (int i = num_elites; i < p.size(); i++)
            {
                if (randfloat(0, 1) < params.prop_replace)
                {
                    // Replace most of the non-elite individuals with new individuals. This allows possible noise effects
                    // to discover badly classified but actually good individuals in the dominated set.
                    //
                    // Possibly generate a new node by crossover, then mutate.
                    if (randfloat(0, 1) < params.prob_xover)
                    {
                        int p1 = so_select(num_elites);
                        int p2 = so_select(num_elites);
                        Individual *n = crossover(p[p1], p[p2]);
                        n->objectives.push_back(NAN);
                        n->objectives.push_back(NAN);
                        delete p[i];
                        p[i] = n;
                    }
                    else
                    {
                        delete p[i];
                        p[i] = new Individual(4, true, compnodes, actionnodes);
                    }
                    mutateparams(p[i]);
                    mutatepoint(p[i]);
                    mutatesubtree(p[i]);
                }
            }
        }
        if (params.verbose) print_pop_so();
    }
    printf("Final population\n");
    sort_population();
    if (!params.controlled) output_population();
    if (params.verbose)     print_pop_so();

    printf("%s finished\n", __FUNCTION__);

    
//    if (0)
//    {
//        // Make a final evaluation with many more repeats
//        int rep = 64;
//        sim->evaluate_bt(bt(), rep, sim->params.steps);
//        update_fitness(sim->all_fitness, sim->all_parsimony, sim->all_evc);
//        sort_population();
//        //for (int i = 0; i < p.size(); i++)
//        //    print_bar_chart(std::vector<float>(sim->all_fitness.begin() + i * rep, sim->all_fitness.begin() + i * rep + rep - 1));
//        print_pop_so();
//        output_population(true);
//    }
}





