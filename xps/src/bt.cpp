
//#define DEBUG
//#define DEBUG2

#include "bt.h"


#include <iostream>
#include <stdlib.h>
#include <cstdarg>








float rand_realrange(float low, float high)
{
    float r = ((float)rand()/RAND_MAX) * (high - low) + low;
    return r;
}






// Return the size in bytes of the node type.
// Normally, this returns the sizeof the Node struct, which is 2 + the
// size of the union Ndata, but the structs composing Ndata are variable
// in size. Because of the limited space on the kilobot, we return the
// actual used space, allowing successive nodes to overlap the unused space
// at the end of previous nodes.
//
// SJ10/12/2016 Reusing this code and the same applies now we are executing
// on GPU. We want to run thousands in parallel, so space is again critical. We
// also do not want a standard memory allocator because:
//  a) We cannot use pointers across architectures
//  b) We want contiguous allocation
int clsize(Nodetype type)
{
    // Space is very tight on the kilobot
    switch (type)
    {
        case SEQ2:
        case SEQM2:
        case SEL2:
        case SELM2:     return sizeof(struct clSM2) + 4;  //
        case SEQ3:
        case SEQM3:
        case SEL3:
        case SELM3:     return sizeof(struct clSM3) + 4;  // 9
        case SEQ4:
        case SEQM4:
        case SEL4:
        case SELM4:     return sizeof(struct clSM4) + 4;  // 11
        case REPEATI:
        case REPEATR:   return sizeof(struct clREP) + 4;  // 6
        case SUCCESSD:
        case FAILURED:
        case INVERT:    return sizeof(struct clFIX) + 4;  // 4
        case SUCCESSL:
        case FAILUREL:  return 4;
        case MOVCS:
        case MOVCV:     return sizeof(struct clMVC) + 4;
        case MULAS:
        case MULAV:     return sizeof(struct clMAC) + 4;
        case ROTAV:     return sizeof(struct clRAC) + 4;
        case IFPROB:    return sizeof(struct clIFP) + 4;
        case IFQUAD:    return sizeof(struct clIFQ) + 4;    // 6
        case IFSECT:    return sizeof(struct clIFS) + 4;    // 6
    }
    return sizeof(struct clNode);
}

typedef struct Node* Nodep;



std::string BT::get_btstr()
{
    std::string ts;
    get_tree_str(ts, root, 0);
    return ts;
}


void BT::print_tree()
{
    print_tree(root, 0);
    //std::string ts;
    //get_tree_str(ts, ts, root, 0);
    
    printf("nodes: %5d inner: %5d leaves: %5d ratio: %10.2f size: %5d\n",
           num_nodes, num_inner, num_leaves, (float)num_leaves/(float)num_inner, (int)cltree.size());
}
void BT::print_tree(cl_ushort root, int level, uchar *status)
{
    clNode *bt = reinterpret_cast<clNode*>(&cltree[root]);
    uchar s = 0;
    uchar r = 0;
    if (status)
    {
        s = status[bt->ptr];
        r = status[(bt->ptr) + 1];
    }
    std::string pad (level * 2, ' ');
    const std::string red("\x1B[31m");
    const std::string green("\x1B[32m");
    const std::string blue("\x1B[34m");
    const std::string magenta("\x1B[35m");
    const std::string clear("\x1B[0m");
    // 0 failure
    // 1 success
    // 2 running
    // 3 running_stale
    // 4 invalid
    const std::string slu[5] = {red, green, blue, magenta, clear};
    printf("%d %d %3d %s%s", s, r, level, pad.c_str(), slu[s].c_str());
    switch(bt->type)
    {
        case SEQ2:
        {
            printf("seq2%s\n", clear.c_str());
            print_tree(bt->data.sm2.op[0], level+1, status);
            print_tree(bt->data.sm2.op[1], level+1, status);
            break;
        }
        case SEQ3:
        {
            printf("seq3%s\n", clear.c_str());
            print_tree(bt->data.sm3.op[0], level+1, status);
            print_tree(bt->data.sm3.op[1], level+1, status);
            print_tree(bt->data.sm3.op[2], level+1, status);
            break;
        }
        case SEQ4:
        {
            printf("seq4%s\n", clear.c_str());
            print_tree(bt->data.sm4.op[0], level+1, status);
            print_tree(bt->data.sm4.op[1], level+1, status);
            print_tree(bt->data.sm4.op[2], level+1, status);
            print_tree(bt->data.sm4.op[3], level+1, status);
            break;
        }
        case SEL2:
        {
            printf("sel2%s\n", clear.c_str());
            print_tree(bt->data.sm2.op[0], level+1, status);
            print_tree(bt->data.sm2.op[1], level+1, status);
            break;
        }
        case SEL3:
        {
            printf("sel3%s\n", clear.c_str());
            print_tree(bt->data.sm3.op[0], level+1, status);
            print_tree(bt->data.sm3.op[1], level+1, status);
            print_tree(bt->data.sm3.op[2], level+1, status);
            break;
        }
        case SEL4:
        {
            printf("sel4%s\n", clear.c_str());
            print_tree(bt->data.sm4.op[0], level+1, status);
            print_tree(bt->data.sm4.op[1], level+1, status);
            print_tree(bt->data.sm4.op[2], level+1, status);
            print_tree(bt->data.sm4.op[3], level+1, status);
            break;
        }
        case SEQM2:
        {
            printf("seqm2%s (%d)\n", clear.c_str(), r);
            print_tree(bt->data.sm2.op[0], level+1, status);
            print_tree(bt->data.sm2.op[1], level+1, status);
            break;
        }
        case SEQM3:
        {
            printf("seqm3%s (%d)\n", clear.c_str(), r);
            print_tree(bt->data.sm3.op[0], level+1, status);
            print_tree(bt->data.sm3.op[1], level+1, status);
            print_tree(bt->data.sm3.op[2], level+1, status);
            break;
        }
        case SEQM4:
        {
            printf("seqm4%s (%d)\n", clear.c_str(), r);
            print_tree(bt->data.sm4.op[0], level+1, status);
            print_tree(bt->data.sm4.op[1], level+1, status);
            print_tree(bt->data.sm4.op[2], level+1, status);
            print_tree(bt->data.sm4.op[3], level+1, status);
            break;
        }
        case SELM2:
        {
            printf("selm2%s (%d)\n", clear.c_str(), r);
            print_tree(bt->data.sm2.op[0], level+1, status);
            print_tree(bt->data.sm2.op[1], level+1, status);
            break;
        }
        case SELM3:
        {
            printf("selm3%s (%d)\n", clear.c_str(), r);
            print_tree(bt->data.sm3.op[0], level+1, status);
            print_tree(bt->data.sm3.op[1], level+1, status);
            print_tree(bt->data.sm3.op[2], level+1, status);
            break;
        }
        case SELM4:
        {
            printf("selm4%s (%d)\n", clear.c_str(), r);
            print_tree(bt->data.sm4.op[0], level+1, status);
            print_tree(bt->data.sm4.op[1], level+1, status);
            print_tree(bt->data.sm4.op[2], level+1, status);
            print_tree(bt->data.sm4.op[3], level+1, status);
            break;
        }
        case REPEATI:
        {
            printf("repeati%s %d(%d)\n", clear.c_str(),  bt->data.rep.repeat, r);
            print_tree(bt->data.rep.op, level+1, status);
            break;
        }
        case REPEATR:
        {
            printf("repeatr%s %d(%d)\n", clear.c_str(),  bt->data.rep.repeat, r);
            print_tree(bt->data.rep.op, level+1, status);
            break;
        }
        case SUCCESSD:
        {
            printf("successd%s\n", clear.c_str());
            print_tree(bt->data.fix.op, level+1, status);
            break;
        }
        case FAILURED:
        {
            printf("failured%s\n", clear.c_str());
            print_tree(bt->data.fix.op, level+1, status);
            break;
        }
        case INVERT:
        {
            printf("invert%s\n", clear.c_str());
            print_tree(bt->data.fix.op, level+1, status);
            break;
        }
        case MOVCS:
        {
            printf("movcs%s %d %d\n", clear.c_str(),  bt->data.mvc.dest, bt->data.mvc.i);
            break;
        }
        case MOVCV:
        {
            printf("movcv%s %d %d\n", clear.c_str(),  bt->data.mvc.dest, bt->data.mvc.i);
            break;
        }
        case MULAS:
        {
            printf("mulas%s %d %d %f %d\n", clear.c_str(),  bt->data.mac.dest, bt->data.mac.src1,
                   bt->data.mac.f, bt->data.mac.src2);
            break;
        }
        case MULAV:
        {
            printf("mulav%s %d %d %f %d\n", clear.c_str(),  bt->data.mac.dest, bt->data.mac.src1,
                   bt->data.mac.f, bt->data.mac.src2);
            break;
        }
        case ROTAV:
        {
            printf("rotav%s %d %d %d %d\n", clear.c_str(),  bt->data.rac.dest, bt->data.rac.src1,
                   bt->data.rac.i, bt->data.rac.src2);
            break;
        }
        case IFPROB:
        {
            printf("ifprob%s %d %f %f\n", clear.c_str(),  bt->data.ifp.src,
                   (float)bt->data.ifp.k / 8, (float)bt->data.ifp.l / 8);
            break;
        }
        case IFQUAD:
        {
            printf("ifquad%s %d %d\n", clear.c_str(),  bt->data.ifq.src, bt->data.ifq.i);
            break;
        }
        case IFSECT:
        {
            printf("ifsect%s %d %d %d\n", clear.c_str(),  bt->data.ifs.src, bt->data.ifs.i, bt->data.ifs.j);
            break;
        }
        case SUCCESSL:
        {
            printf("successl%s\n", clear.c_str());
            break;
        }
        case FAILUREL:
        {
            printf("failurel%s\n", clear.c_str());
            break;
        }
        default:
            break;
    }
}

void BT::get_tree_str(std::string &ts, cl_ushort root, int level)
{
    clNode *bt = reinterpret_cast<clNode*>(&cltree[root]);
    switch(bt->type)
    {
        case SEQ2:
        {
            ts += string_format("seq2 ");
            get_tree_str(ts, bt->data.sm2.op[0], level+1);
            get_tree_str(ts, bt->data.sm2.op[1], level+1);
            break;
        }
        case SEQ3:
        {
            ts += string_format("seq3 ");
            get_tree_str(ts, bt->data.sm3.op[0], level+1);
            get_tree_str(ts, bt->data.sm3.op[1], level+1);
            get_tree_str(ts, bt->data.sm3.op[2], level+1);
            break;
        }
        case SEQ4:
        {
            ts += string_format("seq4 ");
            get_tree_str(ts, bt->data.sm4.op[0], level+1);
            get_tree_str(ts, bt->data.sm4.op[1], level+1);
            get_tree_str(ts, bt->data.sm4.op[2], level+1);
            get_tree_str(ts, bt->data.sm4.op[3], level+1);
            break;
        }
        case SEL2:
        {
            ts += string_format("sel2 ");
            get_tree_str(ts, bt->data.sm2.op[0], level+1);
            get_tree_str(ts, bt->data.sm2.op[1], level+1);
            break;
        }
        case SEL3:
        {
            ts += string_format("sel3 ");
            get_tree_str(ts, bt->data.sm3.op[0], level+1);
            get_tree_str(ts, bt->data.sm3.op[1], level+1);
            get_tree_str(ts, bt->data.sm3.op[2], level+1);
            break;
        }
        case SEL4:
        {
            ts += string_format("sel4 ");
            get_tree_str(ts, bt->data.sm4.op[0], level+1);
            get_tree_str(ts, bt->data.sm4.op[1], level+1);
            get_tree_str(ts, bt->data.sm4.op[2], level+1);
            get_tree_str(ts, bt->data.sm4.op[3], level+1);
            break;
        }
        case SEQM2:
        {
            ts += string_format("seqm2 ");
            get_tree_str(ts, bt->data.sm2.op[0], level+1);
            get_tree_str(ts, bt->data.sm2.op[1], level+1);
            break;
        }
        case SEQM3:
        {
            ts += string_format("seqm3 ");
            get_tree_str(ts, bt->data.sm3.op[0], level+1);
            get_tree_str(ts, bt->data.sm3.op[1], level+1);
            get_tree_str(ts, bt->data.sm3.op[2], level+1);
            break;
        }
        case SEQM4:
        {
            ts += string_format("seqm4 ");
            get_tree_str(ts, bt->data.sm4.op[0], level+1);
            get_tree_str(ts, bt->data.sm4.op[1], level+1);
            get_tree_str(ts, bt->data.sm4.op[2], level+1);
            get_tree_str(ts, bt->data.sm4.op[3], level+1);
            break;
        }
        case SELM2:
        {
            ts += string_format("selm2 ");
            get_tree_str(ts, bt->data.sm2.op[0], level+1);
            get_tree_str(ts, bt->data.sm2.op[1], level+1);
            break;
        }
        case SELM3:
        {
            ts += string_format("selm3 ");
            get_tree_str(ts, bt->data.sm3.op[0], level+1);
            get_tree_str(ts, bt->data.sm3.op[1], level+1);
            get_tree_str(ts, bt->data.sm3.op[2], level+1);
            break;
        }
        case SELM4:
        {
            ts += string_format("selm4 ");
            get_tree_str(ts, bt->data.sm4.op[0], level+1);
            get_tree_str(ts, bt->data.sm4.op[1], level+1);
            get_tree_str(ts, bt->data.sm4.op[2], level+1);
            get_tree_str(ts, bt->data.sm4.op[3], level+1);
            break;
        }
        case REPEATI:
        {
            ts += string_format("repeati %d ", bt->data.rep.repeat);
            get_tree_str(ts, bt->data.rep.op, level+1);
            break;
        }
        case REPEATR:
        {
            ts += string_format("repeatr %d ", bt->data.rep.repeat);
            get_tree_str(ts, bt->data.rep.op, level+1);
            break;
        }
        case SUCCESSD:
        {
            ts += string_format("successd ");
            get_tree_str(ts, bt->data.fix.op, level+1);
            break;
        }
        case FAILURED:
        {
            ts += string_format("failured ");
            get_tree_str(ts, bt->data.fix.op, level+1);
            break;
        }
        case INVERT:
        {
            ts += string_format("invert ");
            get_tree_str(ts, bt->data.fix.op, level+1);
            break;
        }
        case MOVCS:
        {
            ts += string_format("movcs %d %d ", bt->data.mvc.dest, bt->data.mvc.i);
            break;
        }
        case MOVCV:
        {
            ts += string_format("movcv %d %d ", bt->data.mvc.dest, bt->data.mvc.i);
            break;
        }
        case MULAS:
        {
            ts += string_format("mulas %d %d %f %d ", bt->data.mac.dest, bt->data.mac.src1,
                   bt->data.mac.f, bt->data.mac.src2);
            break;
        }
        case MULAV:
        {
            ts += string_format("mulav %d %d %f %d ", bt->data.mac.dest, bt->data.mac.src1,
                   bt->data.mac.f, bt->data.mac.src2);
            break;
        }
        case ROTAV:
        {
            ts += string_format("rotav %d %d %d %d ", bt->data.rac.dest, bt->data.rac.src1,
                   bt->data.rac.i, bt->data.rac.src2);
            break;
        }
        case IFPROB:
        {
            ts += string_format("ifprob %d %f %f ", bt->data.ifp.src,
                   (float)bt->data.ifp.k / 8, (float)bt->data.ifp.l / 8);
            break;
        }
        case IFQUAD:
        {
            ts += string_format("ifquad %d %d ", bt->data.ifq.src, bt->data.ifq.i);
            break;
        }
        case IFSECT:
        {
            ts += string_format("ifsect %d %d %d ", bt->data.ifs.src, bt->data.ifs.i, bt->data.ifs.j);
            break;
        }
        case SUCCESSL:
        {
            ts += string_format("successl ");
            break;
        }
        case FAILUREL:
        {
            ts += string_format("failurel ");
            break;
        }
        default:
            break;
    }
}


cl_ushort get_align(Nodetype type)
{
    return 4;
}
void dmp(unsigned char *p)
{
    for(int i=0; i<16; i++)
        DBPRINT("%02x ",*p++);
    DBPRINT("\n");
}


cl_ushort BT::clnewnode(Nodetype type, ...)
{
    // Return an integer offset into cltree to a new node of type type, with space allocated
    // and fields filled in

    va_list args;
    va_start(args, type);
    
    // All fields get zeroed, this means status starts at INVALID.
    // Allocate space in the vector, then get a pointer to the new space.
    // We have to tweak the offset to account for the required alignment of the
    // type
    cl_ushort nn = cltree.size();
    cl_ushort align = get_align(type);
    cl_ushort adjust = (nn % align) == 0 ? 0 : align - (nn % align);
    cl_ushort size = clsize(type);
    nn += adjust;
    cltree.insert(cltree.end(), adjust + size, 0);
    DBPRINT("growing tree by %d to %x, entry %x\n", adjust + size, (int)cltree.size(), nn);
    // Cast into pointer so we can access elements of data structure
    clNode *n = reinterpret_cast<clNode*>(&cltree[nn]);
    n->type = type;
    n->status = BT_INVALID;
    n->ptr = num_nodes << 1;
    num_nodes++;
    
    switch(type)
    {
        case SEQM2:case SEQ2:
        case SELM2:case SEL2:
        {
            n->data.sm2.op[0] = va_arg(args, int);
            n->data.sm2.op[1] = va_arg(args, int);
            DBPRINT("%04x %8s %04x %04x\n", nn, words[type].c_str(), n->data.sm2.op[0], n->data.sm2.op[1]);
            dmp((unsigned char*)n);
            num_inner++;
            break;
        }
        case SEQM3:case SEQ3:
        case SELM3:case SEL3:
        {
            n->data.sm3.op[0] = va_arg(args, int);
            n->data.sm3.op[1] = va_arg(args, int);
            n->data.sm3.op[2] = va_arg(args, int);
            DBPRINT("%04x %8s %04x %04x %04x\n", nn, words[type].c_str(), n->data.sm3.op[0], n->data.sm3.op[1], n->data.sm3.op[2]);
            dmp((unsigned char*)n);
            num_inner++;
            break;
        }
        case SEQM4:case SEQ4:
        case SELM4:case SEL4:
        {
            n->data.sm4.op[0] = va_arg(args, int);
            n->data.sm4.op[1] = va_arg(args, int);
            n->data.sm4.op[2] = va_arg(args, int);
            n->data.sm4.op[3] = va_arg(args, int);
            DBPRINT("%04x %8s %04x %04x %04x %04x\n", nn, words[type].c_str(), n->data.sm4.op[0], n->data.sm4.op[1], n->data.sm4.op[2], n->data.sm4.op[3]);
            dmp((unsigned char*)n);
            num_inner++;
            break;
        }
        case REPEATI:
        case REPEATR:
        {
            n->data.rep.repeat  = va_arg(args, int);
            n->data.rep.op      = va_arg(args, int);
            DBPRINT("%04x %8s %04x\n", nn, words[type].c_str(), n->data.rep.repeat);
            dmp((unsigned char*)n);
            num_inner++;
            break;
        }
        case SUCCESSD:
        case FAILURED:
        case INVERT:
        {
            n->data.fix.op      = va_arg(args, int);
            DBPRINT("%04x %8s %04x\n", nn, words[type].c_str(), n->data.fix.op);
            dmp((unsigned char*)n);
            num_inner++;
            break;
        }
        case MOVCS:
        case MOVCV:
        {
            n->data.mvc.dest    = va_arg(args, int);
            n->data.mvc.i       = va_arg(args, int);
            DBPRINT("%04x %8s %2d %04x\n", nn, words[type].c_str(), n->data.mvc.dest, n->data.mvc.i);
            dmp((unsigned char*)n);
            num_leaves++;
            break;
        }
        case MULAS:
        case MULAV:
        {
            n->data.mac.dest    = va_arg(args, int);
            n->data.mac.src1    = va_arg(args, int);
            n->data.mac.f       = va_arg(args, double);
            n->data.mac.src2    = va_arg(args, int);
            DBPRINT("%04x %8s %2d %02x %f %02x\n", nn, words[type].c_str(), n->data.mac.dest,
                    n->data.mac.src1, n->data.mac.f, n->data.mac.src2);
            dmp((unsigned char*)n);
            num_leaves++;
            break;
        }
        case ROTAV:
        {
            n->data.rac.dest    = va_arg(args, int);
            n->data.rac.src1    = va_arg(args, int);
            n->data.rac.i       = va_arg(args, int);
            n->data.rac.src2    = va_arg(args, int);
            DBPRINT("%04x %8s %2d %02x %d %02x\n", nn, words[type].c_str(), n->data.rac.dest,
                    n->data.rac.src1, n->data.rac.i, n->data.rac.src2);
            dmp((unsigned char*)n);
            num_leaves++;
            break;
        }
        case IFPROB:
        {
            n->data.ifp.src = va_arg(args, int);
            float a = va_arg(args, double);
            //printf("%f\n", a);
            n->data.ifp.k = (int8_t)(a * 8);
            a = va_arg(args, double);
            //printf("%f\n", a);

            n->data.ifp.l = (int8_t)(a * 8);
            DBPRINT("%04x %8s %2d %f %f\n", nn, words[type].c_str(), n->data.ifp.src,
                    (float)n->data.ifp.k / 8, (float)n->data.ifp.l / 8);
            dmp((unsigned char*)n);
            num_inner++;
            break;
        }
        case IFQUAD:
        {
            n->data.ifq.src = va_arg(args, int);
            n->data.ifq.i   = va_arg(args, int);
            DBPRINT("%04x %8s %2d %2d\n", nn, words[type].c_str(), n->data.ifq.src, n->data.ifq.i);
            dmp((unsigned char*)n);
            num_inner++;
            break;
        }
        case IFSECT:
        {
            n->data.ifs.src = va_arg(args, int);
            n->data.ifs.i   = va_arg(args, int);
            n->data.ifs.j   = va_arg(args, int);
            DBPRINT("%04x %8s %2d %2d %2d\n", nn, words[type].c_str(), n->data.ifs.src, n->data.ifs.i, n->data.ifs.j);
            dmp((unsigned char*)n);
            num_inner++;
            break;
        }
        case SUCCESSL:
        case FAILUREL:
        {
            DBPRINT("%04x %8s\n", nn, words[type].c_str());
            dmp((unsigned char*)n);
            num_leaves++;
            break;
        }

            
    }
    va_end(args);
    return nn;
}


std::string BT::check(std::string msg)
{
    // Return the current token, or if we are at the end of the token stream,
    // report and error out. This should not happen with a well formed BT.
    //
    // a is the regex_token_iterator, each dereference returns the next token
    std::regex_token_iterator<std::string::iterator>    rend;
    bool empty = true;
    std::string s;
    while (empty)
    {
        if (tokens == rend)
        {
            printf("Unexpected end of BTstring!! %s\n", msg.c_str());
            exit(1);
        }
        s = *tokens;
        if (s.size())
            empty = false;
        else
            tokens++;
    }
    //std::string s = *tokens;
    DBPRINT("<%s>", s.c_str());
    
    return s;
}




cl_ushort BT::clpt(int d)
{
    // Recursive descent parser. Call to check() returns current token or
    // errors out if no token. tokens++ moves on to next token.
    //
    // This gives us a stream of tokens which are either words or
    // signed decimal numbers (no exponents).
    auto s = check("Unexpected end of tokens parsing node\n");
    tokens++;
    if (s == words[SEQ2])
    {
        DBPRINT("depth:%2d parsing: seq2\n", d);
        cl_ushort op1 = clpt(d+1);
        cl_ushort op2 = clpt(d+1);
        return clnewnode(SEQ2, op1, op2);
    }
    else if (s == words[SEQ3])
    {
        DBPRINT("depth:%2d parsing: seq3\n",d);
        cl_ushort op1 = clpt(d+1);
        cl_ushort op2 = clpt(d+1);
        cl_ushort op3 = clpt(d+1);
        return clnewnode(SEQ3, op1, op2, op3);
    }
    else if (s == words[SEQ4])
    {
        DBPRINT("depth:%2d parsing: seq4\n",d);
        cl_ushort op1 = clpt(d+1);
        cl_ushort op2 = clpt(d+1);
        cl_ushort op3 = clpt(d+1);
        cl_ushort op4 = clpt(d+1);
        return clnewnode(SEQ4, op1, op2, op3, op4);
    }
    else if (s == words[SEL2])
    {
        DBPRINT("depth:%2d parsing: sel2\n",d);
        cl_ushort op1 = clpt(d+1);
        cl_ushort op2 = clpt(d+1);
        return clnewnode(SEL2, op1, op2);
    }
    else if (s == words[SEL3])
    {
        DBPRINT("depth:%2d parsing: sel3\n",d);
        cl_ushort op1 = clpt(d+1);
        cl_ushort op2 = clpt(d+1);
        cl_ushort op3 = clpt(d+1);
        return clnewnode(SEL3, op1, op2, op3);
    }
    else if (s == words[SEL4])
    {
        DBPRINT("depth:%2d parsing: sel4\n",d);
        cl_ushort op1 = clpt(d+1);
        cl_ushort op2 = clpt(d+1);
        cl_ushort op3 = clpt(d+1);
        cl_ushort op4 = clpt(d+1);
        return clnewnode(SEL4, op1, op2, op3, op4);
    }
    if (s == words[SEQM2])
    {
        DBPRINT("depth:%2d parsing: seqm2\n",d);
        cl_ushort op1 = clpt(d+1);
        cl_ushort op2 = clpt(d+1);
        return clnewnode(SEQM2, op1, op2);
    }
    else if (s == words[SEQM3])
    {
        DBPRINT("depth:%2d parsing: seqm3\n",d);
        cl_ushort op1 = clpt(d+1);
        cl_ushort op2 = clpt(d+1);
        cl_ushort op3 = clpt(d+1);
        return clnewnode(SEQM3, op1, op2, op3);
    }
    else if (s == words[SEQM4])
    {
        DBPRINT("depth:%2d parsing: seqm4\n",d);
        cl_ushort op1 = clpt(d+1);
        cl_ushort op2 = clpt(d+1);
        cl_ushort op3 = clpt(d+1);
        cl_ushort op4 = clpt(d+1);
        return clnewnode(SEQM4, op1, op2, op3, op4);
    }
    else if (s == words[SELM2])
    {
        DBPRINT("depth:%2d parsing: selm2\n",d);
        cl_ushort op1 = clpt(d+1);
        cl_ushort op2 = clpt(d+1);
        return clnewnode(SELM2, op1, op2);
    }
    else if (s == words[SELM3])
    {
        DBPRINT("depth:%2d parsing: selm3\n",d);
        cl_ushort op1 = clpt(d+1);
        cl_ushort op2 = clpt(d+1);
        cl_ushort op3 = clpt(d+1);
        return clnewnode(SELM3, op1, op2, op3);
    }
    else if (s == words[SELM4])
    {
        DBPRINT("depth:%2d parsing: selm4\n",d);
        cl_ushort op1 = clpt(d+1);
        cl_ushort op2 = clpt(d+1);
        cl_ushort op3 = clpt(d+1);
        cl_ushort op4 = clpt(d+1);
        return clnewnode(SELM4, op1, op2, op3, op4);
    }
    else if (s == words[REPEATI])
    {
        DBPRINT("depth:%2d parsing: repeati\n",d);
        s = check("Unexpected end of tokens parsing repeati\n");
        tokens++;
        int reps = atol(s.c_str());
        return clnewnode(REPEATI, reps, clpt(d+1));
    }
    else if (s == words[REPEATR])
    {
        DBPRINT("depth:%2d parsing: repeatr\n",d);
        s = check("Unexpected end of tokens parsing repeatr\n");
        tokens++;
        int reps = atol(s.c_str());
        return clnewnode(REPEATR, reps, clpt(d+1));
    }
    else if (s == words[SUCCESSD])
    {
        DBPRINT("depth:%2d parsing: successd\n",d);
        return clnewnode(SUCCESSD, clpt(d+1));
    }
    else if (s == words[FAILURED])
    {
        DBPRINT("depth:%2d parsing: failured\n",d);
        return clnewnode(FAILURED, clpt(d+1));
    }
    else if (s == words[INVERT])
    {
        DBPRINT("depth:%2d parsing: invert\n",d);
        return clnewnode(INVERT, clpt(d+1));
    }
    else if (s == words[MOVCS])
    {
        s = check("Unexpected end of tokens parsing movcs dest\n");
        tokens++;
        int dest = atol(s.c_str());
        s = check("Unexpected end of tokens parsing movcs i\n");
        tokens++;
        int i = atol(s.c_str());
        return clnewnode(MOVCS, dest, i);
    }
    else if (s == words[MOVCV])
    {
        s = check("Unexpected end of tokens parsing movcv dest\n");
        tokens++;
        int dest = atol(s.c_str());
        s = check("Unexpected end of tokens parsing movcv i\n");
        tokens++;
        int i = atol(s.c_str());
        return clnewnode(MOVCV, dest, i);
    }
    else if (s == words[MULAS])
    {
        s = check("Unexpected end of tokens parsing mulas dest\n");
        tokens++;
        int dest = atol(s.c_str());
        s = check("Unexpected end of tokens parsing mulas src1\n");
        tokens++;
        int src1 = atol(s.c_str());
        s = check("Unexpected end of tokens parsing mulas f\n");
        tokens++;
        float f = atof(s.c_str());
        s = check("Unexpected end of tokens parsing mulas src2\n");
        tokens++;
        int src2 = atol(s.c_str());
        return clnewnode(MULAS, dest, src1, f, src2);
    }
    else if (s == words[MULAV])
    {
        s = check("Unexpected end of tokens parsing mulav dest\n");
        tokens++;
        int dest = atol(s.c_str());
        s = check("Unexpected end of tokens parsing mulav src1\n");
        tokens++;
        int src1 = atol(s.c_str());
        s = check("Unexpected end of tokens parsing mulav f\n");
        tokens++;
        float f = atof(s.c_str());
        s = check("Unexpected end of tokens parsing mulav src2\n");
        tokens++;
        int src2 = atol(s.c_str());
        return clnewnode(MULAV, dest, src1, f, src2);
    }
    else if (s == words[ROTAV])
    {
        s = check("Unexpected end of tokens parsing rotav dest\n");
        tokens++;
        int dest = atol(s.c_str());
        s = check("Unexpected end of tokens parsing rotav src1\n");
        tokens++;
        int src1 = atol(s.c_str());
        s = check("Unexpected end of tokens parsing rotav f\n");
        tokens++;
        int i = atol(s.c_str());
        s = check("Unexpected end of tokens parsing rotav src2\n");
        tokens++;
        int src2 = atol(s.c_str());
        return clnewnode(ROTAV, dest, src1, i, src2);
    }
    else if (s == words[IFPROB])
    {
        s = check("Unexpected end of tokens parsing ifprob src\n");
        tokens++;
        int src = atol(s.c_str());
        s = check("Unexpected end of tokens parsing ifprob k\n");
        tokens++;
        float k = atof(s.c_str());
        s = check("Unexpected end of tokens parsing ifprob l\n");
        tokens++;
        float l = atof(s.c_str());
        return clnewnode(IFPROB, src, k, l);
    }
    else if (s == words[IFQUAD])
    {
        s = check("Unexpected end of tokens parsing ifquad op1\n");
        tokens++;
        int op1 = atol(s.c_str());
        s = check("Unexpected end of tokens parsing ifquad op2\n");
        tokens++;
        int op2 = atol(s.c_str());
        return clnewnode(IFQUAD, op1, op2);
    }
    else if (s == words[IFSECT])
    {
        s = check("Unexpected end of tokens parsing ifsect op1\n");
        tokens++;
        int op1 = atol(s.c_str());
        s = check("Unexpected end of tokens parsing ifsect i\n");
        tokens++;
        int i = atol(s.c_str());
        s = check("Unexpected end of tokens parsing ifsect j\n");
        tokens++;
        int j = atol(s.c_str());
        return clnewnode(IFSECT, op1, i, j);
    }
    else if (s == words[SUCCESSL])
    {
        return clnewnode(SUCCESSL);
    }
    else if (s == words[FAILUREL])
    {
        return clnewnode(FAILUREL);
    }
    else
    {
        printf("Unrecognised token! >>%s<<\n", s.c_str());
        exit(1);
    }
    return 0;
}





void BT::parse_tree(std::string s)
{
    // Preparse the stream and perform macro substitution and comment elimination
    std::regex_token_iterator<std::string::iterator>    rend;
    std::string exps;
    tokens = std::regex_token_iterator<std::string::iterator>(s.begin(), s.end(), e, -1);
    while (tokens != rend)
    {
        // Get next token. ##NOTE## dereference with postincrement doesn't seem
        // to work properly on token iterator!!
        std::string s = *tokens;
        tokens++;
        DBPRINT("checking %s for expansion\n", s.c_str());
        Ntype n;
        bool foundmacro = false;
        // See if the word is a macro
        for(int i = EXPLORE; i < allnodes.size(); i++)
            if (s == allnodes[i].name)
            {
                n = allnodes[i];
                if (n.p_arity == 0)
                    exps += string_format(n.macro);
                else if (n.p_arity == 1)
                {
                    s = *tokens;
                    tokens++;
                    float op1 = atof(s.c_str());
                    exps += string_format(n.macro, op1);
                    DBPRINT("expanded >%s< >%s< >%f< to %s\n", n.macro.c_str(), s.c_str(), op1, exps.c_str());
                }
                else if (n.p_arity == 2)
                {
                    s = *tokens;
                    tokens++;
                    float op1 = atof(s.c_str());
                    s = *tokens;
                    tokens++;
                    float op2 = atof(s.c_str());
                    exps += string_format(n.macro, op1, op2);
                }
                foundmacro = true;
                break;
            }
        
        if (!foundmacro)
        {
            exps += s;
            exps += " ";
        }
    }
    DBPRINT("Expanded bt string >>%s<<\n", exps.c_str());
    // Parse the token stream
    tokens = std::regex_token_iterator<std::string::iterator>(exps.begin(), exps.end(), e, -1);
    root = clpt(0);
}


// Parse a Behaviour Tree string into a tree data structure.
// Use a regex to tokenise
BT::BT(cl_uint _blackboard, std::string _s)
:   blackboard  (_blackboard),
    s           (_s),
    // Regex to match lower case alphabetic words with optional single digit ending,
    // and numbers possibly prefixed
    // with + or - and possibly having a decimal point with one or more digits after it
    //e       ("[a-z]+[0-9]?|[-+]?[0-9]+\\.?[0-9]*")
    // Regex matches the separators between tokens. In this case, both white space
    // and everything between brackets, to get rid of comments
    e       ("[\\s]+|\\(.*\\)")
    // Valid Behaviour Tree node names
{
    // The BT layout in mem is:
    //  0   root
    //  2   size
    //  4   nodes
    //  6   num bb entries
    //  8   BT start
    

    cltree.insert(cltree.end(), 4 * sizeof(cl_ushort), 0);
    // Set to some non-zero seed value
    //cltree[0] = 0x1e; cltree[1] = 0x45; cltree[2] = 0x22; cltree[3] = 0x99;
    DBPRINT("BT parse string:>>%s<<\n", s.c_str());
    
    // Construct vector of all node types
    allnodes.insert(allnodes.end(), composition.begin(), composition.end());
    allnodes.insert(allnodes.end(), actionatom.begin(), actionatom.end());
    allnodes.insert(allnodes.end(), action.begin(), action.end());
    
    // Build the arity map
    for(int i = 0; i < MAX_ARITY; i++)
    {
        // For each arity value, build a vector of nodes that have that arity
        std::vector<int>    nodes;
        for(int j = 0; j < allnodes.size(); j++)
        {
            if (allnodes[j].c_arity == i)
                nodes.push_back(j);
        }
        aritymap.push_back(nodes);
    }
    
    for(auto i : allnodes)
        words.push_back(i.name);
    
    parse_tree(s);
    
    // Set the tree root
    *((ushort *)&(cltree[0])) = root;
    *((ushort *)&(cltree[2])) = (ushort)cltree.size();
    *((ushort *)&(cltree[4])) = (ushort)num_nodes;
    *((ushort *)&(cltree[6])) = (ushort)blackboard;
    //*((ushort *)&(cltree[6])) = (ushort)((blackboard + 2) * sizeof(cl_float));
    //printf("Set tree size, start  to: %02x%02x %02x%02x\n", cltree[5],cltree[4],cltree[7],cltree[6]);
    
    // Calculate parsimony
    int max_nodes = (MAX_BTVARS_SIZE - BLACKBOARD_ENTRIES * 8 - 8) / 2;
    float bt_space_left = 1 - (float)cltree.size() / MAX_BT_SIZE;
    float bt_nodes_left = 1 - (float)num_nodes / max_nodes;
    parsimony = bt_space_left < bt_nodes_left ? bt_space_left : bt_nodes_left;
    
}





