
#ifndef GP_H
#define GP_H

#include <vector>
#include "bt.h"
#include "gpu2.h"
#include "genetic.hpp"
#include <zmq.hpp>
#include <unistd.h>
#include <set>
#include <inttypes.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "clock.h"


// Nodes, Terminals
// Inner nodes have at least one node as a child
// Leaf nodes have only terminals as children
// Terminals are always ephemeral constants, that is, a constant that
// is randomly created at the time of creation of the terminal, and which
// may be altered by mutation but otherwise does not change


// Nodes may be composition or action (leaf).
// Action nodes have no other nodes as children.
// All nodes may have parameters, which are typed, ranged ephemeral constants.
// ADBs are named subtrees with a parameter list. Certain parameters within the
// subtree are substituted with the parameter list.
//
// The terminology of BT and GP is slightly different, in GP, some action nodes
// would be terminals, as would parameters.

#include <random>
float randfloat(float low, float high, int seed = 1);
int randint(int low, int high, int seed = 1);



#define ZMQPORT_POSE            "5678"
#define ZMQPORT_GENESERVER      "5999"
#define ZMQPORT_GALOG           "5998"
#define ZMQPORT_LOCALFIT        "5997"



class Evoparams
{
public:
    int         popsize         = 64;
    int         repeats         = 8;
    int         generations     = 64;
    int         epochs          = 8;
    std::string genepool        = "";
    std::string hostname        = "";
    int         n_elites        = 3;
    float       prop_elites     = 0.25;
    float       prop_replace    = 0.25;
    int         n_weakest       = 0;
    int         n_tsize         = 3;
    int         gendepth        = 6;
    int         mode            = 3;
    float       prob_mparam     = 0.05;
    float       prob_mpoint     = 0.05;
    float       prob_msubtree   = 0.05;
    float       prob_mshuffle   = 0.05;
    float       prob_xover      = 0.5;
    float       prob_compnode   = 0.9;
    bool        use_bias_mut    = false;
    float       mparam_bias     = 10.0;
    float       mpoint_bias     = 10.0;
    float       mshuffle_bias   = 10.0;
    int         seed            = 1;
    int         verbose         = 0;
    int         model           = 0;
    int         gsfittest       = 8;
    bool        grabdetail      = false;
    bool        dumpjson        = false;
    std::string dumppath        = "";
    bool        controlled      = false;
    std::string treefile        = "";
    int         adapt_gens      = 5;
    float       adapt_decay     = 0.5;
    bool        listen_rp       = false;
    float       adapt_rp_thresh = 0.002;

    std::vector<Nodetype>   allowed_comp        =
    {   SEQ2, SEQ3, SEQ4, SEL2, SEL3, SEL4, SEQM2, SEQM3, SEQM4, SELM2, SELM3, SELM4,
        //REPEATI, REPEATR, SUCCESSD, FAILURED, INVERT
        REPEATI, SUCCESSD, FAILURED, INVERT
    };
    std::vector<Nodetype>   allowed_actionatom  =
    {
        MOVCS, MOVCV, MULAS, MULAV, ROTAV, IFPROB, IFSECT, SUCCESSL, FAILUREL
        //MOVCS, MOVCV, MULAS, MULAV, ROTAV, IFSECT, SUCCESSL, FAILUREL
        //MOVCS, MOVCV, MULAS, MULAV, IFPROB, IFSECT, SUCCESSL, FAILUREL
    };
    std::vector<Nodetype>   allowed_action      =
    {
        //EXPLORE, UPFIELD, ATTRACT, NEIGHBOUR, FIXEDPROB, AVOIDING, CLEFT, CRIGHT, CFRONT
        UPFIELD, ATTRACT, CLEFT, CRIGHT, CFRONT, BSEARCH
        //UPFIELD, ATTRACT, CLEFT, CRIGHT, CFRONT, BSEARCH, RWALK
        //UPFIELD, ATTRACT, NEIGHBOUR, FIXEDPROB, CLEFT, CRIGHT, CFRONT, BSEARCH, RWALK
    };
    std::vector<int>        allowed_rvreg   = {0, 2, 4, 6, 8, 14, 16, 18};
    std::vector<int>        allowed_rsreg   = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 14, 15, 16, 17, 18, 19};
};

class GPnode
{
public:
    GPnode(){}
    Ntype                   type;
    int                     nodes_below;
    int                     depth;
    std::vector<float>      params;
};

typedef std::vector<GPnode>    Tree;


class Individual
{
public:
    Individual(int depth, bool grow, Nodeset &compnodes, Nodeset &actionnodes);
    Individual();
    Individual(Json i, Nodeset &allnodes);
    Individual(std::string tstr, Nodeset &allnodes);
    ~Individual() {}
    Tree                    tree;
    std::vector<Tree>       adb;
    float                   fitness     = NAN;
    float                   lfitness    = NAN;
    float                   real_lfitness = NAN;
    float                   parsimony   = 1.0f;
    float                   variance    = NAN;
    float                   m2          = 0;
    int                     evaluations = 0;
    float                   fitness95   = 0;
    std::vector<float>      objectives;
    Individual              *clone();
    int                     myid        = 0;
    int                     p1id        = 0;
    int                     p2id        = 0;
    int                     mutations   = 0;


    
    std::vector<int>        cnodes;
    std::vector<int>        anodes;
    bool                    stale_nodecache = true;
    bool                    stale_bt = true;
    BT                      *bt = NULL;

    GPnode                  gen_node(Nodeset &set);
    int                     gen_tree(Nodeset &composition, Nodeset &action, int current_depth, int depth, bool grow);
    int                     gen_tree(Nodeset &allnodes, std::regex_token_iterator<std::string::iterator> &tokens, int current_depth);
    void                    gen_tree_from_string(std::string t, Nodeset &allnodes);
    float                   gen_param(Ptype t);
    void                    update_cache();
    void                    print_tree();
    std::string             string_tree();
    std::string             string_tree(int idx);
    void                    print_tree(int idx);
    BT                      *get_bt();
    std::string             get_btstr();
    
    // optional storage of initial conditions for exact reproduction
    std::vector<gpusim::Evalcond>   evc;
    
private:
    std::string             btstr;
    std::string             btprefix    = "sel2 avoiding ";
    //std::string             btprefix    = "";

};



typedef std::vector<Individual*> Population;




class GP
{
public:
    GP(Evoparams &_params, std::vector<std::string> &_trees);
    ~GP()
    {
        int linger = 0;
        if (genesrv)
        {
            genesrv->setsockopt(ZMQ_LINGER, &linger, sizeof(int));
            genesrv->close();
        }
        if (sub_telem)
        {
            sub_telem->setsockopt(ZMQ_LINGER, &linger, sizeof(int));
            sub_telem->close();
        }
        if (sub_localfit)
        {
            sub_localfit->setsockopt(ZMQ_LINGER, &linger, sizeof(int));
            sub_localfit->close();
        }
        if (pub_log)
        {
            pub_log->setsockopt(ZMQ_LINGER, &linger, sizeof(int));
            pub_log->close();
        }
        context.close();
    }
    
    void setup_evo(gpusim::Sim *_sim);
    void setup_moevo(gpusim::Sim *_sim);
    void setup_soevo(gpusim::Sim *_sim);
    void setup_soevo_island(gpusim::Sim *_sim);
    
    
    Individual *crossover(Individual *p1, Individual *p2, bool use_all_p2 = false);
    
    void mutateparams(Individual *i);
    void mutatepoint(Individual *i);
    void mutatesubtree(Individual *&i);
    void mutateshuffle(Individual *&i);

    void insert_genes(std::string &gs);
    void fill_ramped_half_half();
    void print_pop();
    
    void update_fitness(gpusim::Sim *sim, bool secondary = false);
    void sort_population(bool use_f95 = false);
    void make_next_generation();
    void report_population(bool meval = false);
    void output_population(bool final = false);
    int tournament();
    void                    dump_json(Individual *i);


    std::string                 logfile;
    std::vector<std::string>    trees;
    std::vector<BT*>            *bt();
    std::vector<BT*>            b;
    VNodeset                        universenodes;
    VNodeset                        allnodes;
    VNodeset                        compnodes;
    VNodeset                        actionnodes;
    VNodeset                        noatomnodes;
    std::vector<std::vector<int> >  aritymap;
    
    Evoparams   params;
    Population  p;
    int         generation      = 0;
    int         inserted_genes  = 0;

    zmq::socket_t               *pub_log    = 0;

private:
    std::vector<int>            indices;
    
    gpusim::Sim                 *sim;
    uint64_t                    lap_time;
    Clock                       clk;
    
    
    // mo stuff-----------------------
    const int                   n_means = 10;
    const float                 prop_pf = 0.25;
    std::vector<int>            dominated;
    std::vector<int>            kcluster;
    
    struct Kmeans
    {
        Kmeans() : count(0), mean({0,0}) {}
        int     count;
        float2  mean;
    };
    std::vector<Kmeans>         kmeans;
    void                        sort_kc();
    void                        print_pop_mo();
    void                        print_dom();
    int                         calc_kmeans();
    void                        print_kmeans();
    
    typedef std::vector<std::pair<int,int> > Kc;
    Kc                          kc;
    std::vector<int>            pf;
    //----------------------------------
    
    
    // new so stuff---------------------
    void                        print_pop_so();
    int                         so_select(int range);

    // ---------------------------------
    // Genepool stuff
    bool                        usegenepool = false;
    bool                        connect_to_geneserver();
    void                        send_fittest_to_geneserver(int num);
    void                        get_response_from_geneserver();
    void                        get_local_fitness();
    
    void                        check_cmd_state();
    std::set<int>               get_possibles(int num);
    void                        send_pop_to_galogger();
    std::string                 create_json(Individual *i, bool minimal = false);
    zmq::context_t              context;
    zmq::socket_t               *genesrv    = 0;
    zmq::socket_t               *sub_telem  = 0;
    zmq::socket_t               *sub_localfit = 0;
    
    // --------------------------------------
    // Adaptive response stuff
    void                        adapt_to_environment();
    float                       rprobe = 0.0f;
    
};


void print_bar_chart(std::vector<float> f);

#endif



