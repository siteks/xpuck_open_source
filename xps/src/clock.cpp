

#include "clock.h"

Clock::Clock()
{
    // Setup initial times
    start();
    stop();
}


void Clock::start()
{
    m_start     = hr_clock::now();
    m_last      = m_start;
}

void Clock::stop()
{
    m_stop      = hr_clock::now();
}

long long Clock::elapsed()
{
    m_current   = hr_clock::now();
    return std::chrono::duration_cast<clock_freq>(m_current - m_start).count();
}

long long Clock::lap()
{
    m_current   = hr_clock::now();
    long long duration = std::chrono::duration_cast<clock_freq>(m_current - m_last).count();
    m_last      = m_current;
    return duration;
}

long long Clock::difference()
{
    return std::chrono::duration_cast<clock_freq>(m_stop - m_start).count();
}

long long Clock::current()
{
    m_current   = hr_clock::now();
    return std::chrono::duration_cast<clock_freq>(m_current.time_since_epoch()).count();
}

