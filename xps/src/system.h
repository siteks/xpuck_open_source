

#ifndef _SYSTEM_H
#define _SYSTEM_H



#include <string>
#include <cstdlib>
#include <vector>
#include <stdio.h>


#include "gpu2.h"
#include "gp.h"
#include "json11.hpp"


class Sysparams
{
public:
    bool    full_log            = false;
    int     verbose             = 0;
    int     eval_count          = 0;
    int     example             = 0;
    int     seed                = 1;
    int     wfindex             = 0;
    std::string logfile         = "evo.log";
    std::string worldfile       = "";
    bool    controlled          = false;
    bool    controlled_sim      = false;
    bool    force_cam_update    = false;
    std::string capture_bb_file = "";
    std::string hostname        = "";
};


class System
{
public:
    System(int argc, char**argv);
    
    void run();
    void evolve();
    void evolve_mo();
    void evolve_so();
    void gui();
    void capture_bb();
    void read_json();
    void measure_fitness();
    
private:
    
    // What the program will do
    enum Action
    {
        USEGUI,             // Start the gui. Tree defined either on command line or canned. Default of other options not selected
        MEASURE_SIM,        // Benchmark the simulator performance, giving r_acc values
        MEASURE_FITNESS,    // Measure the fitness of a tree
        EVOLVE,             // Run an evolution
        CAPTURE_BB
        
    } action;
    
    Simparams                   simparams;
    Evoparams                   evoparams;
    Sysparams                   sysparams;
    std::vector<std::string>    trees;
    Json                        wf;
    std::vector<BT*>            bts;
    
    zmq::context_t              zmqcontext;
    zmq::socket_t               *pub_log = 0;

    
};

#endif
