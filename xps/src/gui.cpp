
#include "gui.h"
#include <functional>

#define ESC_KEY 27

static Gui *tg;

static void mouse(int button, int state, int x, int y)
{
    int vx, vy, vw, vh;
    GLUI_Master.get_viewport_area(&vx, &vy, &vw, &vh);
    printf("%d %d %d %d\n",vx,vy,vw,vh);
    float posx = (float)x / vw * (tg->right - tg->left) + tg->left;
    float posy = (float)y / vh * (tg->bottom - tg->top) + tg->top;
    printf("Mouse event %d:%d %d %d %f %f\n", button, state, x, y, posx, posy);
    
    if (button == 0 && state == 0 && tg->dragging < 0)
    {
        for (int i = 0; i < tg->sim->sim_body_count; i++)
        {
            //printf("%2d %10f %10f\n", i,scene.bodies[i]->position.x, scene.bodies[i]->position.y);
            if ((fabs(posx - tg->sim->sim_positions[i].x) < tg->sim->robot_param.radius) && (fabs(posy - tg->sim->sim_positions[i].y) < tg->sim->robot_param.radius))
            {
                printf("On object %d\n", i);
                tg->dragging = i;
                break;
            }
        }
    }
    if (button == 0 && state == 1 && tg->dragging >= 0)
    {
        tg->dragging = -1;
    }
}

static void mouse_move(int x, int y)
{
    printf("mouse move %d %d\n", x, y);
    if (tg->dragging >= 0)
    {
        int vx, vy, vw, vh;
        GLUI_Master.get_viewport_area(&vx, &vy, &vw, &vh);
        float posx = (float)x / vw * (tg->right - tg->left) + tg->left;
        float posy = (float)y / vh * (tg->bottom - tg->top) + tg->top;
        tg->sim->sim_positions[tg->dragging].x = posx;
        tg->sim->sim_positions[tg->dragging].y = posy;
        tg->sim->sim_dragged = tg->dragging;
    }
}

void Gui::display_text(int x, int y, int r, int g, int b, const char *string)
{
	int j = strlen(string);
    
    // get the size of the window in pixels, this gives us the
    // number of lines and columns to use
    int vx, vy, vw, vh;
    GLUI_Master.get_viewport_area(&vx, &vy, &vw, &vh);
    int lines = vh / 15;
    int columns = vw / 9;
    
    float xp = left + (right - left) / columns * x;
    float yp = top + (bottom - top) / lines * (y + 1);
	glColor3f(r, g, b);
	glRasterPos2f(xp, yp);
	for( int i = 0; i < j; i++ ) {
        //glutBitmapCharacter(GLUT_BITMAP_9_BY_15, string[i]);
        glutBitmapCharacter(GLUT_BITMAP_9_BY_15, string[i]);
	}
}

static void rescale()
{
    tg->left    = -tg->width * tg->scale * 0.5;
    tg->right   = tg->width * tg->scale * 0.5;
    tg->bottom  = -tg->height * tg->scale * 0.5;
    tg->top     = tg->height * tg->scale * 0.5;
    printf("Window scale:%10f  %10f %10f %10f %10f\n", tg->scale, tg->left, tg->right, tg->bottom, tg->top);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(tg->left, tg->right, tg->bottom, tg->top);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    
}

static void keyboard(unsigned char key, int x, int y)
{
    switch(key)
    {
        case ESC_KEY:
            exit( 0 );
            break;
            
        case 'f':
            break;
        case ' ':
            break;
        case 'z':
            tg->scale *= 1.2;
            rescale();
            break;
        case 'x':
            tg->scale /= 1.2;
            rescale();
            break;
    }
    
}

static void reshape(int width, int height)
{
    tg->pixelwidth  = tg->width;
    tg->pixelheight = tg->height;
    GLUI_Master.auto_set_viewport();
}




static void button(int a)
{
    printf("Button pressed %d\n", a);
    switch (a)
    {
        case 1:
            tg->runstate = !tg->runstate;
            if (tg->runstate)
                tg->runstop_button->set_name("Stop");
            else
                tg->runstop_button->set_name("Run");
            break;
        case 2:
            // Step button
            tg->singlestep = true;
            break;
        case -1:
            tg->finish = true;
            tg->sim->postamble();
            exit(1);
            break;
        default:
            return;
    }
}


// This is used to control the frame rate (60Hz).
void Gui::run_physics()
{
    
    //glutTimerFunc(frame_period, run_physics, 0);
    
    if (runstate || singlestep)
    {

        
        if ((runstate && ((fcount % frames_per_step) == 0)) || singlestep)
        {
            if (sim->params.flip_params)
            {
                if ((steps % abs(sim->params.flip_params)) == 0)
                {
                    sim->object_param.mass = sim->params.flip_params > 0    ? sim->params.fr_mass_alt[flipstate]
                                                                            : sim->params.fr_mass_alt[1 - flipstate];
                    sim->update_object = true;
                    flipstate = 1 - flipstate;
                    printf("Step %d setting frisbee to %f\n", steps, sim->object_param.mass);
                }
            }
            tg->sim->step();
            steps++;
            
            
            
            if (singlestep)
                tg->sim->print_btstatus();
            sim_time += 0.1 * sim->sim_param.ctrl_steps;
        }
        singlestep = false;
    }
    fcount++;
    
}

static void redraw_window()
{
    glutSetWindow(tg->main_window);
    glClearColor(0.9, 0.9, 0.9, 0.0);
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    
    tg->run_physics();
    tg->sim->render(true);
    //tg->display_text(0,0,1,1,1,"testing");
    char text[100];
    sprintf(text, "%8.1f", tg->sim_time);
    tg->display_text(0,1,1,1,1, text);
    
    glutPostRedisplay();
    glutSwapBuffers( );
    
}


Gui::Gui(Sim *_sim) : sim(_sim)
{
    tg = this;
    int argc = 0;
    char **argv;
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE);
    glutInitWindowSize(pixelwidth, pixelheight);
    main_window = glutCreateWindow("PhyEngine");
    glutDisplayFunc(redraw_window);
    
    GLUI_Master.set_glutKeyboardFunc(keyboard);
    GLUI_Master.set_glutMouseFunc(mouse);
    glutMotionFunc(mouse_move);
    GLUI_Master.set_glutReshapeFunc(reshape);
    
    GLUI *ui = GLUI_Master.create_glui_subwindow(main_window, GLUI_SUBWINDOW_RIGHT);
    ui->set_main_gfx_window(main_window);
    GLUI_Master.auto_set_viewport();
    
    runstop_button  = ui->add_button("Run", 1, button);
    step_button     = ui->add_button("Step", 2, button);
    //ui->add_separator();
    //motor_text      = ui->add_statictext("-000 -000");
    //motor_text->set_font(GLUT_BITMAP_8_BY_13);
    //motor_ctrl      = ui->add_translation("Motor control", GLUI_TRANSLATION_XY, mm);
    //ui->add_separator();
    ui->add_button("Exit", -1, button);

    rescale();

}





