
#ifndef GUI_H
#define GUI_H

#include <cstdlib>
#include "glui/glui.h"
#include "gpu2.h"


using namespace gpusim;

class Gui
{
public:
    Gui(Sim *_sim);
    
    void display_text(int x, int y, int r, int g, int b, const char *string);
    void run_physics();


    int32_t main_window;
    
    float   scale           = 1.0f;
    float   width           = 4.0f;
    float   height          = 3.0f;
    int     pixelwidth      = 1024;
    int     pixelheight     = 768;
    float   sim_time        = 0.0f;
    int     frames_per_step = 6;
    
    float   left, right, bottom, top;
    int     dragging = -1;

    Sim     *sim;
    
    GLUI_Button     *runstop_button;
    bool            runstate = false;
    GLUI_Button     *step_button;
    bool            singlestep = false;
    bool            finish = false;
    GLUI_Panel      *btpanel;
    GLUI_StaticText *bt;
    
    GLUI_Translation   *motor_ctrl;
    float           mm[2];
    float           mo, mv;
    int             fcount;
    int             steps = 0;
    int             flipstate = 0;

};


#endif
