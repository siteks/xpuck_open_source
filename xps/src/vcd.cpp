

#include "vcd.h"

#include <stdio.h>
#include <stdlib.h>
#include <bitset>


VCD::VCD(std::string _filename)
:   filename(_filename)
{
    
}



void VCD::addevent(std::string name, uint64_t t1, uint64_t t2, uint64_t t3, uint64_t t4)
{
    // Add a profiled event from OpenCL queue
    //FIXME quick hack - rotate round several different signals
    auto s = name + "_" + std::to_string(current_signal[name]);
    //printf("%s\n", s.c_str());
    addchange(s, t1, 3);
    addchange(s, t2, 2);
    addchange(s, t3, 1);
    addchange(s, t4, 0);
    current_signal[name] = (current_signal[name] + 1) % interleave;
}

void VCD::initevent(int queues)
{
    for (int q = 0; q < queues; q++)
    {
        auto qs = std::to_string(q);
        for (int r = 0; r < interleave; r++)
        {
            auto rs = std::to_string(r);
            auto    n = "ndrange_kernel"    + qs; current_signal[n] = 0; addsignal(n + "_" + rs, 2, 0);
                    n = "map_buffer"        + qs; current_signal[n] = 0; addsignal(n + "_" + rs, 2, 0);
                    n = "unmap_mem_object"  + qs; current_signal[n] = 0; addsignal(n + "_" + rs, 2, 0);
                    n = "barrier"           + qs; current_signal[n] = 0; addsignal(n + "_" + rs, 2, 0);
        }
    }
}

void VCD::addsignal(std::string name, uint32_t width, uint32_t value)
{
    current_state[tagidx]   = value;
    initial_state[tagidx]   = value;
    signal_width[tagidx]    = width;
    tags[name]              = tagidx;
    tagidx++;
}

void VCD::addchange(std::string name, uint64_t timestamp, uint32_t value)
{
    uint16_t tag    = tags[name];
    if (current_state[tag] != value)
    {
        // this is a change
        data[timestamp][tag]    = value;
        current_state[tag]      = value;
    }
    //printf("%s %llu %d\n", name.c_str(), timestamp, value);
}

std::string VCD::tagstr(uint16_t tag)
{
    //FIXME make properly multicharacter, this only works with 93 signals
    std::string s (1, (char)(tag + 33));
    return s;
}

std::string VCD::binary(uint32_t x)
{
    std::bitset<32> v(x);
    std::string s = v.to_string();
    auto i = s.find('1');
    if (i != std::string::npos)
        return s.substr(i);
    else
        return "0";
}

void VCD::dump()
{
    FILE *fp = fopen(filename.c_str(),"w");
    if (!fp)
    {
        printf("Failed to open %s for VCD\n", filename.c_str());
        exit(1);
    }
    // Header
    fprintf(fp, "$date $end\n$version OpenCL events tracer $end\n$timescale 1ns $end\n");
    // Variable definition
    fprintf(fp, "$scope module opencl $end\n");
    for (auto i = tags.begin(); i != tags.end(); ++i)
    {
        uint16_t    tag     = i->second;
        std::string name    = i->first;
        fprintf(fp, "$var wire %d %s %s $end\n", signal_width[tag], tagstr(tag).c_str(), name.c_str());
    }
    fprintf(fp, "$upscope $end\n$enddefinitions $end\n");
    // dumpvars
    for (auto i = tags.begin(); i != tags.end(); ++i)
    {
        uint16_t    tag     = i->second;
        if (signal_width[tag] == 1)
        {
            // Single bit
            fprintf(fp, "%1d%s\n", initial_state[tag], tagstr(tag).c_str());
        }
        else
        {
            // Multibit
            fprintf(fp, "b%s %s\n", binary(initial_state[tag]).c_str(), tagstr(tag).c_str());
        }
    }
    fprintf(fp, "$end\n");
    // Value change
    // Use first timestamp value as zero
    uint64_t origin = data.begin()->first;
    for (auto i = data.begin(); i != data.end(); ++i)
    {
        uint64_t    timestamp   = i->first - origin;
        Values      v           = i->second;
        fprintf(fp, "#%llu\n", timestamp);
        for (auto j = v.begin(); j != v.end(); ++j)
        {
            uint16_t tag    = j->first;
            uint32_t value  = j->second;
            if (signal_width[tag] == 1)
            {
                // Single bit
                fprintf(fp, "%1d%s\n", value, tagstr(tag).c_str());
            }
            else
            {
                // Multibit
                fprintf(fp, "b%s %s\n", binary(value).c_str(), tagstr(tag).c_str());
            }
        }
    }
    
    fclose(fp);
    
}










